<?php

/**
 * This is the model class for table "matricula.reporte_formulario_inscripcion".
 *
 * The followings are the available columns in table 'matricula.reporte_formulario_inscripcion':
 * @property integer $id
 * @property integer $formulario_id
 * @property string $tipo_error
 * @property string $mensaje
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property string $estatus
 * @property string $origen
 * @property string $identificacion
 * @property string $nombres
 * @property string $apellidos
 * @property string $documento_identidad
 * @property string $fecha_nacimiento
 * @property string $sexo
 * @property integer $afinidad
 * @property integer $orden_nacimiento
 *
 * The followings are the available model relations:
 * @property FormularioInscripcionPeriodo $formulario
 * @property UsergroupsUser $usuarioIni
 */
class ReporteFormularioInscripcion extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'matricula.reporte_formulario_inscripcion';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('formulario_id, tipo_error, mensaje, fecha_ini', 'required'),
			array('formulario_id, usuario_ini_id, afinidad, orden_nacimiento', 'numerical', 'integerOnly'=>true),
			array('tipo_error', 'length', 'max'=>20),
			//array('fecha_ini', 'length', 'max'=>6),
			array('estatus', 'length', 'max'=>1),
			array('identificacion, documento_identidad', 'length', 'max'=>15),
			array('nombres, apellidos', 'length', 'max'=>60),
			array('fecha_nacimiento', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, formulario_id, tipo_error, mensaje, usuario_ini_id, fecha_ini, estatus, origen, identificacion, nombres, apellidos, documento_identidad, fecha_nacimiento, sexo, afinidad, orden_nacimiento', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'formulario' => array(self::BELONGS_TO, 'FormularioInscripcionPeriodo', 'formulario_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'formulario_id' => 'Formulario',
			'tipo_error' => 'Tipo Error',
			'mensaje' => 'Mensaje',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'estatus' => 'Estatus',
			'origen' => 'Origen',
			'identificacion' => 'Identificacion',
			'nombres' => 'Nombres',
			'apellidos' => 'Apellidos',
			'documento_identidad' => 'Documento Identidad',
			'fecha_nacimiento' => 'Fecha Nacimiento',
			'sexo' => 'Sexo',
			'afinidad' => 'Afinidad',
			'orden_nacimiento' => 'Orden Nacimiento',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('formulario_id',$this->formulario_id);
		$criteria->compare('tipo_error',$this->tipo_error,true);
		$criteria->compare('mensaje',$this->mensaje,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('origen',$this->origen,true);
		$criteria->compare('identificacion',$this->identificacion,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('documento_identidad',$this->documento_identidad,true);
		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		$criteria->compare('sexo',$this->sexo,true);
		$criteria->compare('afinidad',$this->afinidad);
		$criteria->compare('orden_nacimiento',$this->orden_nacimiento);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReporteFormularioInscripcion the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
