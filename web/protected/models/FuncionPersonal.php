<?php

/**
 * This is the model class for table "personal.funcion_personal".
 *
 * The followings are the available columns in table 'personal.funcion_personal':
 * @property integer $id
 * @property integer $personal_id
 * @property integer $plantel_id
 * @property integer $funcion_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * 
 * @property Docente $personal
 * @property Plantel $plantel
 * @property Funcion $funcion
 */
class FuncionPersonal extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal.funcion_personal';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('funcion_id', 'required', 'message' => 'El campo: {attribute}, no debe estar vacio', 'on'=>'gestionFuncionPersonal'),
			array('funcion_id,personal_plantel_id,usuario_act_id,usuario_act_id', 'numerical', 'integerOnly'=>true,'message' => 'El campo: {attribute}, debe ser un valor numérico', 'on'=>'gestionFuncionPersonal'),
			array('estatus', 'length', 'max'=>1), 
			array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('funcion_id', 'safe', 'on'=>'search_funcion_personal'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UserGroupsUser', 'usuario_ini_id'),
			//'personal' => array(self::BELONGS_TO, 'Docente', 'personal_id'),
			//'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
			'Funcion' => array(self::BELONGS_TO, 'Funcion', 'funcion_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Funcion del Personal',
			//'personal_id' => 'Personal',
			//'plantel_id' => 'Plantel',
			'funcion_id' => 'Funcion del Personal',
			'fecha_ini' => 'Fecha Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                
                
                /*$criteria->with = array(
                                        'usuarioIni' => array('alias' => 'usuarioIni'),
                                        'usuarioAct' => array('alias' => 'usuarioAct'),
                                        'Funcion' => array('alias' => 'Funcion'),
                                            );*/
                

		$criteria->compare('id',$this->id);
		//$criteria->compare('personal_id',$this->personal_id);
		//$criteria->compare('plantel_id',$this->plantel_id);
		$criteria->compare('funcion_id',$this->funcion_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		//$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
        
        public function search_funcion_personal($personal_plantel_id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('Funcion');
                
                /*$criteria->with = array(
                                        'usuarioIni' => array('alias' => 'usuarioIni'),
                                        'usuarioAct' => array('alias' => 'usuarioAct'),
                                        'Funcion' => array('alias' => 'Funcion'),
                                            ); */
                
                
               
                
                $criteria->compare('personal_plantel_id',$personal_plantel_id);	                		
		//$criteria->compare('funcion_id',$this->funcion_id);	
		//$criteria->compare('t.estatus',$this->estatus);

		$sort = new CSort();
                $sort->defaultOrder = '"t".estatus ASC, "t".id DESC';

                return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'sort' => $sort,
                ));
	}
        
        
        
        

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FuncionPersonal the static model class
	 */
        
        
        public function getErroresAdicionales($erroresAdicionales)
        {  
            
            
                 $erroresAdicionales.="</br></br>".' <b>Nota: </b> En  caso de persistir el error o tiene alguna duda al respecto, por favor contacte al personal de soporte mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>'; 
                 $this->addError('erroresAdicionales',$erroresAdicionales);
                 
                     
            
        } // fin de la funcion para mostrar los errores adicionales generados despues del metodo validate
        
        
        public function getValidarFuncionPersonal($id,$personal_plantel_id,$funcion_id,$pantalla)
        {
            $criteria = new CDbCriteria();
            
            if($pantalla=="registro")
            {
                $criteria->condition = " personal_plantel_id='$personal_plantel_id' and funcion_id='$funcion_id' and estatus='A' "; 
            }
            
            if($pantalla=="edicion")
            {
                $criteria->condition = "id <> '$id' and personal_plantel_id='$personal_plantel_id' and funcion_id='$funcion_id' and estatus='A'  "; 
            }
                
            $resultado = FuncionPersonal::model()->findAll($criteria);
            return $resultado;
        } // fin del metodo para buscar los datos del personal, esquema personal 
        
        
        public function  getFunciones()
        {
            $criteria = new CDbCriteria();
            $criteria->order = 'nombre ASC';
            $criteria->condition = "estatus='A' "; 
            $funciones = Funcion::model()->findAll($criteria);
            
            return $funciones;
        }
        
        
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
