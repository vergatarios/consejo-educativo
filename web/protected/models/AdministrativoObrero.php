<?php

/**
 * This is the model class for table "personal.administrativo_obrero".
 *
 * The followings are the available columns in table 'personal.administrativo_obrero':
 * @property integer $id
 * @property string $nombres
 * @property string $apellidos
 * @property string $tdocumento_identidad
 * @property string $documento_identidad
 * @property integer $usuario_id
 * @property integer $grado_instruccion_id
 * @property integer $cargo_nominal_id
 * @property integer $especificación_cargo_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuario
 * @property CargoNominal $cargoNominal
 * @property EspecificacionCargo $especificaciónCargo
 * @property GradoInstruccion $gradoInstruccion
 */
class AdministrativoObrero extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal.administrativo_obrero';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nombres, apellidos, tdocumento_identidad, documento_identidad, grado_instruccion_id, cargo_nominal_id, especificación_cargo_id, fecha_ini, usuario_ini_id', 'required'),
			array('usuario_id, grado_instruccion_id, cargo_nominal_id, especificación_cargo_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('nombres, apellidos', 'length', 'max'=>60),
			array('tdocumento_identidad, estatus', 'length', 'max'=>1),
			array('documento_identidad', 'length', 'max'=>15),
			array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nombres, apellidos, tdocumento_identidad, documento_identidad, usuario_id, grado_instruccion_id, cargo_nominal_id, especificación_cargo_id, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'usuario' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_id'),
			'cargoNominal' => array(self::BELONGS_TO, 'CargoNominal', 'cargo_nominal_id'),
			'especificaciónCargo' => array(self::BELONGS_TO, 'EspecificacionCargo', 'especificación_cargo_id'),
			'gradoInstruccion' => array(self::BELONGS_TO, 'GradoInstruccion', 'grado_instruccion_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nombres' => 'Nombres',
			'apellidos' => 'Apellidos',
			'tdocumento_identidad' => 'Tdocumento Identidad',
			'documento_identidad' => 'Documento Identidad',
			'usuario_id' => 'Usuario',
			'grado_instruccion_id' => 'Grado Instruccion',
			'cargo_nominal_id' => 'Cargo Nominal',
			'especificación_cargo_id' => 'Especificación Cargo',
			'fecha_ini' => 'Fecha Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('tdocumento_identidad',$this->tdocumento_identidad,true);
		$criteria->compare('documento_identidad',$this->documento_identidad,true);
		$criteria->compare('usuario_id',$this->usuario_id);
		$criteria->compare('grado_instruccion_id',$this->grado_instruccion_id);
		$criteria->compare('cargo_nominal_id',$this->cargo_nominal_id);
		$criteria->compare('especificación_cargo_id',$this->especificación_cargo_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return AdministrativoObrero the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
