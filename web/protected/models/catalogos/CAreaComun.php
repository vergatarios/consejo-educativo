<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CAreaComun extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 2,
    'nombre' => 'ASDASDAA',
    'estatus' => 'E',
  ),
  1 => 
  array (
    'id' => 4,
    'nombre' => 'BLABLABLA',
    'estatus' => 'E',
  ),
  2 => 
  array (
    'id' => 6,
    'nombre' => 'PATIO',
    'estatus' => 'E',
  ),
  3 => 
  array (
    'id' => 1,
    'nombre' => 'LALA',
    'estatus' => 'E',
  ),
  4 => 
  array (
    'id' => 5,
    'nombre' => 'BLABLABLA',
    'estatus' => 'E',
  ),
  5 => 
  array (
    'id' => 3,
    'nombre' => 'AASDFFS',
    'estatus' => 'E',
  ),
  6 => 
  array (
    'id' => 10,
    'nombre' => 'JARDIN',
    'estatus' => 'A',
  ),
  7 => 
  array (
    'id' => 11,
    'nombre' => 'BAÑOS',
    'estatus' => 'A',
  ),
  8 => 
  array (
    'id' => 12,
    'nombre' => 'PASILLOS',
    'estatus' => 'A',
  ),
  9 => 
  array (
    'id' => 13,
    'nombre' => 'OFICINAS',
    'estatus' => 'A',
  ),
)		; 

	}
}