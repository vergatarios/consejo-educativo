<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CCampoConocimiento extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 1,
    'nombre' => 'Ambito 1',
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 2,
    'nombre' => 'Ambito 2',
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 4,
    'nombre' => 'Ambito 3',
    'estatus' => 'A',
  ),
  3 => 
  array (
    'id' => 5,
    'nombre' => 'Nuevo',
    'estatus' => 'A',
  ),
  4 => 
  array (
    'id' => 6,
    'nombre' => 'Nuevo 2',
    'estatus' => 'E',
  ),
  5 => 
  array (
    'id' => 9,
    'nombre' => 'DS ',
    'estatus' => 'A',
  ),
)		; 

	}
}