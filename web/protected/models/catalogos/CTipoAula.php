<?php
/**
* Catalogo de $nombreClass
*
* @author Generador de Código
*/
class CTipoAula extends CCatalogo { 

protected static $columns =
array (
  0 => 'id',
  1 => 'nombre',
  2 => 'estatus',
);

/**
* Setea la data en una propiedad static llamada data
*/
protected static function setData(){

self::$data =
array (
  0 => 
  array (
    'id' => 1,
    'nombre' => 'AULA DE CLASES',
    'estatus' => 'A',
  ),
  1 => 
  array (
    'id' => 2,
    'nombre' => 'LABORATORIO',
    'estatus' => 'A',
  ),
  2 => 
  array (
    'id' => 3,
    'nombre' => 'TALLER',
    'estatus' => 'A',
  ),
)		; 

	}
}