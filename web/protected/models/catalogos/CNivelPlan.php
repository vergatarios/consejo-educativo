<?php
class CNivelPlan extends CCatalogo {

    protected static $columns =
        array (
            0 => 'id',
            1 => 'nivel_id',
            2 => 'plan_id',
            3 => 'estatus',
        );

    /**
     * Setea la data en una propiedad static llamada data
     */
    protected static function setData(){

        self::$data= array(
            array (
                0 =>
                    array (
                        'id' => 39,
                        'nivel_id' => 2,
                        'plan_id' => 2,
                        'estatus' => 'A',
                    ),
                1 =>
                    array (
                        'id' => 41,
                        'nivel_id' => 4,
                        'plan_id' => 4,
                        'estatus' => 'A',
                    ),
                2 =>
                    array (
                        'id' => 42,
                        'nivel_id' => 7,
                        'plan_id' => 38,
                        'estatus' => 'A',
                    ),
                3 =>
                    array (
                        'id' => 43,
                        'nivel_id' => 8,
                        'plan_id' => 7,
                        'estatus' => 'A',
                    ),
                4 =>
                    array (
                        'id' => 44,
                        'nivel_id' => 9,
                        'plan_id' => 349,
                        'estatus' => 'A',
                    ),
                5 =>
                    array (
                        'id' => 45,
                        'nivel_id' => 1,
                        'plan_id' => 1,
                        'estatus' => 'A',
                    ),
                6 =>
                    array (
                        'id' => 46,
                        'nivel_id' => 4,
                        'plan_id' => 5,
                        'estatus' => 'A',
                    ),
                7 =>
                    array (
                        'id' => 47,
                        'nivel_id' => 8,
                        'plan_id' => 8,
                        'estatus' => 'A',
                    ),
                8 =>
                    array (
                        'id' => 48,
                        'nivel_id' => 10,
                        'plan_id' => 26,
                        'estatus' => 'A',
                    ),
                9 =>
                    array (
                        'id' => 49,
                        'nivel_id' => 11,
                        'plan_id' => 26,
                        'estatus' => 'A',
                    ),
                10 =>
                    array (
                        'id' => 50,
                        'nivel_id' => 3,
                        'plan_id' => 37,
                        'estatus' => 'A',
                    ),
                11 =>
                    array (
                        'id' => 51,
                        'nivel_id' => 14,
                        'plan_id' => 37,
                        'estatus' => 'A',
                    ),
                12 =>
                    array (
                        'id' => 52,
                        'nivel_id' => 13,
                        'plan_id' => 37,
                        'estatus' => 'A',
                    ),
                13 =>
                    array (
                        'id' => 53,
                        'nivel_id' => 16,
                        'plan_id' => 37,
                        'estatus' => 'A',
                    ),
                14 =>
                    array (
                        'id' => 55,
                        'nivel_id' => 1,
                        'plan_id' => 434,
                        'estatus' => 'A',
                    ),
                15 =>
                    array (
                        'id' => 56,
                        'nivel_id' => 1,
                        'plan_id' => 435,
                        'estatus' => 'A',
                    ),
                16 =>
                    array (
                        'id' => 60,
                        'nivel_id' => 1,
                        'plan_id' => 437,
                        'estatus' => 'A',
                    ),
                17 =>
                    array (
                        'id' => 61,
                        'nivel_id' => 7,
                        'plan_id' => 438,
                        'estatus' => 'A',
                    ),
                18 =>
                    array (
                        'id' => 62,
                        'nivel_id' => 1,
                        'plan_id' => 439,
                        'estatus' => 'A',
                    ),
                19 =>
                    array (
                        'id' => 63,
                        'nivel_id' => 3,
                        'plan_id' => 440,
                        'estatus' => 'A',
                    ),
                20 =>
                    array (
                        'id' => 64,
                        'nivel_id' => 3,
                        'plan_id' => 441,
                        'estatus' => 'A',
                    ),
                21 =>
                    array (
                        'id' => 65,
                        'nivel_id' => 3,
                        'plan_id' => 442,
                        'estatus' => 'A',
                    ),
                22 =>
                    array (
                        'id' => 66,
                        'nivel_id' => 3,
                        'plan_id' => 443,
                        'estatus' => 'A',
                    ),
                23 =>
                    array (
                        'id' => 67,
                        'nivel_id' => 2,
                        'plan_id' => 444,
                        'estatus' => 'A',
                    ),
                24 =>
                    array (
                        'id' => 69,
                        'nivel_id' => 8,
                        'plan_id' => 38,
                        'estatus' => 'A',
                    ),
            )		);

    }
}
