<?php

/**
 * This is the model class for table "personal.personal_estudio".
 *
 * The followings are the available columns in table 'personal.personal_estudio':
 * @property string $id
 * @property integer $estudio_id
 * @property integer $personal_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Personal $personal
 * @property Estudio $estudio  
 * @property Estudio $especifique_estudio
 */
class PersonalEstudio extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal.personal_estudio';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.   
		return array(
			array('estudio_id,especifique_estudio', 'required','message' => 'El campo: {attribute}, no debe estar vacio', 'on'=>'gestionPersonalEstudio'),
			array('estudio_id,personal_id', 'numerical', 'integerOnly'=>true,'message' => 'El campo: {attribute}, debe ser numérico','on'=>'gestionPersonalEstudio'),
			array('estatus', 'length', 'max'=>1),
                        array('especifique_estudio', 'length', 'min'=>5, 'max'=>250),
                        array('especifique_estudio', 'caracteresPermitidosNumerosLetras'),
			//array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('estudio_id','safe', 'on'=>'search_personal_estudio'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'personal' => array(self::BELONGS_TO, 'Personal', 'personal_id'),
			'estudio' => array(self::BELONGS_TO, 'Estudio', 'estudio_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'estudio_id' => 'Estudio',
			'personal_id' => 'Personal',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
                        'especifique_estudio' => 'Especificaci&oacute;n del Estudio',
                    
                    
                    
                    
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('estudio_id',$this->estudio_id);
		$criteria->compare('personal_id',$this->personal_id);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        
                
        public function search_personal_estudio($personal_id)
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('personal');
                

               
                
                $criteria->compare('personal_id',$personal_id);	                		
		

		$sort = new CSort();
                $sort->defaultOrder = '"t".estatus ASC, "t".id DESC';

                return new CActiveDataProvider($this, array(
                    'criteria' => $criteria,
                    'sort' => $sort,
                ));
	}
        
        
        
        public function getErroresAdicionales($erroresAdicionales)
        {  


                 $erroresAdicionales.="</br></br>".' <b>Nota: </b> En  caso de persistir el error o tiene alguna duda al respecto, por favor contacte al personal de soporte mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>'; 
                 $this->addError('erroresAdicionales',$erroresAdicionales);



        } // fin de la funcion para mostrar los errores adicionales generados despues del metodo validate
        
        
        public function getValidarPersonalEstudio($id,$personal_id,$estudio_id,$pantalla)
        {
            $criteria = new CDbCriteria();
            
            if($pantalla=="registro")
            {
                $criteria->condition = " personal_id='$personal_id' and estudio_id='$estudio_id'  ";  // and estatus='".Constantes::ESTATUS_ACTIVO."'
            }
            
            if($pantalla=="edicion")
            {
                $criteria->condition = "id <> '$id' and personal_id='$personal_id' and estudio_id='$estudio_id'   ";  // and estatus='".Constantes::ESTATUS_ACTIVO."'
            }
                
            $resultado = PersonalEstudio::model()->findAll($criteria);
            return $resultado;
        } // fin del metodo para buscar los datos del personal, esquema personal 
        
        
        public function  getEstudios()
        {
            $criteria = new CDbCriteria();
            $criteria->order = 'nombre ASC';
            $criteria->condition = "estatus='".Constantes::ESTATUS_ACTIVO."' "; 
            $lista_estudios = Estudio::model()->findAll($criteria);
            
            return $lista_estudios;
        }
        
        
        public function caracteresPermitidosNumerosLetras($attribute, $params)
        {


            $descripcion_atributos=$this->attributeLabels();
            $utiles=new Utiles();
            if(!$utiles->validar_caracteres_numeros_letras($this->$attribute))
            {
                $this->addError('errorCaracteres',"El campo: $descripcion_atributos[$attribute], contiene caracteres inv&aacute;lidos s&oacute;lo se permiten n&uacute;meros y letras");
            }

        } // verificar que la cadena sea valida  
        
        
        
        
        
        

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PersonalEstudio the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
