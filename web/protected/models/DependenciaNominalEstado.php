<?php

/**
 * This is the model class for table "gplantel.dependencia_nominal_estado".
 *
 * The followings are the available columns in table 'gplantel.dependencia_nominal_estado':
 * @property integer $cod_estado
 * @property string $nom_estado
 * @property integer $cod_dependencia
 * @property string $nom_dependencia_nominal
 * @property integer $id
 */
class DependenciaNominalEstado extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.dependencia_nominal_estado';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cod_estado, cod_dependencia', 'numerical', 'integerOnly'=>true),
			array('nom_estado, nom_dependencia_nominal', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('cod_estado, nom_estado, cod_dependencia, nom_dependencia_nominal, id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cod_estado' => 'Cod Estado',
			'nom_estado' => 'Nom Estado',
			'cod_dependencia' => 'Cod Dependencia',
			'nom_dependencia_nominal' => 'Nom Dependencia Nominal',
			'id' => 'ID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('cod_estado',$this->cod_estado);
		$criteria->compare('nom_estado',$this->nom_estado,true);
		$criteria->compare('cod_dependencia',$this->cod_dependencia);
		$criteria->compare('nom_dependencia_nominal',$this->nom_dependencia_nominal,true);
		$criteria->compare('id',$this->id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DependenciaNominalEstado the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}