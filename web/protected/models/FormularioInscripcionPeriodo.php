<?php

/**
 * This is the model class for table "matricula.formulario_inscripcion_periodo".
 *
 * The followings are the available columns in table 'matricula.formulario_inscripcion_periodo':
 * @property integer $id
 * @property integer $periodo_id
 * @property integer $plantel_id
 * @property integer $seccion_plantel_id
 * @property string $archivo
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property string $fecha_act
 * @property string $estatus
 * @property string $extension
 *
 * The followings are the available model relations:
 * @property PeriodoEscolar $periodo
 * @property Plantel $plantel
 * @property UsergroupsUser $usuarioIni
 */
class FormularioInscripcionPeriodo extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'matricula.formulario_inscripcion_periodo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('periodo_id, plantel_id, seccion_plantel_id, archivo, usuario_ini_id, fecha_ini, extension', 'required'),
            array('periodo_id, plantel_id, seccion_plantel_id, usuario_ini_id', 'numerical', 'integerOnly'=>true),
            //array('fecha_ini, fecha_act', 'length', 'max'=>6),
            array('estatus', 'length', 'max'=>1),
            array('extension', 'length', 'max'=>4),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, periodo_id, plantel_id, seccion_plantel_id, archivo, usuario_ini_id, fecha_ini, fecha_act, estatus, extension', 'safe', 'on'=>'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'periodo' => array(self::BELONGS_TO, 'PeriodoEscolar', 'periodo_id'),
            'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'periodo_id' => 'Periodo',
            'plantel_id' => 'Plantel',
            'seccion_plantel_id' => 'Seccion Plantel',
            'archivo' => 'Archivo',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'fecha_act' => 'Fecha Act',
            'estatus' => 'Estatus',
            'extension' => 'Extension',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('periodo_id',$this->periodo_id);
        $criteria->compare('plantel_id',$this->plantel_id);
        $criteria->compare('seccion_plantel_id',$this->seccion_plantel_id);
        $criteria->compare('archivo',$this->archivo,true);
        $criteria->compare('usuario_ini_id',$this->usuario_ini_id);
        $criteria->compare('fecha_ini',$this->fecha_ini,true);
        $criteria->compare('fecha_act',$this->fecha_act,true);
        $criteria->compare('estatus',$this->estatus,true);
        $criteria->compare('extension',$this->extension,true);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    public function existeFormularioInscripcionPeriodo($seccion_plantel_id,$plantel_id,$periodo_id){
        $estatus='P';
        $sql = "SELECT COUNT(id) FROM matricula.formulario_inscripcion_periodo WHERE seccion_plantel_id=:seccion_plantel_id AND plantel_id=:plantel_id AND periodo_id=:periodo_id AND estatus=:estatus";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $busqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        return $busqueda->queryScalar();
    }
    public function existeProcesoActivo(){
        $estatus='A';
        $sql = "SELECT COUNT(id) FROM matricula.formulario_inscripcion_periodo WHERE estatus=:estatus";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        return $busqueda->queryScalar();
    }
    public function buscarFormulariosProcesados(){
        $estatus='A';
        $sql = "select (archivo||'.'||extension) as archivo from matricula.formulario_inscripcion_periodo where estatus <> 'P' ORDER BY id ASC ";
        $busqueda = Yii::app()->db->createCommand($sql);
        return $busqueda->queryColumn();
    }
    public function buscarFormularioPendiente(){
        $estatus='P';
        $sql = "SELECT f.id,f.seccion_plantel_id,f.plantel_id,f.periodo_id,(f.archivo ||'.'||f.extension) archivo, u.email,e.nombre as estado
                FROM matricula.formulario_inscripcion_periodo f
                INNER JOIN seguridad.usergroups_user u ON (u.id=f.usuario_ini_id)
                INNER JOIN gplantel.plantel p ON (p.id=f.plantel_id)
                INNER JOIN public.estado e ON (e.id=p.estado_id)
                WHERE f.estatus=:estatus
                ORDER BY f.fecha_ini ASC LIMIT 1";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        return $busqueda->queryRow();
    }
    public function insertFormularioInscripcionPeriodo($periodo_id, $plantel_id, $seccion_plantel_id, $archivo, $usuario_ini_id,$extension){
        $estatus='P';
        $sql = "INSERT INTO matricula.formulario_inscripcion_periodo(
                periodo_id, plantel_id, seccion_plantel_id, archivo, usuario_ini_id, estatus, extension)
                VALUES (:periodo_id, :plantel_id, :seccion_plantel_id, :archivo, :usuario_ini_id, :estatus, :extension) RETURNING id";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":periodo_id", $periodo_id, PDO::PARAM_INT);
        $busqueda->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $busqueda->bindParam(":seccion_plantel_id", $seccion_plantel_id, PDO::PARAM_INT);
        $busqueda->bindParam(":archivo", $archivo, PDO::PARAM_STR);
        $busqueda->bindParam(":usuario_ini_id", $usuario_ini_id, PDO::PARAM_STR);
        $busqueda->bindParam(":extension", $extension, PDO::PARAM_STR);
        $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        return $busqueda->queryScalar();
    }
    public function actualizarFormulario($id,$estatus){
        $sql = "UPDATE matricula.formulario_inscripcion_periodo
        SET estatus=:estatus
        WHERE id=:id";
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":id", $id, PDO::PARAM_INT);
        $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_STR);
        return $busqueda->execute();
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return FormularioInscripcionPeriodo the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}
