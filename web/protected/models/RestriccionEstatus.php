<?php

/**
 * This is the model class for table "personal.restrinccion_estatus".
 *
 * The followings are the available columns in table 'personal.restrinccion_estatus':
 * @property integer $id
 * @property integer $tipo_personal_id
 * @property integer $especificacion_estatus_id
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property TipoPersonal $tipoPersonal
 * @property EspecificacionEstatus $especificacionEstatus
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 */
class RestriccionEstatus extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'personal.restriccion_estatus';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('tipo_personal_id, especificacion_estatus_id, fecha_ini, usuario_ini_id', 'required'),
			array('tipo_personal_id, especificacion_estatus_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('estatus', 'length', 'max'=>1),
			array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, tipo_personal_id, especificacion_estatus_id, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'tipoPersonal' => array(self::BELONGS_TO, 'TipoPersonal', 'tipo_personal_id'),
			'especificacionEstatus' => array(self::BELONGS_TO, 'EspecificacionEstatus', 'especificacion_estatus_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'tipo_personal_id' => 'Tipo Personal',
			'especificacion_estatus_id' => 'Especificacion Estatus',
			'fecha_ini' => 'Fecha Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('tipo_personal_id',$this->tipo_personal_id);
		$criteria->compare('especificacion_estatus_id',$this->especificacion_estatus_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		if(in_array($this->estatus,array('A','I',))){
                    $criteria->compare('estatus',$this->estatus);
                }

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        public function DropDown_Especi() {

        $estatus = 'A';
         
        $sql = $sql = "SELECT DISTINCT npl.especificacion_estatus_id, n.nombre || ' [' || m.nombre ||']' as nombre, n.nombre as nombrePlan
             FROM 
            personal.restriccion_estatus npl
             INNER JOIN personal.especificacion_estatus n on (npl.especificacion_estatus_id = n.id)          
            LEFT JOIN personal.estatus_docente m on (n.estatus_docente_id = m.id)
             WHERE npl.id IN (especificacion_estatus_id)
             AND npl.estatus= :estatus
             AND n.estatus= :estatus
             AND m.estatus= :estatus
 ORDER BY npl.especificacion_estatus_id ASC";
            
            
        $busqueda = Yii::app()->db->createCommand($sql);
        $busqueda->bindParam(":estatus", $estatus, PDO::PARAM_INT);
        $resultado = $busqueda->queryAll();

//        
        //  var_dump($resulta);
        //die()
         return $resultado;
        }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RestriccionEstatus the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
