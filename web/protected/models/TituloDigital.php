<?php

/**
 * This is the model class for table "titulo.titulo_digital".
 *
 * The followings are the available columns in table 'titulo.titulo_digital':
 * @property integer $id
 * @property string $codigo_verificacion
 * @property integer $titulo_id
 * @property string $origen_estudiante
 * @property integer $cedula_estudiante
 * @property string $serial_titulo
 * @property string $zona_educativa
 * @property string $plantel
 * @property string $codigo_plantel
 * @property string $mencion
 * @property string $nacido_en
 * @property string $lugar_expedicion
 * @property string $fecha_expedicion
 * @property string $fecha_nacimiento
 * @property integer $anio_egreso
 * @property string $origen_drcee_zona
 * @property integer $cedula_drcee_zona
 * @property string $nombre_drcee_zona
 * @property string $firma_digital_director_zona_educativa
 * @property string $origen_director_plantel
 * @property integer $cedula_director_plantel
 * @property string $nombre_director_plantel
 * @property string $firma_digital_director_plantel
 * @property string $origen_funcionario_designado
 * @property integer $cedula_funcionario_designado
 * @property string $nombre_funcionario_designado
 * @property string $nombre_plan
 * @property string $firma_digital_funcionario_designado
 * @property string $observacion
 * @property integer $estudiante_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property integer $periodo_escolar_id
 * @property integer $cod_plan
 * @property string $nombre_estudiante
 * @property integer $is_legacy
 * @property integer $envios_fallidos
 * @property string $correo_estudiante
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioIni
 * @property UsergroupsUser $usuarioAct
 * @property Titulo $titulo
 * @property PeriodoEscolar $periodoEscolar
 * @property Estudiante $estudiante
 */
class TituloDigital extends CActiveRecord
{

        /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'titulo.titulo_digital';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(

			array('estudiante_id, usuario_ini_id, estatus, periodo_escolar_id', 'required'),
			array('titulo_id, cedula_estudiante, anio_egreso, cedula_drcee_zona, cedula_director_plantel, cedula_funcionario_designado, estudiante_id, usuario_ini_id, usuario_act_id, periodo_escolar_id,cod_plan, is_legacy,envios_fallidos', 'numerical', 'integerOnly'=>true),
			array('codigo_verificacion, nombre_estudiante', 'length', 'max'=>50),
			array('origen_estudiante, origen_drcee_zona, origen_director_plantel, origen_funcionario_designado, estatus', 'length', 'max'=>1),
			array('serial_titulo', 'length', 'max'=>30),
			array('zona_educativa', 'length', 'max'=>80),
			array('plantel', 'length', 'max'=>255),
			array('codigo_plantel', 'length', 'max'=>16),
			array('lugar_expedicion', 'length', 'max'=>100),
			array('mencion', 'length', 'max'=>160),
			array('nacido_en', 'length', 'max'=>200),
			array('nombre_drcee_zona, nombre_director_plantel, nombre_funcionario_designado,nombre_plan', 'length', 'max'=>180),
            array('correo_estudiante', 'length', 'max'=>180),
			array('fecha_expedicion,fecha_nacimiento, firma_digital_director_zona_educativa, firma_digital_director_plantel, firma_digital_funcionario_designado, observacion', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, codigo_verificacion, titulo_id, origen_estudiante, cedula_estudiante, serial_titulo, zona_educativa, plantel, codigo_plantel, mencion, nacido_en, lugar_expedicion, fecha_expedicion,fecha_nacimiento, anio_egreso, origen_drcee_zona, cedula_drcee_zona, nombre_drcee_zona, firma_digital_director_zona_educativa, origen_director_plantel, cedula_director_plantel, nombre_director_plantel, firma_digital_director_plantel, origen_funcionario_designado, cedula_funcionario_designado, nombre_funcionario_designado,nombre_plan, firma_digital_funcionario_designado, observacion, estudiante_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, periodo_escolar_id,cod_plan, nombre_estudiante, is_legacy, envios_fallidos', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'titulo' => array(self::BELONGS_TO, 'Titulo', 'titulo_id'),
			'periodoEscolar' => array(self::BELONGS_TO, 'PeriodoEscolar', 'periodo_escolar_id'),
			'estudiante' => array(self::BELONGS_TO, 'Estudiante', 'estudiante_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'codigo_verificacion' => 'Codigo Verificacion',
			'titulo_id' => 'Titulo',
			'origen_estudiante' => 'Origen Estudiante',
			'cedula_estudiante' => 'Cedula Estudiante',
			'serial_titulo' => 'Serial Titulo',
			'zona_educativa' => 'Zona Educativa',
			'plantel' => 'Plantel',
			'codigo_plantel' => 'Codigo Plantel',
			'mencion' => 'Mencion',
			'nacido_en' => 'Nacido En',
			'lugar_expedicion' => 'Lugar Expedicion',
			'fecha_expedicion' => 'Fecha Expedicion',
            'fecha_nacimiento' => 'Fecha de Nacimiento',
			'anio_egreso' => 'Anio Egreso',
			'origen_drcee_zona' => 'Origen Drcee Zona',
			'cedula_drcee_zona' => 'Cedula Drcee Zona',
			'nombre_drcee_zona' => 'Nombre Drcee Zona',
			'firma_digital_director_zona_educativa' => 'Firma Digital Director Zona Educativa',
			'origen_director_plantel' => 'Origen Director Plantel',
			'cedula_director_plantel' => 'Cedula Director Plantel',
			'nombre_director_plantel' => 'Nombre Director Plantel',
			'firma_digital_director_plantel' => 'Firma Digital Director Plantel',
			'origen_funcionario_designado' => 'Origen Funcionario Designado',
			'cedula_funcionario_designado' => 'Cedula Funcionario Designado',
			'nombre_funcionario_designado' => 'Nombre Funcionario Designado',
			'nombre_plan' => 'Nombre Plan',
			'firma_digital_funcionario_designado' => 'Firma Digital Funcionario Designado',
			'observacion' => 'Observacion',
			'estudiante_id' => 'Estudiante',
			'usuario_ini_id' => 'Usuario Ini',
			'fecha_ini' => 'Fecha Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
			'periodo_escolar_id' => 'Periodo Escolar',
            'cod_plan' => 'Codigo de Plan',
			'nombre_estudiante' => 'Nombre Estudiante',
			'is_legacy'=>'Is Legacy',
                        'correo_estudiante'=>'Correo del Estudiante',
            'envios_fallidos'=>'Cantidad de Fallo'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('codigo_verificacion',$this->codigo_verificacion,true);
		$criteria->compare('titulo_id',$this->titulo_id);
		$criteria->compare('origen_estudiante',$this->origen_estudiante,true);
		$criteria->compare('cedula_estudiante',$this->cedula_estudiante);
		$criteria->compare('serial_titulo',$this->serial_titulo,true);
		$criteria->compare('zona_educativa',$this->zona_educativa,true);
		$criteria->compare('plantel',$this->plantel,true);
		$criteria->compare('codigo_plantel',$this->codigo_plantel,true);
		$criteria->compare('mencion',$this->mencion,true);
		$criteria->compare('nacido_en',$this->nacido_en,true);
		$criteria->compare('lugar_expedicion',$this->lugar_expedicion,true);
		$criteria->compare('fecha_expedicion',$this->fecha_expedicion,true);
		$criteria->compare('fecha_nacimiento',$this->fecha_nacimiento,true);
		$criteria->compare('anio_egreso',$this->anio_egreso);
		$criteria->compare('origen_drcee_zona',$this->origen_drcee_zona,true);
		$criteria->compare('cedula_drcee_zona',$this->cedula_drcee_zona);
		$criteria->compare('nombre_drcee_zona',$this->nombre_drcee_zona,true);
		$criteria->compare('firma_digital_director_zona_educativa',$this->firma_digital_director_zona_educativa,true);
		$criteria->compare('origen_director_plantel',$this->origen_director_plantel,true);
		$criteria->compare('cedula_director_plantel',$this->cedula_director_plantel);
		$criteria->compare('nombre_director_plantel',$this->nombre_director_plantel,true);
		$criteria->compare('firma_digital_director_plantel',$this->firma_digital_director_plantel,true);
		$criteria->compare('origen_funcionario_designado',$this->origen_funcionario_designado,true);
		$criteria->compare('cedula_funcionario_designado',$this->cedula_funcionario_designado);
		$criteria->compare('nombre_funcionario_designado',$this->nombre_funcionario_designado,true);
		$criteria->compare('nombre_plan',$this->nombre_plan,true);
		$criteria->compare('firma_digital_funcionario_designado',$this->firma_digital_funcionario_designado,true);
		$criteria->compare('observacion',$this->observacion,true);
		$criteria->compare('estudiante_id',$this->estudiante_id);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);
		$criteria->compare('periodo_escolar_id',$this->periodo_escolar_id);
		$criteria->compare('cod_plan',$this->cod_plan);
		$criteria->compare('nombre_estudiante',$this->nombre_estudiante,true);
		$criteria->compare('is_legacy',$this->is_legacy,true);
		$criteria->compare('envios_fallidos',$this->envios_fallidos,true);

                $criteria->compare('correo_estudiante',$this->correo_estudiante,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TituloDigital the static model class
	 */
	public static function model($className=__CLASS__)
	{
            return parent::model($className);
	}

	/**
         * 
         * @param integer $isLegacy
         * @param integer $tituloId
         * @param integer $estudianteId
         * @return array
         */
        public function getDataTituloDigital($isLegacy, $tituloId, $estudianteId) {
            $grupo_id = 45; // GRUPO DE USUARIO -> DRCEE_ZONA
            $resultado = null;
            if (is_numeric($tituloId) && is_numeric($estudianteId) && in_array($isLegacy, array(0, 1))) {
                $sql = $this->getQueryDbForTitulo($isLegacy);
                $consulta = Yii::app()->db->createCommand($sql);
                $consulta->bindParam(":tituloId", $tituloId, PDO::PARAM_STR);
                $consulta->bindParam(":estuadianteId", $estudianteId, PDO::PARAM_STR);
                $resultado = $consulta->queryRow();
            }
            return $resultado;
	}
        
        public function getQueryDbForTitulo($isLegacy){
            
            $sql = '';
            if(in_array($isLegacy, array(0, 1))){
                if($isLegacy){
                    $sql = "SELECT DISTINCT t.id as titulo_id,
                                    e.tdocumento_identidad AS origen_estudiante,
                                    t.calumno AS cedula_estudiante,
                                    t.ntitulo AS serial_titulo,
                                    ze.nombre AS zona_educativa,
                                    p.nombre AS plantel,
                                    t.cdea AS codigo_plantel,
                                    m.nombre AS mencion,
                                    pq.nombre||', Estado '||es.nombre AS nacido_en,
                                    ese.nombre AS lugar_expedicion,
                                    t.fecha AS fecha_expedicion,
                                    t.anoagreso AS anio_egreso,
                                    '' AS origen_drcee_zona,
                                    '' AS cedula_drcee_zona,
                                    '' AS nombre_drcee_zona,
                                    '' AS origen_director_plantel,
                                    '' AS cedula_director_plantel,
                                    '' AS nombre_director_plantel,
                                    '' AS origen_funcionario_designado,
                                    '' AS cedula_funcionario_designado ,
                                    '' AS nombre_funcionario_designado,
                                    e.id as estudiante_id,
                                    gplantel.get_id_periodo_by_anio_fin(t.anoagreso::INT) AS periodo_escolar_id,
                                    e.nombres || ' ' || e.apellidos AS nombre_estudiante,
                                    e.fecha_nacimiento,
                                    e.correo AS correo_estudiante,
                                    e.sexo,
                                    p.cod_estadistico,
                                    pl.nombre AS nombre_plan,
                                    pl.cod_plan AS cod_plan,
                                    mu.nombre AS municipio_nac,
                                    e.fecha_nacimiento,
                                    t.fecha AS fecha_otorgamiento,
                                    t.anoagreso AS anio_egreso
                              FROM legacy.titulos t
                              LEFT JOIN gplantel.plantel p ON (p.cod_estadistico = t.cestadistico)
                             INNER JOIN gplantel.plan pl ON (pl.cod_plan = t.cplan)
                              LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
                             INNER JOIN matricula.estudiante e ON (e.documento_identidad=t.calumno::CHARACTER VARYING)
                              LEFT JOIN municipio mu on (mu.id=e.municipio_nac_id)
                              LEFT JOIN parroquia pq on (pq.id=e.parroquia_nac_id)
                              LEFT JOIN estado es ON (es.id=e.estado_nac_id)
                              LEFT JOIN estado ese ON (p.estado_id=ese.id)
                             INNER JOIN gplantel.zona_educativa ze ON (ze.id = p.zona_educativa_id)
                             WHERE t.id = :tituloId
                               AND e.id = :estuadianteId
                             LIMIT 1";
                }else{
                    $sql = "SELECT DISTINCT t.id AS titulo_id,
                                    e.tdocumento_identidad AS origen_estudiante,
                                    e.documento_identidad AS cedula_estudiante,
                                    pm.prefijo||' '||pm.serial AS serial_titulo,
                                    ze.nombre AS zona_educativa,
                                    p.nombre AS plantel,
                                    p.cod_plantel AS codigo_plantel,
                                    m.nombre AS mencion,
                                    pq.nombre||', Estado '||es.nombre AS nacido_en,
                                    ese.nombre AS lugar_expedicion,
                                    t.fecha_otorgamiento AS fecha_expedicion,
                                    t.anio_egreso,
                                    u.origen AS origen_drcee_zona,
                                    u.cedula AS cedula_drcee_zona,
                                    u.nombre ||' '|| u.apellido AS nombre_drcee_zona,
                                    us.origen AS origen_director_plantel,
                                    us.cedula AS cedula_director_plantel,
                                    us.nombre ||' '|| us.apellido AS nombre_director_plantel,
                                    use.origen AS origen_funcionario_designado,
                                    use.cedula AS cedula_funcionario_designado ,
                                    use.nombre ||' '|| use.apellido AS nombre_funcionario_designado,
                                    e.id AS estudiante_id,
                                    t.periodo_id AS periodo_escolar_id,
                                    e.nombres || ' ' || e.apellidos AS nombre_estudiante,
                                    e.fecha_nacimiento,
                                    e.correo AS correo_estudiante,
                                    e.sexo,
                                    p.cod_estadistico,
                                    pl.nombre AS nombre_plan,
                                    pl.cod_plan AS cod_plan,
                                    mu.nombre AS municipio_nac,
                                    e.fecha_nacimiento,
                                    t.fecha_otorgamiento,
                                    t.anio_egreso
                         FROM matricula.estudiante e
                        INNER JOIN titulo.titulo t ON (t.estudiante_id = e.id)
                        INNER JOIN gplantel.plantel p ON (p.id = e.plantel_actual_id)
                        INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id)
                        INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
                         LEFT JOIN estado es on ( es.id=e.estado_nac_id)
                         LEFT JOIN estado ese on ( p.estado_id=ese.id)
                         LEFT JOIN municipio mu on (mu.id=e.municipio_nac_id)
                         LEFT JOIN parroquia pq on (pq.id=e.parroquia_nac_id)
                         LEFT JOIN gplantel.mencion m on (pl.mencion_id = m.id)
                         LEFT JOIN gplantel.mencion me on (me.id=pl.mencion_id)
                         LEFT JOIN gplantel.autoridad_plantel aut on (t.plantel_id = aut.plantel_id and aut.cargo_id= 20)
                         LEFT JOIN seguridad.usergroups_user use on (aut.usuario_id = use.id)
                        INNER JOIN gplantel.zona_educativa ze on ze.id =p.zona_educativa_id
                        INNER JOIN seguridad.usergroups_user u ON (u.group_id = 45)
                        INNER JOIN estado est ON (est.id = p.estado_id AND est.id = u.estado_id)
                        INNER JOIN gplantel.autoridad_plantel au on (t.plantel_id = au.plantel_id and au.cargo_id= 3)
                        INNER JOIN seguridad.usergroups_user us on (au.usuario_id = us.id)
                        WHERE t.id = :tituloId 
                          AND e.id = :estuadianteId 
                          AND t.estatus_actual_id=2
                        LIMIT 1";
                }
            }

            return $sql;
        }

        
        /**
         * @author Jean Carlos Barboza
         * 
         * @param type $tipoBusqueda
         * @param type $EstuadianteCedula
         * @return type
         */
        public function buscarEstTituloDigital($tipoBusqueda, $EstuadianteCedula) {
		$grupo_id = 45; //DRCEE_ZONA

		if (in_array($tipoBusqueda, array('V', 'E')) AND is_numeric($EstuadianteCedula)) {

			$sql = "SELECT distinct e.id as estudiante_id,e.documento_identidad, e.nombres, e.apellidos,t.anio_egreso,t.fecha_otorgamiento,
                                       e.fecha_nacimiento, e.correo,e.tdocumento_identidad,e.sexo, p.cod_plantel, p.cod_estadistico,
                                       p.nombre as nombreplantel, pm.serial, pl.nombre as nombre_plan, pl.cod_plan as cod_plan,
                                       m.nombre as nombre_mencion,t.anio_egreso,t.fecha_otorgamiento,es.nombre as estado_nac,
                                       mu.nombre as municipio_nac,e.fecha_nacimiento,ese.nombre as estado_plantel,t.fecha_otorgamiento,
                                       t.anio_egreso,pm.prefijo,t.id as titulo_id, ze.nombre as nombre_zona_educativa,
                                       me.nombre as nombre_mencion,u.origen as origen_drcee_zona, u.cedula as cedula_drcee_zona ,
                                       u.nombre ||' '|| u.apellido as nombre_apellido_drcee_zona, us.origen as origen_dir_plantel,
                                       us.cedula as cedula_dir_plantel, us.nombre ||' '|| us.apellido as nombre_dir_plantel,
                                       use.origen as origen_funcio_desig,use.cedula as cedula_funcio_desig ,
                                       use.nombre ||' '|| use.apellido as nombre_funcio_desig, t.periodo_id as periodo_escolar
                                 FROM matricula.estudiante e
                                INNER JOIN titulo.titulo t ON (t.estudiante_id = e.id)
                                INNER JOIN gplantel.plantel p ON (p.id = e.plantel_actual_id)
                                 LEFT JOIN estado es on ( es.id=e.estado_nac_id)
                                 LEFT JOIN estado ese on ( p.estado_id=ese.id)
                                 LEFT JOIN municipio mu on (mu.id=e.municipio_nac_id)
                                 LEFT JOIN parroquia pq on (pq.id=e.parroquia_nac_id)
                                INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id)
                                INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
                                 LEFT JOIN gplantel.mencion m on (pl.mencion_id = m.id)
                                INNER JOIN gplantel.zona_educativa ze on ze.id =pm.zona_educativa_id
                                INNER JOIN seguridad.usergroups_user u ON (u.group_id = 45)
                                INNER JOIN estado est ON (est.id = p.estado_id AND est.id = u.estado_id)
                                INNER JOIN gplantel.autoridad_plantel au on (t.plantel_id = au.plantel_id and au.cargo_id= 3)
                                INNER JOIN seguridad.usergroups_user us on (au.usuario_id = us.id)
                                 LEFT JOIN gplantel.autoridad_plantel aut on (t.plantel_id = aut.plantel_id and aut.cargo_id= 20)
                                 LEFT JOIN seguridad.usergroups_user use on (aut.usuario_id = use.id)
                                WHERE e.documento_identidad = :estuadianteCedula  and  e.tdocumento_identidad = :tipoBusqueda and t.estatus_actual_id=2
                                LIMIT 1";
			$consulta = Yii::app()->db->createCommand($sql);
                        // echo "$sql";
                        // die();
			$consulta->bindParam(":tipoBusqueda", $tipoBusqueda, PDO::PARAM_STR);
			$consulta->bindParam(":EstuadianteCedula", $EstuadianteCedula, PDO::PARAM_STR);
			$resultado = $consulta->queryAll();
			return $resultado;
		}
	}
        
        /**
         * @author Jean Carlos Barboza
         * 
         */
	public function buscarEstTituloDigitalLegacy($tipoBusqueda, $EstuadianteCedula) {

                // aqui entra cuando el titulo no se encuentra en el esquema titulo( es decir entrega de titulo antes del 2014)
		$sql = "SELECT distinct e.id as estudiante_id,t.ntitulo as serial,t.cdea as cod_plantel,t.cestadistico as cod_estadistico,t.id as titulo_id,t.cplan,pl.nombre,
                t.calumno as documento_identidad,e.nombres,
                e.apellidos,p.nombre as nombreplantel, m.nombre as nombre_mencion,
                pl.nombre as nombre_plan,e.tdocumento_identidad,
                e.sexo,e.correo,e.fecha_nacimiento,t.femision as fecha_otorgamiento,
                t.anoagreso as ano_egreso,
                es.nombre as estado_nac,mu.nombre as municipio_nac,ese.nombre as estado_plantel
                FROM legacy.titulos t
                left JOIN gplantel.plantel p ON (p.cod_estadistico = t.cestadistico)
                INNER JOIN gplantel.plan pl ON (pl.cod_plan = t.cplan)
                LEFT JOIN gplantel.mencion m on (pl.mencion_id = m.id)
                inner join matricula.estudiante e on (e.documento_identidad=t.calumno::CHARACTER VARYING)
                LEFT JOIN estado es on ( es.id=e.estado_nac_id)
                LEFT JOIN municipio mu on (mu.id=e.municipio_nac_id)
                LEFT JOIN estado ese on ( p.estado_id=ese.id)
                where calumno=:EstuadianteCedula and e.tdocumento_identidad =:tipoBusqueda
                limit 1; ";
		$consulta = Yii::app()->db->createCommand($sql);
		$consulta->bindParam(":tipoBusqueda", $tipoBusqueda, PDO::PARAM_STR);
		$consulta->bindParam(":EstuadianteCedula", $EstuadianteCedula, PDO::PARAM_STR);
//                                echo $sql;
//////var_dump($resultado);
//                die();
		$resultado = $consulta->queryAll();
		return $resultado;
	}

        /**
         * @author Jean Carlos Barboza
         **/

	public function verificarQr($codigo_verificacion){
		$resultado="";
		$sql="select e.origen_estudiante,
           e.cedula_estudiante,e.nombre_estudiante,e.zona_educativa,e.codigo_verificacion,
           e.plantel,e.codigo_plantel, serial_titulo, e.mencion,e.nacido_en,e.fecha_nacimiento,e.lugar_expedicion,
           e.fecha_expedicion,e.anio_egreso, e.firma_digital_director_zona_educativa,e.origen_director_plantel,
           e.cedula_director_plantel,e.nombre_director_plantel,e.firma_digital_director_plantel,
           e.origen_funcionario_designado,e.cedula_funcionario_designado, e.nombre_funcionario_designado,e.nombre_plan,
           e.firma_digital_funcionario_designado,e.envios_fallidos from titulo.titulo_digital e where e.codigo_verificacion=:codigo_verificacion";
		//echo "<pre>   $sql </pre>"; die();
		$consulta=Yii::app()->db->createCommand($sql);
		$consulta->bindParam(":codigo_verificacion", $codigo_verificacion,PDO::PARAM_STR);
		$resultado=$consulta->queryRow();
		return $resultado;
	}

    /**
     * @author Nelson Javier Gonzalez Gonzalez
     **/
    public function exiteSolucitudTituloDigital($id){
        $sql='select count(*) AS existe from titulo.titulo_digital AS t where t.titulo_id=:id';
        $resultado = Yii::app()->db
            ->createCommand("$sql")
            ->bindParam(":id", $id, PDO::PARAM_INT)
            ->queryScalar();

        return $resultado;
    }
    /**
     * @author Nelson Javier Gonzalez Gonzalez
     **/
    public function exiteListaTituloDigitalActivo(){
        $sql="select count(*) AS existe from titulo.titulo_digital AS t where t.estatus=:estatus and correo_estudiante <> '' LIMIT 1";
        $resultado = Yii::app()->db
            ->createCommand("$sql")
            ->bindParam(":estatus", 'A', PDO::PARAM_STR)
            ->queryScalar();

        return $resultado;
    }
    /**
     * @author Nelson Javier Gonzalez Gonzalez
     **/
    public function listadoEmailActivos(){
        //$sql="select * from titulo.titulo_digital AS t where t.estatus='A' and correo_estudiante <> ''";
//        $resultado = Yii::app()->db
//            ->createCommand("$sql")
//            ->queryAll();
        //->bindParam(":estatus", 'A', PDO::PARAM_STR)
        $condicion="t.estatus='A' and correo_estudiante <> ''";
        //$parametro="A";
        //$producto=Productos::model()->find('id_producto=:id', array(':id'=>1));

        $resultado=TituloDigital::model()->findAll($condicion);
        return $resultado;
    }





}


