<?php

/**
 * This is the model class for table "gplantel.congreso_pedagogico_docente".
 *
 * The followings are the available columns in table 'gplantel.congreso_pedagogico_docente':
 * @property integer $id
 * @property integer $congreso_pedagogico_id
 * @property string $tdocumento_identidad
 * @property string $documento_identidad
 * @property string $nombres
 * @property string $apellidos
 * @property string $fecha_ini
 * @property integer $usuario_ini_id
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 *
 * The followings are the available model relations:
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property Plantel $congresoPedagogico
 */
class CongresoPedagogicoDocenteForm extends CFormModel
{
    public $tdocumento_identidad;
    public $documento_identidad;
    public $nombres;
    public $apellidos;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'gplantel.congreso_pedagogico_docente';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('congreso_pedagogico_id, tdocumento_identidad, documento_identidad, nombres, apellidos, fecha_ini, usuario_ini_id', 'required'),
			array('congreso_pedagogico_id, usuario_ini_id, usuario_act_id', 'numerical', 'integerOnly'=>true),
			array('tdocumento_identidad, estatus', 'length', 'max'=>1),
			array('documento_identidad', 'length', 'max'=>15),
			array('nombres, apellidos', 'length', 'max'=>60),
			array('fecha_act, fecha_elim', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, congreso_pedagogico_id, tdocumento_identidad, documento_identidad, nombres, apellidos, fecha_ini, usuario_ini_id, usuario_act_id, fecha_act, fecha_elim, estatus', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
			'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
			'congresoPedagogico' => array(self::BELONGS_TO, 'Plantel', 'congreso_pedagogico_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'congreso_pedagogico_id' => 'Congreso Pedagogico',
			'tdocumento_identidad' => 'Tipo de Documento de Identidad',
			'documento_identidad' => 'Documento de Identidad',
			'nombres' => 'Nombres',
			'apellidos' => 'Apellidos',
			'fecha_ini' => 'Fecha Ini',
			'usuario_ini_id' => 'Usuario Ini',
			'usuario_act_id' => 'Usuario Act',
			'fecha_act' => 'Fecha Act',
			'fecha_elim' => 'Fecha Elim',
			'estatus' => 'Estatus',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('congreso_pedagogico_id',$this->congreso_pedagogico_id);
		$criteria->compare('tdocumento_identidad',$this->tdocumento_identidad,true);
		$criteria->compare('documento_identidad',$this->documento_identidad,true);
		$criteria->compare('nombres',$this->nombres,true);
		$criteria->compare('apellidos',$this->apellidos,true);
		$criteria->compare('fecha_ini',$this->fecha_ini,true);
		$criteria->compare('usuario_ini_id',$this->usuario_ini_id);
		$criteria->compare('usuario_act_id',$this->usuario_act_id);
		$criteria->compare('fecha_act',$this->fecha_act,true);
		$criteria->compare('fecha_elim',$this->fecha_elim,true);
		$criteria->compare('estatus',$this->estatus,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CongresoPedagogicoDocente the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
