<?php

/**
 * This is the model class for table "titulo.titulo".
 *
 * The followings are the available columns in table 'titulo.titulo':
 * @property integer $id
 * @property integer $papel_moneda_id
 * @property integer $estatus_actual_id
 * @property string $observacion
 * @property integer $estudiante_id
 * @property integer $tipo_documento_id
 * @property integer $motivo_retencion_id
 * @property integer $tipo_evaluacion_id
 * @property integer $ano_egreso
 * @property integer $seccion_plantel_periodo_id
 * @property integer $grado_id
 * @property integer $seccion_id
 * @property integer $plan_id
 * @property integer $usuario_ini_id
 * @property string $fecha_ini
 * @property integer $usuario_act_id
 * @property string $fecha_act
 * @property string $fecha_elim
 * @property string $estatus
 * @property string $codigo_verificacion
 * @property integer $estatus_solicitud_id
 * @property integer $plantel_id
 * @property integer $cargo_calificacion
 * @property integer $periodo_id
 * @property integer $asignado_zona_edu
 * @property integer $asignado_plantel
 * @property integer $asignado_estudiante
 * @property integer $cambio_identificacion
 * @property integer $cambio_nombre
 * @property integer $cambio_serial
 * @property integer $cambio_apellido
 * @property string $fecha_otorgamiento
 *
 * The followings are the available model relations:
 * @property Plantel $plantel
 * @property Seccion $seccion
 * @property TipoDocumento $tipoDocumento
 * @property UsergroupsUser $usuarioAct
 * @property UsergroupsUser $usuarioIni
 * @property TipoEvaluacion $tipoEvaluacion
 * @property EstatusTitulo $estatusActual
 * @property Grado $grado
 * @property MotivoRetencion $motivoRetencion
 * @property PapelMoneda $papelMoneda
 * @property Plan $plan
 * @property FirmaAutoridadTitulo[] $firmaAutoridadTitulos
 */
class Titulo extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'titulo.titulo';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('estudiante_id, tipo_documento_id, seccion_plantel_periodo_id, plan_id, usuario_ini_id, estatus, estatus_solicitud_id', 'required'),
            array('papel_moneda_id, estatus_actual_id, estudiante_id, tipo_documento_id, motivo_retencion_id, tipo_evaluacion_id, ano_egreso, seccion_plantel_periodo_id, grado_id, seccion_id, plan_id, usuario_ini_id, usuario_act_id, estatus_solicitud_id, plantel_id, cargo_calificacion, periodo_id, asignado_zona_edu, asignado_plantel, asignado_estudiante, cambio_identificacion, cambio_nombre, cambio_serial, cambio_apellido', 'numerical', 'integerOnly' => true),
            array('observacion', 'length', 'max' => 150),
            array('fecha_ini, fecha_act, fecha_elim', 'length', 'max' => 6),
            array('estatus', 'length', 'max' => 1),
            array('codigo_verificacion', 'length', 'max' => 255),
            array('fecha_otorgamiento', 'safe'),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('id, papel_moneda_id, estatus_actual_id, observacion, estudiante_id, tipo_documento_id, motivo_retencion_id, tipo_evaluacion_id, ano_egreso, seccion_plantel_periodo_id, grado_id, seccion_id, plan_id, usuario_ini_id, fecha_ini, usuario_act_id, fecha_act, fecha_elim, estatus, codigo_verificacion, estatus_solicitud_id, plantel_id, cargo_calificacion, periodo_id, asignado_zona_edu, asignado_plantel, asignado_estudiante, cambio_identificacion, cambio_nombre, cambio_serial, cambio_apellido, fecha_otorgamiento', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'plantel' => array(self::BELONGS_TO, 'Plantel', 'plantel_id'),
            'seccion' => array(self::BELONGS_TO, 'Seccion', 'seccion_id'),
            'tipoDocumento' => array(self::BELONGS_TO, 'TipoDocumento', 'tipo_documento_id'),
            'usuarioAct' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_act_id'),
            'usuarioIni' => array(self::BELONGS_TO, 'UsergroupsUser', 'usuario_ini_id'),
            'tipoEvaluacion' => array(self::BELONGS_TO, 'TipoEvaluacion', 'tipo_evaluacion_id'),
            'estatusActual' => array(self::BELONGS_TO, 'EstatusTitulo', 'estatus_actual_id'),
            'grado' => array(self::BELONGS_TO, 'Grado', 'grado_id'),
            'motivoRetencion' => array(self::BELONGS_TO, 'MotivoRetencion', 'motivo_retencion_id'),
            'papelMoneda' => array(self::BELONGS_TO, 'PapelMoneda', 'papel_moneda_id'),
            'plan' => array(self::BELONGS_TO, 'Plan', 'plan_id'),
            'firmaAutoridadTitulos' => array(self::HAS_MANY, 'FirmaAutoridadTitulo', 'titulo_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'id' => 'Este el campo primary key de la tabla.',
            'papel_moneda_id' => 'Papel moneda que contiene los seriales de los titulos.',
            'estatus_actual_id' => 'Estatus en el que se encuentra la solicitud del titulo.',
            'observacion' => 'Observaciones referentes al titulo.',
            'estudiante_id' => 'Estudiantes al que se le asigna los titulos.',
            'tipo_documento_id' => 'Contiene el id del tipo de documento que se requiere ingresar un titulo, certificado, titulo de bachiller integral',
            'motivo_retencion_id' => 'Contiene el id de los motivos de retención de título',
            'tipo_evaluacion_id' => 'Tipo Evaluacion',
            'ano_egreso' => 'Ano Egreso',
            'seccion_plantel_periodo_id' => 'Contiene el id del periodo escolar actual.',
            'grado_id' => 'Contiene el id del grado del estudiante.',
            'seccion_id' => 'Contiene el id de la sección del estudiante.',
            'plan_id' => 'Contiene el id el plan al que pertenece el estudiante.',
            'usuario_ini_id' => 'Usuario Ini',
            'fecha_ini' => 'Fecha Ini',
            'usuario_act_id' => 'Usuario Act',
            'fecha_act' => 'Fecha Act',
            'fecha_elim' => 'Fecha Elim',
            'estatus' => 'Estatus',
            'codigo_verificacion' => 'Codigo Verificacion',
            'estatus_solicitud_id' => 'Estatus Solicitud',
            'plantel_id' => 'Plantel',
            'cargo_calificacion' => 'Contiene 1 si fue cargada las calificaciones, y 0 sino fueron cargadas',
            'periodo_id' => 'Contiene el periodo escolar actual.',
            'asignado_zona_edu' => 'Contiene 1 cuando ya fue validado la entrega de lotes de seriales a las zonas educativas.',
            'asignado_plantel' => 'Asignado Plantel',
            'asignado_estudiante' => 'Asignado Estudiante',
            'cambio_identificacion' => 'Cambio Identificacion',
            'cambio_nombre' => 'Cambio Nombre',
            'cambio_serial' => 'Cambio Serial',
            'cambio_apellido' => 'Cambio Apellido',
            'fecha_otorgamiento' => 'Fecha Otorgamiento',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('id', $this->id);
        $criteria->compare('papel_moneda_id', $this->papel_moneda_id);
        $criteria->compare('estatus_actual_id', $this->estatus_actual_id);
        $criteria->compare('observacion', $this->observacion, true);
        $criteria->compare('estudiante_id', $this->estudiante_id);
        $criteria->compare('tipo_documento_id', $this->tipo_documento_id);
        $criteria->compare('motivo_retencion_id', $this->motivo_retencion_id);
        $criteria->compare('tipo_evaluacion_id', $this->tipo_evaluacion_id);
        $criteria->compare('ano_egreso', $this->ano_egreso);
        $criteria->compare('seccion_plantel_periodo_id', $this->seccion_plantel_periodo_id);
        $criteria->compare('grado_id', $this->grado_id);
        $criteria->compare('seccion_id', $this->seccion_id);
        $criteria->compare('plan_id', $this->plan_id);
        $criteria->compare('usuario_ini_id', $this->usuario_ini_id);
        $criteria->compare('fecha_ini', $this->fecha_ini, true);
        $criteria->compare('usuario_act_id', $this->usuario_act_id);
        $criteria->compare('fecha_act', $this->fecha_act, true);
        $criteria->compare('fecha_elim', $this->fecha_elim, true);
        $criteria->compare('estatus', $this->estatus, true);
        $criteria->compare('codigo_verificacion', $this->codigo_verificacion, true);
        $criteria->compare('estatus_solicitud_id', $this->estatus_solicitud_id);
        $criteria->compare('plantel_id', $this->plantel_id);
        $criteria->compare('cargo_calificacion', $this->cargo_calificacion);
        $criteria->compare('periodo_id', $this->periodo_id);
        $criteria->compare('asignado_zona_edu', $this->asignado_zona_edu);
        $criteria->compare('asignado_plantel', $this->asignado_plantel);
        $criteria->compare('asignado_estudiante', $this->asignado_estudiante);
        $criteria->compare('cambio_identificacion', $this->cambio_identificacion);
        $criteria->compare('cambio_nombre', $this->cambio_nombre);
        $criteria->compare('cambio_serial', $this->cambio_serial);
        $criteria->compare('cambio_apellido', $this->cambio_apellido);
        $criteria->compare('fecha_otorgamiento', $this->fecha_otorgamiento, true);

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Titulo the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }

    public function solicitudTitulo($plantel_id = null) {


        $periodo_Escolar = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_Escolar_id = $periodo_Escolar['id'];

//AND pp.plan_id = sp.plan_id ojo

        $sql = "SELECT distinct e.documento_identidad,e.tdocumento_identidad,e.cedula_escolar, e.nombres, e.apellidos, e.id,
                    e.plantel_actual_id,g.nombre as grado,s.nombre as nombre_seccion,p.nombre as nombre_plantel,
                    ie.seccion_plantel_periodo_id,g.id as id_grado, s.id as id_seccion,pl.id as plan_id , pl.nombre as nombrePlan,
                    spp.periodo_id, m.nombre as mencion
                    from matricula.estudiante e
                    inner join gplantel.grado g on g.id=e.grado_actual_id
                    inner join gplantel.plantel p on p.id=:plantel_actual_id
                    INNER JOIN gplantel.plan_plantel pp ON (pp.plantel_id = p.id )
                    INNER JOIN gplantel.plan pl ON (pl.id = pp.plan_id)
                    LEFT JOIN gplantel.mencion m ON (m.id = pl.mencion_id)
                    inner join matricula.inscripcion_estudiante ie on e.id=ie.estudiante_id
                    inner join gplantel.seccion_plantel_periodo spp on ie.seccion_plantel_periodo_id=spp.id
                    inner join gplantel.seccion_plantel sp on (spp.seccion_plantel_id=sp.id AND sp.plan_id=pp.plan_id)
                    inner join gplantel.seccion s on sp.seccion_id=s.id
                    where spp.periodo_id=$periodo_Escolar_id
                    and e.plantel_actual_id= :plantel_actual_id
                    and e.grado_actual_id IN (select id from gplantel.grado where es_final=1)
                    and e.id not in (select estudiante_id from titulo.titulo where periodo_id=$periodo_Escolar_id  and plantel_id =:plantel_actual_id )
                    order by nombre_seccion,e.documento_identidad asc";

//        $sql = "SELECT e.cedula_identidad, e.nombres, e.apellidos, e.id,
//                    e.plantel_actual_id,g.nombre as grado,s.nombre as nombre_seccion,p.nombre as nombre_plantel,
//                    ie.seccion_plantel_periodo_id,g.id as id_grado, s.id as id_seccion,pl.id as plan_id , pl.nombre as nombrePlan,
//                    spp.periodo_id, m.nombre as mencion
//                    from matricula.estudiante e
//                    inner join gplantel.grado g on g.id=e.grado_actual_id
//                    inner join gplantel.plantel p on p.id=:plantel_actual_id
//                    inner join matricula.inscripcion_estudiante ie on e.id=ie.estudiante_id
//                    inner join gplantel.seccion_plantel_periodo spp on ie.seccion_plantel_periodo_id=spp.id
//                    inner join gplantel.seccion_plantel sp on spp.seccion_plantel_id=sp.id
//                    inner join gplantel.seccion s on sp.seccion_id=s.id
//                    INNER JOIN gplantel.plan_plantel pp ON (pp.plantel_id = p.id )
//                    INNER JOIN gplantel.plan pl ON (pl.id = pp.plan_id)
//                    LEFT JOIN gplantel.mencion m ON (m.id = pl.mencion_id)
//                    where spp.periodo_id=$periodo_Escolar_id
//                    and e.plantel_actual_id= :plantel_actual_id
//                    and e.grado_actual_id IN (select id from gplantel.grado where es_final=1)
//                    and e.id not in (select estudiante_id from titulo.titulo where periodo_id=$periodo_Escolar_id  and plantel_id =:plantel_actual_id ) "
//                . "order by nombre_seccion,e.cedula_identidad asc "
//
//        ;
        $solicitud = Yii::app()->db->createCommand($sql);
//
//        echo "$sql";
//        echo "$plantel_id";
//        die();
        $solicitud->bindParam(":plantel_actual_id", $plantel_id, PDO::PARAM_INT);

        $resulSolicitud = $solicitud->queryAll();
        return $resulSolicitud;
    }

    public function registroSolicitud($idEstudiante_pg_array, $gradoIdEstudiante_pg_array, $seccionIDEstudiante_pg_array, $planIdEstudiante_pg_array, $seccionPlantelPeriodoIdEstudiante_pg_array, $estatus_pg_array, $tipoDocumentoId_pg_array, $usuarioIniId_pg_array, $periodoId_pg_array, $estatusSolicitudId_pg_array, $estatusActualId_pg_array, $plantelId_pg_array, $modulo, $ip, $username) {

        $sql = "SELECT titulo.registrar_solicitud($idEstudiante_pg_array, $gradoIdEstudiante_pg_array, $seccionIDEstudiante_pg_array, $planIdEstudiante_pg_array, $seccionPlantelPeriodoIdEstudiante_pg_array, $estatus_pg_array, $tipoDocumentoId_pg_array, $usuarioIniId_pg_array, $periodoId_pg_array, $estatusSolicitudId_pg_array, $estatusActualId_pg_array, $plantelId_pg_array, :modulo, :ip, :username)";

        $solicitud = Yii::app()->db->createCommand($sql);
        $solicitud->bindParam(":modulo", $modulo, PDO::PARAM_INT);
        $solicitud->bindParam(":ip", $ip, PDO::PARAM_INT);
        $solicitud->bindParam(":username", $username, PDO::PARAM_INT);
//      echo $sql;
//        $inscripcionEstudiante->bindParam(":periodo_id", $periodo, PDO::PARAM_INT);
//        $inscripcionEstudiante->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

        $resultado = $solicitud->execute();
    }

    /* ------  MARISELA ------ */

    public function serialesNoAsignados($plantel_id, $a) {

        $estatus_actual_id = 4; // Este estatus es "Asignado al Plantel" de la tabla estatus_titulo.
        $estatus_actual = 3; // Este estatus es "En Ministerio" de la tabla estatus_titulo.
//AND pm.id = t.papel_moneda_id)
        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_actual_id = $periodo_escolar_actual_id['id'];
        $sql = "SELECT DISTINCT pm.serial
                                    FROM titulo.titulo t
                                    INNER JOIN titulo.papel_moneda pm ON (pm.plantel_asignado_id = t.plantel_id)
                                    WHERE t.estatus_actual_id BETWEEN $estatus_actual AND $estatus_actual_id
                                    AND pm.estatus_actual_id BETWEEN $estatus_actual AND $estatus_actual_id
                                    AND pm.plantel_asignado_id IS NOT NULL
                                    AND t.plantel_id = :plantel_id
                                    AND t.periodo_id = :periodo_actual_id
                                    ORDER BY pm.serial ASC";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);

        if ($a == 'N') {
            $resultado = $consulta->queryAll();

            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        } elseif ($a == 'S') {

            $resultado = $consulta->queryColumn();
//  var_dump($resultado);
            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        }
    }

    public function datosDirector($plantel_id) {

        $cargo_director = 3;
        $estatus = 'A';
        $sql = "SELECT u.nombre, u.apellido, u.cedula
                            FROM gplantel.autoridad_plantel a
                            INNER JOIN seguridad.usergroups_user u ON (u.id = a.usuario_id)
                            WHERE plantel_id = :plantel_id
                            AND cargo_id = :cargo_director
                            AND estatus = :estatus ";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":cargo_director", $cargo_director, PDO::PARAM_INT);
        $consulta->bindParam(":estatus", $estatus, PDO::PARAM_INT);
        $resultado = $consulta->queryAll();
        if ($resultado == array()) {
            return false;
        } else {
            return $resultado;
        }
    }

    public function liquidacionSeriales($plantel_id, $serialTotal, $observacionTotal, $liquidacionTotal, $modulo, $ip, $username, $nombre, $apellido, $cedula) {

        $plantel_id = (int) $plantel_id;
        $usuario_id = (int) Yii::app()->user->id;

        $sql = "SELECT titulo.liquidacion_seriales( $plantel_id, $serialTotal, $observacionTotal, $liquidacionTotal, $usuario_id, :modulo, :ip, :username, :nombre, :apellido, $cedula)";

        $atencionSolicitud = Yii::app()->db->createCommand($sql);
        $atencionSolicitud->bindParam(":modulo", $modulo, PDO::PARAM_STR);
        $atencionSolicitud->bindParam(":ip", $ip, PDO::PARAM_STR);
        $atencionSolicitud->bindParam(":username", $username, PDO::PARAM_STR);
        $atencionSolicitud->bindParam(":nombre", $nombre, PDO::PARAM_STR);
        $atencionSolicitud->bindParam(":apellido", $apellido, PDO::PARAM_STR);

        $resultado = $atencionSolicitud->execute();
    }

    public function mostrarLiquidacionDeSeriales($plantel_id) {

        $devueltoMinisterio = 6;
//        $hurtado = 7;
//        $extraviado = 8;
//        $inutilizado = 9;
        $anulado = 10;
        $sql = " SELECT pm.serial, est.nombre, pm.observacion
                            FROM titulo.papel_moneda pm
                            INNER JOIN titulo.estatus_titulo est ON (est.id = pm.estatus_actual_id)
                            WHERE pm.plantel_asignado_id = :plantel_id
                            AND pm.estatus_actual_id BETWEEN $devueltoMinisterio AND $anulado";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $resultado = $consulta->queryAll();

        if ($resultado == array()) {
            return false;
        } else {
            return $resultado;
        }
    }

    /* ------ FIN  ------ */

//    public function solicitudRealizadaa($plantel_id, $a) {
//
//
//        $estatus_actual_id = 4; // Este estatus es "Asignado al Plantel" de la tabla estatus_titulo.
//        $estatus_actual = 3; // Este estatus es "En Ministerio" de la tabla estatus_titulo.
//
//        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
//        $periodo_actual_id = $periodo_escolar_actual_id['id'];
//
//        $sql = "SELECT DISTINCT pm.serial
//                                    FROM titulo.titulo t
//                                    INNER JOIN titulo.papel_moneda pm ON (pm.plantel_asignado_id = t.plantel_id AND pm.id = t.papel_moneda_id)
//                                    WHERE t.estatus_actual_id BETWEEN $estatus_actual AND $estatus_actual_id
//                                    AND pm.estatus_actual_id BETWEEN $estatus_actual AND $estatus_actual_id
//                                    AND pm.plantel_asignado_id IS NOT NULL
//                                    AND t.plantel_id = :plantel_id
//                                    AND t.periodo_id = :periodo_actual_id
//                                    ORDER BY pm.serial ASC";
//        $consulta = Yii::app()->db->createCommand($sql);
//        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
//        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
////            $consulta->bindParam(":estatus_actual", $estatus_actual, PDO::PARAM_INT);
//        if ($a == 'N') {
//            $resultado = $consulta->queryAll();
//
//            if ($resultado == array()) {
//                return false;
//            } else {
//                return $resultado;
//            }
//        } elseif ($a == 'S') {
//
//            $resultado = $consulta->queryColumn();
//            var_dump($resultado);
//            if ($resultado == array()) {
//                return false;
//            } else {
//                return $resultado;
//            }
//        }
//    }

    public function cadidatoAsignacionTitulo($plantel_id, $periodo_actual_id) {

        $sql = "SELECT DISTINCT e.id AS id_estudiante,e.nombres, e.apellidos,
                            s.nombre AS seccion, g.nombre AS grado,
                            e.documento_identidad, pl.nombre AS nombre_plan,
                            m.nombre AS nombre_mencion, pl.cod_plan AS codigo_plan,
                           pm.serial
                            FROM titulo.titulo t
                            INNER JOIN gplantel.plantel p on (p.id = t.plantel_id)
                            INNER JOIN titulo.papel_moneda pm on (pm.id = t.papel_moneda_id)
                            INNER JOIN matricula.estudiante e on (e.id = t.estudiante_id)
                            INNER JOIN matricula.inscripcion_estudiante ie on (ie.estudiante_id = t.estudiante_id)
                            INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
                            LEFT JOIN gplantel.mencion m on (pl.mencion_id = m.id)
                            INNER JOIN gplantel.seccion s on (s.id = t.seccion_id)
                            INNER JOIN gplantel.grado g on (g.id = t.grado_id)
                            WHERE t.plantel_id = $plantel_id
                            AND t.periodo_id = $periodo_actual_id
                            AND t.estatus_actual_id = 2
                            AND t.estatus_solicitud_id = 2
                            ORDER BY  s.nombre ASC, pm.serial ASC";

        $cadidatoTitulo = Yii::app()->db->createCommand($sql);
//    $cadidatoTitulo->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
//   $cadidatoTitulo->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $cadidatoAsignacionTitulo = $cadidatoTitulo->queryAll();

        return $cadidatoAsignacionTitulo;
    }

    public function registrarAsignacionTitulo($serialesAsignarTitulo_pg_array, $usuarioIniId, $plantel_id, $modulo, $ip, $username, $nombreUsuario, $apellidoUsuario, $cedulaUsuario) {

        $sql = "SELECT titulo.registrar_otorgacion_titulo($serialesAsignarTitulo_pg_array, '$usuarioIniId', $plantel_id, '$modulo', '$ip', '$username', '$nombreUsuario', '$apellidoUsuario', '$cedulaUsuario')";

        $solicitud = Yii::app()->db->createCommand($sql);
//echo $sql;
//        die();
        $resultado = $solicitud->execute();
    }

    /* ------  MARISELA ------ */

    public function asignacionSerial($plantel_id, $periodo_actual_id, $a) {

        if ($a == 'D') {
            $sql = "SELECT DISTINCT e.documento_identidad AS cedula_estud, e.nombres AS nombre_estud, e.apellidos AS apellido_estud,
                                            g.nombre AS grado_estud, s.nombre AS seccion_estud, p.nombre AS nombre_plantel,
                                            pl.nombre AS plan_estud, m.nombre AS mencion_estud, e.id AS estudiante_id,
                                            g.id AS grado_id, s.id AS seccion_id, pl.id AS plan_id, spp.id AS seccion_plantel_periodo_id
                                         FROM titulo.titulo t
                                         INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                         INNER JOIN gplantel.seccion_plantel_periodo spp ON (t.seccion_plantel_periodo_id = spp.id)
                                         INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
                                         INNER JOIN  matricula.inscripcion_estudiante ie ON (ie.estudiante_id = t.estudiante_id)
                                         INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
                                         LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
                                         INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
                                         INNER JOIN gplantel.grado g ON (g.id = t.grado_id)
                                         WHERE t.plantel_id = :plantel_id
                                         AND t.estatus_actual_id = 11 AND t.estatus_solicitud_id = 1
                                         AND t.periodo_id = :periodo_escolar_actual_id
                                         ORDER BY s.nombre ASC, e.documento_identidad ASC,
                                                           e.nombres ASC, e.apellidos ASC";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
            $consulta->bindParam(":periodo_escolar_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();

            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        } elseif ($a == 'N') {
            $sql = "SELECT DISTINCT e.documento_identidad AS cedula_estud, pm.serial, e.nombres AS nombre_estud, e.apellidos AS apellido_estud,
                                            g.nombre AS grado_estud, s.nombre AS seccion_estud, p.nombre AS nombre_plantel,
                                            pl.nombre AS plan_estud, m.nombre AS mencion_estud, e.id AS estudiante_id,
                                            g.id AS grado_id, s.id AS seccion_id, pl.id AS plan_id, spp.id AS seccion_plantel_periodo_id
                                         FROM titulo.titulo t
                                         INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                         INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id AND t.plantel_id = pm.plantel_asignado_id)
                                         INNER JOIN gplantel.seccion_plantel_periodo spp ON (t.seccion_plantel_periodo_id = spp.id)
                                         INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
                                         INNER JOIN  matricula.inscripcion_estudiante ie ON (ie.estudiante_id = t.estudiante_id)
                                         INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
                                         LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
                                         INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
                                         INNER JOIN gplantel.grado g ON (g.id = t.grado_id)
                                         WHERE t.plantel_id = $plantel_id
                                         AND t.estatus_actual_id IN (2,5)
                                         AND t.estatus_solicitud_id IN (2,4)
                                         AND pm.estatus_actual_id IN (2,4)
                                         AND t.periodo_id = $periodo_actual_id
                                         ORDER BY s.nombre ASC, e.documento_identidad ASC,
                                                  e.nombres ASC, e.apellidos ASC, pm.serial ASC";
            $consulta = Yii::app()->db->createCommand($sql);
//    echo $sql;
            $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
            $consulta->bindParam(":periodo_escolar_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();

            return $resultado;
        }
    }

    public function verificarEstatusApartadoEstudiante($plantel_id, $periodo_actual_id) {

        $sql = "SELECT count(estatus_actual_id)"
                . " FROM titulo.titulo"
                . " WHERE plantel_id = :plantel_id"
                . " AND periodo_id = :periodo_escolar_actual_id"
                . " AND estatus_actual_id = 2";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_escolar_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultado = $consulta->queryScalar();

        return $resultado;
    }

    public function verificarEstatusAsignacionEstudiante($plantel_id, $periodo_actual_id) {

        $sql = "SELECT count(estatus_actual_id)"
                . " FROM titulo.titulo"
                . " WHERE plantel_id = :plantel_id"
                . " AND periodo_id = :periodo_escolar_actual_id"
                . " AND estatus_actual_id = 5";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_escolar_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultado = $consulta->queryScalar();

        return $resultado;
    }

    public function asignacionSerialesPorPlantel($estudiante_id, $periodo_actual_id, $plantel_id, $cedula, $nombre, $apellido, $modulo, $ip, $username, $usuario_id) {

        $sql = "SELECT titulo.asignar_seriales_por_plantel($estudiante_id, $periodo_actual_id, $plantel_id, $cedula, '$nombre', '$apellido', '$modulo', '$ip', '$username', $usuario_id)";

        $asignacionSeriales = Yii::app()->db->createCommand($sql);
        $resultado = $asignacionSeriales->execute();
    }

    public function planteles($estado_id, $periodo_actual_id, $a) {

        if ($a == 'N') {
            $sql = "SELECT DISTINCT p.nombre, p.cod_estadistico, p.cod_plantel, p.id,
                        (SELECT count(t_.estudiante_id)
                            FROM titulo.titulo t_
                            INNER JOIN gplantel.plantel p_ ON (p_.id = t_.plantel_id)
                            WHERE t_.estatus_actual_id = 11 AND t_.estatus_solicitud_id = 1
                            AND t_.plantel_id = p.id
                            AND p.liquido=1
                            AND p_.estado_id = :estado_id
                            AND t_.periodo_id = :periodo_actual_id
                            LIMIT 1) as cant_estudiantes
		     FROM titulo.titulo t
		     INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
		     WHERE t.estatus_actual_id = 11 AND t.estatus_solicitud_id = 1
                                                     AND p.liquido=1
		     AND p.estado_id = :estado_id
		     AND t.periodo_id = :periodo_actual_id
		     ORDER BY p.cod_estadistico ASC";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();

            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        } elseif ($a == 'S') {

            $sql = "SELECT DISTINCT p.id as plantel_id
		     FROM titulo.titulo t
		     INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
		     INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
		     WHERE t.estatus_actual_id = 2 AND t.estatus_solicitud_id = 2
		     AND p.estado_id = :estado_id
		     AND t.periodo_id = :periodo_actual_id";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $resultadoPlanteles = $consulta->queryAll();

            if ($resultadoPlanteles == array()) {
                return false;
            } else {
                return $resultadoPlanteles;
            }
        }
    }

    public function mostrarInformacionGeneral($estado_id, $periodo_actual_id, $a) {

        if ($a == 'S') {
            $sql = "SELECT count(t_.estudiante_id) as cant_estudiantes,
                                (SELECT count(pm_.id)
                                FROM titulo.papel_moneda pm_
                                LEFT JOIN gplantel.plantel p ON(p.id = pm_.plantel_asignado_id)
                                LEFT JOIN estado e ON(p.estado_id = e.id)
                                WHERE pm_.estatus_actual_id IN (1,3)
                                AND pm_.zona_educativa_id = z.id) as seriales_asignados_plantel
                            FROM titulo.titulo t_
                            INNER JOIN gplantel.plantel p_ ON (p_.id = t_.plantel_id)
                            INNER JOIN titulo.papel_moneda pm ON (pm.id = t_.papel_moneda_id)
                            INNER JOIN gplantel.zona_educativa z ON(z.id = pm.zona_educativa_id)
                            INNER JOIN estado e ON(z.estado_id = e.id)
                            WHERE t_.estatus_actual_id IN (2,5)
                            AND t_.estatus_solicitud_id IN  (2,4)
                            AND p_.estado_id = :estado_id
                            AND t_.periodo_id = :periodo_actual_id
                            group by z.id ";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
//   echo $sql;
            $resultado = $consulta->queryAll();

            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        } elseif ($a == 'N') {

            $sql = "SELECT count(pm_.id)
                                FROM titulo.papel_moneda pm_
                                LEFT JOIN gplantel.plantel p ON(p.id = pm_.plantel_asignado_id)
                                LEFT JOIN estado e ON(p.estado_id = e.id)
                                WHERE pm_.estatus_actual_id IN (1,3)
                                AND pm_.zona_educativa_id = (SELECT z.id FROM gplantel.zona_educativa z
				 INNER JOIN estado e ON (e.id = z.estado_id)
				 WHERE e.id=$estado_id)";
            $consulta = Yii::app()->db->createCommand($sql);
//   echo $sql;
            $resultado = $consulta->queryScalar();

            return $resultado;
        }
    }

    public function asignarSerialesPorEstados($estado_id, $periodo_actual_id, $plantel_id, $cedula, $nombre, $apellido, $modulo, $ip, $username, $usuario_id) {

        $sql = "SELECT titulo.asignar_seriales_por_estados($estado_id, $periodo_actual_id, $plantel_id, $cedula, '$nombre', '$apellido', '$modulo', '$ip', '$username', $usuario_id)";

        $atencionSolicitud = Yii::app()->db->createCommand($sql);
//echo $sql;
        $resultado = $atencionSolicitud->execute();
    }

    public function borrarControlAsignacionError($estado_id, $periodo_actual_id) {

        $sql = "DELETE FROM titulo.control_asignacion_estado
                        WHERE estado_id = $estado_id
                        AND periodo_id = $periodo_actual_id
                        AND estatus_id = 2";

        $borrar = Yii::app()->db->createCommand($sql);
        $resultado = $borrar->execute();
    }

    public function obtenerZonaEducativa($estado_id) {
        $sql = "SELECT z.id as v_zona_educativa_id
                    FROM gplantel.zona_educativa z
                    INNER JOIN public.estado e on (e.id =z.estado_id)
                    WHERE z.estado_id = $estado_id
                    LIMIT 1 ";
        $obtenerZona = Yii::app()->db->createCommand($sql);
        $resultado = $obtenerZona->queryScalar();
        return $resultado;
    }

    public function obtenerDatosReporteSerialesPorEstado($estado_id, $periodo_actual_id, $zona_educativa) {

//        $sql = " SELECT DISTINCT p.nombre::text as nombre_plantel, p.cod_plantel::text, g.nombre::text as grado,
//		pl.cod_plan::text as codigo_plan, s.nombre::text as seccion,
//		(pe.anio_inicio||'-'|| pe.anio_fin)::text as periodo, e.documento_identidad::text,
//		e.nombres::text, e.apellidos::text, e.nacionalidad::text, (pm.prefijo || ' ' || pm.serial)::text as serial, 'SI' as adicional, e.cedula_escolar::text
//		     FROM titulo.titulo t
//		     INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
//		     INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
//		     INNER JOIN gplantel.zona_educativa ze ON (ze.id = p.zona_educativa_id)
//                                                    INNER JOIN estado est ON (est.id = p.estado_id)
//		     LEFT JOIN gplantel.denominacion d ON (d.id = p.denominacion_id)
//		     INNER JOIN municipio mun ON (mun.id = p.municipio_id)
//		     INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
//                                                    LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
//                                                    INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
//                                                    INNER JOIN gplantel.grado g ON (g.id = t.grado_id)
//                                                    INNER JOIN gplantel.periodo_escolar pe ON (pe.id = t.periodo_id)
//		     INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
//		     WHERE t.estatus_actual_id IN (2,5)
//		     AND t.estatus_solicitud_id IN (2,4)
//		     AND p.estado_id = :estado_id
//		     AND t.periodo_id = :periodo_actual_id
//
//
//		     UNION
//                                                SELECT DISTINCT p.nombre::text as nombre_plantel, p.cod_plantel::text, 'No Asignado'::text as grado,
//		'No Asignado'::text as codigo_plan, 'No Asignado'::text as seccion,
//		'No Asignado'::text as periodo, 'No Asignado'::text as cedula_identidad,
//		'No Asignado'::text as nombres,  'No Asignado'::text as  apellidos,  'No Asignado'::text as  nacionalidad, prefijo || ' ' || pm.serial as serial,'NO' as adicional, ''::text as cedula_escolar
//                                                FROM titulo.titulo t
//		     INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
//		     INNER JOIN titulo.papel_moneda pm ON (pm.plantel_asignado_id = t.plantel_id)
//                                                    INNER JOIN gplantel.zona_educativa ze ON (ze.id = p.zona_educativa_id)
//		     INNER JOIN estado est ON (est.id = p.estado_id)
//		     LEFT JOIN gplantel.denominacion d ON (d.id = p.denominacion_id)
//		     INNER JOIN municipio mun ON (mun.id = p.municipio_id)
//		     INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
//                                                    LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
//                                                    INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
//                                                    INNER JOIN gplantel.grado g ON (g.id = t.grado_id)
//                                                    INNER JOIN gplantel.periodo_escolar pe ON (pe.id = t.periodo_id)
//		     INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
//		     WHERE pm.estatus_actual_id = 3
//		     AND p.estado_id = :estado_id
//		     AND t.periodo_id = :periodo_actual_id
//		ORDER BY serial ASC, cod_plantel ASC, seccion ASC";

        $sql = "SELECT DISTINCT p.nombre::text as nombre_plantel, p.cod_plantel::text, g.nombre::text as grado,
		pl.cod_plan::text as codigo_plan, s.nombre::text as seccion,
		(pe.anio_inicio||'-'|| pe.anio_fin)::text as periodo, e.documento_identidad::text as documento_identidad,
		e.nombres::text, e.apellidos::text, e.nacionalidad::text, (pm.prefijo || ' ' || pm.serial)::text as serial,
                                                'SI' as adicional, e.cedula_escolar::text,e.tdocumento_identidad::text as tdocumento_identidad
		FROM titulo.titulo t
		INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
		INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
		INNER JOIN gplantel.zona_educativa ze ON (ze.id = p.zona_educativa_id)
                                                INNER JOIN estado est ON (est.id = p.estado_id)
		LEFT JOIN gplantel.denominacion d ON (d.id = p.denominacion_id)
		INNER JOIN municipio mun ON (mun.id = p.municipio_id)
		INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
                                                LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
                                                INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
                                                INNER JOIN gplantel.grado g ON (g.id = t.grado_id)
                                                INNER JOIN gplantel.periodo_escolar pe ON (pe.id = t.periodo_id)
		INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
		WHERE t.estatus_actual_id IN (2,5)
		AND t.estatus_solicitud_id IN (2,4)
		AND p.estado_id = :estado_id
		AND t.periodo_id = :periodo_actual_id
	UNION
                                                SELECT DISTINCT coalesce(p.nombre::text, 'No Asignado') as nombre_plantel, coalesce(p.cod_plantel::text,'No Asignado'), 'No Asignado'::text as grado,
		'No Asignado'::text as codigo_plan, 'No Asignado'::text as seccion,
		'No Asignado'::text as periodo, 'No Asignado'::text as documento_identidad,
		'No Asignado'::text as nombres,  'No Asignado'::text as  apellidos,  'No Asignado'::text as  nacionalidad,
		prefijo || ' ' || pm.serial as serial,'NO' as adicional, 'No Asignado'::text as cedula_escolar
                ,e.tdocumento_identidad::text as tdocumento_identidad
                                                FROM titulo.papel_moneda pm
		LEFT JOIN gplantel.plantel p ON (p.id = pm.plantel_asignado_id)
                                                LEFT JOIN gplantel.zona_educativa ze ON (ze.id = p.zona_educativa_id)
		LEFT JOIN estado est ON (est.id = p.estado_id)
                LEFT join titulo.titulo t on (t.papel_moneda_id =pm.id)
                LEFT JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
		WHERE pm.estatus_actual_id IN (1,3)
		AND pm.zona_educativa_id=$zona_educativa
		ORDER BY serial ASC, seccion ASC, documento_identidad ASC, cod_plantel ASC";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultadoPlanteles = $consulta->queryAll();

        if ($resultadoPlanteles == array()) {
            return false;
        } else {
            return $resultadoPlanteles;
        }
    }

//    public function obtenerDatosReporteSerialesPorEstadoRestantes($estado_id) {
//
//        $sql = "SELECT DISTINCT (pm.prefijo || ' ' || pm.serial) as serial_restante, p.cod_plantel, p.nombre as nombre_plantel
//		     FROM titulo.titulo t
//		     INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
//		     INNER JOIN titulo.papel_moneda pm ON (pm.plantel_asignado_id = t.plantel_id)
//		     WHERE pm.estatus_actual_id = 3
//		     AND p.estado_id = :estado_id
//		     ORDER BY serial ASC, p.cod_plantel ASC";
//        $consulta = Yii::app()->db->createCommand($sql);
//        $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
//        $resultado = $consulta->queryAll();
//
//        if ($resultado == array()) {
//            return false;
//        } else {
//            return $resultado;
//        }
//    }

    public function guardarControlAsignacionAntes($estado_id, $periodo_actual_id) {

        $estatus_id = 2; // Estatus en proceso
        $sql = "INSERT INTO titulo.control_asignacion_estado (estado_id, periodo_id, estatus_id)
                    VALUES (:estado_id, :periodo_actual_id, :estatus_id)";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $consulta->bindParam(":estatus_id", $estatus_id, PDO::PARAM_INT);
        $resultado = $consulta->execute();

        return $resultado;
    }

    public function guardarControlAsignacionDespues($estado_id, $periodo_actual_id) {

        $estatus_id = 4; // Estatus completo
        $sql = "UPDATE titulo.control_asignacion_estado
	SET estatus_id = :estatus_id
	WHERE estado_id = :estado_id
	AND periodo_id = :periodo_actual_id";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $consulta->bindParam(":estatus_id", $estatus_id, PDO::PARAM_INT);
        $resultado = $consulta->execute();

        return $resultado;
    }

    public function verificarImpresionReporte($estado_id, $periodo_actual_id) {

        $sql = "SELECT  estatus_id as estatus"
                . " FROM titulo.control_asignacion_estado"
                . " WHERE estado_id = :estado_id"
                . " AND periodo_id = :periodo_actual_id"
                . " ORDER BY estatus_id ASC";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultado = $consulta->queryScalar();
//  var_dump($resultado);
        return $resultado;
    }

    public function verificarEstudiantesConSeriales($plantel_id, $periodo_actual_id) {


        $sql = "SELECT DISTINCT count(t.estudiante_id) as estudiantes_con_serial,
                        (
                        SELECT DISTINCT count(t.estudiante_id)
                                                                 FROM titulo.titulo t
                                                                 INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                                                 WHERE t.plantel_id = $plantel_id
                                                                 AND t.periodo_id = $periodo_actual_id
                        ) as estudiantes_total
                                                                 FROM titulo.titulo t
                                                                 INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                                                 INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id)
                                                                 WHERE t.plantel_id = $plantel_id
                                                                 AND t.estatus_actual_id IN (2,5)
                                                                 AND t.estatus_solicitud_id IN (2,4)
                                                                 AND t.periodo_id = $periodo_actual_id";

//        $sql = "SELECT DISTINCT count(t.estudiante_id) as estudiantes
//                                         FROM titulo.titulo t
//                                         INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
//                                         INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id)
//                                         WHERE t.plantel_id = $plantel_id
//                                         AND t.estatus_actual_id BETWEEN 2 AND 5
//                                         AND t.estatus_solicitud_id BETWEEN  2 AND 4
//                                         AND t.periodo_id = $periodo_actual_id";

        $consulta = Yii::app()->db->createCommand($sql);
//   $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
//  $consulta->bindParam(":periodo_escolar_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultado = $consulta->queryAll();

        return $resultado;
    }

    public function obtenerDatosReporteSerialesPorPlantel($plantel_id, $periodo_actual_id) {

        $sql = "SELECT DISTINCT pl.nombre as nombre_plan, m.nombre as mencion, g.nombre as grado,
		pl.cod_plan as codigo_plan, s.nombre as seccion,
		pe.anio_inicio||'-'|| pe.anio_fin as periodo, e.documento_identidad,
		e.nombres, e.apellidos, pm.prefijo || ' ' || pm.serial as serial,
		e.nacionalidad, e.sexo, e.fecha_nacimiento, t.observacion, e.cedula_escolar,
                                                e.tdocumento_identidad
		     FROM titulo.titulo t
		     INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                                     INNER JOIN estado est ON (est.id = p.estado_id)
		     LEFT JOIN gplantel.denominacion d ON (d.id = p.denominacion_id)
		     INNER JOIN municipio mun ON (mun.id = p.municipio_id)
		     INNER JOIN gplantel.zona_educativa ze ON (ze.id = p.zona_educativa_id)
		     INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
                                                     LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
                                                     INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
                                                     INNER JOIN gplantel.grado g ON (g.id = t.grado_id)
                                                     INNER JOIN gplantel.periodo_escolar pe ON (pe.id = t.periodo_id)
		     INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
		     INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
		     WHERE  t.estatus_actual_id IN (2,5)
                                                     AND t.estatus_solicitud_id IN (2,4)
		     AND t.plantel_id = :plantel_id
		     AND t.periodo_id = :periodo_actual_id
		     ORDER BY serial ASC, s.nombre ASC, e.documento_identidad ASC, pl.cod_plan ASC";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultadoPlanteles = $consulta->queryAll();

        return $resultadoPlanteles;
    }

    public function obtenerSeccionDeTitulo($plantel_id_decoded, $periodo_actual_id) {

        $sql = "SELECT DISTINCT s.id as seccion_id
		     FROM titulo.titulo t
		     INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                                     INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
                                                     INNER JOIN gplantel.periodo_escolar pe ON (pe.id = t.periodo_id)
		     INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
		     WHERE t.estatus_actual_id IN (2,5)
                                                     AND t.estatus_solicitud_id IN  (2,4)
                                                     AND pm.estatus_actual_id IN (2,5)
		     AND t.plantel_id = :plantel_id
		     AND t.periodo_id = :periodo_actual_id
		     ORDER BY s.id ASC";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id_decoded, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultadoPlanteles = $consulta->queryAll();

        return $resultadoPlanteles;
    }

    public function mostrarAsignados($plantel_id, $periodo_actual_id) {

        $sql = "SELECT e.nombres, e.apellidos, e.documento_identidad, m.nombre AS nombre_mencion,
                           s.nombre AS seccion, g.nombre AS grado, pm.serial, pl.cod_plan AS codigo_plan
                           FROM titulo.titulo t
                           INNER JOIN matricula.estudiante e ON (t.estudiante_id = e.id)
                           INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id)
                           INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
                           LEFT JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
                           INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
                           INNER JOIN gplantel.grado g ON (g.id = t.grado_id)
                           WHERE t.estatus_actual_id = 5
                           AND t.estatus_solicitud_id = 4
                           AND pm.estatus_actual_id = 5
                           AND t.plantel_id = :plantel_id
                           AND t.periodo_id = $periodo_actual_id";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultado = $consulta->queryAll();

        return $resultado;
    }

    public function tieneSolicitudes($plantel_id, $periodo_actual_id) {

        $sql = "SELECT id
                           FROM titulo.titulo t
                           WHERE  t.plantel_id = $plantel_id
                           AND t.periodo_id = $periodo_actual_id
                           AND t.estatus_actual_id IN (2,5)
                           AND t.estatus_solicitud_id IN  (2,4)";

        $consulta = Yii::app()->db->createCommand($sql);

        $resultado = $consulta->execute();

        return $resultado;
    }

    public function obtenerCorreos($grupo_id) {

        $resultado = array();

        if (is_numeric($grupo_id) || is_array($grupo_id)) {

            if (!is_array($grupo_id)) {
                $sql = "SELECT email as correo, nombre, apellido FROM seguridad.usergroups_user u WHERE  u.group_id = $grupo_id";
            } else {
                $sql = "SELECT email as correo, nombre, apellido FROM seguridad.usergroups_user u WHERE  u.group_id IN (" . implode(',', $grupo_id) . ')';
            }

            $consulta = Yii::app()->db->createCommand($sql);

            $resultado = $consulta->queryAll();
        }

        return $resultado;
    }

    public function obtenerCorreosUsuarios($usuario_id) {

        $resultado = array();

        if (is_numeric($usuario_id)) {

            $sql = "SELECT email as correo, nombre, apellido FROM seguridad.usergroups_user u WHERE  u.id =:usuario_id";


            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);

            $resultado = $consulta->queryAll();
        }
        if ($resultado != array())
            return $resultado;
        else
            return false;
    }

//    public function functionName($serial, $estatus_id, $fecha_ini, $fecha_act, $usuario_ini, $usuario_act, $plantel_id, $estudiante_id) {
//        //      titulo.get_codigo_verificacion(serial_vi character varying, estatus_id_vi integer, fecha_ini_vi timestamp without time zone, fecha_act_vi timestamp without time zone, usuario_ini_vi integer, usuario_act_vi integer, plantel_vi integer, estudiante_vi integer)
//
//        $sql = "SELECT titulo.get_codigo_verificacion($serial, $estatus_id, $fecha_ini, $fecha_act, $usuario_ini, $usuario_act, $plantel_id, $estudiante_id)";
//
//        $obtenerCodigo = Yii::app()->db->createCommand($sql);
//        //echo $sql;
//        $resultado = $obtenerCodigo->execute();
//    }



    public function guardarReporteSerialesEstado($usuario_id, $nombre_pdf, $estado_id) {

        $sql = "INSERT INTO titulo.reporte_seriales_estado
	   (usuario_ini_id, nombre, estado_id)
                    VALUES (:usuario_id, :nombre_pdf, :estado_id)";
        $guardar = Yii::app()->db->createCommand($sql);
        $guardar->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
        $guardar->bindParam(":nombre_pdf", $nombre_pdf, PDO::PARAM_STR);
        $guardar->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
        $resultado = $guardar->execute();

        return $resultado;
    }

    public function buscarReporte($estado_id_decoded) {


        $sql = "SELECT nombre FROM titulo.reporte_seriales_estado WHERE estado_id=:estado_id";
        $guardar = Yii::app()->db->createCommand($sql);
        $guardar->bindParam(":estado_id", $estado_id_decoded, PDO::PARAM_INT);
        $resultado = $guardar->queryScalar();

        return $resultado;
    }

    public function existenSerialesZonaEducativa($zona_eduvativa_id) {

        $sql = "SELECT COUNT(id) as v_cant_seriales FROM  titulo.papel_moneda
			WHERE estatus_actual_id = 1
			AND plantel_asignado_id IS NULL
			AND zona_educativa_id = $zona_eduvativa_id
			LIMIT 1";
        $guardar = Yii::app()->db->createCommand($sql);
        $resultado = $guardar->queryScalar();

        return $resultado;
    }

    public function verificarProcesosCorriendo() {

        $sql = "SELECT COUNT(id) FROM  titulo.control_asignacion_estado
			WHERE estatus_id = 2
			LIMIT 1";
        $guardar = Yii::app()->db->createCommand($sql);
        $resultado = $guardar->queryScalar();

        return $resultado;
    }

    public function guardarRegistroPdfEstadoAntes($estado_id, $periodo_actual_id, $nombrePdf) {

        $estatus_id = 2; // Estatus en proceso
        $sql = "INSERT INTO titulo.control_pdf_estado (estado_id, periodo_id, estatus_id, nombre)
                    VALUES (:estado_id, :periodo_actual_id, :estatus_id, '$nombrePdf')";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $consulta->bindParam(":estatus_id", $estatus_id, PDO::PARAM_INT);
        $resultado = $consulta->execute();

        return $resultado;
    }

    public function guardarRegistroPdfEstadoDespues($estado_id, $periodo_actual_id, $nombrePdf, $a) {

        if ($a == 'N') {
            $estatus_id = 4; // Estatus completo
            $sql = "UPDATE titulo.control_pdf_estado
	SET estatus_id = :estatus_id
	WHERE estado_id = :estado_id
	AND periodo_id = :periodo_actual_id
                        AND nombre='$nombrePdf'";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $consulta->bindParam(":estatus_id", $estatus_id, PDO::PARAM_INT);
            $resultado = $consulta->execute();

            return $resultado;
        } elseif ($a == 'S') {
            $estatus_id = 2; // Estatus En proceso
            $sql = "UPDATE titulo.control_pdf_estado
	SET estatus_id = :estatus_id
	WHERE estado_id = :estado_id
	AND periodo_id = :periodo_actual_id
                        AND nombre='$nombrePdf'";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $consulta->bindParam(":estatus_id", $estatus_id, PDO::PARAM_INT);
            $resultado = $consulta->execute();

            return $resultado;
        }
    }

    public function borrarRegistroPdfEstadoError($estado_id, $periodo_actual_id, $nombrePdf) {

        $sql = "DELETE FROM titulo.control_pdf_estado
                        WHERE estado_id = $estado_id
                        AND periodo_id = $periodo_actual_id
                        AND nombre = $nombrePdf
                        AND estatus_id = 2";

        $borrar = Yii::app()->db->createCommand($sql);
        $resultado = $borrar->execute();
    }

    public function verificarProcesosCorriendoPDF() {

        $sql = "SELECT COUNT(id) FROM  titulo.control_pdf_estado
			WHERE estatus_id = 2
			LIMIT 1";
        $guardar = Yii::app()->db->createCommand($sql);
        $resultado = $guardar->queryScalar();

        return $resultado;
    }

    public function verificarImpresionReportePDF($estado_id, $periodo_actual_id, $nombrePdf) {

        $sql = "SELECT  estatus_id as estatus"
                . " FROM titulo.control_pdf_estado"
                . " WHERE estado_id = :estado_id"
                . " AND periodo_id = :periodo_actual_id"
                . " AND nombre = '$nombrePdf' ";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultado = $consulta->queryScalar();

        if ($resultado == false) {
            $resultado = 0;
            return $resultado;
        } else {
            return $resultado;
        }
    }

    public function verificarExistenciaDeRegistroPDF($estado_id, $periodo_actual_id, $nombrePdf) {

        $sql = "SELECT  estatus_id as estatus"
                . " FROM titulo.control_pdf_estado"
                . " WHERE estado_id = :estado_id"
                . " AND periodo_id = :periodo_actual_id"
                . " AND nombre = '$nombrePdf' ";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estado_id", $estado_id, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultado = $consulta->execute();
        return $resultado;
    }

    /*      SEGUIMIENTO DE TITULO FASE II       */

    /*      Validar entrega de lotes a Zona educativa    */

    function datosPorZonaEdu($periodo_actual_id, $mostrar) {

        if ($mostrar == 0) {
            $sql = "SELECT DISTINCT ze.nombre AS zona_educativa,
                                           COUNT(t.estudiante_id) AS cant_seriales_asignado,
                                           ze.id AS id_zona_educativa
                                FROM titulo.titulo t
                                INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                INNER JOIN titulo.papel_moneda pm ON(pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
                                INNER JOIN gplantel.zona_educativa ze ON(ze.id = pm.zona_educativa_id)
                                WHERE t.periodo_id = :periodo_actual_id
                                AND t.estatus_actual_id IN (2)
                                AND t.estatus_solicitud_id IN  (2)
                                AND t.asignado_zona_edu IS NULL
                                AND ze.id NOT IN (26)
                                GROUP BY ze.id
                                ORDER BY ze.nombre";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();

            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        } elseif ($mostrar == 1) {
            $sql = "SELECT DISTINCT ze.nombre AS zona_educativa,
                                           COUNT(t.estudiante_id) AS cant_seriales_asignado,
                                           ze.id AS id_zona_educativa
                                FROM titulo.titulo t
                                INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                INNER JOIN titulo.papel_moneda pm ON(pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
                                INNER JOIN gplantel.zona_educativa ze ON(ze.id = pm.zona_educativa_id)
                                WHERE t.periodo_id = :periodo_actual_id
                                AND t.estatus_actual_id IN (2)
                                AND t.estatus_solicitud_id IN  (2)
                                AND t.asignado_zona_edu=1
                                AND ze.id NOT IN (26)
                                GROUP BY ze.id
                                ORDER BY ze.nombre";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();

            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        }
    }

    public function guardarControlSeguimientoAntes($estatus_ubicacion_titulo, $periodo_actual_id) {

        $estatus_id = 2; // Estatus en proceso
        $sql = "INSERT INTO titulo.control_seguimiento_titulo (estatus_ubicacion_titulo, periodo_id, estatus_id)
                    VALUES (:estatus_ubicacion_titulo, :periodo_actual_id, :estatus_id)";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estatus_ubicacion_titulo", $estatus_ubicacion_titulo, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $consulta->bindParam(":estatus_id", $estatus_id, PDO::PARAM_INT);
        $resultado = $consulta->execute();

        return $resultado;
    }

    public function guardarControlSeguimientoDespues($estatus_ubicacion_titulo, $periodo_actual_id) {

        $estatus_id = 4; // Estatus completo
        $sql = "UPDATE titulo.control_seguimiento_titulo
	SET estatus_id = :estatus_id
	WHERE estatus_ubicacion_titulo = :estatus_ubicacion_titulo
	AND periodo_id = :periodo_actual_id";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estatus_ubicacion_titulo", $estatus_ubicacion_titulo, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $consulta->bindParam(":estatus_id", $estatus_id, PDO::PARAM_INT);
        $resultado = $consulta->execute();

        return $resultado;
    }

    public function verificarControlSeguimiento($estatus_ubicacion_titulo, $periodo_actual_id) {

        $sql = "SELECT  estatus_id as estatus"
                . " FROM titulo.control_seguimiento_titulo"
                . " WHERE estatus_ubicacion_titulo = :estatus_ubicacion_titulo"
                . " AND periodo_id = :periodo_actual_id"
                . " ORDER BY estatus_id ASC";
        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":estatus_ubicacion_titulo", $estatus_ubicacion_titulo, PDO::PARAM_INT);
        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $resultado = $consulta->queryScalar();
//  var_dump($resultado);
        return $resultado;
    }

    public function borrarControlSeguimientoError($estatus_ubicacion_titulo, $periodo_actual_id) {

        $sql = "DELETE FROM titulo.control_seguimiento_titulo
                        WHERE estatus_ubicacion_titulo = $estatus_ubicacion_titulo
                        AND periodo_id = $periodo_actual_id
                        AND estatus_id = 2";

        $borrar = Yii::app()->db->createCommand($sql);
        $resultado = $borrar->execute();
    }

    public function asignarZonaEducativa($periodo_actual_id, $zona_educativa_id, $cedula, $nombre, $apellido, $modulo, $ip, $username, $usuario_id) {

        $sql = "SELECT titulo.asignar_zona_educativa($periodo_actual_id, $zona_educativa_id, $cedula, '$nombre', '$apellido', '$modulo', '$ip', '$username', $usuario_id)";

        $asignarZonaEducativa = Yii::app()->db->createCommand($sql);
//   echo $sql;

        $resultado = $asignarZonaEducativa->execute();
    }

    /*      FIN      */


    /*      Validacion de entrega de lotes de seriales a los planteles     */

    function datosPorPlantel($periodo_actual_id, $zona_educativa_id, $mostrar) {
        if ($mostrar == 0 && $zona_educativa_id != false) {
            $sql = "SELECT DISTINCT p.nombre AS plantel,
                                           COUNT(t.estudiante_id) AS cant_seriales_asignado_por_plantel,
                                           p.id AS id_plantel
                                FROM titulo.titulo t
                                INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                INNER JOIN titulo.papel_moneda pm ON(pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
                                INNER JOIN gplantel.zona_educativa ze ON(ze.id = pm.zona_educativa_id)
                                WHERE t.periodo_id = :periodo_actual_id
                                AND t.estatus_actual_id IN (2)
                                AND t.estatus_solicitud_id IN  (2)
                                AND t.asignado_zona_edu=1
                                AND t.asignado_plantel IS NULL
                                AND ze.id=:zona_educativa_id
                                GROUP BY p.id
                                ORDER BY p.nombre";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $consulta->bindParam(":zona_educativa_id", $zona_educativa_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();

            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        } elseif ($mostrar == 1 && $zona_educativa_id != false) {
            $sql = "SELECT DISTINCT p.nombre AS plantel,
                                           COUNT(t.estudiante_id) AS cant_seriales_asignado_por_plantel,
                                           p.id AS id_plantel
                                FROM titulo.titulo t
                                INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                INNER JOIN titulo.papel_moneda pm ON(pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
                                INNER JOIN gplantel.zona_educativa ze ON(ze.id = pm.zona_educativa_id)
                                WHERE t.periodo_id = :periodo_actual_id
                                AND t.estatus_actual_id IN (2)
                                AND t.estatus_solicitud_id IN  (2)
                                AND t.asignado_plantel=1
                                AND ze.id=:zona_educativa_id
                                GROUP BY p.id
                                ORDER BY p.nombre";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $consulta->bindParam(":zona_educativa_id", $zona_educativa_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();

            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        }
    }

    function obtenerIdZonaEdu($usuario_id) {

        if (is_numeric($usuario_id) && $usuario_id != null && $usuario_id != '') {
// $grupo_id = Yii::app()->user->group;
//      if ($grupo_id == 25) { // Ojo revisar preguntar si es un jefe zona
            $sql = "SELECT DISTINCT ze.id AS zona_educativa_id
                                FROM gplantel.autoridad_zona_educativa az
                                INNER JOIN gplantel.zona_educativa ze ON(ze.id = az.zona_educativa_id)
	        INNER JOIN seguridad.usergroups_user u ON(u.id = az.usuario_id)
                                WHERE az.usuario_id = :usuario_id";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
            $resultado = $consulta->queryScalar();
            return $resultado;

//      }
        }
    }

    public function asignarPlantel($periodo_actual_id, $plantel_id, $cedula, $nombre, $apellido, $modulo, $ip, $username, $usuario_id) {

        $sql = "SELECT titulo.asignar_plantel($periodo_actual_id, $plantel_id, $cedula, '$nombre', '$apellido', '$modulo', '$ip', '$username', $usuario_id)";

        $asignarPlantel = Yii::app()->db->createCommand($sql);
//   echo $sql;

        $resultado = $asignarPlantel->execute();
    }

    /*      FIN      */





    /*      Validacion de entrega de titulo a estudiantes     */

//    function obtenerDatosAutoridad($id_estudiante, $periodo_actual_id) {
//
//        $sql = "SELECT DISTINCT f.autoridad_titulo_id, UPPER((COALESCE(s.primer_nombre,'') || ' ' || COALESCE(s.segundo_nombre,'') || ' ' || COALESCE(s.primer_apellido,'') || ' ' || COALESCE(s.segundo_apellido,''))) AS autoridad
//                                            FROM titulo.titulo t
//                                            INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
//                                            INNER JOIN titulo.firma_autoridad_titulo f ON(f.titulo_id = t.id)
//                                            INNER JOIN auditoria.saime s ON(s.cedula = f.documento_identidad::int AND s.origen = f.tdocumento_identidad)
//                                            WHERE t.periodo_id = :periodo_actual_id
//                                            AND e.id = :id_estudiante
//                                            AND t.asignado_zona_edu=1
//                                            AND t.asignado_plantel=1
//                                            AND t.asignado_estudiante=1
//                                            GROUP BY autoridad, f.autoridad_titulo_id
//                                            ORDER BY f.autoridad_titulo_id ASC";
//        $consulta = Yii::app()->db->createCommand($sql);
//        $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
//        $consulta->bindParam(":id_estudiante", $id_estudiante, PDO::PARAM_INT);
//
//        $resultado = $consulta->queryAll();
//
//        if ($resultado == array()) {
//            return false;
//        } else {
//            return $resultado;
//        }
//    }

    function ObtenerDatosDirector($plantel_id, $periodo_actual_id) {
        $director = 3;
        $sql = "SELECT DISTINCT u.nombre as nombre_director, u.apellido as apellido_director, (COALESCE(u.origen,'') || '-' || COALESCE(u.cedula,null)) AS documento_identidad_director
	From gplantel.autoridad_plantel ap
	INNER JOIN seguridad.usergroups_user u ON(ap.usuario_id = u.id)
	WHERE ap.plantel_id=:plantel_id
	AND ap.estatus='A'
                        AND ap.cargo_id=:director
                         --AND ap.periodo_id=:periodo_actual_id
                        ";
        $consulta = Yii::app()->db->createCommand($sql);
        //$consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":director", $director, PDO::PARAM_INT);
//        echo $sql;
//        die();
        $resultadoDirector = $consulta->queryAll();

//        var_dump($resultadoDirector);
//        die();

        return $resultadoDirector;
    }

    function ObtenerDatosJefeControlEst($plantel_id, $periodo_actual_id) {
        $grupo_id = 45; //DRCEE_ZONA
        $sql = "SELECT DISTINCT u.nombre as nombre_jefe, u.apellido as apellido_jefe, (COALESCE(u.origen,'') || '-' || COALESCE(u.cedula,null)) AS documento_identidad_jefe
	From gplantel.plantel p
	INNER JOIN seguridad.usergroups_user u ON(u.group_id = :grupo_id)
	INNER JOIN estado e ON(e.id = p.estado_id AND e.id = u.estado_id)
	WHERE p.id=:plantel_id
	AND p.estatus='A'
                        LIMIT 1";
        $consulta = Yii::app()->db->createCommand($sql);
        //$consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":grupo_id", $grupo_id, PDO::PARAM_INT);
        //     echo $sql;
//            die();
        $resultadoJefeControlEst = $consulta->queryAll();

        //    var_dump($resultadoJefeControlEst);
//        die();

        return $resultadoJefeControlEst;
    }

    function ObtenerDatosFuncionario($plantel_id, $periodo_actual_id) {
        $cargo_id = 20;
        $sql = "SELECT DISTINCT u.nombre as nombre_funcionario, u.apellido as apellido_funcionario, (COALESCE(u.origen,'') || '-' || COALESCE(u.cedula,null)) AS documento_identidad_funcionario
	From gplantel.autoridad_plantel ap
	INNER JOIN seguridad.usergroups_user u ON(ap.usuario_id = u.id)
	WHERE ap.plantel_id=:plantel_id
                        AND ap.cargo_id=:cargo_id
	AND ap.estatus='A'
	 --AND ap.periodo_id=:periodo_actual_id
                        LIMIT 1";
        $consulta = Yii::app()->db->createCommand($sql);
        //$consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $consulta->bindParam(":cargo_id", $cargo_id, PDO::PARAM_INT);
        //    echo $sql;
//            die();
        $resultadoFuncionario = $consulta->queryAll();

//        var_dump($resultadoFuncionario);
//        die();
        return $resultadoFuncionario;
    }

    function datosPorEstudiante($periodo_actual_id, $plantel_id, $mostrar) {

        if ($mostrar == 0 && $plantel_id != false) {

            $sql = "SELECT DISTINCT replace(e.tdocumento_identidad, '0', '') AS tdocumento_identidad, e.documento_identidad,
                                            e.nombres, e.apellidos, pm.serial, pm.id AS serial_id, e.id AS id_estudiante
                                            FROM titulo.titulo t
                                            INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                            INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
                                            INNER JOIN titulo.papel_moneda pm ON(pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
                                            WHERE t.periodo_id = :periodo_actual_id
                                            AND t.estatus_actual_id IN (2)
                                            AND t.estatus_solicitud_id IN  (2)
                                            AND t.asignado_zona_edu=1
                                            AND t.asignado_plantel=1
                                            AND t.asignado_estudiante IS NULL
                                            AND t.plantel_id=:plantel_id
                                            GROUP BY t.estudiante_id, e.id, pm.serial, pm.id
                                            ORDER BY e.documento_identidad ASC, pm.serial ASC, e.nombres ASC";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
//            echo $sql;
//            die();
            $resultado = $consulta->queryAll();

//            var_dump($resultado);
//            die();
            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
//   }
        } elseif ($mostrar == 1 && $plantel_id != false) {

            $sql = "SELECT DISTINCT e.tdocumento_identidad, e.documento_identidad,
                                            e.nombres, e.apellidos, pm.serial, pm.id AS serial_id, e.id AS id_estudiante,
                                            t.anio_egreso, te.nombre as tipo_evaluacion, t.fecha_otorgamiento,
                                            e.fecha_nacimiento, e.sexo, pm.prefijo,
                                            replace(t.asignado_estudiante::Text,'1','Entregado al Estudiante') AS asignado_estudiante,
                                            plan.cod_plan AS codigo_plan, plan.nombre AS nombre_plan, m.nombre AS mencion,
                                           g.nombre AS grado, s.nombre AS seccion,
                                            (SELECT et.nombre
		FROM titulo.cambio_papel_moneda cpm
		INNER JOIN titulo.estatus_titulo et ON (et.id = cpm.estatus_titulo_id)
		WHERE cpm.plantel_id = :plantel_id
		AND cpm.estudiante_id = e.id
                                                LIMIT 1
                                            ) AS estatus_titulo
                                            FROM titulo.titulo t
                                            INNER JOIN gplantel.plan plan ON (plan.id = t.plan_id)
                                            INNER JOIN gplantel.mencion m ON (m.id = plan.mencion_id)
                                            INNER JOIN gplantel.grado g ON (g.id = t.grado_id)
                                            INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
                                            INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                            INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
                                            INNER JOIN titulo.papel_moneda pm ON(pm.id = t.papel_moneda_id AND pm.plantel_asignado_id = t.plantel_id)
                                            INNER JOIN matricula.tipo_evaluacion te ON (te.id = t.tipo_evaluacion_id)
                                            INNER JOIN titulo.estatus_titulo et ON (et.id = pm.estatus_actual_id)
                                            WHERE t.periodo_id = :periodo_actual_id
                                            AND t.estatus_actual_id IN (2)
                                            AND t.estatus_solicitud_id IN  (2)
                                            AND t.asignado_zona_edu=1
                                            AND t.asignado_plantel=1
                                            AND t.asignado_estudiante=1
                                            AND t.plantel_id=:plantel_id
                                            GROUP BY t.estudiante_id, e.id, pm.serial, pm.id, t.anio_egreso, tipo_evaluacion, t.fecha_otorgamiento, estatus_titulo,
                                            asignado_estudiante, codigo_plan, nombre_plan, mencion, grado, seccion
                                            ORDER BY seccion, e.documento_identidad ASC, pm.serial ASC, e.nombres ASC";
//            $sql = "SELECT DISTINCT e.tdocumento_identidad, e.documento_identidad,
//                                            e.nombres, e.apellidos, pm.serial, pm.id AS serial_id, e.id AS id_estudiante,
//                                            UPPER((COALESCE(s.primer_nombre,'') || ' ' || COALESCE(s.segundo_nombre,''))) AS nombreAutoridad, UPPER((COALESCE(s.primer_apellido,'') || ' ' || COALESCE(s.segundo_apellido,''))) AS apellidoAutoridad
//                                            FROM titulo.titulo t
//                                            INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
//                                            INNER JOIN matricula.estudiante e ON (e.id = t.estudiante_id)
//                                            INNER JOIN titulo.papel_moneda pm ON(pm.id = t.papel_moneda_id)
//                                            INNER JOIN titulo.firma_autoridad_titulo f ON(f.titulo_id = t.id)
//                                            INNER JOIN auditoria.saime s ON(s.cedula = f.documento_identidad::int AND s.origen = f.tdocumento_identidad)
//                                            WHERE t.periodo_id = :periodo_actual_id
//                                            AND t.estatus_actual_id IN (2)
//                                            AND t.estatus_solicitud_id IN  (2)
//                                            AND t.asignado_zona_edu=1
//                                            AND t.asignado_plantel=1
//                                            AND t.asignado_estudiante=1
//                                            AND t.plantel_id=:plantel_id
//                                            GROUP BY t.estudiante_id, e.id, pm.serial, pm.id, nombreAutoridad, apellidoAutoridad
//                                            ORDER BY e.documento_identidad ASC, pm.serial ASC, e.nombres ASC";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":periodo_actual_id", $periodo_actual_id, PDO::PARAM_INT);
            $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();

//            var_dump($resultado);
//            die();
            if ($resultado == array()) {
                return false;
            } else {
                return $resultado;
            }
        }
    }

//    function obtenerIdPlantel($usuario_id) {
//
//        if (is_numeric($usuario_id) && $usuario_id != null && $usuario_id != '') {
//// $grupo_id = Yii::app()->user->group;
////      if ($grupo_id == 29) { // Director
//            $sql = "SELECT DISTINCT p.id, p.nombre
//                                FROM gplantel.autoridad_plantel ap
//                                INNER JOIN gplantel.plantel p ON(p.id = ap.plantel_id)
//                                INNER JOIN titulo.titulo t ON(t.plantel_id = p.id)
//	        INNER JOIN seguridad.usergroups_user u ON(u.id = ap.usuario_id)
//                                WHERE ap.usuario_id = :usuario_id
//                                AND ap.estatus='A' ";
//            $consulta = Yii::app()->db->createCommand($sql);
//            $consulta->bindParam(":usuario_id", $usuario_id, PDO::PARAM_INT);
//            $resultado = $consulta->queryAll();
//
//            if ($resultado == array())
//                return false;
//            else
//                return $resultado;
//
////      }
//        }
//    }

    public function obtenerZonaEduId($plantel_id) {
//        var_dump($plantel_id);
//        die();
        if ($plantel_id != false) {

            $sql = "SELECT ze.id
                                FROM gplantel.plantel p
                                INNER JOIN gplantel.zona_educativa ze ON(ze.id = p.zona_educativa_id)
                                WHERE p.id=:plantel_id
                                AND p.estatus='A'
                                AND ze.estatus='A' ";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
            $resultado = $consulta->queryScalar();

            if ($resultado == array())
                return false;
            else
                return $resultado;
        }
    }

    public function SerialesDisponibles($zona_educativa) {

        if ($zona_educativa != false) {
            // foreach ($zona_educativa as $value) {

            $sql = "SELECT id , serial AS nombre
                                FROM titulo.papel_moneda
                                WHERE zona_educativa_id = :zona_educativa
                                AND plantel_asignado_id IS NULL
                                AND estatus_actual_id = 1
                                AND estatus='A'
                                ORDER BY serial ASC";
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":zona_educativa", $zona_educativa, PDO::PARAM_INT);
//                echo $sql;
//                die();
            $resultado = $consulta->queryAll();
//                var_dump($resultado);
//                die();
            //       }
            if ($resultado != array())
                return $resultado;
            else
                return false;
        }
    }

    public function asignarEstudiante($estatusTitulo_array_pg_array, $fechaOtorgamiento, $anioEgreso_array_pg_array, $tipoEvaluacion_array_pg_array, $tdocumento_director_array, $documento_director_array_, $tdocumento_jefe_array_, $documento_jefe_array_, $tdocumento_funcionario_array_, $documento_funcionario_array_, $plantel_id, $estudiantes_array, $estudiante_pg_array, $serialAnterior, $serialNuevo, $username, $nombre, $apellido, $cedula, $usuario_id, $grupo_id, $ip, $modulo) {
//        var_dump($serialAnterior);
//        die();
        $sql = "SELECT titulo.asignar_estudiante($estatusTitulo_array_pg_array, '$fechaOtorgamiento', $anioEgreso_array_pg_array, $tipoEvaluacion_array_pg_array, '$tdocumento_director_array', '$documento_director_array_', '$tdocumento_jefe_array_', '$documento_jefe_array_', '$tdocumento_funcionario_array_', '$documento_funcionario_array_', $plantel_id, $estudiantes_array,  $estudiante_pg_array, $serialAnterior, $serialNuevo, '$username', '$nombre', '$apellido', $cedula, $usuario_id, $grupo_id, '$ip', '$modulo')";

        $asignarEstudiante = Yii::app()->db->createCommand($sql);
//        echo $sql;
//        die();
        $resultado = $asignarEstudiante->execute();
    }

    /*      FIN      */

    public function verificarProcesosCorriendoSeguimiento($estatus_ubicacion_titulo) {

        $sql = "SELECT COUNT(id) FROM  titulo.control_seguimiento_titulo
			WHERE estatus_id = 2
                                                                        AND estatus_ubicacion_titulo=$estatus_ubicacion_titulo
			LIMIT 1";
        $guardar = Yii::app()->db->createCommand($sql);
        $resultado = $guardar->queryScalar();

        return $resultado;
    }

    /*      FIN      */



    /* ------  FIN ------ */

    /*      Verificar si contiene plantel disponible para otorgar titulo a los estudiante      */

    public function verificarMostrarIcono($usuario_id, $grupo_id, $variable = '', $plantel_id = null) {
        // LA LETRA D=DIRECTOR Y LA JD=JEFE DRCEE
        if ($variable != '' && $variable == 'D') {
            if (is_numeric($usuario_id) && $usuario_id != null && $usuario_id != '') {
                $sql = " SELECT DISTINCT p.id
                                 FROM gplantel.autoridad_plantel ap
                                 INNER JOIN gplantel.plantel p ON (p.id = ap.plantel_id)
 	         INNER JOIN seguridad.usergroups_user u ON (u.id = ap.usuario_id)
 	         INNER JOIN titulo.titulo t ON (t.plantel_id = p.id)
                                 WHERE  ap.usuario_id = $usuario_id
                                 AND u.group_id=$grupo_id
                                 AND ap.estatus='A'
                                 AND t.estatus_actual_id IN (2)
                                           AND t.estatus_solicitud_id IN  (2)
                                            AND t.asignado_zona_edu=1
                                          AND t.asignado_plantel=1";
                $verificar = Yii::app()->db->createCommand($sql);
//                echo $sql;
//                die();
                $resultado = $verificar->queryScalar();
                return $resultado;
            }
        } else {
            if ($variable != '' && $variable == 'JD' && $plantel_id != null) {
                $sql = "SELECT DISTINCT p.id
                                    FROM titulo.titulo t
                                    INNER JOIN gplantel.plantel p ON (p.id = t.plantel_id)
                                    WHERE t.plantel_id = :plantel_id
                                    AND t.estatus='A'
                                    AND t.estatus_actual_id IN (2)
                                    AND t.estatus_solicitud_id IN (2)
                                    AND t.asignado_zona_edu=1
                                    AND t.asignado_plantel=1
                                    AND t.asignado_estudiante=1";
                $verificar = Yii::app()->db->createCommand($sql);
//                echo $sql;
//                die();
                $verificar->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
                $resultado = $verificar->queryScalar();
                return $resultado;
            }
        }
    }

    /*      Fin      */


//        $sql = "SELECT DISTINCT u.nombre as nombre_jefe, u.apellido as apellido_jefe, (COALESCE(u.origen,'') || '-' || COALESCE(u.cedula,null)) AS documento_identidad_jefe
//	From gplantel.plantel p
//	INNER JOIN seguridad.usergroups_user u ON(u.group_id = $grupo_id)
//	INNER JOIN estado e ON(e.id = p.estado_id AND e.id = u.estado_id)
//	WHERE p.id=$plantel_id
//	AND p.estatus='A'

    /* ------  JEAN ------ */

    public function buscarEstTitulo($tipoBusqueda, $EstuadianteCedula) {
        $grupo_id = 45; //DRCEE_ZONA

        if (in_array($tipoBusqueda, array('V', 'E')) AND is_numeric($EstuadianteCedula)) {

            $sql = "SELECT distinct e.id as estudiante_id,e.documento_identidad, e.nombres, e.apellidos,t.anio_egreso,t.fecha_otorgamiento,
       e.fecha_nacimiento, e.correo,e.tdocumento_identidad,e.sexo, p.cod_plantel, p.cod_estadistico,
       p.nombre as nombreplantel, pm.serial, pl.nombre as nombre_plan, pl.cod_plan as cod_plan,
m.nombre as nombre_mencion,t.anio_egreso,t.fecha_otorgamiento,es.nombre as estado_nac,
mu.nombre as municipio_nac,e.fecha_nacimiento,ese.nombre as estado_plantel,t.fecha_otorgamiento,
t.anio_egreso,pm.prefijo,t.id as titulo_id, ze.nombre as nombre_zona_educativa,ni.nombre as nombre_nivel,
me.nombre as nombre_mencion,u.origen as origen_drcee_zona, u.cedula as cedula_drcee_zona ,
u.nombre ||' ' || u.apellido as nombre_apellido_drcee_zona,us.origen as origen_dir_plantel,
us.cedula as cedula_dir_plantel ,us.nombre ||' ' || us.apellido as nombre_dir_plantel,
use.origen as origen_funcio_desig,use.cedula as cedula_funcio_desig ,
use.nombre ||' ' || use.apellido as nombre_funcio_desig ,t.periodo_id as periodo_escolar


FROM matricula.estudiante e
INNER JOIN titulo.titulo t ON (t.estudiante_id = e.id)
INNER JOIN gplantel.plantel p ON (p.id = e.plantel_actual_id)
LEFT JOIN estado es on ( es.id=e.estado_nac_id)
LEFT JOIN estado ese on ( p.estado_id=ese.id)
INNER JOIN titulo.papel_moneda pm ON (pm.id = t.papel_moneda_id)
INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
LEFT JOIN gplantel.mencion m on (pl.mencion_id = m.id)
LEFT JOIN municipio mu on (mu.id=e.municipio_nac_id)
INNER JOIN gplantel.zona_educativa ze on ze.id =pm.zona_educativa_id
INNER JOIN gplantel.nivel_plan np on t.plan_id =np.plan_id
INNER JOIN gplantel.nivel ni on ni.id =np.nivel_id
LEFT JOIN gplantel.mencion me on (me.id=pl.mencion_id)
INNER JOIN seguridad.usergroups_user u ON (u.group_id = 45)
INNER JOIN estado est ON (est.id = p.estado_id AND est.id = u.estado_id)
INNER JOIN gplantel.autoridad_plantel au on (t.plantel_id = au.plantel_id and au.cargo_id= 3)
INNER JOIN seguridad.usergroups_user us on (au.usuario_id = us.id)
LEFT JOIN gplantel.autoridad_plantel aut on (t.plantel_id = aut.plantel_id and aut.cargo_id= 20)
LEFT JOIN seguridad.usergroups_user use on (aut.usuario_id = use.id)

 where e.documento_identidad =:EstuadianteCedula  and  e.tdocumento_identidad=:tipoBusqueda and t.estatus_actual_id=2";
            $consulta = Yii::app()->db->createCommand($sql);
//            echo "$sql";
//            die();
            $consulta->bindParam(":tipoBusqueda", $tipoBusqueda, PDO::PARAM_STR);
            $consulta->bindParam(":EstuadianteCedula", $EstuadianteCedula, PDO::PARAM_STR);
            $resultado = $consulta->queryAll();

            if ($resultado) {

                return $resultado;
            } else {
// aqui entra cuando el titulo no se encuentra en el esquema titulo( es decir entrega de titulo antes del 2014)
                $sql = "SELECT distinct e.id as estudiante_id,t.ntitulo as serial,t.cestadistico as cod_estadistico,t.cplan,pl.nombre,t.calumno as documento_identidad,e.nombres, e.apellidos,p.nombre as nombreplantel, m.nombre as nombre_mencion,pl.nombre as nombre_plan,e.tdocumento_identidad,e.sexo,e.correo,e.fecha_nacimiento,t.femision as fecha_otorgamiento,t.anoagreso as ano_egreso "
                        . "FROM legacy.titulos t left JOIN gplantel.plantel p ON (p.cod_estadistico = t.cestadistico) left JOIN gplantel.plan pl ON (pl.cod_plan = t.cplan) LEFT JOIN gplantel.mencion m on (pl.mencion_id = m.id) inner join matricula.estudiante e on (e.documento_identidad=t.calumno::CHARACTER VARYING)"
                        . " where calumno=:EstuadianteCedula and e.tdocumento_identidad =:tipoBusqueda;";
                $consulta = Yii::app()->db->createCommand($sql);
                $consulta->bindParam(":tipoBusqueda", $tipoBusqueda, PDO::PARAM_STR);
                $consulta->bindParam(":EstuadianteCedula", $EstuadianteCedula, PDO::PARAM_STR);
                $resultado = $consulta->queryAll();
//                echo $sql;
//////var_dump($resultado);
//                die();
                return $resultado;
            }
        } else {

            if ($tdocumento_identidad == 'P') {
                $sql = "SELECT pasaporte, (primer_nombre || ' ' || segundo_nombre) AS nombre, (primer_apellido || ' ' || segundo_apellido) AS apellido, fecha_nacimiento  "
                        . " FROM auditoria.saime_pasaporte s"
                        . " WHERE "
                        . " s.pasaporte= :pasaporte";
                $buqueda = Yii::app()->db->createCommand($sql);
                $buqueda->bindParam(":pasaporte", $documento_identidad, PDO::PARAM_INT);
                $buqueda->bindParam(":pasaporte", $documento_identidad, PDO::PARAM_INT);
                $resultadoCedula = $buqueda->queryRow();
            }
            /* $sql = "select u_u.id, s.origen ,s.cedula, u_u.nombre, u_u.apellido "
              . " from seguridad.usergroups_user u_u "
              . " inner join auditoria.saime s on (s.origen = u_u.origen AND s.cedula = u_u.cedula)"
              . " where "
              . " u_u.cedula= :cedula AND "
              . " u_u.origen= :origen ";
             *
             */

            if ($resultadoCedula !== array()) {
                return $resultadoCedula;
            } else {
                return false;
            }
        }
    }

    /* ------  FIN ------ */

    public function mostrarSolicitudtitulo($plantel_id) {
        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_actual_id = $periodo_escolar_actual_id['id'];

        $sql = "SELECT e.nombres,e.apellidos,e.documento_identidad,m.nombre as nombre_mencion,s.nombre as nombre_seccion,
  g.nombre as nombre_grado
  FROM titulo.titulo t
  inner join matricula.estudiante e on t.estudiante_id=e.id
  INNER JOIN gplantel.plan pl ON (pl.id = t.plan_id)
  inner JOIN gplantel.mencion m ON (pl.mencion_id = m.id)
  INNER JOIN gplantel.seccion s ON (s.id = t.seccion_id)
  INNER JOIN gplantel.grado g ON (g.id = t.grado_id)
  where t.estatus_actual_id=11
  and t.estatus_solicitud_id=1
  and t.plantel_id=:plantel_id
  and t.periodo_id=$periodo_actual_id;";

        $consulta = Yii::app()->db->createCommand($sql);
        $consulta->bindParam(":plantel_id", $plantel_id, PDO::PARAM_INT);
        $resultado = $consulta->queryAll();
        return $resultado;
    }

    public function buscarNotaEstudiante($EstuadianteEgresado, $coincidir) {
        if ($coincidir == 'P') {
            $sql = "select mo.materia, meo.mencion, mmno.nota,id_materia_mencion
from legacy.materia_mencion_nota_opsu mmno
inner join legacy.materia_mencion_opsu mmp on mmno.codigo_materia_mencion =  mmp.id_materia_mencion
inner join legacy.materias_opsu mo on mo.id_materia =  mmp.id_materia
inner join legacy.menciones_opsu meo on meo.id_mencion =  mmp.id_mencion
where mmno.cedula_estudiante=:EstuadianteEgresado and meo.mencion not like '%1%' and meo.mencion not like '%2%' and meo.mencion not like '%3%'
order by meo.mencion,mo.materia ";
//and meo.mencion like '%" . $coincidir . "%'" . "
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":EstuadianteEgresado", $EstuadianteEgresado, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();
//        echo $sql;
//           die();
            return $resultado;
        } else {
            $sql = "select mo.materia, meo.mencion, mmno.nota,id_materia_mencion
from legacy.materia_mencion_nota_opsu mmno
inner join legacy.materia_mencion_opsu mmp on mmno.codigo_materia_mencion =  mmp.id_materia_mencion
inner join legacy.materias_opsu mo on mo.id_materia =  mmp.id_materia
inner join legacy.menciones_opsu meo on meo.id_mencion =  mmp.id_mencion
where mmno.cedula_estudiante=:EstuadianteEgresado and meo.mencion like '%" . $coincidir . "%'" . "
order by meo.mencion,mo.materia ";
//and meo.mencion like '%" . $coincidir . "%'" . "
            $consulta = Yii::app()->db->createCommand($sql);
            $consulta->bindParam(":EstuadianteEgresado", $EstuadianteEgresado, PDO::PARAM_INT);
            $resultado = $consulta->queryAll();
//        echo $sql;
//           die();
            return $resultado;
        }
    }

}
