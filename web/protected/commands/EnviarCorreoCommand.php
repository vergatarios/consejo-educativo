<?php

class EnviarCorreoCommand extends CConsoleCommand {

    public function actionIndex($estado_id) {
        date_default_timezone_set('America/Caracas');
        if (is_numeric($estado_id)) {
            $correo = new Correo();
            $countPersonas = $correo->countPersonasEstado($estado_id);
            if ($countPersonas > 0) {
                echo "SE INICIA EL PROCESO DE ENVÍO MASIVO DE CORREOS ELECTRÓNICOS PARA EL ESTADO " . $countPersonas . PHP_EOL;
                $cadena = "";
                $exito = array();
                $fallido = array();
                $cant_destinatarios = 0;
                $contadorEnvio = 0;
                $datosPersonas = $correo->getDatosPersonasEstado($estado_id);
                //$zona_educativa = (isset($datosPersonas[0]['zona'])) ? $datosPersonas[0]['zona'] : null;
                //echo "SE INICIA EL PROCESO DE ENVÍO MASIVO DE CORREOS ELECTRÓNICOS PARA EL ESTADO " . $zona_educativa . ', TOTAL DE CORREOS : ' . $countPersonas . PHP_EOL;
                $mensaje = '<h4>Calendario Escolar</h4>
<ul>
<li>Calendario Escolar 2014-2015 Jóvenes  (<a href="http://www.me.gob.ve/media/contenidos/2014/d_28992_800.pdf">http://www.me.gob.ve/media/contenidos/2014/d_28992_800.pdf</a>)</li>
<li>Calendario Escolar 2014-2015 Niños (<a href="http://www.me.gob.ve/media/contenidos/2014/d_28992_799.pdf">http://www.me.gob.ve/media/contenidos/2014/d_28992_799.pdf</a>)</li>
</ul>

Ministerio del Poder Popular para la Educación


';
                foreach ($datosPersonas as $fila => $data) {

                    $id = (isset($data['id'])) ? $data['id'] : null;
                    $destinatario_nombre = (isset($data['nombre'])) ? $data['nombre'] : null;
                    $destinatario_apellido = (isset($data['apellido'])) ? $data['apellido'] : null;
                    $destinatario_correo = (isset($data['correo'])) ? $data['correo'] : null;
                   /* $remitente_apellido = (isset($data['apellido_drcee_zona'])) ? $data['apellido_drcee_zona'] : null;
                    $remitente_correo = (isset($data['correo_drcee_zona'])) ? $data['correo_drcee_zona'] : null;
                    $destinatario_correo = (isset($data['correo_director_actualizado'])) ? $data['correo_director_actualizado'] : null;
                    $zona_educativa = (isset($data['zona'])) ? ucfirst(strtolower($data['zona'])) : null;
                    $telefono_drcee_zona = (isset($data['telefono_drcee_zona'])) ? ucfirst(strtolower($data['telefono_drcee_zona'])) : null;
                    $usuario_director = (isset($data['usuario_director'])) ? $data['usuario_director'] : null;
                    $clave_activacion_director = (isset($data['clave_activacion_director'])) ? $data['clave_activacion_director'] : null;*/

                    $mensaje_completo = $mensaje;
                    /*$mensaje_completo = str_replace('{ZONA_EDUCATIVA_NOMBRE}', $remitente_nombre . ' ' . $remitente_apellido, $mensaje_completo);
                    $mensaje_completo = str_replace('{ZONA_EDUCATIVA}', $zona_educativa, $mensaje_completo);
                    $mensaje_completo = str_replace('{ZONA_EDUCATIVA_TELF}', $telefono_drcee_zona, $mensaje_completo);
                    $mensaje_completo = str_replace('{ZONA_EDUCATIVA_CORREO}', $remitente_correo, $mensaje_completo);
                    $mensaje_completo = str_replace('{DIRECTOR_NOMBRE}', $destinatario_nombre . ' ' . $destinatario_apellido, $mensaje_completo);
                    $mensaje_completo = str_replace('{DIRECTOR_USUARIO}', $usuario_director, $mensaje_completo);
                    $mensaje_completo = str_replace('{DIRECTOR_CODIGO}', $clave_activacion_director, $mensaje_completo);*/
                    $contadorEnvio++;
                    $cant_destinatarios = $fila;
                    $mailer = Yii::createComponent('application.extensions.mailer.EMailer');
                    $mailer->Host = 'mail.me.gob.ve:25';
                    $mailer->IsSMTP();
                    $mailer->From = 'soporte_gescolar@me.gob.ve';
                    $mailer->FromName = 'Sistema de Gestión Escolar';
                    $mailer->AddAddress($destinatario_correo, $destinatario_nombre . ' ' . $destinatario_apellido);
                    $mailer->AddBCC('gescolar.mppe@gmail.com', 'Sistema de Gestión Escolar');
                    $mailer->CharSet = 'UTF-8';
                    $mailer->Subject = 'Calendario escolar 2014 - 2015';
                    $mailer->IsHTML(true);
                    $mailer->MsgHTML($mensaje_completo);

                    if (!$mailer->Send())
                    //$fallido[] = array('email' => $destinatario_correo, 'error' => $mailer->ErrorInfo);
                        echo "ENVÍO FALLIDO A: " . $destinatario_correo . '  ERROR: ' . $mailer->ErrorInfo . PHP_EOL;
                    else {
                        if ($contadorEnvio == 100) {
                            $contadorEnvio = 0;
                            sleep(90);
                        }
                        $model = $correo->findByPk($id);
                        $model->estatus = 'E';
                        $model->save();
                        //$exito[] = array('email' => $destinatario_correo);
                        echo "ENVÍO EXITOSO A: " . $destinatario_correo . PHP_EOL;
                    }
                }
                echo "PROCESO CULMINADO \n";
            } else {
                echo "NO EXISTEN PERSONAS QUE CORRESPONDAN CON EL ESTADO_ID SUMINISTRADO Ó YA SE LES ENVÍO EL CORREO ELECTRÓNICO \n";
            }
        } else {
            echo "EL DATO SUMINISTRADO DEBE SER NUMERICO Y DEBE CORRESPONDER CON LOS ESTADOS QUE ESTAN EN LA BASE DE DATOS \n";
        }
    }

    public function getViewPath() {
        return Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'views';
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath() . $viewName . '.php';
    }

    /**
     * Modeified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }

}
