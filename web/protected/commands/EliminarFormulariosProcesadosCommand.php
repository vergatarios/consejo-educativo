<?php

class EliminarFormulariosProcesadosCommand extends CConsoleCommand {

    public $directorio_procesados='/var/www/gescolar/web/public/uploads/Procesados/';
    public $directorio_formularios='/var/www/gescolar/web/public/uploads/Formularios/';
    public $directorio_home='/home/jgonzalezp/FormulariosProcesados/';
    public function actionIndex() {
        date_default_timezone_set('America/Caracas');

        /*
         * BUSCANDO ARCHIVOS PROCESADOS
         */

        $model=new FormularioInscripcionPeriodo();
        $archivos = $model->buscarFormulariosProcesados();
        foreach($archivos as $index => $valor){

            echo 'PROCESANDO ARCHIVO '.$valor.PHP_EOL;
            if(file_exists($this->directorio_formularios.$valor)){
                copy ($this->directorio_formularios.$valor,$this->directorio_home.$valor);
                if(file_exists($this->directorio_home.$valor)){
                    try{
                        echo 'ELIMINANDO ARCHIVO '.$valor.PHP_EOL .' DEL DIRECTORIO '.$this->directorio_formularios.PHP_EOL;
                        unlink($this->directorio_formularios.$valor);

                        if(file_exists($this->directorio_procesados.$valor)) {
                            echo 'ELIMINANDO ARCHIVO ' . $valor . PHP_EOL . ' DEL DIRECTORIO ' . $this->directorio_procesados . PHP_EOL;
                            unlink($this->directorio_procesados . $valor);
                        }

                    }
                    catch(Exception $e){
                        echo 'OCURRIO UN ERROR ELIMINANDO EL ARCHIVO '.$valor.PHP_EOL;
                    }
                }
                else{
                    echo "NO SE PUDO COPIAR EL ARCHIVO ".$valor.' EN EL DIRECTORIO '.$this->directorio_home.PHP_EOL;
                }

            }
            else {
                echo "NO EXISTE EL ARCHIVO ".$valor.' EN LOS DIRECTORIOS '.$this->directorio_formularios.' Y '.$this->directorio_procesados.PHP_EOL;
            }
        }

    }

    public function getViewPath() {
        return Yii::app()->getBasePath() . DIRECTORY_SEPARATOR . 'views';
    }

    /**
     * Modified copy of getViewFile
     * @see CController::getViewFile
     * @param $viewName
     * @return string
     */
    public function getViewFile($viewName) {
        return $this->getViewPath() . $viewName . '.php';
    }

    /**
     * Modeified copy of renderPartial from CController
     * @see CController::renderPartial
     * @param $view
     * @param $data
     * @param $return
     * @return mixed
     * @throws CException
     */
    public function renderPartial($view, $data, $return) {
        if (($viewFile = $this->getViewFile($view)) !== false) {
            $output = $this->renderFile($viewFile, $data, true);
            if ($return)
                return $output;
            else
                echo $output;
        } else
            throw new CException(Yii::t('yii', '{class} cannot find the requested view "{view}".', array('{class}' => get_class($this), '{view}' => $view)));
    }

}
