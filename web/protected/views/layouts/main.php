<?php
if (empty($this->breadcrumbs)) {
    $this->breadcrumbs = array(
        'Noticias'
    );
}

if (empty($this->pageTitle)) {
    $this->pageTitle = 'Noticias';
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="utf-8" />
    <title><?php echo CHtml::encode($this->pageTitle); ?> | Sistema de Gestión Escolar</title>
    <meta name="description" content="<?php echo $this->pageTitle; ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <!-- basic styles -->
    <!-- blueprint CSS framework -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/screen.css" media="screen, projection" />
    <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/print.css" media="print" /> -->
    <!--[if lt IE 8]>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/ie.css" media="screen, projection" />
    <![endif]-->

    <!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/main.css" /> -->
    <!--<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/form.css" /> -->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/font-awesome.min.css" rel="stylesheet" />

    <!--[if IE 7]>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/font-awesome-ie7.min.css" />
    <![endif]-->

    <!-- page specific plugin styles -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/jquery-ui-1.10.3.full.min.css" />
    <!-- fonts -->

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/css5c0a.css?family=Open+Sans:400,300" />

    <!-- ace styles -->

    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/ace.min.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/ace-rtl.min.css" />
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/ace-skins.min.css" />
    <link rel="stylesheet" href="/public/js/fancybox-1.3.4/jquery.fancybox-1.3.4.css">

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/ace-ie.min.css" />
    <![endif]-->

    <!-- inline styles related to this page -->

    <!-- ace settings handler -->

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/ace-extra.min.js"></script>

    <!--[if !IE]> -->

    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/jquery-migrate-1.2.1.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/bootstrap.js"></script>

    <!-- <![endif]-->

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/html5shiv.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/respond.min.js"></script>
    <![endif]-->
    <link href="<?php echo Yii::app()->request->baseUrl; ?>/public/images/favicon.ico" rel="shortcut icon">

    <script type='text/javascript'>
        idleTime = 0;
        ruta = '<?php print(Yii::app()->request->baseUrl . "/logout"); ?>';
        desarrollador = '<?php print(UserGroups::DESARROLLADOR); ?>';
        group_id = '<?php echo Yii::app()->user->group; ?>';
        $(document).ready(function() {
            if (group_id != desarrollador && group_id != 1) {
                //Increment the idle time counter every minute.
                var idleInterval = setInterval('timerIncrement()', 60000); // 1 minute
                //Zero the idle timer on mouse movement.
                $(this).mousemove(function(e)
                {
                    idleTime = 0;
                });
                $(this).keypress(function(e)
                {
                    idleTime = 0;
                });
            }
        });
        function timerIncrement()
        {
            idleTime = idleTime + 1;
            if (idleTime > 39) // 40 minutes
            {
                title = "<div class='widget-header'><h4 class='smaller'><i class='icon-off'></i> Sesión Expirada </h4></div>";
                $('#alerta_sesion').removeClass('hide').dialog({
                    width: 500,
                    resizable: false,
                    draggable: false,
                    position: ['center', 150],
                    modal: true,
                    title: title,
                    title_html: true,
                    buttons: [
                        {
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Cerrar",
                            'class': 'btn btn-danger btn-xs',
                            click: function() {
                                $(this).dialog('close');
                                window.location = ruta;
                            }
                        }
                    ],
                    close: function() {
                        $(this).dialog('close');
                        window.location = ruta;
                    }
                });
            }

            if ($('#loading').css('display') == 'block') {
                idleTime = 0;
            }

        }

    </script>
    <?php
    if (Yii::app()->user->name == '' || Yii::app()->user->name == null) {
        echo "<script type='text/javascript'>
			window.location='" . Yii::app()->request->baseUrl . "/logout';
		</script>";
    }
    echo "<script type='text/javascript'>
			$(document).ready(function() {
				$(window).bind('beforeunload', function(){
					window.location='" . Yii::app()->request->baseUrl . "/logout';
				});
			});
	</script>";
    ?>
</head>

<body oncontextmenu='return false'>

<header class="main-header">
    <div id="ministerio-header">
        <img class="pull-left" id="img-gb" src="<?php echo Yii::app()->request->baseUrl; ?>/public/images/logo_ministerio.png" />
        <img class="pull-right" id="img-cv" src="<?php echo Yii::app()->request->baseUrl; ?>/public/images/logo_campanha.png" />
    </div>
    <div id="gescolar-header">
        <img class="pull-left" id="img-il" src="<?php echo Yii::app()->request->baseUrl; ?>/public/images/sintillo.png" height="46" />
        <img class="pull-right" id="img-pg" src="<?php echo Yii::app()->request->baseUrl; ?>/public/images/logo_sistema.png" />
    </div>
</header>

<div class="main-container" id="main-container">


    <noscript>
        <div class="errorDialogBox">
            <p>
                Su navegador no tiene soporte JavaScript! Debe activar el soporte a Javascript para poder hacer uso de la Aplicación.
            </p>
        </div>
    </noscript>

    <script type="text/javascript">
        try {
            ace.settings.check('main-container', 'fixed')
        } catch (e) {
        }
    </script>

    <div class="main-container-inner">
        <a class="menu-toggler" id="menu-toggler" href="#">
            <span class="menu-text"></span>
        </a>

        <div class="sidebar" id="sidebar">
            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'fixed')
                } catch (e) {
                }
            </script>

            <div class="sidebar-shortcuts" id="sidebar-shortcuts">
                <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">


                    <a href="/ayuda/ticket" class="btn btn-danger">
                        <i class="icon-envelope" title="Solicitud"> </i>
                        </span>
                    </a>

                    <a href="/" class="btn btn-danger">
                        <i class="icon-signal" class="btn btn-danger"></i>


                    </a>

                    <a href="/" class="btn btn-danger">
                        <i class="icon-pencil" class="btn btn-danger"></i>
                    </a>

                    <a href="/" class="btn btn-danger">
                        <i class="icon-group" class="btn btn-danger"></i>
                    </a>
                </div>

                <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
                    <span class="btn btn-success"></span>

                    <span class="btn btn-info"></span>

                    <span class="btn btn-warning"></span>

                    <span class="btn btn-danger"></span>
                </div>
            </div><!-- #sidebar-shortcuts -->

            <!-- .nav-list Menu Principal-->
            <?php
            $this->renderPartial('//layouts/menu')
            ?>
            <!-- /.nav-list Menu Principal-->

            <div class="sidebar-collapse" id="sidebar-collapse">
                <i class="icon-double-angle-left" data-icon1="icon-double-angle-left" data-icon2="icon-double-angle-right"></i>

            </div>

            <script type="text/javascript">
                try {
                    ace.settings.check('sidebar', 'collapsed')
                } catch (e) {
                }
            </script>
        </div>

        <div class="main-content">
            <div class="breadcrumbs" id="breadcrumbs">
                <script type="text/javascript">
                    try {
                        ace.settings.check('breadcrumbs', 'fixed')
                    } catch (e) {
                    }
                </script>

                <!-- .breadcrumb -->
                <?php if (isset($this->breadcrumbs)): ?>
                    <?php
                    $this->widget('zii.widgets.CBreadcrumbs', array(
                        'htmlOptions' => array('class' => 'breadcrumb row-fluid'),
                        'links' => $this->breadcrumbs,
                        'separator' => '',
                        'homeLink' => '<li><i class="icon-home home-icon"></i>' . CHtml::link('Inicio', Yii::app()->homeUrl) . '</li>',
                        'activeLinkTemplate' => '<li><a href="{url}">{label}</a></li>',
                        'inactiveLinkTemplate' => '<li><span>{label}</span></li>',
                    ));
                    ?><!-- breadcrumbs -->
                <?php endif ?>
                <!-- .breadcrumb -->

                <div class="navbar-header pull-right" role="navigation">
                    <ul class="user-link-profile">
                        <li>
                            <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                                        <span class="user-info" title="<?php echo Yii::app()->user->nombre . ' ' . Yii::app()->user->apellido; ?>">
                                            <i class="icon-user"></i>
                                            &nbsp;<?php
                                            if (!Yii::app()->user->isGuest) {
                                                echo ucwords(strtolower(Yii::app()->user->nombre . ' ' . Yii::app()->user->apellido));
                                            }
                                            ?>
                                        </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <ul class="user-menu pull-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">

                                <?php if (!Yii::app()->user->isGuest): ?>
                                    <li>
                                        <a href="/perfil">
                                            <i class="icon-user"></i>
                                            Mi Perfil
                                        </a>
                                    </li>
                                <?php endif; ?>

                                <li class="divider"></li>

                                <li>
                                    <a href="<?php echo $url = Yii::app()->baseUrl . "/logout"; ?>">
                                        <i class="icon-off"></i>
                                        Cerrar Sesi&oacute;n
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>

            </div>

            <div class="page-content">
                <div class="row-fluid" id="main-container" style="min-height: 500px;">
                    <!-- <div class="col-xs-12">-->
                    <?php echo $content; ?>
                    <!-- </div> -->
                    <!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.page-content -->
        </div><!-- /.main-content -->

    </div><!-- /.main-container-inner -->

    <a href="#" id="btn-scroll-up" class="btn-scroll-up btn btn-sm btn-inverse">
        <i class="icon-double-angle-up icon-only bigger-110"></i>
    </a>
</div><!-- /.main-container -->
<div id="alerta_sesion" class="hide">
    <div class="alertDialogBox">
        <p> Estimado usuario, se ha detectado un espacio de tiempo de inactividad en el sistema debe volver a iniciar sesión. Disculpe las molestias ocasionadas.</p>
    </div>
</div>


<footer id="footer">
    <div class="main-container-inner">
        <p class="text-muted credit center">
            <a href="http://www.me.gob.ve/">MPPE</a> |
            <a href="http://www.me.gob.ve/contenido.php?id_seccion=50&id_contenido=26185&modo=2">FEDE</a> |
            <a href="http://fundabit.me.gob.ve">FUNDABIT</a>
        </p>
        <p class="text-muted credit center">
            Dirección General de Tecnolog&iacute;a de la Informaci&oacute;n y la Comunicaci&oacute;n para el Desarrollo Educativo.
            <br/>
            <span title="Ministerio del Poder Popular para la Educación">MPPE</span> &copy; 2014
        </p>
    </div>
    <a id="link_lightbox" class="hide" href="/public/images/error/aviso.jpg">
    </a>
</footer>

<!-- basic scripts -->

<!--[if IE]>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<![endif]-->
<!--[if !IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='js/jquery-2.0.3.min.js'>" + "<" + "/script>");
</script>
<!-- <![endif]-->

<!--[if IE]>
<script type="text/javascript">
    window.jQuery || document.write("<script src='js/jquery-1.10.2.min.js'>"+"<"+"/script>");
</script>
<![endif] -->

<script type="text/javascript">
    if ("ontouchend" in document)
        document.write("<script src='js/jquery.mobile.custom.min.js'>" + "<" + "/script>");</script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/typeahead-bs2.min.js"></script>

<!-- page specific plugin scripts -->

<!-- ace scripts -->

<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/ace-elements.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/ace.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/main.js"></script>

<!-- page specific plugin scripts -->

<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/jquery-ui-1.10.3.full.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/jquery.ui.touch-punch.min.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/jquery.ui.dialog.titlehtml.js"></script>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/jquery.alphanumeric.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/jquery.numeric.js"></script>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/fancybox-1.3.4/jquery.fancybox-1.3.4.js"></script>

<!-- inline scripts related to this page -->

<script>
    (function(i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function() {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
            m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '<?php echo Yii::app()->request->baseUrl; ?>/public/js/analytics.js', 'ga');
    ga('create', 'UA-49585060-1', 'gob.ve');
    ga('send', 'pageview');</script>
<!--         Piwik
        <script type="text/javascript">
            var _paq = _paq || [];
            _paq.push(['trackPageView']);
            _paq.push(['enableLinkTracking']);
            (function() {
//                var u = (("https:" == document.location.protocol) ? "https" :
//                        "http") + "://172.16.40.22/piwik/";
                var u = "http://172.16.40.22/piwik/";
                _paq.push(['setTrackerUrl', u + 'piwik.php']);
                _paq.push(['setSiteId', 1]);
                var d = document, g = d.createElement('script'),
                        s = d.getElementsByTagName('script')[0];
                g.type = 'text/javascript';
                g.defer = true;
                g.async = true;
                g.src = u + 'piwik.js';
                s.parentNode.insertBefore(g, s);
            })();
        </script>
        <noscript><p><img src="http://172.16.40.22/piwik/piwik.php?idsite=1" style="border:0;" alt="" /></p></noscript>
         End Piwik Code -->


<script language="Javascript">
    document.oncontextmenu = function() {
        return false
    }
    $("a#link_lightbox").fancybox({
        'overlayShow'	: true,
        'transitionIn'	: 'elastic',
        'transitionOut'	: 'elastic',
        'overlayOpacity': 0.5,
        'titlePosition'	: 'over',
        'opacity'	: true,
        //title: "Puede <a href='/public/formatos/formato_seriales.ods'>Descargar</a> el Formato de Carga en Lotes de Seriales en este <a href='/public/formatos/formato_seriales.ods'>link</a>.",
    });
    $(document).ready(function() {

        var aviso =<?php echo (isset($this->aviso)) ? $this->aviso : "'" . "'"; ?>;
        if (aviso == '1') {




             //$("a#link_lightbox").click();
            /*var dialogInicio = $("#dialog_informacionInicio").removeClass('hide').dialog({
             modal: true,
             width: '450px',
             draggable: false,
             resizable: false,
             title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-info-circle'></i> Información</h4></div>",
             title_html: true,
             buttons: [
             {
             html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
             "class": "btn btn-xs",
             click: function() {
             dialogInicio.dialog("close");
             }
             },
             ]
             });

             $("#dialog_informacionInicio").show();*/
        }
    });
</script>

</body>
</html>