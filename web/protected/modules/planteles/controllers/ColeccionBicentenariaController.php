<?php
class ColeccionBicentenariaController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Coleccion Bicentenaria',
        'write' => 'Coleccion Bicentenaria ',
        'admin' => 'Coleccion Bicentenaria',
//        'label' => 'Solicitud de titulo'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'GuardarBicentenaria'),
                'pbac' => array('write', 'admin', 'read'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', '', ''),
                'pbac' => array('read'),
            ),
//            array('allow', // allow all users to perform 'index' and 'view' actions
//                'actions' => array('indexTitulo', 'MostrarSolicitanteAlTitulo'),
//                'pbac' => array('read','write','admin'),
//            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    const MODULO = "Servicio.ColeccionBicentenaria";

    public function actionIndex($id) {


        $model = new ServicioPlantelPeriodo;
        $plantel_id = base64_decode($id);
        $servicio = 69;
        $periodo_Escolar = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_Escolar_id = $periodo_Escolar['id'];

            $CondicionServicion = CondicionServicio::model()->findAll(array('order' => 'nombre ASC'));

//        var_dump($CondicionServicion);
//        echo "hola";
//        die();

        if (!is_numeric($plantel_id)) {
            throw new CHttpException(404, 'No se ha encontrado el plantel que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
        } else {
            $resultado = ServicioPlantelPeriodo::model()->buscarColeccionBicentenaria($servicio, $periodo_Escolar_id, $plantel_id);
            if ($resultado == 0) {


                $plantel_id = base64_encode($plantel_id);
            $this->render('index', array(
                'plantel_id' => $plantel_id,
                'model' => $model,
                'CondicionServicion' => $CondicionServicion,
            ));
        } else {
                $this->render('mensajeFinal', array(
                ));
            }
        }
    }

    public function actionGuardarBicentenaria() {
        if (Yii::app()->request->isAjaxRequest) {


            $model = new ServicioPlantelPeriodo;
            $cantidad = $_POST['cantidad'];
            $condicionServicio = $_POST['condicionServicio'];
            $plantel_id = $_POST['plantel_id'];
            $plantel_id = base64_decode($plantel_id);
//            var_dump($plantel_id);
//            var_dump($cantidad);
//            var_dump($condicionServicio);
//            die();
            if (is_numeric($cantidad) and is_numeric($condicionServicio) and is_numeric($plantel_id)) {
                $periodo_Escolar = PeriodoEscolar::model()->getPeriodoActivo();
                $periodo_Escolar_id = $periodo_Escolar['id'];

                $model->cantidad = (int) $cantidad;
                $model->condicion_servicio_id = (int) $condicionServicio;
                $model->plantel_id = (int) $plantel_id;
                $model->periodo_id = $periodo_Escolar_id;
                $model->usuario_ini_id = (int) Yii::app()->user->id;

                $model->servicio_id = 69;
                $model->estatus = 'A';
//                                var_dump($model);
//                die();
                if ($model->save()) {

                    $class = 'successDialogBox';
                    $message = 'Estimado usuario, se ha completado exitosamente el proceso de registro de la colección bicentenaria.';

                    $this->renderPartial('//msgBox', array(
                        'class' => $class,
                        'message' => $message,
                    ));
                
                }
            } else {
                $class = 'errorDialogBox';
                $message = 'Estimado usuario,los datos que ha ingresado son invalida .';

                $this->renderPartial('//msgBox', array(
                    'class' => $class,
                    'message' => $message,
                ));
            }
        } else {

            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

}
