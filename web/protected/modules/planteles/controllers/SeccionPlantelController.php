<?php

class SeccionPlantelController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    const MODULO = "Planteles.SeccionPlantel";

    public $defaultAction = 'admin';
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consulta de Secciones al Plantel',
        'write' => 'Asignación de Secciones al Plantel',
        'admin' => 'Asignación y Eliminación de Secciones al Plantel',
        'label' => 'Secciones del Plantel'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /* protected function beforeAction($event) {
      /*if ($event->id == 'admin') {
      if (!(Yii::app()->user->id == 1 OR Yii::app()->user->group == UserGroups::DESARROLLADOR OR Yii::app()->user->group == UserGroups::JEFE_DRCEE OR Yii::app()->user->group == UserGroups::ADMIN_DRCEE OR Yii::app()->user->group == UserGroups::ADMIN_REG_CONTROL)) {
      $plantel_id = $this->getRequest('id');
      $plantel_id_decoded = base64_decode($plantel_id);
      if (AutoridadPlantel::model()->esAutoridadDelPlantel($plantel_id_decoded))
      return true;
      else {
      $this->registerLog('LECTURA', self::MODULO . $event->id, 'ILEGAL', 'Intento entrar a un plantel en el cual no es autoridad, plantel_id : ' . $plantel_id_decoded);

      throw new CHttpException(401, 'Estimado usuario usted no es una Autoridad de este Plantel, por lo tanto no tiene acceso a esta acción. ');
      }
      } else
      return true;
      } else
      return true;
      $this->render("//msgBox", array('class' => 'infoDialogBox', 'message' => 'Estimado Usuario, esta funcionalidad esta inhabilitada, disculpe las molestias ocasionadas.'));
      Yii::app()->end();
      } */

    public function actions() {
        return array(
            'addTabularInputs' => array(
                'class' => 'ext.yii-playground.actions.XTabularInputAction',
                'modelName' => 'AsignaturaDocente',
                'viewName' => 'tabular/_tabularInput',
            ),
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('cargarFormulario', 'validarFormularioInscripcion', 'confirmarEliminacion', 'mostrarGrado', 'mostrarRegistrarSeccion', 'vizualizar', 'mostrarSeccion', 'admin', 'mostrarPlan', 'detallesSeccion', 'imprimirPdf'),
                'pbac' => array('read', 'write', 'admin'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('eliminarSeccion', 'modificarSeccion', 'guardarSeccion', 'activarSeccion', 'obtenerAsignaturasCrear', 'obtenerDatosPersona', 'formularioInscripcion', 'imprimirPdf'),
                'pbac' => array('write', 'admin'),
            ),
            array('allow',
                'actions' => array('CambiarEstatusProcesoMatriculacion', 'formatoCuadraturaMedia'),
                'pbac' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    public function actionCargarFormulario() {

        $upload_handler = new UploadHandler(null, true, null, null, "/public/uploads/Formularios/");
    }

    public function actionValidarFormularioInscripcion() {
        if (Yii::app()->request->isAjaxRequest) {
            if ($this->hasPost('archivo')) {
                // FORMULARIO_CODPLANTEL_GRADO_SECCION-PLANTEL_ID-SECCIONPLANTELID-PERIODOID.xls
                $archivo = $this->getPost('archivo');
                $ruta = Yii::getPathOfAlias('webroot') . '/public/uploads/Formularios/';
                $seccion_plantel_id = null;
                $plantel_id = null;
                $periodo_id = null;
                $existe = 0;
                $existe_formulario_inscripcion = 0;
                $respuesta = array();
                $extension = '';
                $guardo = 0;
                $usuario_id = Yii::app()->user->id;
                if (!strrpos($archivo, '(')) {
                    $archivo_sin_ext = explode('.', $archivo);
                    $extension = $archivo_sin_ext[1];
                    $archivo_exploded = explode('-', $archivo_sin_ext[0]);
                    $plantel_id = (isset($archivo_exploded[1])) ? $archivo_exploded[1] : null;
                    $seccion_plantel_id = (isset($archivo_exploded[2])) ? $archivo_exploded[2] : null;
                    $periodo_id = (isset($archivo_exploded[3])) ? $archivo_exploded[3] : null;
                    if ($periodo_id != null AND $seccion_plantel_id != null AND $plantel_id != null AND $periodo_id == 14) {
                        $model = new SeccionPlantel();
                        $existe = $model->existeSeccionPlantel($seccion_plantel_id, $plantel_id);
                        if ($existe != 0) {
                            $modelFormulario = new FormularioInscripcionPeriodo();
                            $existe_formulario_inscripcion = $modelFormulario->existeFormularioInscripcionPeriodo($seccion_plantel_id, $plantel_id, $periodo_id);
                            if ($existe_formulario_inscripcion == 0) {
                                $guardo = $modelFormulario->insertFormularioInscripcionPeriodo($periodo_id, $plantel_id, $seccion_plantel_id, $archivo_sin_ext[0], $usuario_id, $extension);
                                if ($guardo > 0) {
                                    $respuesta['mensaje'] = 'Estimado usuario, se ha cargado exitosamente el formulario. Cuando sea procesado recibira un correo electronico con el resultado.';
                                    $respuesta['statusCode'] = 'SUCCESS';
                                    echo json_encode($respuesta);
                                    Yii::app()->end();
                                } else {
                                    unlink($ruta . $archivo);
                                    $respuesta['mensaje'] = 'Estimado usuario, ha ocurrido un error durante el proceso. Recargue la página e intente nuevamente ';
                                    $respuesta['statusCode'] = 'ERROR';
                                    echo json_encode($respuesta);
                                    Yii::app()->end();
                                }
                            } else {
                                unlink($ruta . $archivo);
                                $respuesta['mensaje'] = 'Estimado usuario, este formulario ya fue cargado anteriormente y esta en la cola para ser procesado. Cuando dicho proceso termine se le enviara un correo eletronico con el resultado.';
                                $respuesta['statusCode'] = 'INFO';
                                echo json_encode($respuesta);
                                Yii::app()->end();
                            }
                        } else {
                            unlink($ruta . $archivo);
                            $respuesta['mensaje'] = 'Estimado usuario, el nombre del archivo es invalido. Decargue nuevamente el formulario y no modifique el nombre.';
                            $respuesta['statusCode'] = 'ERROR';
                            echo json_encode($respuesta);
                            Yii::app()->end();
                        }
                    } else {
                        unlink($ruta . $archivo);
                        $respuesta['mensaje'] = 'Estimado usuario, el nombre del archivo es invalido. Decargue nuevamente el formulario y no modifique el nombre.';
                        $respuesta['statusCode'] = 'ERROR';
                        echo json_encode($respuesta);
                        Yii::app()->end();
                    }
                } else {
                    unlink($ruta . $archivo);
                    $respuesta['mensaje'] = 'Estimado usuario, este formulario ya fue cargado anteriormente y esta en la cola para ser procesado. Cuando dicho proceso termine se le enviara un correo eletronico con el resultado.';
                    $respuesta['statusCode'] = 'INFO';
                    echo json_encode($respuesta);
                    Yii::app()->end();
                }
            } else {
                $respuesta['mensaje'] = 'Estimado usuario, ha ocurrido un error durante el proceso. Recargue la página e intente nuevamente.';
                $respuesta['statusCode'] = 'INFO';
                echo json_encode($respuesta);
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function columnaAcciones($data) {
        $estatus_proc_matri = Yii::app()->getSession()->get('estatus_proc_matri');
        $id = $data->id;
        $estatus = $data->estatus;
        $columna = '<div class="btn-group">
                        <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
                            Seleccione
                            <span class="icon-caret-down icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-yellow pull-right">';
        $capacidad = isset($data['capacidad']) ? $data['capacidad'] : null;
        $nivel_id = isset($data['nivel_id']) ? $data['nivel_id'] : null;
        $grado_id = isset($data['grado_id']) ? $data['grado_id'] : null;
        $periodo_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_id = $periodo_id['id'];
        $data_description = is_object($data->seccion) ? $data->seccion->nombre : '';
        //$ultimoGrado = Grado::model()->obtenerUltimoGradoNivel($nivel_id, $grado_id);
        //$modalidad = (is_object($data->plantel) AND isset($data->plantel->modalidad_id)) ? $data->plantel->modalidad_id : '';

        if (($estatus == 'A') || ($estatus == '')) {
            // $columna .= CHtml::link("", "", array('onClick' => "vizualizar('" . base64_encode($id) . "')", "class" => "fa fa-search", "title" => "Consultar los Datos de la Sección")) . '&nbsp;&nbsp;';
            if (Yii::app()->user->pbac('planteles.seccionPlantel.read')) {
                $columna .='<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Visualizar</span>", "/planteles/seccionPlantel/detallesSeccion/id/" . base64_encode($id), array("class" => "fa fa-search blue", "title" => "Consultar Sección")) . '</li>';
            }
            if ($estatus_proc_matri == 'E') {
                if (Yii::app()->user->pbac('planteles.seccionPlantel.write') or Yii::app()->user->pbac('planteles.seccionPlantel.admin')) {
                    $columna .='<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Modificar</span>", "", array('onClick' => "mostrarSeccion('" . base64_encode($id) . "')", "class" => "fa fa-pencil green", "title" => "Modificar los Datos de la Sección")) . '</li>';
                }
                /* if(in_array(Yii::app()->user->group,array(UserGroups::ADMIN_0,UserGroups::JEFE_DRCEE)))
                  $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Asignar Docentes</span>", "/planteles/docente/lista/id/" . base64_encode($data->id) . "/plantel/" . base64_encode($data->plantel_id), array("class" => "fa fa-male purple docentes", 'data-id' => base64_encode($data->id), 'data-description' => $data_description, "title" => "Asignar Docentes")) . '</li>'; */
                if (Yii::app()->user->pbac('planteles.matricula.read') && Yii::app()->user->pbac('planteles.matricula.write'))
                // if ($grado_id == $ultimoGrado || $modalidad = 2) {
                //if (in_array($nivel_id,array(1,2))) {
                    if (($estatus == 'A')) {
                        $datosSeccion = SeccionPlantel::model()->existeEstudiantesInscritosEnSeccion($data->id, $periodo_id);
                        if ($datosSeccion == 0)
                            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Inscripción Inicial</span>", "/planteles/matricula/inscripcion/id/" . base64_encode($data->id) . "/plantel/" . base64_encode($data->plantel_id), array("class" => "fa fa-users orange inscribir", 'data-id' => base64_encode($data->id), 'data-description' => $data_description, "title" => "Inscripción Inicial de Estudiantes")) . '</li>';
                        if ($datosSeccion < $capacidad && $datosSeccion > 0)
                            $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Inscripción Individual</span>", "/planteles/matricula/inscripcionIndividual/id/" . base64_encode($data->id) . "/plantel/" . base64_encode($data->plantel_id) . "/key/" . base64_encode($data->plantel_id * 9), array("class" => "fa fa-user blue inscribir", 'data-id' => base64_encode($data->id), 'data-description' => $data_description, "title" => "Inscripción Individual de Estudiantes ")) . '</li>';
                        //$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Formulario de Inscripción</span>", "/planteles/seccionPlantel/formularioInscripcion/id/" . base64_encode($data->id) . "/plantel/" . base64_encode($data->plantel_id), array("class" => "fa fa-cloud-download blue ", 'data-id' => base64_encode($data->id), 'data-description' => $data_description, "title" => "Formulario de Inscripción Masiva")) . '</li>';
                    }
                // }

                if (Yii::app()->user->id == '1' or Yii::app()->user->pbac('planteles.calificaciones.write')) {
                    if (Yii::app()->user->group == UserGroups::DOCENTE) {
                        //$vista = "_regularMediaGeneral"; //regular basica pero no me muestra
                        $columna .='<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Calificaciónes</span>", "/planteles/calificaciones/Docente/id/" . base64_encode($id) . "/plantel/" . base64_encode($data->plantel_id), array("class" => "fa fa-check-square orange", "title" => "Consultar calificaciones")) . '</li>';
                    } else {
                        $columna .='<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Calificaciónes</span>", "/planteles/calificaciones/index/id/" . base64_encode($id) . "/plantel/" . base64_encode($data->plantel_id) ."/periodo/".base64_encode($periodo_id), array("class" => "fa fa-check-square orange", "title" => "Consultar calificaciones")) . '</li>';
                    }
                }

                if (Yii::app()->user->pbac('planteles.seccionPlantel.write') or Yii::app()->user->pbac('planteles.seccionPlantel.admin')) {
                    $columna .='<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Inactivar</span>", "", array('onClick' => "confirmarEliminacion('" . base64_encode($id) . "')", "class" => "fa fa-trash-o red", "title" => "Inactivar la Sección")) . '</li>';
                }
            }
        } else if ($estatus == 'E') {
            $columna .='<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Visualizar</span>", "/planteles/seccionPlantel/detallesSeccion/id/" . base64_encode($id), array("class" => "fa fa-search blue", "title" => "Consultar Sección")) . '</li>';
            $columna .='<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Activar</span>", "", array('onClick' => "activarSeccion('" . base64_encode($id) . "')", "class" => "fa fa-check green", "title" => "Activar la Sección")) . '</li>';
        }

        $columna .= '</ul></div>';

        return $columna;
    }

    public function estatusSeccion($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'E') {
            return 'Inactivo';
        }
    }

    public function actionMostrarPlan() {

        if (is_numeric($_REQUEST['plantel_id'])) {
            $plantel_id = $_REQUEST['plantel_id'];
        }

        if (is_numeric($_REQUEST['nivel_id'])) {
            $nivel_dropDown_id = $_REQUEST['nivel_id'];
        } else {
            $nivel_dropDown_id = null;
        }

        $verificarExistenciaNivel = NivelPlantel::model()->verificarExistenciaNivelPlantel($plantel_id, $nivel_dropDown_id);

        if ($verificarExistenciaNivel == false) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $nivelPlan = NivelPlan::model()->getPlan($verificarExistenciaNivel, $plantel_id);

            if ($nivelPlan == false) {
                echo 'false';
            } else {
                for ($x = 0; $x < count($nivelPlan); $x++) {
                    if ($nivelPlan[$x]['nombre'] == null)
                        $nombre[] = array(
                            'nombre' => $nivelPlan[$x]['nombreplan'],
                            'plan_id' => $nivelPlan[$x]['plan_id']
                        );
                    if ($nivelPlan[$x]['nombre'] != null)
                        $nombre[] = array(
                            'nombre' => $nivelPlan[$x]['nombre'],
                            'plan_id' => $nivelPlan[$x]['plan_id']
                        );
                }

                if ($nombre != null) {
                    $listaNivelPlan = CHtml::listData($nombre, 'plan_id', 'nombre');
                    echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);
                    foreach ($listaNivelPlan as $valor => $descripcion) {
                        echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
                    }
                }
            }
        }
    }

    public function actionMostrarGrado() {

        if (is_numeric($_REQUEST['plan_id'])) {
            $plan_dropDown_id = $_REQUEST['plan_id'];
        } else {
            $plan_dropDown_id = null;
        }

        if (is_numeric($_REQUEST['nivel_id'])) {
            $nivel_dropDown_id = $_REQUEST['nivel_id'];
        } else {
            $nivel_dropDown_id = null;
        }

        $verificarExistenciaPlan = NivelPlan::model()->verificarExistenciaPlan($plan_dropDown_id, $nivel_dropDown_id);
        if ($verificarExistenciaPlan == false) {
            $lista = array('empty' => '-SELECCIONE-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            //var_dump($verificarExistenciaPlan); die();
            $planGrado = PlanesGradosAsignaturas::model()->getGrado($verificarExistenciaPlan);
            // var_dump($planGrado); die();
            if ($planGrado == false) {
                echo 'false';
            } else {
                $listaNivelPlan = CHtml::listData($planGrado, 'grado_id', 'nombre');
                //  var_dump($listaNivelPlan); die();
                echo CHtml::tag('option', array('value' => ''), CHtml::encode('-SELECCIONE-'), true);
                foreach ($listaNivelPlan as $valor => $descripcion) {
                    echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
                }
            }
        }
    }

    public function actionEliminarSeccion($id) {

        $modelSeccion = new SeccionPlantel;
        $seccionId = base64_decode($id);
        $usuario_id = Yii::app()->user->id;
//        var_dump($seccionId . ' seccion_plantel');
//        die();
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "No se ha encontrado la sección que ha solicitado para Inactivar. Recargue la página e intentelo de nuevo.";
            Yii::app()->user->setFlash('mensajeError', "$mensaje");
            $this->renderPartial('//flashMsgv2');
            // Yii::app()->end();
        } else {
            $model = SeccionPlantel::model()->findByPk($seccionId);

            if ($model != null) {

                $eliminacion = $modelSeccion->eliminarSeccion($seccionId);
                $this->registerLog('INACTIVAR', 'planteles.seccionPlantel.EliminarSeccion', 'EXITOSO', 'Permite inactivar un registro de una sección que pertenece a un plantel en especifico');
                if ($eliminacion == 1) {
                    $estudiantesInscritos = $modelSeccion->estudiantesInscritos($seccionId);
                    if ($estudiantesInscritos != array()) {
                        $inactivarEstudiantesInscritos = $modelSeccion->inactivarEstudiantesInscritos($estudiantesInscritos, $usuario_id);
                        //   var_dump($inactivarEstudiantesInscritos);
                        if ($inactivarEstudiantesInscritos == 1) {
                            $nombre = $model->seccion->nombre;
                            $grado = $model->grado->nombre;

                            Yii::app()->user->setFlash('mensajeExitoso', "Inactivación Exitosa de la sección y de los estudiantes inscritos en la sección:" . '&nbsp;&nbsp;' . $nombre . '&nbsp;&nbsp;' . "y Grado" . '&nbsp;&nbsp;' . $grado);
                            $this->renderPartial('//flashMsg');

                            $this->registerLog('INACTIVAR', 'planteles.seccionPlantel.EliminarSeccion', 'EXITOSO', 'Permite inactivar los estudiantes inscritos en la sección ' . '&nbsp;&nbsp;' . $nombre . '&nbsp;&nbsp;' . "y Grado" . '&nbsp;&nbsp;' . $grado);
                        }
                    } else {
                        $nombre = $model->seccion->nombre;
                        $grado = $model->grado->nombre;

                        Yii::app()->user->setFlash('mensajeExitoso', "Inactivación Exitosa de la sección:" . '&nbsp;&nbsp;' . $nombre . '&nbsp;&nbsp;' . "y Grado" . '&nbsp;&nbsp;' . $grado);
                        $this->renderPartial('//flashMsg');
                    }
                } else {
                    Yii::app()->user->setFlash('mensajeError', "Inactivación invalida, por favor intente nuevamente");
                    $this->renderPartial('//flashMsgv2');
                    Yii::app()->end();
                }
            } else {
                Yii::app()->user->setFlash('mensajeError', "Por favor seleccione una opcion correcta para eliminar, intente nuevamente");
                $this->renderPartial('//flashMsgv2');
                Yii::app()->end();
            }
        }
    }

    public function actionActivarSeccion($id) {

        $model = new SeccionPlantel;
        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "No se ha encontrado la sección que ha solicitado para Activar. Recargue la página e intentelo de nuevo.";
            Yii::app()->user->setFlash('mensajeError', "$mensaje");
            $this->renderPartial('//flashMsgv2');
            // Yii::app()->end();
        } else {
            $model = SeccionPlantel::model()->findByPk($seccionId);

            if ($model != null) {
                //  $idSeccion = $model->id;
                //  $nombre = trim(strtoupper($_REQUEST['Seccion']['nombre']));
                $resultadoActivacion = $model->activarSeccion($seccionId);

                if ($resultadoActivacion == 1) {
                    $nombre = $model->seccion->nombre;
                    $grado = $model->grado->nombre;
                    $this->registerLog('ACTIVAR', 'planteles.seccionPlantel.ActivarSeccion', 'EXITOSO', 'Permite activar un registro de una sección que pertenece a un plantel en especifico');
                    Yii::app()->user->setFlash('mensajeExitoso', "Activación Exitosa de la Sección:" . '&nbsp;&nbsp;' . $nombre . '&nbsp;&nbsp;' . "y Grado" . '&nbsp;&nbsp;' . $grado);
                    $this->renderPartial('//flashMsg');
                } else { // error que no guardo
                    Yii::app()->user->setFlash('mensajeError', "Activación invalida, por favor intente nuevamente");
                    $this->renderPartial('//flashMsgv2');
                    Yii::app()->end();
                }
            } else {
                Yii::app()->user->setFlash('mensajeError', "Por favor seleccione una opcion correcta para activar, intente nuevamente");
                $this->renderPartial('//flashMsgv2');
                Yii::app()->end();
            }
        }
    }

    public function actionVizualizar($id) {
        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 45px'>" . "No se ha encontrado la sección que ha solicitado para consultar. Recargue la página e intentelo de nuevo." . "</div>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
            // Yii::app()->end();
        } else {
            $periodo_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_id = $periodo_id['id'];
            $existeSeccionPlantelPeriodo = SeccionPlantel::model()->existeEstudiantesInscriptosEnSeccion($seccionId, $periodo_id);
            //  var_dump($existeSeccionPlantelPeriodo);
            if ($existeSeccionPlantelPeriodo !== array()) {
                //$fecha_nacimiento=$existeSeccionPlantelPeriodo[0]['fecha_nacimiento'];
                $model = SeccionPlantel::model()->findByPk($seccionId);
                //   $edad=  Estudiante::model()->calcularEdad($fecha_nacimiento);
                $mostrarInscriptos = true;

                $dataProviderResultados = $this->dataProviderEstudiantesInscriptos($existeSeccionPlantelPeriodo);

                Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
                $this->renderPartial('vizualizar', array(
                    'model' => $model,
                    'mostrarInscriptos' => $mostrarInscriptos,
                    'dataProvider' => $dataProviderResultados
                        ), false, true);
            } else {
                $model = SeccionPlantel::model()->findByPk($seccionId);
                $mostrarInscriptos = false;
                //var_dump($model);
                $this->renderPartial('vizualizar', array(
                    'model' => $model,
                    'mostrarInscriptos' => $mostrarInscriptos
                ));
            }
        }
    }

    public function actionDetallesSeccion($id) {
        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 45px'>" . "No se ha encontrado la sección que ha solicitado para consultar. Recargue la página e intentelo de nuevo." . "</div>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
            // Yii::app()->end();
        } else {

            $periodo = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_id = $periodo['id'];
            $existeSeccionPlantelPeriodo = SeccionPlantel::model()->existeEstudiantesInscriptosEnSeccion($seccionId, $periodo_id);
            //$datosDocente = Docente::model()->obtenerAsignaturasSeccionPlantel($seccionId);
            //  var_dump($existeSeccionPlantelPeriodo);
            if ($existeSeccionPlantelPeriodo !== array()) {
                //$fecha_nacimiento=$existeSeccionPlantelPeriodo[0]['fecha_nacimiento'];
                $model = SeccionPlantel::model()->findByPk($seccionId);
                //   $edad=  Estudiante::model()->calcularEdad($fecha_nacimiento);
                $plantel_id = $model->plantel_id;
                // $plantel_id = base64_decode($plantel_id);
                $mostrarInscriptos = true;
                $dataPlantel = Plantel::model()->obtenerDatosIdentificacion($plantel_id);
                //var_dump($dataPlantel);die();
                $dataProviderResultados = $existeSeccionPlantelPeriodo;
                $dataSeccion = seccionPlantel::model()->cargarDetallesSeccion($seccionId, $plantel_id);
                $totalInscritos = seccionPlantel::model()->calcularInscritosPorSeccion($seccionId, $periodo_id);
                //var_dump($totalInscritos);die();
                //Yii::app()->clientScript->scriptMap['jquery.min.js'] = false;
                //Yii::app()->clientScript->scriptMap['jquery.ba-bbq.js'] = false;
                $this->render('detallesSeccion', array(
                    'model' => $model,
                    'totalInscritos' => $totalInscritos,
                    'plantel_id' => $plantel_id,
                    'datosPlantel' => $dataPlantel,
                    'mostrarInscriptos' => $mostrarInscriptos,
                    'dataProvider' => $dataProviderResultados,
                    'datosSeccion' => $dataSeccion,
                    //'datosDocente'=>$datosDocente,
                    'seccionId' => $seccionId,
                    'periodo_escolar' => $periodo
                ));
            } else {
                $model = SeccionPlantel::model()->findByPk($seccionId);
                $plantel_id = $model->plantel_id;
                $dataPlantel = Plantel::model()->obtenerDatosIdentificacion($plantel_id);
                $dataSeccion = seccionPlantel::model()->cargarDetallesSeccion($seccionId, $plantel_id);
                $totalInscritos = seccionPlantel::model()->calcularInscritosPorSeccion($seccionId, $periodo_id);
                $mostrarInscriptos = false;
                //var_dump($model);
                $this->render('detallesSeccion', array(
                    'model' => $model,
                    'totalInscritos' => $totalInscritos,
                    'plantel_id' => $plantel_id,
                    'datosPlantel' => $dataPlantel,
                    'mostrarInscriptos' => $mostrarInscriptos,
                    //'dataProvider' => $dataProviderResultados,
                    'datosSeccion' => $dataSeccion,
                    //'datosDocente'=>$datosDocente,
                    'seccionId' => $seccionId,
                    'periodo_escolar' => $periodo
                ));
            }
        }
    }

    public function dataProviderEstudiantesInscriptos($existeSeccionPlantelPeriodo) {

        foreach ($existeSeccionPlantelPeriodo as $key => $value) {

            $cedula_escolar = $value['cedula_escolar'];
            $fecha_nacimiento = $value['fecha_nacimiento'];
            $nombresApellidos = $value['nomape'];
            $cedula_identidad = $value['cedula_identidad'];
            $edad = Estudiante::model()->calcularEdad($fecha_nacimiento);


            $rawData[] = array(
                'id' => $key,
                'cedula_escolar' => '<center>' . $cedula_escolar . '</center>',
                'edad' => '<center>' . $edad . '</center>',
                'nomape' => '<center>' . $nombresApellidos . '</center>',
                'cedula_identidad' => '<center>' . $cedula_identidad . '</center>'
            );
        }
//           var_dump($rawData);
//          die();
        return new CArrayDataProvider($rawData, array(
            //  'pagination' =>false,
            'pagination' => array(
                'pageSize' => 8,
            ),
                )
        );
    }

    public function actionMostrarRegistrarSeccion() {

        $model = new SeccionPlantel;
        // var_dump($_REQUEST['plantel_id']);
        if (isset($_REQUEST['plantel_id']) && !is_numeric($_REQUEST['plantel_id'])) {
            $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 25px'>" . "No se ha encontrado el plantel asociado para asignar una sección. Recargue la página e intentelo de nuevo." . "</div>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
        } else {
            $plantel_id = $_REQUEST['plantel_id'];

            $plan = array();
            $grado = array();
            //$turno = Turno::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
            $nivel = NivelPlantel::model()->nivelPlantel($plantel_id);
            //$plan=$this->actionMostrarPlan($nivel);
            if ($nivel == false) {
                $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 25px'>" . "999: Estimado Usuario, debe asociar los niveles al plantel y despues asignar las secciones." . "</div>";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
            } else {
                //throw new CHttpException(999, 'Estimado Usuario, debe asociar los niveles al plantel y despues asignar las secciones.');
                // var_dump($nivel);
                // $periodo = PeriodoEscolar::model()->findAll(array('order' => 'periodo ASC', 'condition' => "estatus='A'"));
                $seccion = Seccion::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
                $this->renderPartial('_formRegistrarSeccion', array('model' => $model,
                    'plantel_id' => $plantel_id,
                    'plan' => $plan,
                    'grado' => $grado,
                    //'turno' => $turno,
                    'nivel' => $nivel,
                    'seccion' => $seccion));
            }
        }
    }

    public function actionMostrarSeccion($id) {

        // $id= $_REQUEST['id'];
        $model = new SeccionPlantel;
        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            // throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
            $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 45px'>" . "No se ha encontrado la sección que ha solicitado para modificar. Recargue la página e intentelo de nuevo." . "</div>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
            // Yii::app()->end();
        } else {
            $model = SeccionPlantel::model()->findByPk($seccionId);
            //   var_dump($model); die();
            if (is_numeric($_REQUEST['plantel_id']))
                $plantel_id = $_REQUEST['plantel_id'];

            $plan = array($model->plan_id => $model->plan->nombre);
            $grado = array($model->grado_id => $model->grado->nombre);
            //  var_dump($plan); die();
            $turno = Turno::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
            $nivel = NivelPlantel::model()->nivelPlantel($plantel_id);
            //$plan=$this->actionMostrarPlan($nivel);
            if ($nivel == false) {
                $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 25px'>" . "999: Estimado Usuario, debe asociar los niveles al plantel y despues asignar las secciones." . "</div>";
                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
            } else {
                //throw new CHttpException(999, 'Estimado Usuario, debe asociar los niveles al plantel y despues asignar las secciones.');
                // var_dump($nivel);
                // $periodo = PeriodoEscolar::model()->findAll(array('order' => 'periodo ASC', 'condition' => "estatus='A'"));
                $seccion = Seccion::model()->findAll(array('order' => 'nombre ASC', 'condition' => "estatus='A'"));
                $this->renderPartial('_formRegistrarSeccion', array('model' => $model,
                    'plantel_id' => $plantel_id,
                    'plan' => $plan,
                    'grado' => $grado,
                    'turno' => $turno,
                    'nivel' => $nivel,
                    'seccion' => $seccion));
            }
        }
    }

    public function actionGuardarSeccion() {
        $docentes = $this->getQuery('AsignaturaDocente');
        $capacidad = base64_decode($this->getQuery('capacidad'));
        $grado_id = base64_decode($this->getQuery('grado_id'));
        $nivel_id = base64_decode($this->getQuery('nivel_id'));
        $plan_id = base64_decode($this->getQuery('plan_id'));
        $plantel_id = base64_decode($this->getQuery('plantel_id'));
        $turno_id = base64_decode($this->getQuery('turno_id'));
        $seccion_id = base64_decode($this->getQuery('seccion_id'));
        $model = new SeccionPlantel;
        $rollback = false;
        $guardar = false;
        $usuario_id = Yii::app()->user->id;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);
        $mensaje = '';
        if (isset($plantel_id)) {
            if (!is_numeric($plantel_id))
                $mensaje.= "Han sido alterado los datos del plantel ya que solo puede contener números, Por favor intente nuevamente <br>";
            $model->seccion_id = $seccion_id;
            $model->nivel_id = $nivel_id;
            $model->plan_id = $plan_id;
            $model->grado_id = $grado_id;
            $model->turno_id = $turno_id;
            $model->capacidad = $capacidad;
            $model->usuario_ini_id = $usuario_id;
            $model->estatus = 'A';
            $model->plantel_id = $plantel_id;

            if ($model->validate()) {
                if (is_numeric($model->seccion_id)) {
                    $resultadoSeccion = $model->validarSeccion($model->seccion_id);
                    if ($resultadoSeccion == 0) {
                        $mensaje.="La sección no se encuentra, Por favor intente nuevamente <br>";
                    } else {
                        $seccion_id = $model->seccion_id;
                    }
                } else {
                    $mensaje.= "El campo Sección solo puede contener números <br>";
                }

                if (is_numeric($nivel_id)) {
                    $nivel_id = $nivel_id;
                } else {
                    $mensaje.= "El campo Nivel solo puede contener números <br>";
                }


                if (is_numeric($plan_id)) {

                    $plan_id = $plan_id;
                } else {
                    $mensaje.= "El campo Plan solo puede contener números <br>";
                }

                if (is_numeric($grado_id)) {

                    $grado_id = $grado_id;
                } else {
                    $mensaje.= "El campo Grado solo puede contener números <br>";
                }

                if (is_numeric($turno_id)) {

                    $resultadoTurno = $model->validarTurno($turno_id);
                    if ($resultadoTurno == 0) {
                        $mensaje.="El turno no se encuentra, Por favor intente nuevamente <br>";
                    } else {
                        $turno_id = $turno_id;
                    }
                } else {
                    $mensaje.= "El campo Turno solo puede contener números <br>";
                }

                if (is_numeric($capacidad)) {
                    $capacidad_est = (int) $capacidad;
//                    if (strlen($capacidad_est) > 2) {
//                        $mensaje.= "El campo Capacidad solo puede contener 3 números <br>";
//                    }
                    if ($capacidad_est < 10) {
                        $mensaje.= "El campo Capacidad debe contener al menos 10 estudiante <br>";
                    }
                    if ($capacidad_est > 100) {
                        $mensaje.= "Una sección debe contener maximo 100 estudiante <br>";
                    }
                } else {
                    $mensaje.= "El campo Capacidad solo puede contener números <br>";
                }

                if ($mensaje != null) {
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
                $secciones[] = array(
                    'plantel_id' => $plantel_id,
                    'seccion_id' => $seccion_id,
                    'nivel_id' => $nivel_id,
                    'plan_id' => $plan_id,
                    'grado_id' => $grado_id,
                    'turno_id' => $turno_id,
                    'capacidad' => $capacidad
                );
                if ($secciones != array()) {
                    $verificarNivelPlanGrado = $model->validarNivelPlanGrado($plantel_id, $nivel_id, $plan_id, $grado_id);
                    if ($verificarNivelPlanGrado != 0) {
                        // var_dump($verificarNivelPlanGrado); die();
                        $validacionSeccion = $model->validacionSeccion($secciones);

                        if ($validacionSeccion == array()) {

                            $validacion = $model->validacionUniqueGradoSeccionTurno($secciones);

                            if ($validacion == array()) {
                                if (count($docentes) > 0) {
                                    foreach ($docentes as $index => $valor) {
                                        if ((isset($valor['tdocumento_identidad']) AND isset($valor['documento_identidad'])) AND ( $valor['documento_identidad'] == '' OR $valor['tdocumento_identidad'] == '' )) {
                                            unset($docentes[$index]);
                                        }
                                    }
                                    if (count($docentes) > 0) {
                                        $docentes = array_values($docentes);
                                        foreach ($docentes as $index => $valor) {
                                            if (isset($valor['escalafon_id']) AND $valor['escalafon_id'] == '') {
                                                $mensaje = "Estimado usuario, no ha seleccionado el Escalafon de por lo menos un (1) Docente.";
                                                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                                                Yii::app()->end();
                                            } else {
                                                if (!(isset($valor['nombres']) AND isset($valor['apellidos']) AND $valor['nombres'] != '' AND $valor['apellidos'] != '')) {
                                                    $mensaje = "Estimado usuario, por lo menos un (1) Docente no posee información en el campo Nombre Completo.";
                                                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                                                    Yii::app()->end();
                                                }
                                            }
                                        }
                                    }
                                }
                                $transaction = Yii::app()->db->beginTransaction();

                                try {
                                    if ($model->save()) {
                                        $periodo_actual = PeriodoEscolar::model()->getPeriodoActivo();
                                        $periodo_id = $periodo_actual['id'];
                                        if ($model->insertarAsignaturaDocente($model->id, $model->usuario_ini_id, $periodo_id)) {
                                            if (count($docentes) > 0) {
                                                $modelSeccionPP = new SeccionPlantelPeriodo();
                                                $periodo_id = PeriodoEscolar::model()->getPeriodoActivo();
                                                $modelSeccionPP->seccion_plantel_id = $model->id;
                                                $modelSeccionPP->usuario_ini_id = $usuario_id;
                                                $modelSeccionPP->estatus = 'A';
                                                $modelSeccionPP->periodo_id = $periodo_id['id'];

                                                if ($modelSeccionPP->save(false)) {
                                                    //var_dump($modelSeccionPP->id);
                                                    foreach ($docentes as $index => $valor) {
                                                        $docente = new Docente();
                                                        $asignaturaDocente = new AsignaturaDocente();
                                                        $docente_id = base64_decode($valor['docente_id']);
                                                        if ($index > 0) {
                                                            $existeDocente = $docente->busquedaDocente($valor['documento_identidad'], $valor['tdocumento_identidad']);
                                                            if ($existeDocente != null) {
                                                                $docente_id = $existeDocente['id'];
                                                            }
                                                        }
                                                        if ($docente_id == '') {
                                                            $docente->usuario_ini_id = $usuario_id;
                                                            $docente->estatus = 'A';
                                                            $docente->tdocumento_identidad = $valor['tdocumento_identidad'];
                                                            $docente->documento_identidad = $valor['documento_identidad'];
                                                            $docente->nombres = $valor['nombres'];
                                                            $docente->apellidos = $valor['apellidos'];
                                                            $docente->escalafon_id = $valor['escalafon_id'];
                                                            $docente->estatus_docente_id = 1; // ACTIVO
                                                            if ($docente->save(false)) {
                                                                $docente_id = $docente->id;
                                                            } else {
                                                                $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                                                                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                                                                Yii::app()->end();
                                                            }
                                                        }


                                                        $docentePlantel = new DocentePlantel();
                                                        $existeDocentePlantel = $docentePlantel->busquedaDocente($plantel_id, $docente_id);
                                                        if (!$existeDocentePlantel) {
                                                            $docentePlantel->usuario_ini_id = $usuario_id;
                                                            $docentePlantel->estatus = 'A';
                                                            $docentePlantel->plantel_id = $plantel_id;
                                                            $docentePlantel->docente_id = $docente_id;
                                                            if (!$docentePlantel->save(false)) {
                                                                $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                                                                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                                                                Yii::app()->end();
                                                            }
                                                        }
                                                        $asignaturaDocente->estatus = 'A';
                                                        $asignaturaDocente->usuario_ini_id = $usuario_id;
                                                        $asignaturaDocente->docente_id = $docente_id;
                                                        $asignaturaDocente->asignatura_id = $valor['asignatura_id'];
                                                        $asignaturaDocente->seccion_plantel_periodo_id = $modelSeccionPP->id;
                                                        if (!$asignaturaDocente->save(false)) {
                                                            $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                                                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                                                            Yii::app()->end();
                                                        }
                                                    }
                                                } else {
                                                    $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                                                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                                                    Yii::app()->end();
                                                }
                                            }
                                        } else {
                                            $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                                            $transaction->rollback();
                                            Yii::app()->end();
                                        }
                                    } else {
                                        $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                                        Yii::app()->end();
                                    }
                                    $transaction->commit();
                                    $this->registerLog('ESCRITURA', 'planteles.seccionPlantel.GuardarSeccion', 'EXITOSO', 'Permite guardar un registro de una sección que pertenece a un plantel en especifico');
                                } catch (Exception $e) {
                                    $rollback = true;
                                    $transaction->rollback();
                                    $mensaje = 'Estimado usuario, ha ocurrido un error durante el proceso de registro, intente nuevamente. Si el error persiste porfavor comuniquese mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>';
                                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje, 'exception' => $e->getMessage()));
                                    Yii::app()->end();
                                }
                                if ($rollback == false) {
                                    $mensaje = 'Estimado usuario, se ha registrado exitosamente los datos de la Sección.';
                                    $title = 'Notificación de Exito';
                                    echo json_encode(array('statusCode' => 'SUCCESS', 'mensaje' => $mensaje, 'title' => $title));
                                    Yii::app()->end();
                                }
                            } else {
                                $mensaje = "Por favor ingrese los datos correctos ya existe la sección, el turno  y el grado asignado a este plantel";
                                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                                Yii::app()->end();
                            }
                        } else {
                            $mensaje = "Por favor ingrese los datos correctos ya existe esta sección agregada con los mismos datos";
                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                            Yii::app()->end();
                        }
                    } else {
                        $mensaje = "Por favor ingrese los datos correctos el nivel esta relacionado a un plan y a los grados, estos valores fueron alterados, por favor intente nuevamente";
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    }
                } else {
                    $mensaje = "Por favor ingrese al menos una sección para guardar";
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else { // si ingresa datos erroneos muestra mensaje de error.
                $this->renderPartial('//errorSumMsg', array('model' => $model));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(404, 'No se ha especificado la sección que desea agregar. Recargue la página e intentelo de nuevo.'); // esta vacio el request
        }
    }

    public function actionModificarSeccion($id) {

        $modelSeccion = new SeccionPlantel;
        $seccionId = base64_decode($id);
        if (!is_numeric($seccionId)) {
            //throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado para modificar. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico

            $mensaje = "<div class='errorDialogBox bigger-110' style='padding-left: 60px; padding-top: 45px'>" . "No se ha encontrado la sección que ha solicitado para modificar. Recargue la página e intentelo de nuevo." . "</div>";
            Yii::app()->clientScript->scriptMap['jquery.js'] = false;
            $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
        } else {
            $model = SeccionPlantel::model()->findByPk($seccionId);

            if (isset($_POST['id'])) {

                $model->seccion_id = $_REQUEST['seccion_id'];
                $model->plan_id = $_REQUEST['plan_id'];
                $model->nivel_id = $_REQUEST['nivel_id'];
                $model->grado_id = $_REQUEST['grado_id'];
                $model->turno_id = $_REQUEST['turno_id'];
                $model->capacidad = $_REQUEST['capacidad'];
                $mensaje = "";

                if ($model->validate()) {

                    if (is_numeric($_REQUEST['seccion_id'])) {

                        $resultadoSeccion = $model->validarSeccion($_REQUEST['seccion_id']);
                        if ($resultadoSeccion == 0) {
                            $mensaje.="La sección no se encuentra, Por favor intente nuevamente <br>";
                        } else {
                            $seccion_id = $_REQUEST['seccion_id'];
                        }
                    } else {
                        $mensaje.= "El campo Sección solo puede contener números <br>";
                    }

                    if (is_numeric($_REQUEST['nivel_id'])) {

                        $nivel_id = $_REQUEST['nivel_id'];
                    } else {
                        $mensaje.= "El campo Nivel solo puede contener números <br>";
                    }


                    if (is_numeric($_REQUEST['plan_id'])) {

                        $plan_id = $_REQUEST['plan_id'];
                    } else {
                        $mensaje.= "El campo Plan solo puede contener números <br>";
                    }

                    if (is_numeric($_REQUEST['grado_id'])) {

                        $grado_id = $_REQUEST['grado_id'];
                    } else {
                        $mensaje.= "El campo Grado solo puede contener números <br>";
                    }

                    if (is_numeric($_REQUEST['turno_id'])) {

                        $resultadoTurno = $model->validarTurno($_REQUEST['turno_id']);
                        if ($resultadoTurno == 0) {
                            $mensaje.="El turno no se encuentra, Por favor intente nuevamente <br>";
                        } else {
                            $turno_id = $_REQUEST['turno_id'];
                        }
                    } else {
                        $mensaje.= "El campo Turno solo puede contener números <br>";
                    }

                    if (is_numeric($_REQUEST['capacidad'])) {

                        $capacidad_est = (int) $_REQUEST['capacidad'];
                        //   if (strlen($_REQUEST['capacidad']) > 3) {
                        //     $mensaje.= "El campo Capacidad solo puede contener 3 números <br>";
                        // }
                        if ($capacidad_est > 100) {
                            $mensaje.= "Una sección debe contener maximo 100 estudiante <br>";
                        }
                        if ($capacidad_est < 10) {
                            $mensaje.= "El campo Capacidad debe contener al menos 10 estudiante <br>";
                        }
                        $capacidad = $_REQUEST['capacidad'];
                    } else {
                        $mensaje.= "El campo Capacidad solo puede contener números <br>";
                    }

                    if (is_numeric($_REQUEST['plantel_id']))
                        $plantel_id = $_REQUEST['plantel_id'];
                    else
                        $mensaje.= "El plantel ha sido modificado, solo puede contener números <br>";

                    $secciones[] = array(
                        'id' => $seccionId,
                        'plantel_id' => $plantel_id,
                        'seccion_id' => $seccion_id,
                        'nivel_id' => $nivel_id,
                        'plan_id' => $plan_id,
                        'grado_id' => $grado_id,
                        'turno_id' => $turno_id,
                        'capacidad' => $capacidad
                    );



                    /*  VALIDA QUE EXISTA UN GRADO Y SECCION ASIGNADO POR PLANTEL   */
                    $validacionUnique = $model->validacionUniqueGradoSeccionTurno($secciones);

                    foreach ($validacionUnique as $value) {
                        $seccionId = $value["id"];
                    }

                    //   var_dump($seccionId);                    var_dump($model->id);
                    if ($seccionId != $model->id) {
                        $validacion = $model->validacionSeccion($secciones);
                        //     var_dump($validacion);
                        if ($validacion != array()) {
                            // var_dump($validacion);
                            $mensaje.="Por favor ingrese los datos correctos ya existe la sección, el turno  y el grado asignado a este plantel <br>";
                        }
                    }//else{
                    /*     FIN      */


                    if ($mensaje != null) {
                        Yii::app()->user->setFlash('mensajeError', "$mensaje");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                    }



                    if ($model != null) {

                        $verificarNivelPlanGrado = $model->validarNivelPlanGrado($plantel_id, $nivel_id, $plan_id, $grado_id);
                        // var_dump($verificarNivelPlanGrado);
                        if ($verificarNivelPlanGrado != 0) {
                            //  $validacion= $model->validacionUniqueGradoSeccionTurno($secciones);
                            // if ($validacion == array()) {

                            $actualizo = $modelSeccion->actualizoSeccion($secciones);
                            if ($actualizo == 1) {
                                $seccionn_id = '1'; //obtengo id de la seccion del registro que acabo de guardar.
                                // var_dump($seccionn_id);
                                $this->registerLog('ACTUALIZACION', 'planteles.seccionPlantel.ModificarSeccion', 'EXITOSO', 'Permite modificar un registro de una sección que pertenece a un plantel en especifico');

                                $mensaje = "$seccionn_id";
                                Yii::app()->clientScript->scriptMap['jquery.js'] = false;
                                $this->renderPartial('mensaje', array('mensaje' => $mensaje), false, true);
                                // Yii::app()->end();
                            } else { // error que no guardo
                                Yii::app()->user->setFlash('mensajeError', "Registro invalido, por favor intente nuevamente");
                                $this->renderPartial('//flashMsgv2');
                                Yii::app()->end();
                            }
                            /*     } else {
                              Yii::app()->user->setFlash('mensajeError', "Por favor ingrese los datos correctos ya existe la sección, el turno  y el grado asignado a este plantel");
                              $this->renderPartial('//flashMsgv2');
                              Yii::app()->end();
                              } */
                        } else {
                            Yii::app()->user->setFlash('mensajeError', "Por favor ingrese los datos correctos el nivel esta relacionado a un plan y a los grados, estos valores fueron alterados, por favor intente nuevamente");
                            $this->renderPartial('//flashMsgv2');
                            Yii::app()->end();
                        }
                    } else {

                        Yii::app()->user->setFlash('mensajeError', "Por favor ingrese al menos una sección para guardar");
                        $this->renderPartial('//flashMsgv2');
                        Yii::app()->end();
                    }
                    //   }
                } else { // si ingresa datos erroneos muestra mensaje de error.
                    $this->renderPartial('//errorSumMsg', array('model' => $model));
                    Yii::app()->end();
                }
            } else {

                throw new CHttpException(404, 'No se ha especificado la sección que desea agregar. Recargue la página e intentelo de nuevo.'); // esta vacio el request
            }
        }
    }

    /**
     * Manages all models.
     */
    public function actionAdmin($id) {
        $plantel_id = base64_decode($id);
        $estatus_proc_matri = 'E';
        // var_dump($plantel_id);
        if (!is_numeric($plantel_id)) {
            throw new CHttpException(404, 'No se ha encontrado la sección que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
        } else {

            if ($plantel_id != '') {
                $plantelPK = Plantel::model()->findByPk($plantel_id);
                if ($plantelPK == null) {
                    $this->redirect('../../../consultar');
                }
            } else {

                $this->redirect('../../../consultar');
            }
            Yii::import("xupload.models.XUploadForm");
            $model = new SeccionPlantel('search');
            $xupload = new XUploadForm();
            $model->unsetAttributes();  // clear any default values
            if (isset($_GET['SeccionPlantel']))
                $model->attributes = $_GET['SeccionPlantel'];

            $periodo_escolar = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_escolar_id = (isset($periodo_escolar['id'])) ? $periodo_escolar['id'] : null;
            $resultado_consulta = CierreMatriculacionPeriodo::model()->getDatos($plantel_id, $periodo_escolar_id);
            if ($resultado_consulta !== array() AND $resultado_consulta != false)
                $estatus_proc_matri = (isset($resultado_consulta['estatus'])) ? $resultado_consulta['estatus'] : 'A';
            else {
                $estatus_proc_matri = 'E';
            }
            Yii::app()->getSession()->add('estatus_proc_matri', $estatus_proc_matri);
            $this->render('admin', array(
                'model' => $model,
                'plantel_id' => $plantel_id,
                //'grado' => $grado,
                //'turno' => $turno,
                'estatus_proc_matri' => $estatus_proc_matri,
                //   'periodo' => $periodo,
                //'seccion' => $seccion,
                'xupload' => $xupload,
                'plantelPK' => $plantelPK,
                'periodo_escolar' => $periodo_escolar
            ));
        }
    }

    public function actionCambiarEstatusProcesoMatriculacion() {

        $accion = $this->getPost('accion');
        $plantel_id = $this->getPost('plantel_id');
        $plantel_id_decoded = base64_decode($plantel_id);
        $usuario_id = Yii::app()->user->id;
        if (in_array($accion, array('E', 'A')) AND is_numeric($plantel_id_decoded)) {
            $model = new CierreMatriculacionPeriodo();
            $periodo_escolar = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_escolar = (isset($periodo_escolar['id'])) ? $periodo_escolar['id'] : null;
            $resultado_consulta = $model->getDatos($plantel_id_decoded, $periodo_escolar);
            if ($resultado_consulta !== array() AND $resultado_consulta != false) {
                $model = $model->findByPk($resultado_consulta['id']);
                $model->estatus = $accion;
                $model->fecha_act = date('Y-m-d H:s:i');
                $model->usuario_act_id = $usuario_id;
                if ($accion == 'E') {
                    $model->fecha_elim = date('Y-m-d H:s:i');
                } else {
                    $model->fecha_elim = NULL;
                }
            } else {
                $model->plantel_id = $plantel_id_decoded;
                $model->periodo_id = $periodo_escolar;
                $model->estatus = 'A';
                $model->fecha_ini = date('Y-m-d H:s:i');
                $model->usuario_ini_id = $usuario_id;
            }
            if ($model->save()) {
                $accion_mensaje = '';
                if ($accion == 'A') {
                    $accion_mensaje = 'Cerrado';
                    $this->registerLog('ELIMINACION', 'planteles.seccionPlantel.cambiarEstatusProcesoMatriculacion', 'EXITOSO', 'Se ha Inactivado el Proceso de Matriculación para el plantel' . $model->plantel_id . ' y en el periodo ' . $model->periodo_id);
                } else {
                    $accion_mensaje = 'Aperturado';
                    $this->registerLog('ESCRITURA', 'planteles.seccionPlantel.cambiarEstatusProcesoMatriculacion', 'EXITOSO', 'Se ha Activado el Proceso de Matriculación  para el plantel' . $model->plantel_id . ' y en el periodo ' . $model->periodo_id);
                }
                $respuesta['statusCode'] = 'success';
                $respuesta['mensaje'] = 'Estimado Usuario, se ha ' . $accion_mensaje . ' el Proceso de Matriculación satisfactoriamente. Al cerrar esta ventana se recargará la página automaticamente para actualizar la información.';
                echo json_encode($respuesta);
                Yii::app()->end();
            } else {
                $class = 'errorDialogBox';
                $message = '500 Error: No se ha podido completar la operación, comuniquelo al administrador del sistema para su corrección.';
                $this->renderPartial('//msgBox', array(
                    'class' => $class,
                    'message' => $message,
                ));
            }
        } else {

            $class = 'errorDialogBox';
            $message = 'No se ha especificado la acción a tomar sobre el plantel en cuanto al proceso de matriculación, recargue la página e intentelo de nuevo.';

            $this->renderPartial('//msgBox', array(
                'class' => $class,
                'message' => $message,
            ));
        }
    }

    public function actionObtenerAsignaturasCrear() {
        if (Yii::app()->request->isAjaxRequest) {
            $grado_id = base64_decode($this->getQuery('grado_id'));
            $plan_id = base64_decode($this->getQuery('plan_id'));
            $nivel_id = base64_decode($this->getQuery('nivel_id'));
            $plantel_id = base64_decode($this->getQuery('plantel_id'));
            $modelsAsignaturas = array();
            if (in_array(null, array($grado_id, $plan_id, $nivel_id, $plantel_id))) {
                $mensaje = 'Estimado usuario, no se han recibido los parametros necesarios para realizar la acción. Cierre la ventana e intente nuevamente.';
                echo json_encode(array('statusCode' => 'ERROR-A', 'mensaje' => $mensaje));
                Yii::app()->end();
            } else {
                $modelPga = new PlanesGradosAsignaturas();
                $model = new AsignaturaDocente();
                $asignaturas = '';
                $asignaturas = $modelPga->getAsignaturasGradosDocenteCrear($grado_id, $plan_id);
                if (count($asignaturas) > 0) {
                    $this->renderPartial('_formAsignaturasDocente', array('asignaturas' => $asignaturas, 'model' => $model), false, true);
                    Yii::app()->end();
                } else {
                    $mensaje = "Estimado usuario, no se han encontrados Asignaturas con esta combinación. Reporte este inconveniente a <a href='soporte_gescolar@me.gob.ve'>soporte_gescolar@me.gob.ve</a>";
                    echo json_encode(array('statusCode' => 'ERROR-A', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionObtenerDatosPersona() {
        if (Yii::app()->request->isAjaxRequest) {
            $asignaturasDocentes = $this->getQuery('AsignaturaDocente');
            $tDocumentoIdentidad = $this->getQuery('tdocumento_identidad');
            $documentoIdentidad = $this->getQuery('documento_identidad');
            $index = base64_decode($this->getQuery('index'));
            $asignaturaId = base64_decode($this->getQuery('asignatura_id'));
            $autoridadPlantel = '';
            $datosPersona = '';
            $nombresPersona = '';
            $apellidosPersona = '';
            $datosDocente = '';
            $docente = '';
            $escalafonId = '';
            $usuario_id = Yii::app()->user->id;
            unset($asignaturasDocentes[$index]);
            $asignaturasDocentes = array_values($asignaturasDocentes);
            if (in_array($tDocumentoIdentidad, array('V', 'E', 'P'))) {
                if (in_array($tDocumentoIdentidad, array('V', 'E'))) {
                    if (strlen((string) $documentoIdentidad) > 10) {
                        $mensaje = 'Estimado usuario el campo Documento de Identidad no puede superar los diez (10) caracteres. Documento Identidad ' . $tDocumentoIdentidad . '-' . $documentoIdentidad;
                        echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                        Yii::app()->end();
                    } else {
                        if (!is_numeric($documentoIdentidad)) {
                            $mensaje = 'Estimado usuario el campo Documento de Identidad debe poseer solo caracteres numericos. Documento Identidad ' . $tDocumentoIdentidad . '-' . $documentoIdentidad;
                            echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                            Yii::app()->end();
                        }
                    }
                }
                $autoridadPlantel = new AutoridadPlantel();
                $datosPersona = $autoridadPlantel->busquedaSaimeMixta($tDocumentoIdentidad, $documentoIdentidad);
                if ($datosPersona) {
                    (isset($datosPersona['nombre'])) ? $nombresPersona = $datosPersona['nombre'] : $nombresPersona = '';
                    (isset($datosPersona['apellido'])) ? $apellidosPersona = $datosPersona['apellido'] : $apellidosPersona = '';
                    $docente = new Docente();
                    $datosDocente = $docente->busquedaDocente($documentoIdentidad, $tDocumentoIdentidad);
                    if (count($asignaturasDocentes) > 1)
                        foreach ($asignaturasDocentes as $index => $valor) {
                            if (isset($valor['tdocumento_identidad']) AND $valor['tdocumento_identidad'] == $tDocumentoIdentidad AND isset($valor['documento_identidad']) AND $valor['documento_identidad'] == $documentoIdentidad AND isset($valor['escalafon_id'])) {
                                $escalafonId = $valor['escalafon_id'];
                                break;
                            }
                        }
                    if ($datosDocente) {
                        $escalafonId = $datosDocente['escalafon_id'];
                        echo json_encode(array('statusCode' => 'SUCCESS-E', 'nombres' => $nombresPersona, 'apellidos' => $apellidosPersona, 'docente_id' => base64_encode($datosDocente['id']), 'escalafon_id' => base64_encode($escalafonId)));
                        Yii::app()->end();
                    } else {
                        echo json_encode(array('statusCode' => 'SUCCESS', 'nombres' => $nombresPersona, 'apellidos' => $apellidosPersona, 'escalafon_id' => base64_encode($escalafonId)));
                        Yii::app()->end();
                    }
                } else {
                    $mensaje = 'El Documento de Identidad ' . $tDocumentoIdentidad . '-' . $documentoIdentidad . ' no se encuentra registrada en nuestro sistema, por favor contacte al personal de soporte mediante <a href="soporte_gescolar@me.gob.ve">soporte_gescolar@me.gob.ve</a>.';
                    echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                    Yii::app()->end();
                }
            } else {
                $mensaje = 'Estimado usuario, se han enviado valores no permitidos en el campo Tipo de Documento de Identidad.';
                echo json_encode(array('statusCode' => 'ERROR', 'mensaje' => $mensaje));
                Yii::app()->end();
            }
        } else {
            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }
    }

    public function actionFormularioInscripcion() {
        if ($this->hasQuery('id') AND $this->hasQuery('plantel')) {
            $seccion_plantel_id = $this->getQuery('id');
            $plantel_id = $this->getQuery('plantel');
            $seccion_plantel_id_decoded = base64_decode($seccion_plantel_id);
            $plantel_id_decoded = base64_decode($plantel_id);
            $datos_seccion = array();
            $fichero = $_SERVER['DOCUMENT_ROOT'] . "/docs/formulario_DEA_seccion.xls";
            $seccion = '';
            $grado = '';
            $cod_plan = '';
            $cod_plantel = '';
            $form = '';
            if (is_numeric($seccion_plantel_id_decoded) AND is_numeric($plantel_id_decoded)) {
                $modelSeccionPlantel = new SeccionPlantel();
                $datos_seccion = $modelSeccionPlantel->obtenerDatosSeccion($seccion_plantel_id_decoded, $plantel_id_decoded);
                if ($datos_seccion != array() AND ! is_null($datos_seccion) AND $datos_seccion != '') {
                    $periodo_actual = PeriodoEscolar::model()->getPeriodoActivo();
                    $periodo_id = (isset($periodo_actual['id'])) ? $periodo_actual['id'] : '';
                    $seccion = (isset($datos_seccion['seccion'])) ? strtoupper(Utiles::slugify($datos_seccion['seccion'])) : '';
                    $grado = (isset($datos_seccion['grado'])) ? strtoupper(Utiles::slugify($datos_seccion['grado'])) : '';
                    $grado = str_replace('-', '_', $grado);
                    $cod_plan = (isset($datos_seccion['cod_plan'])) ? strtoupper(Utiles::slugify($datos_seccion['cod_plan'])) : '';
                    $cod_plantel = (isset($datos_seccion['cod_plantel'])) ? strtoupper(Utiles::slugify($datos_seccion['cod_plantel'])) : '';
                    $form = 'FORMULARIO_' . $cod_plantel . '_' . $grado . '_' . $seccion . '_' . $cod_plan . '-' . $plantel_id_decoded . '-' . $seccion_plantel_id_decoded . '-' . $periodo_id . '.xls';
                    if (file_exists($fichero)) {
                        header('Content-Description: File Transfer');
                        header('Content-Type: text/html; charset=utf-8');
                        header('Content-Type: ');
                        header('Content-Disposition: attachment; filename=' . basename($form));
                        header('Expires: 0');
                        header('Cache-Control: must-revalidate');
                        header('Pragma: public');
                        header('Content-Length: ' . filesize($fichero));
                        readfile($fichero);
                        exit;
                    } else {
                        throw new CHttpException(403, 'Estimado Usuario, no se ha encontrado el recurso solicitado. Regrese a la página anterior e intente nuevamente.');
                    }
                } else {
                    throw new CHttpException(403, 'Estimado Usuario, no se ha encontrado el recurso solicitado. Regrese a la página anterior e intente nuevamente.');
                }
            } else {
                throw new CHttpException(403, 'Estimado Usuario, no se han recibido los parametros necesarios para realizar esta acción.');
            }
        } else {
            throw new CHttpException(404, 'Estimado Usuario, no se han recibido los parametros necesarios para realizar esta acción.');
        }
    }

    public function actionFormatoCuadraturaMedia() {
        if ($this->hasQuery('id')) {
            $plantel_id_decoded = base64_decode($this->getQuery('id'));
            if (is_numeric($plantel_id_decoded)) {
                $asinaturaDocente = new AsignaturaDocente();
                $datos_cuadratura_media = $asinaturaDocente->getDatosCuadraturaMedia($plantel_id_decoded);
                $cuadratura_media = $asinaturaDocente->getCuadraturaMedia($plantel_id_decoded);
                var_dump($datos_cuadratura_media, $cuadratura_media);
            } else {
                throw new CHttpException(404, 'Estimado Usuario, no se han recibido los parametros necesarios para realizar esta acción.');
            }
        } else {
            throw new CHttpException(404, 'Estimado Usuario, no se han recibido los parametros necesarios para realizar esta acción.');
        }
    }

    /* public function dataProviderTotalEstudiantes($seccion) {

      $boton = '';
      $totalInscritos = seccionPlantel::model()->calcularInscritosPorSeccion($seccionId);
      $rawData = array();
      if ($totalInscritos != array()) {

      foreach ($estudiantes as $key => $value) {
      $total = '<div class="center">' . CHtml::textField('estudiantesIns[]', false, array('id' => 'estudiantesIns[]', 'value' => base64_encode($value['id']))) . "</div>";
      $cedula_escolar = "<div class='center'>" . $value['cedula_escolar'] . "</div>";
      // $edad = "<div class='center'>" . $value[''] . "</div>";
      $nom_completo = '<div class="center">' . $value['nom_completo'] . '</div>';
      $rawData [] = array(
      'id' => $key,
      'nom_completo' => $nom_completo,
      'cedula_escolar' => $cedula_escolar,
      'boton' => $boton
      );
      }
      }
      } */

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return SeccionPlantel the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = SeccionPlantel::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    public function columnaDocumentoIdentidad($value) {
        $documento_identidad = (isset($value['documento_identidad']) AND $value['documento_identidad'] != '' ) ? $value['documento_identidad'] : '';
        $tDocumento_identidad = (isset($value['tdocumento_identidad']) AND $value['tdocumento_identidad'] != '' ) ? $value['tdocumento_identidad'] : '';
        $cedula_escolar = (isset($value['cedula_escolar']) AND $value['cedula_escolar'] != '' ) ? $value['cedula_escolar'] : '';

        if ($documento_identidad != '' AND $documento_identidad != null) {
            if ($tDocumento_identidad != '' AND $tDocumento_identidad != null) {
                $identificacion = $tDocumento_identidad . '-' . $documento_identidad;
            } else {
                $identificacion = $documento_identidad;
            }
        } else {
            if ($cedula_escolar != '' AND $cedula_escolar != null) {
                $identificacion = 'C.E: ' . $cedula_escolar;
            } else {
                $identificacion = 'NO POSEE';
            }
        }

        return $identificacion;
    }

    public function columnaEdad($data) {
        $fecha_nacimiento = (isset($data["fecha_nacimiento"]) AND $data["fecha_nacimiento"] != '' AND ! is_null($data["fecha_nacimiento"])) ? $data["fecha_nacimiento"] : null;
        if (!is_null($fecha_nacimiento)) {
            $edad = Estudiante::model()->calcularEdad($fecha_nacimiento);
        } else {
            $edad = 0;
        }
        $columna = '<center>' . $edad . '</center>';
        return $columna;
    }

    /**
     * Performs the AJAX validation.
     * @param SeccionPlantel $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'seccion-plantel-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionImprimirPdf() {
        $plante_id = base64_decode($this->getRequest('id'));
        $periodo_escolar = base64_decode($this->getRequest('periodo'));
        $periodo = PeriodoEscolar::model()->getPeriodoActivo();
        //$dataSeccion = SeccionPlantel::obtenerDatosSeccionEstadisticas($plante_id);
        $dataPlantel = Plantel::model()->obtenerDatosIdentificacion($periodo_escolar);
        //var_dump($dataPlantel); die();
        $estadisticas_estudiante=  SeccionPlantel::model()->getCEPS($plante_id, $periodo_escolar);
        //var_dump("plantel".$plante_id."<br>"."Periodo".$periodo); die();
        $mPDF = Yii::app()->ePdf->mpdf();
        $mPDF->WriteHTML($this->renderPartial('_pdfHeader', array(), true));
        $mPDF->WriteHTML($this->renderPartial('_reporte_estadistico_estudiante', array('periodo' => $periodo,'datosPlantel'=>$dataPlantel,'estadisticas_estudiante'=>$estadisticas_estudiante),true));
        $mPDF->Output($plante_id.$periodo . '.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
    }

}
