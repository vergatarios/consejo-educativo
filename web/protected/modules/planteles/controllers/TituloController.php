<?php

class TituloController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Mostrar candidatos a solicitud titulo',
        'write' => 'Registro de solicitud de titulo ',
        'admin' => 'Solicitud de titulo',
//        'label' => 'Solicitud de titulo'
    );

    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('registroSolicitudTitulo'),
                'pbac' => array('write', 'admin', 'read'),
            ),
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('indexTitulo', 'MostrarSolicitanteAlTitulo', 'reportePorPlantel'),
                'pbac' => array('read'),
            ),
//            array('allow', // allow all users to perform 'index' and 'view' actions
//                'actions' => array('indexTitulo', 'MostrarSolicitanteAlTitulo'),
//                'pbac' => array('read','write','admin'),
//            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    const MODULO = "Titulo.Solicitud";

    public function actionIndexTitulo($id) {
        $plantel_id = base64_decode($id);
        $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
        $periodo_actual_id = $periodo_escolar_actual_id['id'];
        $tieneSolicitudes = Titulo::model()->tieneSolicitudes($plantel_id, $periodo_actual_id);
        $plantelPK = Plantel::model()->findByPk($plantel_id);
        $nombrePlantel = $plantelPK['nombre'];

        if (!is_numeric($plantel_id)) {
            throw new CHttpException(404, 'No se ha encontrado el plantel que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
        } else {
            $this->render('indexTitulo', array(
                'plantel_id' => $plantel_id,
                'tieneSolicitudes' => $tieneSolicitudes,
                'nombrePlantel' => $nombrePlantel
            ));
        }
    }

    /**
     * Lists all models.
     */
    //	public function actionIndex()
    //	{
    //		$dataProvider=new CActiveDataProvider('Titulo');
    //		$this->render('index',array(
    //			'dataProvider'=>$dataProvider,
    //		));
    //	}

    /**
     * Manages all models.
     */
//    public function actionIndex() {
//        $model = new Titulo('search');
//        $model->unsetAttributes();  // clear any default values
//        if (isset($_GET['Titulo']))
//            $model->attributes = $_GET['Titulo'];
//
//        $this->render('admin', array(
//            'model' => $model,
//        ));
//    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Titulo the loaded model
     * @throws CHttpException
     */
    public function actionMostrarSolicitanteAlTitulo($id) {

        $plantel_id = base64_decode($id);


        if (!is_numeric($plantel_id)) {
            throw new CHttpException(404, 'No se ha encontrado la solicitud de titulo. Vuelva a la página anterior e intentelo de nuevo.'); // es numerico
        } else {


            if ($plantel_id != '') {
                $plantelPK = Plantel::model()->findByPk($plantel_id);

                if ($plantelPK == null) {
                    $this->redirect('../../../consultar');
                }
            } else {

                $this->redirect('../../../consultar');
            }

            $resulSolicitudDeTitulo = Titulo::model()->solicitudTitulo($plantel_id);

//            $cantidaArreglo = count($resulSolicitudDeTitulo);
//
//            var_dump($resulSolicitudDeTitulo);
//            die();

            if ($resulSolicitudDeTitulo == array()) {
                $compararFinalExistoso = 0;
                $this->render('mensajeFinal', array(
                    'compararFinalExistoso' => $compararFinalExistoso,
                    'plantel_id' => $plantel_id
                        ), FALSE, TRUE);


//              $this->createUrl('mensajeFinal');
            } else {

                Yii::app()->session['solicitud'] = $resulSolicitudDeTitulo;

                if ($resulSolicitudDeTitulo) {

                    $dataSolicitud = $this->dataProviderSolicitudTitulo($resulSolicitudDeTitulo);
                    $this->render('admin', array('dataProvider' => $dataSolicitud,
                        'plantel_id' => $plantel_id,
                        'plantelPK' => $plantelPK
                            ), FALSE, TRUE);
                } else {

                }
            }
        }
    }

    function dataProviderSolicitudTitulo($resulSolicitudDeTitulo) {
        foreach ($resulSolicitudDeTitulo as $key => $value) {
            $documento_identidad = (isset($value['documento_identidad']) AND $value['documento_identidad'] != '' ) ? $value['documento_identidad'] : '';
            $tDocumento_identidad = (isset($value['tdocumento_identidad']) AND $value['tdocumento_identidad'] != '' ) ? $value['tdocumento_identidad'] : '';
            $cedula_escolar = (isset($value['cedula_escolar']) AND $value['cedula_escolar'] != '' ) ? $value['cedula_escolar'] : '';
            $nombres = (isset($value['nombres']) AND $value['nombres'] != '' ) ? $value['nombres'] : '';
            $apellidos = (isset($value['apellidos']) AND $value['apellidos'] != '' ) ? $value['apellidos'] : '';
            $plantel_id = (isset($value['plantel_actual_id']) AND $value['plantel_actual_id'] != '' ) ? $value['plantel_actual_id'] : '';
            $plan_nombre = (isset($value['nombreplan']) AND $value['nombreplan'] != '' ) ? $value['nombreplan'] : '';
            $nombre_seccion = (isset($value['nombre_seccion']) AND $value['nombre_seccion'] != '' ) ? $value['nombre_seccion'] : '';
            $grado = (isset($value['grado']) AND $value['grado'] != '' ) ? $value['grado'] : '';
            $mencion = (isset($value['mencion']) AND $value['mencion'] != '' ) ? $value['mencion'] : '';

            $plan_mencion = $plan_nombre . '[' . $mencion . ']';
            $nombreApellido = $nombres . ' ' . $apellidos;

            if ($documento_identidad != '' AND $documento_identidad != null) {
                if ($tDocumento_identidad != '' AND $tDocumento_identidad != null) {
                    $identificacion = $tDocumento_identidad . '-' . $documento_identidad;
                } else {
                    $identificacion = $documento_identidad;
                }
            } else {
                if ($cedula_escolar != '' AND $cedula_escolar != null) {
                    $identificacion = 'C.E: ' . $cedula_escolar;
                } else {
                    $identificacion = 'NO POSEE';
                }
            }

            $botones = "<div class='center'>" . CHtml::checkBox('EstSolicitud[]', "false", array('value' => base64_encode($value['id']),
//                   'onClick' => "Estudiante('')",
                        "title" => "solicitud")
                    ) .
                    "</div>";
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'documento_identidad' => '<center>' . $identificacion . '</center>',
                'nombreApellido' => '<center>' . $nombreApellido . '</center>',
                'plan_nombre' => '<center>' . $plan_mencion . '</center>',
                'nombre_seccion' => "<center>" . $nombre_seccion . '</center>',
                'grado' => "<center>" . $grado . "</center>",
                'botones' => '<center>' . $botones . '</center>'
            );
        }
        // var_dump($rawData);
        //  die();
        return new CArrayDataProvider($rawData, array(
            'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
                )
        );
    }

    public function actionRegistroSolicitudTitulo() {
        if (Yii::app()->request->isAjaxRequest) {


            $model = new Titulo;
            $resulSolicitudDeTitulo = Yii::app()->session->get('solicitud');
            $estudianteSeleccionado = $_POST['EstSeleccionado'];


            $estudianteSeleccionado = array_unique($estudianteSeleccionado);
            foreach ($estudianteSeleccionado as $key => $value) {

                $estudianteSeleccionado[$key] = base64_decode($value);
//            var_dump($estudianteSeleccionado);
//            die();
            }

            foreach ($estudianteSeleccionado as $keY2 => $value2) {

                foreach ($resulSolicitudDeTitulo as $key1 => $value1) {


                    if ((int) $value2 == $resulSolicitudDeTitulo[$key1]['id']) {
//                        echo "paso por el if";
//                        echo '<br>';
                        $idEstudiante[] = $resulSolicitudDeTitulo[$key1]['id'];
                        $gradoIdEstudiante[] = $resulSolicitudDeTitulo[$key1]['id_grado'];
                        $seccionIDEstudiante[] = $resulSolicitudDeTitulo[$key1]['id_seccion'];
                        $planIdEstudiante[] = $resulSolicitudDeTitulo[$key1]['plan_id'];
                        $seccionPlantelPeriodoIdEstudiante[] = $resulSolicitudDeTitulo[$key1]['seccion_plantel_periodo_id'];
                        $estatus[] = "A";
                        $tipoDocumentoId[] = 1;
                        $usuarioIniId[] = Yii::app()->user->id;
                        $periodoId[] = $resulSolicitudDeTitulo[$key1]['periodo_id'];
                        $estatusSolicitudId [] = 1;
                        $estatusActualId[] = 11;
                        $plantelId[] = $resulSolicitudDeTitulo[$key1]['plantel_actual_id'];

//                        var_dump($idEstudiante);
//                        var_dump($gradoIdEstudiante);
//                        var_dump($seccionIDEstudiante);
//                        var_dump($planIdEstudiante);
//                        var_dump($seccionPlantelPeriodoIdEstudiante);
//                        var_dump($tipoDocumentoId);
//                        var_dump($usuarioIniId);
//                        var_dump($periodoId);
//                        var_dump($estatusSolicitudId);
//                        var_dump($estatusActualId);
//                        var_dump($plantelId);
                    }
                }
            }
            $idEstudiante_pg_array = Utiles::toPgArray($idEstudiante);
            $gradoIdEstudiante_pg_array = Utiles::toPgArray($gradoIdEstudiante);
            $seccionIDEstudiante_pg_array = Utiles::toPgArray($seccionIDEstudiante);
            $planIdEstudiante_pg_array = Utiles::toPgArray($planIdEstudiante);
            $seccionPlantelPeriodoIdEstudiante_pg_array = Utiles::toPgArray($seccionPlantelPeriodoIdEstudiante);
            $estatus_pg_array = Utiles::toPgArray($estatus);
            $tipoDocumentoId_pg_array = Utiles::toPgArray($tipoDocumentoId);
            $usuarioIniId_pg_array = Utiles::toPgArray($usuarioIniId);
            $periodoId_pg_array = Utiles::toPgArray($periodoId);
            $estatusSolicitudId_pg_array = Utiles::toPgArray($estatusSolicitudId);
            $estatusActualId_pg_array = Utiles::toPgArray($estatusActualId);
            $plantelId_pg_array = Utiles::toPgArray($plantelId);
//            var_dump($plantelId[0]);
//            die();
            $modulo = self::MODULO;
            $ip = Yii::app()->request->userHostAddress;
            $username = Yii::app()->user->name;
            $transaction = Yii::app()->db->beginTransaction();

            try {

                $resultadoProcesoAlm = Titulo::model()->registroSolicitud($idEstudiante_pg_array, $gradoIdEstudiante_pg_array, $seccionIDEstudiante_pg_array, $planIdEstudiante_pg_array, $seccionPlantelPeriodoIdEstudiante_pg_array, $estatus_pg_array, $tipoDocumentoId_pg_array, $usuarioIniId_pg_array, $periodoId_pg_array, $estatusSolicitudId_pg_array, $estatusActualId_pg_array, $plantelId_pg_array, $modulo, $ip, $username);


//                $respuesta['statusCode'] = 'success';
//                $respuesta['mensaje'] = 'Estimado Usuario, el proceso de solicitud de título se ha realizado exitosamente.';
//                var_dump($estudianteTituloSolicitado);
//                die();

                $compararFinalExistoso = 1;
                if ($compararFinalExistoso == 1) {
                    $estudianteTituloSolicitado = Titulo::model()->mostrarSolicitudtitulo($plantelId[0]);
                    $dataSolicitud = $this->dataProviderMostrarSolicitudTitulo($estudianteTituloSolicitado);
                    $this->renderPartial('mensajeFinal', array('dataSolicitud' => $dataSolicitud,
                        'plantel_id' => $plantelId[0],
                        'compararFinalExistoso' => $compararFinalExistoso
                            ), FALSE, TRUE);
                }
                $transaction->commit();
                unset(Yii::app()->session['solicitud']);
                $_SESSION['solicitud'] = NULL;

                // var_dump($resultadoProcesoAlm);
            } catch (Exception $ex) {


                $transaction->rollback();

                $respuesta['statusCode'] = 'alert';
                $error = $ex->getMessage();
                //      Utiles::enviarCorreo('jeanb241@gmail.com', 'Notificación de error en la solicitud de titulo', $error);
                $capturarCadena = explode("*", $error);
//                var_dump($capturarCadena);
//                die();
                $mensajeSerial = $capturarCadena[1] . ' CI: ' . $capturarCadena[2];

                $respuesta['error'] = $mensajeSerial;
                echo json_encode($respuesta);
            }

//            $this->renderpartial('mensajeFinal', array('model' => $model));
//            $this->createUrl('mensajeFinal');
//            Yii::app()->redo
//            Yii::app()->end();
        } else {

            throw new CHttpException(403, 'No está permitido efectuar la petición de este recurso por esta vía.');
        }




//        var_dump($estudianteSeleccionado);
//
//        die();
    }

    function dataProviderMostrarSolicitudTitulo($resulSolicitudDeTitulo) {
        foreach ($resulSolicitudDeTitulo as $key => $value) {
            $cedula_identidad = $value['documento_identidad'];
            //$serial = $value['serial'];
            $nombres = $value['nombres'];
            $apellidos = $value['apellidos'];
            $nombre_mencion = $value['nombre_mencion'];
            $nombre_seccion = $value['nombre_seccion'];
            $grado = $value['nombre_grado'];
            $nombreApellido = $nombres . ' ' . $apellidos;
//            $columna .= CHtml::checkBox('repitiente[]', false, array('id' => 'repitiente[]', 'title' => 'Repitente')) . '&nbsp;&nbsp;&nbsp;';
            $rawData[] = array(
                'id' => $key,
                'documento_identidad' => '<center>' . $cedula_identidad . '</center>',
                //'serial' => '<center>' . $serial . '</center>',
                'nombreApellido' => '<center>' . $nombreApellido . '</center>',
                'nombre_seccion' => "<center>" . $nombre_seccion . '</center>',
                'grado' => "<center>" . $grado . "</center>",
                'nombre_mencion' => "<center>" . $nombre_mencion . '</center>',
            );

            // var_dump($rawData);
            //  die();
        }
        return new CArrayDataProvider($rawData, array(
            'pagination' => false,
                //    'pagination' => array(
                //      'pageSize' => 5,
                //),
                )
        );
    }

    public function actionReportePorPlantel() {

        $plantel_id = $this->getRequest('id');
        $plantel_id_decoded = base64_decode($plantel_id);
        $modulo = "Planteles.Titulo.ReportePorPlantel";
        $ip = Yii::app()->request->userHostAddress;
        $usuario_id = Yii::app()->user->id;
        $username = Yii::app()->user->name;
        //   var_dump($plantel_id_decoded . ' plantel');
        if (is_numeric($plantel_id_decoded)) {
            $periodo_escolar_actual_id = PeriodoEscolar::model()->getPeriodoActivo();
            $periodo_actual_id = $periodo_escolar_actual_id['id'];
            $secciones = Titulo::model()->obtenerSeccionDeTitulo($plantel_id_decoded, $periodo_actual_id);
            $datosPlantel = Plantel::model()->obtenerDatosPlantel($plantel_id_decoded, $periodo_actual_id);
//            var_dump($datosPlantel);
//            die();
//            $datosEstudiantes = Estudiante::model()->obtenerDatosEstudiantesReporte($datosPlantel, $estado_id_decoded, $periodo_actual_id);
            $datosReporte = Titulo::model()->obtenerDatosReporteSerialesPorPlantel($plantel_id_decoded, $periodo_actual_id, $secciones);
//            var_dump($datosReporte);
//            die();
            $nombre_pdf = $datosPlantel['nombre_plantel'] . ' - ' . $datosPlantel['cod_plantel'];
            $mpdf = new mpdf('', 'LEGAL', 0, '', 15, 15, 53, 50);            //$mpdf->SetMargins(3,69.1,70);
            $header = $this->renderPartial('_headerReporteEstudiantesConSerialesPorPlantel', array('datosPlantel' => $datosPlantel), true);
            //     $footer = $this->renderPartial('_footerReporteEstudiantesConSerialesPorPlantel', array('datosAutoridades' => $datos_autoridades), true);
            $body = $this->renderPartial('_bodyReporteEstudiantesConSerialesPorPlantel', array('datosPlantel' => $datosPlantel, 'datosReporte' => $datosReporte), true);
            $mpdf->SetFont('sans-serif');
            $mpdf->SetHTMLHeader($header);
            //    $mpdf->setHTMLFooter($footer . '<br>' . '<p style="text-align:center;"> {PAGENO} / {nb}</p>');
            $mpdf->WriteHTML($body);

            //  $this->registerLog('LECTURA', self::MODULO . 'Reporte', 'EXITOSO', 'Entró matricular la Seccion Plantel' . $seccion_plantel_id);

            $mpdf->Output($nombre_pdf . '.pdf', 'D');
            $this->registerLog('REPORTES', $modulo . 'PDF', 'EXITOSO', 'Descarga del pdf con el nombre: ' . $nombre_pdf . ' del plantel ' . $datosPlantel['nombre_plantel'], $ip, $usuario_id, $username);
        } else
            throw new CHttpException(404, 'Estimado Usuario, no se ha encontrado el recurso solicitado. Vuelva a la página anterior e intente de nuevo');
    }

    public function columnaDocumentoIdentidad($value) {
        $documento_identidad = (isset($value['documento_identidad']) AND $value['documento_identidad'] != '' ) ? $value['documento_identidad'] : '';
        $tDocumento_identidad = (isset($value['tdocumento_identidad']) AND $value['tdocumento_identidad'] != '' ) ? $value['tdocumento_identidad'] : '';
        $cedula_escolar = (isset($value['cedula_escolar']) AND $value['cedula_escolar'] != '' ) ? $value['cedula_escolar'] : '';

        if ($documento_identidad != '' AND $documento_identidad != null) {
            if ($tDocumento_identidad != '' AND $tDocumento_identidad != null) {
                $identificacion = $tDocumento_identidad . '-' . $documento_identidad;
            } else {
                $identificacion = $documento_identidad;
            }
        } else {
            if ($cedula_escolar != '' AND $cedula_escolar != null) {
                $identificacion = 'C.E: ' . $cedula_escolar;
            } else {
                $identificacion = 'NO POSEE';
            }
        }

        return $identificacion;
    }

}
