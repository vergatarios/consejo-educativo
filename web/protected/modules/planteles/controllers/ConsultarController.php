<?php

class ConsultarController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Consulta de Planteles',
        //  'write' => 'Consulta de Planteles', // no lleva etiqueta write
        'label' => 'Consulta de Planteles'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
                //'accessControl', // perform access control for CRUD operations
                //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow', // allow all users to perform 'index' and 'view' actions
                'actions' => array('index', 'view', 'informacion', 'seleccionarMunicipio', 'seleccionarParroquia', 'reporte', '_reportePlantel', 'prueba', '_reportePlantelCE', 'informacionAula'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update'),
                'users' => array('write'),
            ),
            /* array('allow', // allow admin user to perform 'admin' and 'delete' actions
              'actions'=>array('admin','delete'),
              'users'=>array('@'),
              ), */
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }

    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Este metodo es creado para mostrar la consulta
     * de un plantel en especifico.
     * Developed for: Luis Zambrano
     */
    public function action_reportePlantel($id) {
        #$model = new Plantel;
        $this->renderPartial('_reportePlantel', array('model' => $this->loadModel($id)));
    }

    public function action_reportePlantelCE($id) {
        #$model = new Plantel;
        $this->renderPartial('_reportePlantelCE', array('model' => $this->loadModel($id)));
    }

    public function actionInformacion($id) {

        $groupId = Yii::app()->user->group;
        $usuarioId = Yii::app()->user->id;
        $id = base64_decode($id);

        //$u = UserGroups::OPER_ZONA;
        //var_dump($groupId.'-'.UserGroups::COORD_ZONA);die();

        $plantel = new Plantel;

        /* SI ES UNA SECRETARIA */
        if ((UserGroups::OPER_PLANTEL == $groupId)) {
            $resultado = $plantel->identificacionUsuario($usuarioId, $id, 1);
            if ($resultado == 0) {
                throw new CHttpException(403, "Usted no tiene permiso para acceder a esta acción.");
            }
        }
        /* SI ES UN USUARIO DE OPERADOR DE ZONA EDUCATIVA */
        if ((UserGroups::COORD_ZONA == $groupId)) {
            $resultado = $plantel->identificacionUsuario($usuarioId, $id, 2);
            if ($resultado == 0) {
                throw new CHttpException(403, "Usted no tiene permiso para acceder a esta acción.");
            }
        }
        /* SI ES UN USUARIO ES UN DIRECTOR */
        /* if((UserGroups::DIRECTOR == $groupId))
          {
          $resultado = $plantel->identificacionUsuario($usuarioId, $id,3);
          if($resultado == 0)
          {
          throw new CHttpException(403, "Usted no tiene permiso para acceder a esta acción.");
          }
          } */



        if ((isset($id)) && ($id != '') && (is_numeric($id))) {
            $model = $this->loadModel($id);
            //var_dump($model->modalidad_id);die();

            $modelAula = new Aula('search');
            $modelAula->unsetAttributes();

            if ($model) {
                $this->render('informacion', array(
                    'model' => $model,
                    'modelAula' => $modelAula
                ));
            } else {
                throw new CHttpException(404, "Recurso no encontrado.");
            }
        } else if (!isset($_REQUEST['Aula']['plantel_id'])) {
            throw new CHttpException(404, "Recurso no encontrado.");
        }
        if ((isset($_REQUEST['Aula'])) || (isset($_REQUEST['ajax']))) {
            //$id = $_REQUEST['id'];
            //$plantel_id = base64_decode($id);
            $modelAula = new Aula('search');
            $modelAula->unsetAttributes();  // clear any default values
            if (isset($_GET['Aula']['plantel_id'])) {
                $modelAula->attributes = $_GET['Aula'];
                $plantel_id = $_REQUEST['Aula']['plantel_id'];
                $modelAula->plantel_id = $plantel_id;
            }
            if (isset($_GET['plantel_id'])) {
                $plantel_id = $_REQUEST['plantel_id'];
                $modelAula->plantel_id = $plantel_id;
            }
            $model = Plantel::model()->findAll(array('condition' => 'id = ' . $plantel_id));
            $this->render('_formAula', array(
                'modelPlantel' => $model,
                'model' => $modelAula,
                'plantel_id' => $plantel_id,
            ));
        }
    }

    public function actionSeleccionarMunicipio() {
        $item = $_REQUEST['Plantel']['estado_id'];

        if ($item == '' || $item == NULL) {
            $lista = array('' => '-Seleccione-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = CMunicipio::getData('estado_id', $item);
            $lista = CHtml::listData($lista, 'id', 'nombre');

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-Seleccione-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    public function actionSeleccionarParroquia() {
        $item = $_REQUEST['Plantel']['municipio_id'];

        if ($item == '' || $item == NULL) {
            $lista = array('' => '-Seleccione-');
            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        } else {
            $lista = CParroquia::getData('municipio_id', $item);
            $lista = CHtml::listData($lista, 'id', 'nombre');

            echo CHtml::tag('option', array('value' => ''), CHtml::encode('-Seleccione-'), true);

            foreach ($lista as $valor => $descripcion) {
                echo CHtml::tag('option', array('value' => $valor), CHtml::encode($descripcion), true);
            }
        }
    }

    /* FIN DEL MODULO */

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Plantel'])) {
            $model->attributes = $_POST['Plantel'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

        $model = new Plantel('search');

        $groupId = Yii::app()->user->group;
        $usuarioId = Yii::app()->user->id;
        $groupName = Yii::app()->user->groupname;

        #echo '<script>alert("'.$groupName.'")</script>';

        /* OBTENGO EL ESTADO_ID DEL USUARIO */
        if ($groupId == 25) {
            $estadoId = $model->estadoId($usuarioId);
        } else {
            $estadoId = '';
        }

        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Plantel']))
            $model->attributes = $_GET['Plantel'];

        $this->render('index', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'estadoId' => $estadoId,
            'groupName' => $groupName,
        ));
    }

    public function actionInformacionAula($id) {
        $this->renderPartial('informacionAula', array(
            'model' => $this->loadAula($id),
        ));
    }

    public function columnaAcciones($data) {

        $id = $data["id"];

        $estatus = (is_object($data->estatusPlantel) && isset($data->estatusPlantel->nombre)) ? $data->estatusPlantel->nombre : "";

        $estatus = strtoupper($estatus);

        $groupId = Yii::app()->user->group;
        $usuarioId = Yii::app()->user->id;

        $columna = '<div class="btn-group">
                        <button class="btn btn-xs dropdown-toggle" data-toggle="dropdown">
                            Seleccione
                            <span class="icon-caret-down icon-on-right"></span>
                        </button>
                        <ul class="dropdown-menu dropdown-yellow pull-right">';
        $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Consultar Datos</span>", Yii::app()->createUrl("/planteles/consultar/informacion/?id=" . base64_encode($data->id)), array("class" => "fa fa-search-plus ", "title" => "Consultar Datos del Plantel")) . '</li>';
        if ($estatus == 'ACTIVO') {
            if (Yii::app()->user->pbac('planteles.modificar.read') or Yii::app()->user->pbac('planteles.modificar.write') or Yii::app()->user->pbac('planteles.modificar.admin')) {
                $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Modificar Datos</span>", Yii::app()->createUrl("/planteles/modificar/index?id=" . base64_encode($data->id)), array("class" => "fa fa-pencil green", "title" => "Modificar Datos del Plantel")) . '</li>';
            }

            //if ((Yii::app()->user->id == UserGroups::ADMIN_0) || (Yii::app()->user->id == UserGroups::ADMIN_2)) {
            // if(Yii::app()->user->pbac('planteles.nivel.write') or  Yii::app()->user->pbac('planteles.planes.write') or $groupId != UserGroups::DIRECTOR)

            if (Yii::app()->user->pbac('planteles.nivelPlantel.read') or Yii::app()->user->pbac('planteles.nivelPlantel.write') or Yii::app()->user->pbac('planteles.nivelPlantel.admin')) {
                /* NIVELES */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Niveles</span>", "/planteles/nivelPlantel/index/id/" . base64_encode($data->id), array("class" => "fa fa-sitemap orange", "title" => "Niveles del Plantel")) . '</li>';
            }
            if (Yii::app()->user->pbac('planteles.planes.read') or Yii::app()->user->pbac('planteles.planes.write') or Yii::app()->user->pbac('planteles.planes.admin')) {
                /* PLAN ESTUDIO */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Plan de Estudio</span>", "/planteles/planes/consultar/id/" . base64_encode($data->id), array("class" => "fa fa-book red", "title" => "Planes de Estudio")) . '</li>';
            }
            //if($groupId != UserGroups::ADMIN_REG_CONTROL || ($groupId == UserGroups::JEFE_DRCEE))
            //{
            if (Yii::app()->user->pbac('planteles.seccionPlantel.read') or Yii::app()->user->pbac('planteles.seccionPlantel.write') or Yii::app()->user->pbac('planteles.seccionPlantel.admin')) {
                /* SECCIONES */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Matricula 2013-2014</span>", "/planteles/seccionPlantel/admin/id/" . base64_encode($data->id), array("class" => "fa fa-bookmark pink", "title" => "Consultar Secciones del Plantel")) . '</li>';
            }
            if (Yii::app()->user->pbac('planteles.seccionPlantel15.read') or Yii::app()->user->pbac('planteles.seccionPlantel15.write') or Yii::app()->user->pbac('planteles.seccionPlantel15.admin')) {
                /* SECCIONES */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Matricula 2014-2015</span>", "/planteles/seccionPlantel15/admin/id/" . base64_encode($data->id), array("class" => "fa fa-bookmark pink", "title" => "Consultar Secciones del Plantel")) . '</li>';
            }
//                    if($groupId == UserGroups::ADMIN_0 || $groupId == UserGroups::DIRECTOR || $groupId == UserGroups::ADMIN_1 || ($groupId == UserGroups::JEFE_DRCEE))
//                    {
            /* if (Yii::app()->user->pbac('planteles.matricula.read') and Yii::app()->user->pbac('planteles.matricula.write')) {
              //MATRICULA
              $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Matrícula</span>", "/planteles/seccionPlantel/admin/id/" . base64_encode($data->id), array("class" => "fa fa-users orange", "title" => "Consultar Matrícula")) . '</li>';
              } */
//                    }
            // }
            //}
            /* IMPRIMIR DATOS */
            if (Yii::app()->user->pbac('planteles.titulo.read')) {
                /* TITULO */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Título</span>", "/planteles/Titulo/indexTitulo/id/" . base64_encode($data->id), array("class" => "fa fa-graduation-cap", "title" => "Título")) . '</li>';
            }

            if (Yii::app()->user->pbac('titulo.seguimientoTitulo.read') || Yii::app()->user->pbac('titulo.seguimientoTitulo.write')) {
                if (in_array(Yii::app()->user->group, array(UserGroups::DIRECTOR))) {
                    /* SEGUIMIENTO TITULO */
                    $usuario_id = Yii::app()->user->id;
                    $grupo_id = UserGroups::DIRECTOR;
                    $plantel_id = null;
                    $existePlantelTitulo = Titulo::model()->verificarMostrarIcono($usuario_id, $grupo_id, $variable = 'D', $plantel_id);
                    if ($existePlantelTitulo != false)
                        $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Otorgar Título</span>", "/titulo/seguimientoTitulo/indexPlantel/id/" . base64_encode($data->id), array("class" => "fa fa-graduation-cap", "title" => "Verificar Entrega de Título a Estudiante")) . '</li>';
                }
                if (in_array(Yii::app()->user->group, array(UserGroups::JEFE_DRCEE, UserGroups::ADMIN_0))) {
                    /* SEGUIMIENTO TITULO */
                    $usuario_id = Yii::app()->user->id;
                    $grupo_id = UserGroups::JEFE_DRCEE;
                    $plantel_id = (int) $data->id;
                    $existePlantelTitulo = Titulo::model()->verificarMostrarIcono($usuario_id, $grupo_id, $variable = 'JD', $plantel_id);
                    //  var_dump($existePlantelTitulo);
                    // die();
                    if ($existePlantelTitulo != false)
                        $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Imprimir Hoja de Registro Titulo</span>", "/titulo/seguimientoTitulo/ReporteSerialesEntregados/plantel/" . base64_encode($data->id), array("class" => "fa fa-print", "title" => "Imprimir Hoja de Registro Titulo")) . '</li>';
                }
            }

            if (Yii::app()->user->pbac('estudiante.modificar.read') or Yii::app()->user->pbac('estudiante.modificar.write') or Yii::app()->user->pbac('estudiante.modificar.admin')) {
                /* ESTUDIANTES */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Estudiantes</span>", "/estudiante/?bc=1&id=" . base64_encode($data->id), array("class" => "fa fa-users red", "title" => "Estudiantes")) . '</li>';
            }
            if (Yii::app()->user->pbac('planteles.coleccionBicentenaria.read')) {
                /* Coleccion Bicentenaria */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Colección Bicentenaria</span>", "/planteles/ColeccionBicentenaria/index/id/" . base64_encode($data->id), array("class" => "fa fa-code-fork", "title" => "Colección Bicentenaria")) . '</li>';
            }
            if (Yii::app()->user->pbac('planteles.estructura.read')) {
                /* Docentes */$columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Estructura del Plantel</span>", "/planteles/estructura/lista/id/" . base64_encode($data->id), array("class" => "fa fa-male green", "title" => "Estructura del Plantel")) . '</li>';
            }
        }
        $columna .= '<li>' . CHtml::link("<span style='font-family:Helvetica Neue,Arial,Helvetica,sans-serif;'>&nbsp;&nbsp;Imprimir Datos</span>", "/planteles/consultar/reporte/id/" . base64_encode($data->id), array("class" => "fa fa-print blue", "title" => "Imprimir Datos del Plantel")) . '</li>';
        $columna .= '</ul></div>';

        return $columna;
    }

    public function columnaAccionesAula($data)
    /*
     * Botones del accion (crear, consultar)
     */ {
        $id = $data["id"];
        $estatus = $data["estatus"];
        $columna = CHtml::link("", "#", array("class" => "fa fa-search", "onClick" => "consultarAula($id,'../')", "title" => "Consultar este aula")) . '&nbsp;&nbsp;';

        return $columna;
    }

    public function columnaEstatus($data) {
        $estatus = $data['estatus'];
        if (($estatus == 'A') || ($estatus == '')) {
            return 'Activo';
        } else if ($estatus == 'E') {
            return 'Inactivo';
        }
        return $columna;
    }

    //GENERAR REPORTES EN PDF
    public function actionReporte() {
        if (isset($_GET['id'])) {
            $idPlantel = base64_decode($_GET['id']);
            //var_dump((int)$idPlantel);die();
            if (is_numeric($idPlantel)) {

                $groupName = Yii::app()->user->groupname;
                if (($groupName == 'JCEE-PLANTEL' || $groupName == 'JEFE-DRCEE') || ($groupName == 'root')) {
                    $reporte = '_reportePlantel';
                }/* JEFE O ADMIN */ else if ($groupName == 'CCEE-PLANTEL') {
                    $reporte = '_reportePlantelCE';
                }/* COORDINADOR DE CONTROL DE ESTUDIOS DE PLANTEL */ else if ($groupName == 'COORD-ZONA' || $groupName == 'JEFE-ZONA') {
                    $reporte = '_reportePlantelCE';
                }/* COORDINADOR DE ZONA */ else if ($groupName == 'DIRECTOR') {
                    $reporte = '_reportePlantelD';
                }/* DIRECTOR */ else {
                    $reporte = '_reportePlantelD';
                }
                //var_dump($groupName);die();
                if (isset($idPlantel)) {
                    $plantel = Plantel::model()->findByPk($idPlantel);
                    if ($plantel) {
                        $mPDF = Yii::app()->ePdf->mpdf();
                        $mPDF->WriteHTML($this->renderPartial('_pdfHeader', array(), true));
                        $mPDF->WriteHTML($this->renderPartial($reporte, array('model' => $plantel), true));
                        $mPDF->Output($plantel->cod_plantel . '.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
                    }
                } else {
                    $this->redirect(array('consultar/index'));
                }
            } else {
                throw new CHttpException(404, "Recurso no encontrado.");
            }
        } else {
            throw new CHttpException(404, "Recurso no encontrado.");
        }
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Plantel the loaded model
     * @throws CHttpException
     */
    public function loadAula($id) {
        $model = Aula::model()->findByPk($id);

        if ($model === null)
            throw new CHttpException(404, 'No se ha encontrado el Aula que ha solicitado. Vuelva a la página anterior e intentelo de nuevo.');
        return $model;
    }

    public function loadModel($id) {
        $model = Plantel::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Plantel $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'plantel-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
