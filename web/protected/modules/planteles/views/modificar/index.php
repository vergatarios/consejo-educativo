<div class="tabbable">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#datosGenerales" data-toggle="tab">Datos generales</a></li>
        <li><a href="#desarrollo" data-toggle="tab">Desarrollo endogeno</a></li>
        <li><a href="#servicio" data-toggle="tab">Servicios</a></li>
        <li><a href="#autoridades" data-toggle="tab">Autoridades</a></li>
        <li><a href="#otros" data-toggle="tab">Otros</a></li>
        <li><a href="#aula" data-toggle="tab">Aula</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="datosGenerales">
            <?php
            $this->renderPartial('_datosGenerales', array(
                'model' => $model,
                'proyectoEndo' => $proyectoEndo,
                'usuario' => $usuario,
                'autoridadPlantel' => $autoridadPlantel,
                'distrito' => $distrito,
                'eponimo' => $eponimo,
                'tipoDependencia' => $tipoDependencia,
                'zonaEducativa' => $zonaEducativa,
                'estatusPlantel' => $estatusPlantel,
                'clasePlantel' => $clasePlantel,
                'categoria' => $categoria,
                'condicionEstudio' => $condicionEstudio,
                'regimenEstudio' => $regimenEstudio,
                'genero' => $genero,
                'estado' => $estado,
                'municipio' => $municipio,
                'parroquia' => $parroquia,
                'turno' => $turno,
                'models' => $models,
                'list' => $list,
                'tipo_ubicacion' => $tipo_ubicacion,
                'proyectosEndogenos' => $proyectosEndogenos,
                'cargoSelect' => $cargoSelect
            ));
            ?>
        </div>

        <div class="tab-pane" id="desarrollo">
            <?php
            $this->renderPartial('_formEndogeno', array('proyectoEndo' => $proyectoEndo,
                'proyectosEndogenos' => $proyectosEndogenos,
                'plantel_id' => $id));
            ?>
        </div>

        <!-- ignacio -->
        <div class="tab-pane" id="servicio">
            <?php $this->renderPartial('_formServicio', array('list' => $list, 'plantel_id' => $id)); ?>
        </div>

        <div class="tab-pane" id="autoridades">
            <?php $this->renderPartial('_formAutoridades', array('autoridadPlantel' => $autoridadPlantel, 'cargoSelect' => $cargoSelect, 'usuario' => $usuario, 'plantel_id' => $id)); ?>
        </div>
        <!--<div class="tab-pane" id="otro">Otro</div>-->
        <div class="tab-pane" id="aula">Aula</div>
    </div>
</div>