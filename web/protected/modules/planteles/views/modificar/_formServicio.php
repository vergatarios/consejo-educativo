<script >

</script>
<div class="form" id="_formServicio">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'plantelServicios-form',
        //  'action' => 'guardarServicios',
        // Please note: When you enable ajax validation, make sure the corresponding
        // controller action is handling ajax validation correctly.
        // There is a call to performAjaxValidation() commented in generated controller code.
        // See class documentation of CActiveForm for details on this.
        'enableAjaxValidation' => false,
    ));
    ?>                    
    <?php echo CHtml::hiddenField('plantel_id', $plantel_id, array('id' => 'plantel_id')); ?>
    <div class="tab-pane active" id="servicio">
        <div id="servicioCalid" class="widget-box">

            <div id="resultadoPlantelServicio">
            </div>

            <div id="resultadoServicio" class="infoDialogBox">
                <p>
                    Debe Seleccionar los Servicios del Plantel.
                </p>
            </div>

            <div id ="guardoServicio" class="successDialogBox" style="display: none">
                <p>
                    REGISTRO EXITOSO
                </p>
            </div>

            <div class="widget-header">
                <h5>Servicios</h5>
                <div class="widget-toolbar">
                    <a  href="#" data-action="collapse">
                        <i class="icon-chevron-up"></i>
                    </a>
                </div>
            </div>

            <div id="servicio" class="widget-body" >
                <div class="widget-body-inner" >
                    <div class="widget-main form">  

                        <div class="row">

                            <div class="col-md-4">

                                <label for="servicios">Servicios<span class="required"></span></label><br>
                                <?php
                                echo CHtml::dropDownList('servicios', 'id', $listServicios, array(
                                    'empty' => 'Seleccione un servicio',
                                    'onChange' => 'condicionarServicios(' . $plantel_id . ')'
                                        )
                                );
                                ?>
                            </div>
                            <div class="col-md-8" id ="serviciosUsados">
                                <?php
                                if (isset($dataProvider) && $dataProvider !== array()) {
                                    // var_dump($dataProvider); die();
                                    $this->widget(
                                            'zii.widgets.grid.CGridView', array(
                                        'id' => 'servicio-grid',
                                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                        // 40px is the height of the main navigation at bootstrap
                                        'dataProvider' => $dataProvider,
                                        'summaryText' => false,
                                        'columns' => array(
                                            array(
                                                'name' => 'servicio',
                                                'type' => 'raw',
                                                'header' => '<center>Servicio</center>'
                                            ),
                                            array(
                                                'name' => 'calidad',
                                                'type' => 'raw',
                                                'header' => '<center>Calidad del Servicio</center>'
                                            ),
                                            array(
                                                'name' => 'fecha_desde',
                                                'type' => 'raw',
                                                'header' => '<center>Fecha Desde</center>'
                                            ),
                                            array(
                                                'name' => 'boton',
                                                'type' => 'raw',
                                                'header' => '<center>Acciones</center>'
                                            ),
                                        ),
                                        'pager' => array(
                                            'header' => '',
                                            'htmlOptions' => array('class' => 'pagination'),
                                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                        ),
                                            )
                                    );
                                }
                                ?>
                            </div>
                            <br>
                            <br>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="row">

<!--            <div class="col-md-6">
                <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("planteles/consultar/"); ?>" class="btn btn-danger">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
            </div>-->



        </div>
    </div>



    <?php $this->endWidget(); ?>

</div>