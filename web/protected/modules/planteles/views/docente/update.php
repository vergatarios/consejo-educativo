<?php
/* @var $this DocenteController */
/* @var $model Docente */

$this->pageTitle = 'Actualización de Datos de Docentes';

      $this->breadcrumbs=array(
	'Docentes'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>