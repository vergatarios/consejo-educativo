<?php
/**
 * Created by PhpStorm.
 * User: ignacio
 * Date: 18/09/14
 * Time: 15:10
 */
$tdocumento_identificacion = array(
    array('id'=>'V',
        'nombre'=>'V'),
    array('id'=>'E',
        'nombre'=>'E'),
    array('id'=>'P',
        'nombre'=>'P'),
);
?>
<form id="asignatura-docente-form">
    <?php foreach($asignaturas as $index => $valor){
        ($valor['nombre'])? $nom_asignatura=$valor['nombre']:$nom_asignatura='';
        ($valor['id'])? $asignatura_id=$valor['id']:$asignatura_id='';
        ?>
        <?php echo CHtml::activeHiddenField($model, "[$index]docente_id", array()); ?>
        <?php echo CHtml::activeHiddenField($model, "[$index]nombres", array()); ?>
        <?php echo CHtml::activeHiddenField($model, "[$index]apellidos", array()); ?>
        <?php echo CHtml::activeHiddenField($model, "[$index]asignatura_id", array('value'=>$asignatura_id)); ?>
        <div class="col-md-12">
            <div class="col-md-3">
                <?php
                echo CHtml::activeLabelEx($model, "asignatura_id", array('class' => "col-md-12"));
                echo CHtml::activeTextField($model, "[$index]asignatura_id", array('class' => "span-12", 'data-id' => $asignatura_id,'data-index'=>base64_encode($index),'disabled'=>'disabled', 'value'=>$nom_asignatura));
                ?>
            </div>
            <div class="col-md-4">
                <?php echo CHtml::activeLabelEx($model, "documento_identidad", array('class' => "col-md-12")); ?>
                <div class="col-md-12">
                    <div class="col-md-3">
                        <?php echo CHtml::activeDropDownList($model, "[$index]tdocumento_identidad", CHtml::listData($tdocumento_identificacion, 'id', 'nombre'), array('class' => "span-12")); ?>
                    </div>
                    <div class="col-md-9">
                        <?php echo CHtml::activeTextField($model, "[$index]documento_identidad", array('class' => "span-12 data-doc-ident ", 'data-index'=>base64_encode($index))); ?>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <?php echo CHtml::activeLabelEx($model, "nombre_completo", array('class' => "col-md-12")); ?>
                <?php echo CHtml::activeTextField($model, "[$index]nombre_completo", array('class' => "span-12 data-doc-ident ", 'disabled'=>'disabled','data-index'=>base64_encode($index))); ?>
            </div>
            <div class="col-md-2">
                <?php echo CHtml::activeLabelEx($model, "escalafon", array('class' => "col-md-12")); ?>
                <?php //echo CHtml::activeTextField($model, "[$index]escalafon_id", array('class' => "span-12 data-doc-ident ", 'data-index'=>base64_encode($index))); ?>
                <?php echo CHtml::activeDropDownList($model, "[$index]escalafon_id", CHtml::listData(CTipoDocente::getData('estatus','A'), 'id', 'nombre'), array('class' => "span-12",'empty' => '- SELECCIONE -')) ?>
            </div>
        </div>
    <?php } ?>
</form>
<script type="text/javascript">
    $(document).ready(function(){
        $(".data-doc-ident").click(function(event) {
            event.preventDefault();
            $('.data-doc-ident').unbind('keyup');
            $('.data-doc-ident').bind('keyup', function() {
                keyAlphaNum(this,false,false);
                clearField(this);
                makeUpper(this);
            });
            $('.data-doc-ident').unbind('blur');
            $('.data-doc-ident').bind(' blur', function() {
                var documento_identidad = $(this).val();
                var index_input = base64_decode($(this).attr('data-index'));
                var id_nombres;
                var id_apellidos;
                var id_tdocumento_identidad;
                var tdocumento_identidad;
                var divResultMsgError = 'msgPersonalDocente';
                var divResultMsgSuccess = 'resultPersonalDocente';
                var title;
                var mensaje;
                var divResult='';
                var urlDir = '/planteles/seccionPlantel/obtenerDatosPersona/';
                var datos;
                var loadingEfect=false;
                var showResult=false;
                var method='GET';
                var responseFormat='json';
                var beforeSendCallback=function(){};
                var successCallback;
                var errorCallback=function(){};

                if(documento_identidad !=null && documento_identidad != '' ){
                    id_nom_completo='AsignaturaDocente_'+index_input+'_nombre_completo';
                    id_tdocumento_identidad='AsignaturaDocente_'+index_input+'_tdocumento_identidad';
                    id_documento_identidad='AsignaturaDocente_'+index_input+'_documento_identidad';
                    id_docente='AsignaturaDocente_'+index_input+'_docente_id';
                    id_escalafon='AsignaturaDocente_'+index_input+'_escalafon_id';
                    id_nombres='AsignaturaDocente_'+index_input+'_nombres';
                    id_apellidos='AsignaturaDocente_'+index_input+'_apellidos';
                    tdocumento_identidad = $('#'+id_tdocumento_identidad).val();
                    documento_identidad = $('#'+id_documento_identidad).val();
                    if(tdocumento_identidad !=null && tdocumento_identidad != '' ){
                        datos=$('#asignatura-docente-form').serialize()+'&documento_identidad='+documento_identidad+'&tdocumento_identidad='+tdocumento_identidad+'&index='+base64_encode(index_input);
                        successCallback=function(response){
                            if(response.statusCode =='SUCCESS'){
                                $('#'+id_nom_completo).val(response.nombres+' '+response.apellidos);
                                $('#'+id_nombres).val(response.nombres);
                                $('#'+id_apellidos).val(response.apellidos);
                                if(response.escalafon_id != null)
                                    $('#'+id_escalafon).val(base64_decode(response.escalafon_id));
                            }
                            if(response.statusCode =='SUCCESS-E'){
                                $('#'+id_nom_completo).val(response.nombres+' '+response.apellidos);
                                $('#'+id_nombres).val(response.nombres);
                                $('#'+id_apellidos).val(response.apellidos);
                                $('#'+id_docente).val(response.docente_id);
                                $('#'+id_escalafon).val(base64_decode(response.escalafon_id));

                            }
                            if(response.statusCode =='ERROR'){
                                $('#'+id_nom_completo).val('');
                                $('#'+id_nombres).val('');
                                $('#'+id_apellidos).val('');
                                $('#'+id_documento_identidad).val('');
                                $('#'+id_docente).val('');
                                $('#'+id_escalafon).val('');
                                $("#"+divResultMsgError).html('');
                                $("#"+divResultMsgError).removeClass('hide');
                                displayDialogBox(divResultMsgError,'alert',response.mensaje);
                            }
                        };
                        executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);
                    }
                    else {
                        mensaje='Estimado usuario debe seleccionar un Tipo de Documento de Identidad Valido.';
                        $("#"+divResultMsgError).html('');
                        $("#"+divResultMsgError).removeClass('hide');
                        displayDialogBox(divResultMsgError,'alert',mensaje);
                    }
                }
            });
        });
    });
</script>