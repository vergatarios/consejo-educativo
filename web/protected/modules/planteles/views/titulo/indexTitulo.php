<?php
/* @var $this DefaultController */
$this->breadcrumbs = array(
    'Consultar Planteles' => array('consultar/'),
    'Título del Plantel ' . ' "' . $nombrePlantel . '" '
);
?>


<div  class="col-xs-12">
    <div class="col-md-12"><br><br> </div>
    <p>
        <?php if (Yii::app()->user->pbac('planteles.titulo.read') && Yii::app()->user->pbac('planteles.titulo.admin')) { ?>

        <div class="linkCatalogo" onclick="">
            <span class="titulo">Solucitud de Título</span>
            <a href="/planteles/titulo/mostrarSolicitanteAlTitulo/id/<?php echo base64_encode($plantel_id) ?>" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../../../public/images/iconoCatalogo/identificadores.png' ?>');">
            </a>
        </div>
    <?php } ?>


    <?php
    if ($tieneSolicitudes != 0) {
        if (Yii::app()->user->pbac('planteles.titulo.read') && Yii::app()->user->pbac('planteles.titulo.admin')) {
            ?>
            <div class="linkCatalogo" onclick="">
                <span class="titulo">Reporte de Estudiantes con Seriales Asignados</span>
                <a href="/planteles/titulo/reportePorPlantel/id/<?php echo base64_encode($plantel_id) ?>" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../../../public/images/iconoCatalogo/identificadores.png' ?>');">
                </a>
            </div>
            <?php
        }
    }
    ?>


    <?php if (Yii::app()->user->pbac('planteles.titulo.read') && Yii::app()->user->pbac('planteles.titulo.write') && Yii::app()->user->pbac('planteles.titulo.admin')) { ?>
        <!--
                <div class="linkCatalogo" onclick="">
                    <span class="titulo">Otorgamiento de Título</span>
                    <a href="/planteles/asignacionTitulo/index/id/<?php echo base64_encode($plantel_id) ?>" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../../../public/images/iconoCatalogo/identificadores.png' ?>');">
                    </a>
                </div>
    <?php } ?>


    <?php if (Yii::app()->user->pbac('planteles.titulo.read') && Yii::app()->user->pbac('planteles.titulo.write') && Yii::app()->user->pbac('planteles.titulo.admin')) { ?>
                                                                                 <div class="linkCatalogo" onclick="">
                                                                                 <span class="titulo">Liquidación de Título</span>
                                                                                 <a href="/planteles/liquidacionTitulo/index/id/<?php echo base64_encode($plantel_id) ?>" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../../../public/images/iconoCatalogo/identificadores.png' ?>');">
                                                                                 </a>
                                                                             </div>

    <?php } ?>
</p>
<p>
    <?php if (Yii::app()->user->pbac('planteles.titulo.read') && Yii::app()->user->pbac('planteles.titulo.admin')) { ?>
            <div class="linkCatalogo" onclick="">
                <span class="titulo">Consultar Títulos</span>
                <a href="/planteles/ConsultarTitulo/index/id/<?php echo base64_encode($plantel_id) ?>" class="circle" style="background-image: url('<?php echo Yii::app()->baseUrl . '../../../../public/images/iconoCatalogo/identificadores.png' ?>');">
                </a>
            </div>
    <?php } ?>
</p>

    -->
    <div class="col-md-12"><br><br> </div>

</div>

<?php
echo CHtml::cssFile('/public/css/iconosCatalogo.css');
?>