<?php

class EscolaridadController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';
    static $_permissionControl = array(
        'read' => 'Agregar Escolaridad',
        'write' => 'Agregar Escolaridad',
        'admin' => 'Agregar Escolaridad',
        'label' => 'Consulta de Estudiante'
    );

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            //'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules() {
        return array(
            array('allow',
                'actions' => array(
                    'index', 'view',
                ),
                'pbac' => array('admin'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }



    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['TalumnoAcad'])) {
            $model->attributes = $_POST['TalumnoAcad'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {

        $array=array();

        //var_dump($_REQUEST);die();
//		$dataProvider=new CActiveDataProvider('TalumnoAcad');
//		$this->render('index',array(
//			'dataProvider'=>$dataProvider,
//		));
        $model = new TalumnoAcad;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['TalumnoAcad'])) {
            $model->attributes = $_POST['TalumnoAcad'];
            $periodo = '20' . $_POST['TalumnoAcad']['fperiodoescolar'] - 1 . '-20' . ($_POST['TalumnoAcad']['fperiodoescolar'] );
            $model->s_periodo_nombre = $periodo;
//            $model->id = 99999998;
            $model->s_escolaridad_nombre = strtoupper($_POST['TalumnoAcad']['s_escolaridad_nombre']);
            $model->curso = strtoupper($_POST['TalumnoAcad']['curso']);
            $cod_plantel = $_POST['TalumnoAcad']['cdea'];
            $modelPlantel = Plantel::model()->findAll(array('condition' => "cod_plantel = '" . $cod_plantel."'"));
            // var_dump($modelPlantel);die();
            if ($modelPlantel!=$array){


                $model->s_plantel_id = $modelPlantel[0]['id'];

                $estudiante = Estudiante::model()->findAll(array('condition' => "documento_identidad = '" . $_POST['TalumnoAcad']['calumno'] . "' AND tdocumento_identidad='" . $_POST['TalumnoAcad']['tdocumento_identidad'] . "'"));
                //       var_dump($estudiante);die();
                if ($estudiante == $array ) {
                    //         var_dump($estudiante);die();
                    $model->addError('calumno', 'El estudiante solicitado no se encuentra registrado');
                    $this->render('_form', array(
                        'model' => $model,
                    ));
                    Yii::app()->end();
                } else {

                    $model->s_alumno_id = $estudiante[0]['sigue_id'];

                    if ($model->validate() && $model->save()) {

                        $this->redirect(array('view', 'id' => $model->id));
                    } else {
                        $this->render('_form', array(
                            'model' => $model,
                        ));
                        Yii::app()->end();
                    }
                }

            }else{
                $model->addError('cdea', 'El Código del Plantel indicado no se encuentra registrado');
                $this->render('_form', array(
                    'model' => $model,
                ));
                Yii::app()->end();
            }
        }
        $this->render('_form', array(
            'model' => $model,
        ));
    }
    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new TalumnoAcad('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['TalumnoAcad']))
            $model->attributes = $_GET['TalumnoAcad'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return TalumnoAcad the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = TalumnoAcad::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param TalumnoAcad $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'talumno-acad-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}
