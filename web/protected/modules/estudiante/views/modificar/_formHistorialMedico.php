<?php
/* @var $this HistorialMedicoController */
/* @var $model HistorialMedico */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'historial-medico-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>

<div id="resultadoHistorialMedico"></div>

<div class="widget-box">

    <div class="widget-header">
        <h5>Historial medico del Estudiante</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body" id="idenEstudiante">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main form">
                
                <div class="row">
                    <div id="divEnfermedad" class="col-md-4">
                        <?php echo CHtml::label('Enfermedad', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'enfermedad', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                    
                    <div id="divNombreEnfermedad" class="col-md-4">
                        <?php echo CHtml::label('Nombre de la enfermedad', '', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'nombre_enfermedad', array('size' => 20, 'maxlength' => 80, 'class' => 'span-7')); ?>
                    </div>
                    
                    <div id="divBcg" class="col-md-4">
                        <?php echo CHtml::label('Vacunado con Bcg', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'bcg', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                </div>
                
                <div class="row">
                    <div id="divMedicamento" class="col-md-4">
                        <?php echo CHtml::label('Medicamento', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'medicamento', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                    
                    <div id="divNombreMedicamento" class="col-md-4">
                        <?php echo CHtml::label('Nombre del medicamento', '', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'nombre_medicamento', array('size' => 20, 'maxlength' => 80, 'class' => 'span-7')); ?>
                    </div>
                    
                    <div id="divTriple" class="col-md-4">
                        <?php echo CHtml::label('Vacunado con Triple', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'triple', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                </div>
                
                <div class="row">
                    <div id="divAlergicoAlimento" class="col-md-4">
                        <?php echo CHtml::label('Alergico a algun medicamento', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'alergia_medicamento', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                    
                    <div id="divNombreAlimentoAlergico" class="col-md-4">
                        <?php echo CHtml::label('Nombre del alimento', '', array("class" => "col-md-12")); ?>
                        <?php echo $form->textField($model, 'nombre_alergia_alimento', array('size' => 20, 'maxlength' => 80, 'class' => 'span-7')); ?>
                    </div>
                    
                    <div id="divSarampion" class="col-md-4">
                        <?php echo CHtml::label('Vacunado con Sarampion', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'sarampion', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                </div>
                
                <div class="row">
                    <div id="divAlimento" class="col-md-4">
                        <?php echo CHtml::label('Alergico a algun alimento', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'alergia_alimento', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                    <div id="divPolio" class="col-md-4">
                        <?php echo CHtml::label('Vacunado con Polio', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'polio', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                    
                    <div id="divTrivalente" class="col-md-4">
                        <?php echo CHtml::label('Vacunado con Trivalente', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'trivalente', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                </div>
                
                <div class="row">
                    <div id="divFiebreAmarilla" class="col-md-4">
                        <?php echo CHtml::label('Vacunado con Fiebre Amarilla', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'fiebre_amarilla', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                    <div id="divHepatitisb" class="col-md-4">
                        <?php echo CHtml::label('Vacunado con Hepatitisb', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'hepatitisb', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                    
                    <div id="divToxoide" class="col-md-4">
                        <?php echo CHtml::label('Vacunado con Toxoide', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'toxoide', array(
                            '1' => 'SI',
                            '0' => 'NO',
                                ), array(
                            'empty' => '-Seleccione-',
                            'class' => 'span-7',
                            'id' => 'Canaima'
                                )
                        );
                        ?>
                    </div>
                </div>
                
                <div class="row">
                    <div id="divTipoSangre" class="col-md-4">
                        <?php echo CHtml::label('Tipo de sangre', '', array("class" => "col-md-12")); ?>
                        <?php
                        echo $form->dropDownList(
                                $model, 'tipo_sangre_id', CHtml::listData($tipoSangre, 'id', 'nombre'), array(
                                'empty' => array('' => '- SELECCIONE -'), 'class' => 'span-7',
                            )
                        );
                        ?>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
</div>

<div id="dialog_error" class="hide"><p></p></div>

        <br>
                
        <div class="row">

            <div class="col-xs-6">
                <a class="btn btn-danger" href="/estudiante/consultar" id="btnRegresar">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
            </div>

            <div class="col-xs-6">
                <button class="btn btn-primary btn-next pull-right" title="Pasar al siguiente paso del estudiante" data-last="Finish" type="submit">
                    Actualizar cambios
                    <i class="fa fa-mail-forward"></i>
                </button>
            </div>

        </div>
        

</div><!-- form -->

<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/estudiante/estudiante.js', CClientScript::POS_END);
?>

<?php $this->endWidget(); ?>
