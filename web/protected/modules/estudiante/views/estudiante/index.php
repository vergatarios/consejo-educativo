<?php
/* @var $this EstudianteController */
/* @var $model Estudiante */

$this->breadcrumbs = array(
    'Estudiantes',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#estudiante-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<?php echo CHtml::link('Advanced Search', '#', array('class' => 'search-button')); ?>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
</div><!-- search-form -->

<div class="widget-box">

    <div class="widget-header">
        <h5>Estudiantes</h5>

        <div class="widget-toolbar">
            <a data-action="collapse" href="#">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div class="widget-body-inner" style="display: block;">
            <div class="widget-main">

                <a href="#" class="search-button"></a>
                <div style="display:block" class="search-form">
                    <div class="widget-main form">

                        <div style="padding-left:10px;" class="pull-right">
                            <a class="btn btn-success btn-next btn-sm" data-last="Finish" href="/planteles/crear/">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar nuevo estudiante
                            </a>
                        </div>
                        <div class="col-lg-12"><div class="space-6"></div></div>


                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                            'pager' => array('pageSize' => 1),
                            'summaryText' => false,
                            'id' => 'estudiante-grid',
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                            'columns' => array(
                                array(
                                    'header' => '<center>Código del plantel</center>',
                                    'name' => 'plantel_id',
                                    'value' => '(isset($data->plantel->cod_plantel))? $data->plantel->codg_plantel : ""',
                                    'filter' => CHtml::textField('Estudiante[cedula_identidad_representante]', null),
                                ),
                                array(
                                    'header' => '<center>Cédula de Identidad del representante</center>',
                                    'name' => 'representante_id',
                                    'value' => '(isset($data->representante->cedula_identidad))? $data->representante->cedula_identidad : ""',
                                    'filter' => CHtml::textField('Estudiante[cedula_identidad_representante]', null),
                                ),
                                array(
                                    'header' => '<center>Cédula de Identidad del estudiante</center>',
                                    'name' => 'cedula_identidad',
                                    'filter' => CHtml::textField('Estudiante[cedula_identidad]', null),
                                ),
                                array(
                                    'header' => '<center>Cédula Escolar</center>',
                                    'name' => 'cedula_escolar',
                                    'filter' => CHtml::textField('Estudiante[cedula_escolar]', null),
                                ),
                                array(
                                    'header' => '<center>Nombres del Estudiante</center>',
                                    'name' => 'nombres',
                                    'filter' => CHtml::textField('Estudiante[nombres]', null),
                                ),
                                array(
                                    'header' => '<center>Apellidos del Estudiante</center>',
                                    'name' => 'apellidos',
                                    'filter' => CHtml::textField('Estudiante[apellidos]', null),
                                ),
                                array(
                                    'header' => '<center>Estado</center>',
                                    'name' => 'estado_id',
                                    'value' => '(is_object($data->estado) && isset($data->estado->nombre))? $data->estado->nombre: ""',
                                    'filter' => CHtml::listData(
                                            Estado::model()->findAll(
                                                    array(
                                                        'order' => 'nombre ASC'
                                                    )
                                            ), 'id', 'nombre'
                                    ),
                                ),
                                array('type' => 'raw',
                                    'header' => '<center>Acciones</center>',
                                    'value' => array($this, 'columnaAcciones'),
                                ),
                            ),
                        ));
                        ?>
                    </div><!-- search-form -->
                </div><!-- search-form -->
            </div>
        </div>
    </div>
</div>
<?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/estudiante/estudiante.js',CClientScript::POS_END);
?>
