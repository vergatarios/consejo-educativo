<?php
/* @var $this EscolaridadController */
/* @var $model TalumnoAcad */

$this->breadcrumbs=array(
	'Estudiantes'=>array('/estudiante/'),
	'Escolaridad',
);
?>

<div class="widget-box">

    <div class="widget-header">
        <h5>
               Registro de Escolaridad
        </h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display: block;" class="widget-body-inner">
            <div class="widget-main">

            
            
             
                <table class="table table-striped table-bordered table-hover">
                  <tbody>
                    <div class="col-md-12 center">
    
    <div class="col-md-4">
    
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'calumno',
		'cestadistico',

		'cseccion',
		'cplan',

		'fperiodoescolar',
		'fmatricula',
		'fegreso',

		's_grado_nombre',

		's_modalidad_nombre',
		's_escolaridad_nombre',
            'dobservacion',

	),
)); ?>

    </div> 
  <div class="col-md-4">
</div>
                   <div class="col-md-4">
</div>
</div>
                </table>        
                    
               
                </tbody>
            </div>
              <hr>

                                    <div class="col-md-6">
                                        <a class="btn btn-danger" href="<?php echo $this->createUrl("/estudiante"); ?>" id="btnRegresar">
                                            <i class="icon-arrow-left"></i>
                                            Volver
                                        </a>
                                    </div>
        </div>
    </div>
</div>

