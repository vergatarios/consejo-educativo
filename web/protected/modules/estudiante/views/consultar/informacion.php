<?php
if($nombrePlantel != '' && isset($_REQUEST['bc']) && $_REQUEST['bc'] == '1') {
    $this->breadcrumbs = array(
        'Planteles' => array('/planteles/'),
        'Estudiantes' => array('/estudiante/?bc=1&id=' . base64_encode($nombrePlantel)),
        'Información del Estudiante',
    );
    $urlVolver = '/estudiante/?bc=1&id=' . base64_encode($nombrePlantel);
}
else {
    $this->breadcrumbs = array(
        'Estudiantes' => array('/estudiante/'),
        'Información del Estudiante',
    );
    $urlVolver = '/estudiante/';
}
?>
<div class="form">
    <div class="tabbable">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#representante" id="representanteTab">Representante</a></li>
            <li><a data-toggle="tab" href="#estudiante" id="estudianteTab">Estudiante</a></li>
            <li><a data-toggle="tab" href="#historico" id="historicoTab">Historico del estudiante</a></li>
            <!--<li><a data-toggle="tab" href="#historialMedico" id="historialMedicoTab">Historial medico</a></li>
            -->            <li><a data-toggle="tab" href="#datosAntropometricos" id="datosAntropometricosTab">Datos Antropometricos</a></li>
        </ul>

        <div class="tab-content">

            <div id="historico" class="tab-pane">
                <?php
                $this->renderPartial('_formHistorico', array('model' => $model, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'diversidadFuncional' => $diversidadFuncional, 'plantelPK' => $plantelPK, '$plantelAnteriorPK' => $plantelAnteriorPK, 'urlVolver' => $urlVolver, 'historicoEstudiante' => $historicoEstudiante));
                ?>
            </div>

            <div id="estudiante" class="tab-pane">
                <?php
                $this->renderPartial('_form', array('model' => $model, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'diversidadFuncional' => $diversidadFuncional, 'plantelPK' => $plantelPK, '$plantelAnteriorPK' => $plantelAnteriorPK, 'urlVolver' => $urlVolver, 'historicoEstudiante' => $historicoEstudiante));
                ?>
            </div>

            <div id="historialMedico" class="tab-pane">
                <?php
                $this->renderPartial('_formHistorialMedico', array('model' => $modelHistorialMedico, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'diversidadFuncional' => $diversidadFuncional, 'tipoSangre' => $tipoSangre, 'plantelPK' => $plantelPK));
                ?>
            </div>

            <div id="datosAntropometricos" class="tab-pane">
                <?php
                $this->renderPartial('_formDatosAntropometricos', array('model' => $modelDatosAntropometricos, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'diversidadFuncional' => $diversidadFuncional, 'plantelPK' => $plantelPK));
                ?>
            </div>

            <div id="representante" class="tab-pane active">
                <?php
                $this->renderPartial('_formRepresentante', array('model' => $modelRepresentante, 'estadoCivil' => $estadoCivil, 'genero' => $genero, 'estado' => $estado, 'pais' => $pais, 'zonaUbicacion' => $zonaUbicacion, 'condicionVivienda' => $condicionVivienda, 'tipoVivienda' => $tipoVivienda, 'ubicacionVivienda' => $ubicacionVivienda, 'condicionInfraestructura' => $condicionInfraestructura, 'etnia' => $etnia, 'afinidad' => $afinidad, 'diversidadFuncional' => $diversidadFuncional, 'profesion' => $profesion, 'plantelPK' => $plantelPK, 'urlVolver' => $urlVolver));
                ?>
            </div>

        </div>
        
    </div>
</div>

<script>
    $(document).ready(function() {
        $.mask.definitions['~']='[+-]';
    });
</script>
    <?php Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/jquery.maskedinput.min.js', CClientScript::POS_END);?>