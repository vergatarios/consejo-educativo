<?php

class MencionController extends Controller {
   /**
      * @ Var string el diseño predeterminado de las opiniones. Por defecto es '/ / layouts/column2', significado
      * Usando diseño de dos columnas. Ver 'protected/views/layouts/column2.php'.
      */
    public $layout = '//layouts/column2';

    /**
      * @ Return array filtros de acción
      */
    static $_permissionControl = array(
        'read' => 'Consulta de Menciones',
        'write' => 'Gestion de Menciones',
        'label' => 'Módulo de Menciones'
    );
  /**
      * @Return array filtros de acción
      */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }
   /**
      * Especifica las reglas de control de acceso.
      * Este método es utilizado por el filtro 'AccessControl'.
      * Reglas de control de acceso a una matriz @ return
      */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'activar', 'delete'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
      /**
      * Muestra un modelo en particular.
      * @ Param entero $ id de la ID del modelo de que se muestre.
      */
    public function actionView($id) {
        $id = base64_decode($id);
        $this->renderPartial('view', array(
            'model' => $this->loadModel($id),
        ));
    }
    /**
      * Crea un nuevo modelo.
      * Si la creación se realiza correctamente, el navegador muestra un mensaje con exito de creación,
        y le da la posibilidad de crear una nueva mencion.
      */
    public function actionCreate() {
        $id = $_REQUEST['id'];
        $model = new Mencion;
        if (isset($_POST['Mencion'])) {
            $model->usuario_ini_id = Yii::app()->user->name;
            $model->attributes = $_POST['Mencion'];
            $model->nombre = strtoupper($model->nombre);
            $model->usuario_ini_id = Yii::app()->user->id;
            $model->fecha_ini = date("Y-m-d H:i:s");
            $model->estatus = "A";
            $t = strlen($model->nombre);
            if ($model->validate()) {
                if ($model->save()) {

                    if ($t < 5) {
                        $this->renderPartial("//msgBox", array('class' => 'errorDialogBox', 'message' => 'El registro debe contener al menos 5 caracteres.'));
                    } else if ($t > 5) {
                        $this->registerLog('ESCRITURA', 'catalogo.mencion.create', 'EXITOSO', 'Se ha creado una nueva Mencion');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Exito! ya puede realizar otro registro.'));
                        $model = new Mencion;
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                }
            }
        }
        /**
         * Sirve para llevarme a la vista para asi crear la nueva mencion junto con una variable llamada model que es la que contiene los datos de las propiedades de la mencion a crear.
         */
        $this->renderPartial('create', array(
            'model' => $model,
        ));
    }
  /**
      *Actualiza un modelo en particular.
      *Si la actualización se realiza correctamente, el sistema mustra un mensaje con exito.
      */
    public function actionUpdate() {
        $id = $_REQUEST['id'];
        $id = base64_decode($id);
        $model = $this->loadModel($id);
        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Mencion'])) {
            $model->attributes = $_POST['Mencion'];
            $model->usuario_act_id = Yii::app()->user->id;
            $model->fecha_act = date("Y-m-d H:i:s");
            if ($model->save())
                if ($model->validate()) {
                    if ($model->save()) {
                        $this->registerLog('ESCRITURA', 'catalogo.mencion.update', 'EXITOSO', 'Se ha creado actualizado una Mencion');
                        $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Actualizado con exito.'));
                        $model = $this->loadModel($id);
                    } else {
                        throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                    }
                }
        }
        $this->renderPartial('update', array(
            'model' => $model,
        ));
    }
    public function actionActivar() {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $id = base64_decode($id);
            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_elim = date("Y-m-d H:i:s");
                $model->estatus = "A";
                if ($model->save()) {
                    $this->registerLog('ESCRITURA', 'catalogo.mencion.activar', 'EXITOSO', 'Se ha activado una Mencion');
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Activado con exito.'));
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
//$this->render('borrar',array('model'=>$model,));
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        //if(!isset($_GET['ajax']))
        //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $id = base64_decode($id);
            $model = $this->loadModel($id);
            if ($model) {
                $model->usuario_act_id = Yii::app()->user->id;
                $model->fecha_elim = date("Y-m-d H:i:s");
                $model->estatus = "E";
                if ($model->save()) {
                    $this->registerLog('ESCRITURA', 'catalogo.mencion.borrar', 'EXITOSO', 'Se ha eliminado una Mencion');
                    $this->renderPartial("//msgBox", array('class' => 'successDialogBox', 'message' => 'Eliminado con exito.'));
                    $model = $this->loadModel($id);
                } else {
                    throw new CHttpException(500, 'Error! no se ha completa la operación comuniquelo al administrador del sistema.');
                }
            } else {

                throw new CHttpException(404, 'Error! Recurso no encontrado!');
            }
        }
         //$this->render('borrar',array('model'=>$model,));
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        //if(!isset($_GET['ajax']))
        //$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    /**
     * Lists all models.
     * Función que me permite ir al admin que es la principal del modulo mención
     */
    public function actionIndex() {
       $groupId = Yii::app()->user->group;
        $model = new Mencion('search');
        if (isset($_GET['Mencion']))
            $model->attributes = $_GET['Mencion'];
        $usuarioId = Yii::app()->user->id;
        $dataProvider = new CActiveDataProvider('Mencion');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
        ));
    }
    /**
      * Devuelve el modelo de datos basado en la clave principal dado en la variable GET.
      * Si no se encuentra el modelo de datos, se generará una excepción HTTP.
      * @ Param entero $ id de la ID del modelo de que se cargue
      * @ Return Mencion el modelo cargado
      * @ Throws CHttpException
      */
    public function loadModel($id) {
        $model = Mencion::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }
    /**
     * funcion que muestra el estatus completo en la mención, es decir el estatus se encuentra guardado
       con una sola letra ejemplo si es activo se guarda A. De manera que la funcion permite visualizar
       de forma completa el estatus.
     * @ Param string $data donde se carga las propiedades del modelo
     * @ Return $columna string
     * @ Autor Richard Massri
     */
    public function estatus($data) {
        $id = $data["estatus"];
        if ($id == 'A') {
            $columna = 'Activo';
        } else {
            $columna = 'Inactivo';
        }
        return $columna;
    }
    /*
     *Funcion que muestra las acciones entre ellas buscar, editar, eliminar, activar en caso de estar eliminado
     * @ Param string $datas donde se carga un array de las propiedades del modelo
     * @ Return $columna string
     * @ Autor Richard Massri
     */
    public function columnaAcciones($datas) {
        $id = $datas["id"];
        $id2 = $datas["estatus"];
        $id = base64_encode($id);
        if ($id2 == "A") {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "title" => "Buscar Esta Mención", "onClick" => "VentanaDialog('$id','/catalogo/mencion/view','Mención','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-pencil green", "title" => "Modificar Mención", "onClick" => "VentanaDialog('$id','/catalogo/mencion/update','Modificar Mención','update')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa icon-trash red", "title" => "Eliminar Mención", "onClick" => "VentanaDialog('$id','/catalogo/mencion/borrar','Eliminar Mención','borrar')")) . '&nbsp;&nbsp;';
        } else {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "title" => "Buscar Mencion", "onClick" => "VentanaDialog('$id','/catalogo/mencion/view','Mención','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-check", "title" => "Activar Mencion", "onClick" => "VentanaDialog('$id','/catalogo/mencion/activar','activar Mención','activar')")) . '&nbsp;&nbsp;';
        }
        return $columna;
    }
    /**
     * Performs the AJAX validation.
     * @param Mencion $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'mencion-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
    static function registrarTraza($transaccion, $accion) {
        $Utiles = new Utiles();
        $modulo = "catalogo.MencionController" . $accion;
        $Utiles->traza($transaccion, $modulo, date('Y-m-d H:i:s'));
    }

}
