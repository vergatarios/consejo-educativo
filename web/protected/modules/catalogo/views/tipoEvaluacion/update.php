<?php
/* @var $this TipoEvaluacionController */
/* @var $model TipoEvaluacion */

$this->pageTitle = 'Actualización de Datos de Tipo Evaluacions';

      $this->breadcrumbs=array(
	'Tipo Evaluacions'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>