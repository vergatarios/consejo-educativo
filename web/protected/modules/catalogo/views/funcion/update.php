<?php
/* @var $this FuncionController */
/* @var $model Funcion */

$this->pageTitle = 'Actualización de Datos de Funcions';

      $this->breadcrumbs=array(
	'Funcions'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>