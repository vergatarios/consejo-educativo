<?php
/* @var $this AplicacionController */
/* @var $model Aplicacion */

$this->pageTitle = 'Registro de Aplicacions';

      $this->breadcrumbs=array(
	'Aplicación'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>