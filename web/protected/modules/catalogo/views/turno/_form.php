<?php
/* @var $this TurnoController */
/* @var $model Turno */
/* @var $form CActiveForm */
?>

<div class="widget-box">
    <div id="error"></div>
        <div class="widget-header">
            <h5>
               Turno <?php echo $model->nombre;?>
            </h5>

            <div class="widget-toolbar">
                <a>
                    <i class="icon-chevron-up"></i>
                </a>
            </div>

        </div>

        <div class="widget-body">
            <div class="widget-body-inner" style="">
                <div class="widget-main">

                    <a href="#" class="search-button"></a>
                     <?php
                                $form = $this->beginWidget('CActiveForm', array(
                                    'id' => 'turno-form',
                                    'enableAjaxValidation' => false,
                                ));
                            ?>
                    <div style="display:block" class="form">
                        <div class="widget-main form">
                            
                            <?php if($form->error($model, 'nombre')): ?>
                            <div class="errorDialogBox">
                                <p>
                                    <?php echo $form->errorSummary($model); ?>
                                </p>
                            </div>
                            <?php endif; ?>
                            
                           
                                <input type="hidden" id='id' name="id" value="<?php echo $model->id ?>" />
                                
                                
                                <div class="row">
                                    <div class="col-md-2 bolder align-right"><label for="nombre"> Nombre <span class="required">*</span></label>&nbsp;&nbsp;</div>
                                    <div class="col-md-10">
                                        <?php echo $form->textField($model, 'nombre', array('required' => 'required', 'maxlength' => 30, 'class' => 'span-12', 'id' => 'nombre_turno', 'placeholder' => 'Nombre del Turno',)); ?>
                                    </div>
                                </div>
                            
                            
                                
                        </div><!-- search-form -->
                    </div><!-- search-form -->
                    <?php $this->endWidget(); ?>
                </div>
            </div>
        </div>

    </div>
<script>
    $(document).ready(function(){
        $('#turno-form').on('submit', function(evt){
            evt.preventDefault();
            crearTurno();
        });
        
        $('#nombre_turno').bind('keyup blur', function () {
            keyTextDash(this, true, true);
            makeUpper(this);

        });
        
       });
</script>