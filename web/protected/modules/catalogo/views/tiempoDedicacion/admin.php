<?php

/* @var $this TiempoDedicacionController */
/* @var $model TiempoDedicacion */
$this->breadcrumbs=array(
    'Catalogos'=>array('/catalogo/'),
    'Tiempo de Dedicación',
    'Administración',
);
$this->pageTitle = 'Administración de Tiempo de Dedicación';

?>
    <div class="widget-box">
        <div class="widget-header">
            <h5>Lista de Tiempo de Dedicación</h5>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

        <div class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main">

                    <div class="row space-6"></div>
                    <div>
                        <div id="resultadoOperacion">

                        </div>
                        <div id="div-result">
                            <div id="div-result">
                                <?php
                                if (Yii::app()->user->hasFlash('exito')) {
                                    ?>
                                    <div class="successDialogBox">
                                        <p><?php echo Yii::app()->user->getFlash('exito'); ?></p>
                                    </div>
                                <?php
                                } else
                                    if (Yii::app()->user->hasFlash('error')) { ?>
                                        <div class="errorDialogBox">
                                            <p><?php echo Yii::app()->user->getFlash('error'); ?></p>
                                        </div>
                                    <?php   }
                                    else {
                                        ?>
                                        <div class="infoDialogBox">
                                            <p>
                                                En este módulo podrá registrar y/o actualizar los datos de Tiempo de Dedicación.
                                            </p>
                                        </div>
                                    <?php
                                    }
                                ?>
                            </div>
                        </div>

                        <div class="pull-right" style="padding-left:10px;">
                            <a href="<?php echo $this->createUrl("/catalogo/tiempoDedicacion/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar Nuevo Tiempo de Dedicación                        </a>
                        </div>


                        <div class="row space-20"></div>

                    </div>

                    <?php $this->widget('zii.widgets.grid.CGridView', array(
                        'id'=>'tiempo-dedicacion-grid',
                        'dataProvider'=>$dataProvider,
                        'filter'=>$model,
                        'itemsCssClass' => 'table table-striped table-bordered table-hover',
                        'summaryText' => 'Mostrando {start}-{end} de {count}',
                        'pager' => array(
                            'header' => '',
                            'htmlOptions' => array('class' => 'pagination'),
                            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                        ),
                        'afterAjaxUpdate' => "
                                function(){
                                    $('#TiempoDedicacion_nombre').bind('keyup', function () {
                                    keyAlphaNum(this, true, true);
                                    makeUpper(this);
                                    });
                                    $('#TiempoDedicacion_nombre').bind('blur', function () {
                                    clearField(this);
                                    });

                                }",
                        'columns'=>array(
                            array(
                                'header' => '<center>Nombre</center>',
                                'name' => 'nombre',
                                'filter' => CHtml::textField('TiempoDedicacion[nombre]', $model->nombre, array('title' => '',)),
                                'htmlOptions' => array()),
                            array(
                                'header' => '<center>Estatus</center>',
                                'name' => 'estatus',
                                'filter' => array('A' => 'Activo', 'E' => 'Eliminado', 'I' => 'Inactivo'),
                                'value'=>array($this,'estatus'),
                                'htmlOptions' => array()),
                            array(
                                'type' => 'raw',
                                'header' => '<center>Acción</center>',
                                'value' => array($this, 'getActionButtons'),
                                'htmlOptions' => array('nowrap'=>'nowrap','width'=>'20px'),
                            ),
                        ),
                    )); ?>
                </div>
            </div>
        </div>
    </div>
<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/modules/tiempo-dedicacion/form.js',CClientScript::POS_END
);
?>