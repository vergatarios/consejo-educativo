<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/RestriccionEstatus/admin.js', CClientScript::POS_END
);
?>

<?php

/* @var $this RestriccionEstatusController */
/* @var $model RestriccionEstatus */

$this->breadcrumbs=array(
	'Restrincción Estatus'=>array('lista'),
	'Administración',
);
$this->pageTitle = 'Administración de Restrinccion Estatuses';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Restrincción Estatus</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">
                        <div class="infoDialogBox">
                            <p>
                                En este módulo podrá registrar y/o actualizar los datos de Restrinccion Estatuses.
                            </p>
                        </div>
                    </div>
<?php
  $espe = RestriccionEstatus::model()->DropDown_Especi();
   
   ?>
                    <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/restriccionEstatus/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">
                            <i class="fa fa-plus icon-on-right"></i>
                            Registrar Nueva Restrincción de Estatus                        </a>
                    </div>


                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'restriccion-estatus-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => 'Mostrando {start}-{end} de {count}',
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){

                }",
	'columns'=>array(

        array(
            'header' => '<center>Tipo de Personal</center>',
            'name' => 'tipo_personal_id',
            //'htmlOptions' => array(),          
            'value' => '(is_object($data->tipoPersonal) && isset($data->tipoPersonal->nombre))? $data->tipoPersonal->nombre: ""',
            //'filter' => CHtml::textField('TipoPersonalCargo[tipo_personal_id]', $model->tipo_personal_id, array('title' => '',)),
            
//            'header' => '<center>Tipo de Personal</center>',
//            'name' => 'tipo_personal_id',
//            'htmlOptions' => array(),
//            //'filter' => CHtml::textField('RestriccionEstatus[tipo_personal_id]', $model->tipo_personal_id, array('title' => '',)),
        ),
        array(
             'header' => '<center>Especificación de Estatus</center>',
            'name' => 'especificacion_estatus_id',
            //'htmlOptions' => array(),          
            //'value' => '(is_object($data->especificacionEstatus) && isset($data->especificacionEstatus->nombre))? $data->especificacionEstatus->nombre: ""',
            'value' => '((isset($data->especificacionEstatus->nombre) != null) && (isset($data->especificacionEstatus->estatusDocente->nombre) != null))? $data->especificacionEstatus->nombre."[".$data->especificacionEstatus->estatusDocente->nombre."]" : $data->especificacionEstatus->nombre',
           //'filter' => CHtml::textField('RestriccionEstatus[especificacion_estatus_id]', $model->especificacion_estatus_id, array('title' => '',)), 
           'filter' => CHtml::listData($espe, 'especificacion_estatus_id', 'nombre'),
            // 'filter' => CHtml::dropDownList('RestriccionEstatus[especificacionEstatus]', $model->especificacionEstatus, array($espe), array('title' => '')),
        ),
       
        array(
//            'header' => '<center>Estatus</center>',
//            'name' => 'estatus',
//            'htmlOptions' => array(),
//            //'filter' => CHtml::textField('RestriccionEstatus[estatus]', $model->estatus, array('title' => '',)),
           
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'value' => array($this, 'getEstatus'),
                            'filter' => CHtml::dropDownList('RestriccionEstatus[estatus]', $model->estatus, array('' => '- - -', 'A' => 'Activo', 'I' => 'Inactivo',), array('title' => '')),
                     
        ),
		
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>
<div id="dialogInhabilitar" class="hide">

    <div class="alert alert-warning">
        <p class="bigger-110 center"> ¿Desea usted Inhabilitar esta Restricción de Estatus?</p>
    </div>
</div>
<div id="dialogActivar" class="hide">

    <div class="alertDialogBox">
        
        <p class="bolder center grey"> ¿Desea usted Activar esta Restricción de Estatus?</p>
   </div>
</div>
