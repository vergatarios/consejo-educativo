<?php
/* @var $this SituacionCargoController */
/* @var $model SituacionCargo */

$this->pageTitle = 'Registro de Situacion Cargos';

$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Situación de Cargos'=>array('/catalogo/situacionCargo'),
    'Administración',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>