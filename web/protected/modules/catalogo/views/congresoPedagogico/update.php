<?php
/* @var $this CongresoPedagogicoController */
/* @var $model CongresoPedagogico */

$this->pageTitle = 'Actualización de Datos de Congreso Pedagogicos';

      $this->breadcrumbs=array(
	'Congreso Pedagogicos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>