<?php
/* @var $this AreaComunController */
/* @var $model AreaComun */

$this->pageTitle = 'Registro de Area Comuns';

$this->breadcrumbs=array(
    'Catálogo'=>array('/catalogo/'),
    'Área Común'=>array('/catalogo/areaComun/lista'),
    'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>