<?php
/* @var $this EstudioController */
/* @var $model Estudio */

$this->pageTitle = 'Actualización de Datos de Estudios';

$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Estudios'=>array('lista'),
    'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion','estatus'=>$estatus)); ?>