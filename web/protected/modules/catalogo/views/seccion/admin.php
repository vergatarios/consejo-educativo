

<?php
/* @var $this SeccionController */
/* @var $model Seccion */

$this->breadcrumbs = array(
    'Catalogo' => array('../catalogo/'),
    'Secciones',
);
?>

<div id="resultadoElim">
</div>

<div class="tab-pane active" id="registrarS">

    <div class="widget-box">

        <?php
        Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#seccion-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
        ?>

        <?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
        <div class="search-form" style="display:none">
            <?php
            $this->renderPartial('_search', array(
                'model' => $model,
            ));
            ?>
        </div><!-- search-form -->
        <div id ="guardoRegistro" class="successDialogBox" style="display: none">
            <p>
                Registro Exitoso
            </p>
        </div>

        <div class="widget-header">
            <h4 style="color: #C43030">Secciones</h4>

            <div class="widget-toolbar">
                <a href="#" data-action="collapse">
                    <i class="icon-chevron-up"></i>
                </a>
            </div>

        </div>

        <div id="_formS" class="widget-body">
            <div style="display:block;" class="widget-body-inner">
                <div class="widget-main form">
                    <div id="gridSeccion">
                        <div class="pull-right" style="padding-left: 20px; padding: 20px">
                            <button  id = "btnRegistrarSeccion"  class="btn btn-success btn-next btn-sm" type="button" data-last="Finish" onClick="agregarSeccion()">
                                <i class="fa fa-plus icon-on-right"></i>
                                Agregar Sección
                            </button>
                        </div>

                        <?php
                        $this->widget('zii.widgets.grid.CGridView', array(
                            'id' => 'seccion-grid',
                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                            'summaryText' => false,
                            'dataProvider' => $model->search(),
                            'filter' => $model,
                            'afterAjaxUpdate' => "function(){

                             $('#seccionNombre').bind('keyup blur', function() {
                                keyAlpha(this, false);// true acepta la ñ y para que sea español
                                makeUpper(this);
                            });

                         }",
                            'columns' => array(
                                array(
                                    'header' => '<center>Nombre de Sección</center>',
                                    'name' => 'nombre',
                                    'filter' => CHtml::textField('Seccion[nombre]', null, array('maxlength' => 1, 'id' => 'seccionNombre')),
                                ),
                                array(
                                    'header' => '<center>Estatus</center>',
                                    'name' => 'estatus',
                                    'filter' => array(
                                        'A' => 'Activo',
                                        'E' => 'Inactivo'
                                    ),
                                    'value' => array($this, 'estatusSeccion'),
                                ),
                                array(
                                    'type' => 'raw',
                                    'header' => '<center>Acciones</center>',
                                    'value' => array($this, 'columnaAcciones'),
                                    'htmlOptions' => array('nowrap' => 'nowrap', 'style' => 'width: 100px'),
                                ),
                            ),
                            'pager' => array(
                                'header' => '',
                                'htmlOptions' => array('class' => 'pagination'),
                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                            ),
                        ));
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-2">
        <div class="col-md-6">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("catalogo"); ?>" class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
    </div>
</div>

<div id="dialog_registrarSeccion" class="hide">
<?php $this->renderPartial('_form', array('model' => $model)); ?>
</div>

<div id="dialog_vizualizar" class="hide">
</div>

<div id="dialog_eliminacion" class="hide">
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas seguro(a) que desea eliminar esta sección?
        </p>
    </div>
</div>

<div id="dialog_activacion" class="hide">
    <div class="alertDialogBox bigger-110">
        <p class="bigger-110 bolder center grey">
            &iquest;Estas seguro(a) que desea activar esta sección?
        </p>
    </div>
</div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="css_js">
    <?php
    Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/seccion.js', CClientScript::POS_END);
    ?>
</div>