<?php
/* @var $this CampoConocimientoController */
/* @var $model CampoConocimiento */

$this->pageTitle = 'Actualización de Datos de Campo Conocimientos';

      $this->breadcrumbs=array(
	'Campo Conocimientos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>