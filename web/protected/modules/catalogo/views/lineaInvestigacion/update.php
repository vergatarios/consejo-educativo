<?php
/* @var $this LineaInvestigacionController */
/* @var $model LineaInvestigacion */

$this->pageTitle = 'Actualización de Datos de Linea Investigacions';

      $this->breadcrumbs=array(
	'Linea Investigacions'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>