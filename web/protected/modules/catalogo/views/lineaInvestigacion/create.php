<?php
/* @var $this LineaInvestigacionController */
/* @var $model LineaInvestigacion */

$this->pageTitle = 'Registro de Linea Investigacions';

      $this->breadcrumbs=array(
	'Linea Investigacions'=>array('lista'),
	'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>