<?php
/* @var $this EstatusPlantelController */
/* @var $model EstatusPlantel */

$this->breadcrumbs=array(
	'Estatus Plantels'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List EstatusPlantel', 'url'=>array('index')),
	array('label'=>'Create EstatusPlantel', 'url'=>array('create')),
	array('label'=>'View EstatusPlantel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage EstatusPlantel', 'url'=>array('admin')),
);
?>


<?php $this->renderPartial('_form', array('model'=>$model,'subtitulo'=>'Modificar Estatus de Plantel')); ?>