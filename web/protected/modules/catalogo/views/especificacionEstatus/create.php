<?php
/* @var $this EspecificacionEstatusController */
/* @var $model EspecificacionEstatus */

$this->pageTitle = 'Registro de Especificacion Estatuses';

$this->breadcrumbs=array(
    'Catálogos' => array('/catalogo/'),
    'Especificacion Estatus'=>array('lista'),
    'Registro',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>