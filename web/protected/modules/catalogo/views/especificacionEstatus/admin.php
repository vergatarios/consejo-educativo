<?php
/* @var $this EspecificacionEstatusController */
/* @var $model EspecificacionEstatus */

$this->breadcrumbs = array(
    'Catálogos' => array('/catalogo/'),
    'Especificacion de Estatus' => array('/catalogo/especificacionEstatus'),
    'Administración',
);
$this->pageTitle = 'Administración de Especificación Estatus';
?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Especificacion Estatus</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <!--        <div style="display:block;" class="widget-body-inner">-->
        <div class="widget-main">

            <!--                <div class="row space-6"></div>-->
            <div>
                <div class="row col-sm-12" id="resultadoOperacion">
                    <!--                        <div class="infoDialogBox">
                                                <p>
                                                    En este módulo podrá registrar y/o actualizar los datos de Especificacion Estatus.
                                                </p>
                                            </div>-->
                </div>
                <div class="row space-6"></div>
                <!-- --><?php
                /*                if (Yii::app()->user->pbac('catalogo.especificacionEstatus.admin')):
                                    */?>
                <div class="pull-right" style="padding-left:10px;">
                    <a  type="submit" onclick="VentanaDialog('', '/catalogo/especificacionEstatus/registro', 'Especificación Estatus', 'create', '')" data-last="Finish" class="btn btn-success btn-next btn-sm">
                        <i class="fa fa-plus icon-on-right"></i>
                        Registrar Especificación de Estatus
                    </a>
                </div>
                <!-- --><?php
                /*                endif;
                                */?>
                <div class="row space-20"></div>
                <!--                 <div class="pull-right" style="padding-left:10px;">
                        <a href="<?php echo $this->createUrl("/catalogo/especificacionEstatus/registro"); ?>" type="submit" id='newRegister' data-last="Finish" class="btn btn-success btn-next btn-sm">-->
                <!--<div class="pull-right" style="padding-left:10px;">
                    <a  type="submit" onclick="VentanaDialog('', '/catalogo/especificacionEstatus/registro', 'Especificación Estatus', 'create', '')" data-last="Finish" class="btn btn-success btn-next btn-sm">
                        <i class="fa fa-plus icon-on-right"></i>
                        Registrar Nuevo Especificacion Estatus                        </a>
                </div>-->
                <!--                    <div class="row space-20"></div>

                                </div>-->

                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'id' => 'especificacion-estatus-grid',
                    'dataProvider' => $dataProvider,
                    'filter' => $model,
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'summaryText' => 'Mostrando {start}-{end} de {count}',
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'afterAjaxUpdate' => "
                function(){
                  
                    $('#EspecificacionEstatus_fecha_ini').datepicker();
                    $.datepicker.setDefaults($.datepicker.regional = {
                            dateFormat: 'dd-mm-yy',
                            showOn:'focus',
                            showOtherMonths: false,
                            selectOtherMonths: true,
                            changeMonth: true,
                            changeYear: true,
                            minDate: new Date(1800, 1, 1),
                            maxDate: 'today'
                        });
                    
                    $('#EspecificacionEstatus_nombre').unbind('keyup blur');
                    $('#EspecificacionEstatus_nombre').on('keyup blur', function () {
                        keyText(this, true);
                    });
                    
                    $('#EspecificacionEstatus_nombre').unbind('blur');
                    $('#EspecificacionEstatus_nombre').on('blur', function () {
                        clearField(this);
                    });
                    
                     $('#EspecificacionEstatus_fecha_ini').on('dblclick', function(){
                        $(this).val('');
                        $('#especificacion-estatus-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    });

                }",
                    'columns' => array(
//        array(
//            'header' => '<center>id</center>',
//            'name' => 'id',
//            'htmlOptions' => array(),
//            //'filter' => CHtml::textField('EspecificacionEstatus[id]', $model->id, array('title' => '',)),
//        ),
                        array(
                            'header' => '<center>Nombre</center>',
                            'name' => 'nombre',
                            'htmlOptions' => array(),
                            //'filter' => CHtml::textField('EspecificacionEstatus[nombre]', $model->nombre, array('title' => '',)),
                        ),
                        array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'value' => array($this, 'getEstatus'),
                            'filter' => CHtml::dropDownList('EspecificacionEstatus[estatus]', $model->estatus, array('' => '- - -', 'A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado'), array('title' => '')),
                        ),
                        array(
                            'header' => '<center>Fecha de Creación</center>',
                            'name' => 'fecha_ini',
                            'htmlOptions' => array(),
                            'value' => array($this, 'getFechaIni'),
                            'filter' => CHtml::textField('EspecificacionEstatus[fecha_ini]', $model->fecha_ini, array('title' => 'Fecha de Creación', 'readOnly' => 'readOnly')),
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Acción</center>',
                            'value' => array($this, 'getActionButtons'),
                            //'htmlOptions' => array('nowrap' => 'nowrap'),
                        ),
                    ),
                ));
                ?>
            </div>
        </div>
        <!--    </div>-->
    </div>
    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
    <div id="dialogPantalla" class="hide"></div>
<?php
Yii::app()->clientScript->registerScriptFile(
    Yii::app()->request->baseUrl . '/public/js/modules/catalogo/EspecificacionEstatus/index.js', CClientScript::POS_END
);
?>