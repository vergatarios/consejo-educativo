<?php
/* @var $this EspecificacionEstatusController */
/* @var $model EspecificacionEstatus */

$this->pageTitle = 'Actualización de Datos de Especificacion Estatuses';

$this->breadcrumbs=array(
    'Catálogos' => array('/catalogo/'),
    'Especificacion Estatus'=>array('lista'),
    'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>