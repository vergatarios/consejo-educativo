<?php
/* @var $this DenominacionTipoPersonalController */
/* @var $model DenominacionTipoPersonal */

$this->pageTitle = 'Actualización de Datos de Denominacion Tipo Personals';

$this->breadcrumbs=array(
	'Catálogos'=>array('/catalogo/'),
	'Denominación por Tipo de Personal'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>