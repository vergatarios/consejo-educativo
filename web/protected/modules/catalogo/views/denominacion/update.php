<?php
/* @var $this DenominacionController */
/* @var $model Denominacion */

$this->pageTitle = 'Actualización de Datos de Denominacions';

$this->breadcrumbs=array(
    'Catálogos'=>array('/catalogo/'),
    'Denominación Personal'=>array('lista'),
    'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion','estatus'=>$estatus)); ?>