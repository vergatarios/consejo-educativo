<?php
/* @var $this ModalidadController */
/* @var $model Modalidad */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'modalidad-form',
								// Please note: When you enable ajax validation, make sure the corresponding
								// controller action is handling ajax validation correctly.
								// There is a call to performAjaxValidation() commented in generated controller code.
								// See class documentation of CActiveForm for details on this.
								'enableAjaxValidation'=>false,
							)); ?>
							<!--Evitar la redireccion-->

<div class="widget-box">

        <div class="widget-header">
            <h5><?php echo $subtitulo; ?></h5>

            <div class="widget-toolbar">
                <a >
                    <i class="icon-chevron-up"></i>
                </a>
            </div>
        </div>

							<div class="form">

							
								 <div class="widget-body">
							        
							            	<div class="widget-main form">


							                             
							                                <?php
							                                    if($form->errorSummary($model)):
							                                ?>
							                                <div id ="div-result-message" class="errorDialogBox" >
							                                    <?php echo $form->errorSummary($model); ?>
							                                </div>
							                                <?php
							                                    endif;
							                                ?>
								                               


							                                    <div class="row">


							                                         <input type="hidden" id='id' name="id" value="<?php echo base64_encode($model->id); ?>" />
							                                                                
							                                             <div class="col-md-6">
							<label class="col-md-12" for="groupname">Nombre o Descripción</label>
							<?php echo
							$form->textField($model,'nombre',array('maxlength'=>160,'class
							'=>'span-7', 'required'=>'required')); ?>
							</div>                                     </div>
							<div class="row">
							                                        		
							                                        		
							                                        	</div>
							                </div>

							   
							                        <!--	<div class="form action-center">
							                            					<button type="submit" data-last="Finish" class="btn btn-primary btn-next">
							                                                    Guardar
							                                                    <i class="icon-arrow-right icon-on-right"></i>
							                                                </button>

							                        		<?php /*echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); */?>
							                        		</div>-->
									


         </div>
     </div>
 </div>


<?php $this->endWidget(); ?>
<!-- form -->
<script>
$(document).ready(function (){

$("#Modalidad_nombre").keyup(function(){

$("#Modalidad_nombre").val($("#Modalidad_nombre").val().toUpperCase());

});

$('#Modalidad_nombre').bind('keyup blur', function () {
 keyText(this, true);

});

$('#Modalidad_nombre').bind('blur', function () {
  clearField(this);
 });

							});
</script>
