<?php
/* @var $this DependenciaNominalController */
/* @var $model DependenciaNominal */

$this->breadcrumbs=array(
	'Dependencia Nominals'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List DependenciaNominal', 'url'=>array('index')),
	array('label'=>'Manage DependenciaNominal', 'url'=>array('admin')),
);
?>

<h1>Create DependenciaNominal</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>