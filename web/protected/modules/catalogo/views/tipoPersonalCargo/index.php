<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->request->baseUrl . '/public/js/modules/catalogo/TipoPersonalCargo/index.js', CClientScript::POS_END
);
?>

<?php

/* @var $this TipoPersonalCargoController */
/* @var $model TipoPersonalCargo */

$this->breadcrumbs=array(
	'Tipo Personal Cargos'=>array('/catalogo/tipoPersonalCargo'),
	'Administración',
);
$this->pageTitle = 'Administración de Tipo Personal Cargos';

?>
<div class="widget-box">
    <div class="widget-header">
        <h5>Lista de Tipo Personal Cargos</h5>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>
    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">

                <div class="row space-6"></div>
                <div>
                    <div id="resultadoOperacion">                     
                    </div>
                    <div class="row space-6"></div>
                <?php
                if (Yii::app()->user->pbac('catalogo.tipopersonalcargo.index')):
                    ?>
                     <div class="pull-right" style="padding-left:10px;">
                                            <a  type="submit" onclick="VentanaDialog('', '/catalogo/tipoPersonalCargo/registro', 'Tipo Personal Cargo', 'create', '')" data-last="Finish" class="btn btn-success btn-next btn-sm">
                                                <i class="fa fa-plus icon-on-right"></i>
                                                Registrar Tipo de Personal Cargo
                                            </a>
                                        </div>
                                        <?php
                                    endif;
                                    ?>
                    <div class="row space-20"></div>

                </div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tipo-personal-cargo-grid',
	'dataProvider'=>$dataProvider,
        'filter'=>$model,
        'itemsCssClass' => 'table table-striped table-bordered table-hover',
        'summaryText' => false,
        'pager' => array(
            'header' => '',
            'htmlOptions' => array('class' => 'pagination'),
            'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
            'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
            'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
            'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
        ),
        'afterAjaxUpdate' => "
                function(){
                 $('#TipoPersonalCargo_fecha_ini').datepicker();
                    $('#TipoPersonalCargo_fecha_act').datepicker();
                    $.datepicker.setDefaults($.datepicker.regional = {
                            dateFormat: 'dd-mm-yy',
                            showOn:'focus',
                            showOtherMonths: false,
                            selectOtherMonths: true,
                            changeMonth: true,
                            changeYear: true,
                            minDate: new Date(1800, 1, 1),
                            maxDate: 'today'
                        });
                    
                    $('#TipoPersonalCargo_fecha_act').on('dblclick', function(){
                        $(this).val('');
                        $('#tipo-personal-cargo-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    });
                     $('#TipoPersonalCargo_fecha_ini').on('dblclick', function(){
                        $(this).val('');
                        $('#tipo-personal-cargo-grid').yiiGridView('update', {
                            data: $(this).serialize()
                        });
                    });
                }",
	'columns'=>array(
        
        array(
            'header' => '<center>Tipo de Personal</center>',
            'name' => 'tipo_personal_id',
            //'htmlOptions' => array(),          
            'value' => '(is_object($data->tipoPersonal) && isset($data->tipoPersonal->nombre))? $data->tipoPersonal->nombre: ""',
            //'filter' => CHtml::textField('TipoPersonalCargo[tipo_personal_id]', $model->tipo_personal_id, array('title' => '',)),
            
        ),
        array(
            'header' => '<center>Cargo</center>',
            'name' => 'cargo_id',
            'htmlOptions' => array(),
            'value' => '(is_object($data->cargo) && isset($data->cargo->nombre))? $data->cargo->nombre: ""',
            //'filter' => CHtml::textField('TipoPersonalCargo[cargo_id]', $model->cargo_id, array('title' => '',)),
        ),
        array(
            'header' => '<center>Fecha de Creación</center>',
            'name' => 'fecha_ini',
            'htmlOptions' => array(),
            'value' => array($this, 'getFechaIni'),
            'filter' => CHtml::textField('TipoPersonalCargo[fecha_ini]', $model->fecha_ini, array('title' => 'Fecha de Creación', 'readOnly' => 'readOnly')),
        ),
        array(
            'header' => '<center>Fecha de Actualización</center>',
            'name' => 'fecha_act',
            'htmlOptions' => array(),
            'value' => array($this, 'getFechaAct'),
            'filter' => CHtml::textField('TipoPersonalCargo[fecha_act]', $model->fecha_act, array('title' => 'Fecha de Actualización al hacer, Para ver lista hacer Doble Click', 'readOnly' => 'readOnly',)),
        ),
        
        array(
            'header' => '<center>Estatus</center>',
            'name' => 'estatus',
            'value' => array($this, 'getEstatus'),
            'filter' => CHtml::dropDownList('TipoPersonalCargo[estatus]', $model->estatus, array('' => '- - -', 'A' => 'Activo', 'I' => 'Inactivo', 'E' => 'Eliminado'), array('title' => '')),
        ),
		
		array(
                    'type' => 'raw',
                    'header' => '<center>Acción</center>',
                    'value' => array($this, 'getActionButtons'),
                    //'htmlOptions' => array('nowrap'=>'nowrap'),
                ),
	),
)); ?>
            </div>
        </div>
    </div>
</div>
<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
<div id="dialogPantalla" class="hide"></div>
<div id="dialogInhabilitar" class="hide">

    <div class="alert alert-info bigger-110">
        <p class="bigger-110 center"> ¿Desea usted Inhabilitar este Tipo de Personal Cargo?</p>
    </div>
</div>
<div id="dialogActivar" class="hide">

    <div class="alert alert-info bigger-110">
        <p class="bigger-110 center"> ¿Desea usted Activar este Tipo de Personal Cargo?</p>
    </div>
</div>