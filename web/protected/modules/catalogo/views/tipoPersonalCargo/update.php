<?php
/* @var $this TipoPersonalCargoController */
/* @var $model TipoPersonalCargo */

$this->pageTitle = 'Actualización de Datos de Tipo Personal Cargos';

      $this->breadcrumbs=array(
	'Tipo Personal Cargos'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>