<?php
/* @var $this CredencialController */
/* @var $model Credencial */

$this->breadcrumbs=array(
	'Credencials'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Credencial', 'url'=>array('index')),
	array('label'=>'Create Credencial', 'url'=>array('create')),
	array('label'=>'Update Credencial', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Credencial', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Credencial', 'url'=>array('admin')),
);
?>

<h1>View Credencial #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'nombre',
		'usuario_ini_id',
		'fecha_ini',
		'usuario_act_id',
		'fecha_act',
		'fecha_elim',
		'estatus',
	),
)); ?>
