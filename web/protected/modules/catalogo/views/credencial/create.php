<?php
/* @var $this CredencialController */
/* @var $model Credencial */

$this->breadcrumbs=array(
	'Credencials'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Credencial', 'url'=>array('index')),
	array('label'=>'Manage Credencial', 'url'=>array('admin')),
);
?>

<h1>Create Credencial</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>