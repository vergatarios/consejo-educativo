<?php
/* @var $this ProyectosEndogenosController */
/* @var $model ProyectosEndogenos */

$this->breadcrumbs=array(
	'Proyectos Endogenoses'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProyectosEndogenos', 'url'=>array('index')),
	array('label'=>'Create ProyectosEndogenos', 'url'=>array('create')),
	array('label'=>'View ProyectosEndogenos', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProyectosEndogenos', 'url'=>array('admin')),
);
?>

<h1>Update ProyectosEndogenos <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>