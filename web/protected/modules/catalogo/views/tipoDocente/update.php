<?php
/* @var $this TipoDocenteController */
/* @var $model TipoDocente */

$this->pageTitle = 'Actualización de Datos de Tipo Docentes';

      $this->breadcrumbs=array(
	'Tipo Docentes'=>array('lista'),
	'Actualización',
);
?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'edicion')); ?>