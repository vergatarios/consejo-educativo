<div class="col-md-offset-3 col-md-6">
    <?php
    if (isset($dataProvider) && $dataProvider !== array() && $dataProvider !== null)
        $this->widget('zii.widgets.grid.CGridView', array(
            'itemsCssClass' => 'table table-striped table-bordered table-hover',
            'id' => 'grados-grid',
            'dataProvider' => $dataProvider,
            //'filter' => $model,
            'pager' => array('pageSize' => 1),
            'summaryText' => false,
            'columns' => array(
                array(
                    'name' => 'nombre',
                    'type' => 'raw',
                    'header' => '<center>Nombre del Grado</center>'
                ),
//            array(
//                'name' => 'boton',
//                'type' => 'raw',
//                'header' => '<center>Acciones</center>',
//                'htmlOptions' => array('nowrap' => 'nowrap', 'width' => '15%'),
//            ),
            ),
            'pager' => array(
                'header' => '',
                'htmlOptions' => array('class' => 'pagination'),
                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
            ),
        ));

    else {
        ?>


        <?php
    }
    ?>
</div>
<div id = "dialog_asignatura">
</div>

<script type="text/javascript">
    var grado_id, asignaturas_ids, asignaturas, asignatura;
    function getDatosPhp() {
        asignaturas = '<?php isset($asignaturas) && $asignaturas !== array() ? print json_encode($asignaturas) : print (null) ?>';
        asignaturas_ids = '<?php isset($asignaturas_ids) && $asignaturas_ids !== array() ? print json_encode($asignaturas_ids) : print (null) ?>';
    }

    function dialog_asignatura() {
        var dialog_asignatura = $("#dialog_asignatura").removeClass('hide').dialog({
            modal: true,
            width: '800px',
            draggable: false,
            position: ['center', 50],
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-plus'></i> Asignación de Asignaturas</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                    "class": "btn btn-xs",
                    click: function() {
                        dialog_asignatura.dialog("close");
                    }
                }
            ]
        });


    }
    $('.edit-data-grado').unbind('click');
    $('.edit-data-grado').on('click',
            function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var nivel_id = $("#Plan_nivel_id").select2("val");
                divResult = 'dialog_asignatura';
                urlDir = 'condicionarGrado';
                datos = {
                    grado_id: id,
                    nivel_id: nivel_id
                };
                conEfecto = true;
                showHTML = true;
                method = 'get';
                callback = function() {
                    $("#asignatura").select2({
                        allowClear: true
                    });
                };
                executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                dialog_asignatura();
                //getDatosPhp();
            }

    );
</script>
</div>