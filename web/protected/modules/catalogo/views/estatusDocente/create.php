<?php
/* @var $this EstatusDocenteController */
/* @var $model EstatusDocente */

$this->pageTitle = 'Registro de Estatus Personal';

$this->breadcrumbs=array(
    'Catalogo'=>array('/catalogo/'),
    'Estatus Personal'=>array('/catalogo/estatusDocente/lista'),
    'Registro',
);


?>

<?php $this->renderPartial('_form', array('model'=>$model, 'formType'=>'registro')); ?>