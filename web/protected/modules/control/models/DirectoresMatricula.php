<?php

/**
 * Description of DirectoresMatricula
 *
 * @author ALEXIS MORENO
 */
class DirectoresMatricula extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'gplantel.autoridad_plantel';
    }


    public function reporteEstadisticoDirectores($level, $dependency_id = null) {

        $resultado = array();

        if (in_array($level, array('region', 'estado', 'municipio')) && (is_null($dependency_id) || is_numeric($dependency_id))) {

            if ($level == 'region') {
                $camposSeleccionados = "r.id, r.nombre, 'AAA'||r.nombre AS titulo ";
                $camposSeleccionadosTotales = "0 AS id, 'TOTAL' AS nombre , 'ZZZTOTAL' AS titulo";
                $camposAgrupados = "r.id, r.nombre, titulo";
                $where = '1 = 1';
                $sub_where = '';
                $sub_where_JOIN = '';
                $sub_where_TOTAL = 'WHERE';
                $sub_where_FINAL = '';
                $orderBy = 'titulo ASC, nombre ASC';
            } elseif ($level == 'estado') {
                $camposSeleccionados = "e.region_id AS region_id, r.nombre AS region, e.id AS id, e.nombre, 'AAA'||e.nombre AS titulo  ";
                $camposSeleccionadosTotales = "$dependency_id AS region_id, 'X' AS region, 0 AS id, 'TOTAL' AS nombre , 'ZZZTOTAL' AS titulo";
                $camposAgrupados = "e.region_id, r.nombre, e.id, e.nombre, titulo, r.id";
                $where = 'e.id != 45 AND e.region_id = ' . $dependency_id; // Excluye Dependencias Federales (Id=45)
                $sub_where = 'AND ge.id = e.id';
                $sub_where_JOIN = '';
                $sub_where_TOTAL = 'INNER JOIN public.estado ge ON gp.estado_id = ge.id WHERE ge.region_id = '.$dependency_id.' AND ';
                $sub_where_FINAL = '';
                $orderBy = 'titulo ASC, nombre ASC';
                
            } elseif ($level == 'municipio') {
                
                $camposSeleccionados = "e.region_id AS region_id, r.nombre AS region, e.id AS estado_id, e.nombre AS estado, m.id, m.nombre, 'AAA'||m.nombre AS titulo  ";
                $camposSeleccionadosTotales = "0 AS region_id, 'X' AS region, $dependency_id AS estado_id, 'X' AS estado, 0 AS id, 'TOTAL' AS nombre , 'ZZZTOTAL' AS titulo";
                $camposAgrupados = "e.region_id, r.nombre, e.id, e.nombre, m.id, m.nombre, titulo, r.id";
                $where = 'm.estado_id = ' . $dependency_id;
               
                $sub_where = 'AND ge.id = e.id';
                $sub_where_JOIN = '';
                $sub_where_TOTAL = 'WHERE gp.estado_id = '.$dependency_id.' AND';
                $sub_where_FINAL = ' AND gp.municipio_id = m.id'; 
               
                $orderBy = 'titulo ASC, nombre ASC';
                
            }
            $periodoActualArray = PeriodoEscolar::model()->getPeriodoActivo();
            $periodoActual=$periodoActualArray['id'];

            $sql = "SELECT  $camposSeleccionados 
                            , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL) THEN 1 ELSE 0 END) AS con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL) THEN 1 ELSE 0 END) AS sin_director
                            , (    SELECT 
                                   SUM(CASE WHEN (u.last_login IS NOT NULL) THEN 1 ELSE 0 END) 
                                   FROM seguridad.usergroups_user u 
                                   LEFT JOIN gplantel.plantel gp on u.id = gp.director_actual_id 
                                   LEFT JOIN public.estado ge ON ge.region_id = r.id  $sub_where
                                       $sub_where_JOIN
                                   WHERE gp.estado_id = ge.id $sub_where_FINAL
                              ) AS accedieron
                            , (    SELECT 
                                   SUM(CASE WHEN (u.last_login IS NULL) THEN 1 ELSE 0 END) 
                                   FROM seguridad.usergroups_user u 
                                   LEFT JOIN gplantel.plantel gp on u.id = gp.director_actual_id 
                                   LEFT JOIN public.estado ge ON ge.region_id = r.id $sub_where
                                        $sub_where_JOIN
                                   WHERE gp.estado_id = ge.id $sub_where_FINAL
                              ) AS noAccedieron

                             ,(    SELECT 
                                   COUNT(DISTINCT gp.id) 
                                   FROM matricula.inscripcion_estudiante ie 
                                   LEFT JOIN gplantel.plantel gp ON gp.id = ie.plantel_id 
                                   LEFT JOIN public.estado ge ON ge.region_id = r.id  $sub_where
                                        $sub_where_JOIN
                                   WHERE gp.estado_id = ge.id $sub_where_FINAL AND ie.periodo_id = $periodoActual
                              ) AS matriculo
                              ,(   SELECT COUNT(DISTINCT gp.id) 
                                   FROM gplantel.plantel gp 
                                   LEFT JOIN public.estado ge ON ge.region_id = r.id $sub_where
                                        $sub_where_JOIN
                                   WHERE gp.estado_id = ge.id $sub_where_FINAL AND gp.director_actual_id IS NOT NULL AND NOT EXISTS (
                                       SELECT * FROM matricula.inscripcion_estudiante ie WHERE ie.plantel_id = gp.id AND ie.periodo_id = $periodoActual
                                   )
                                )AS noMatriculo

                        FROM public.region r
                        
                        INNER JOIN public.estado e
                            ON r.id = e.region_id
                            
                        LEFT JOIN gplantel.plantel p
                            ON e.id = p.estado_id
                     
                        LEFT JOIN public.municipio m
                            ON p.municipio_id = m.id
                    WHERE
                        $where
                    GROUP BY
                       $camposAgrupados
                    
                    UNION
                            
                    SELECT  $camposSeleccionadosTotales 
                            , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL) THEN 1 ELSE 0 END) AS con_director
                            , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL) THEN 1 ELSE 0 END) AS sin_director
                            , SUM(CASE WHEN (u.last_login IS NOT NULL AND p.director_actual_id IS NOT NULL) THEN 1 ELSE 0 END) AS accedieron
                            , SUM(CASE WHEN (u.last_login IS  NULL AND p.director_actual_id IS NOT NULL) THEN 1 ELSE 0 END) AS noAccedieron
                          
                            , SUM( 
                                    CASE WHEN (
                                        (SELECT 
                                      COUNT(DISTINCT ie.plantel_id)
                                      FROM matricula.inscripcion_estudiante ie 
                                      INNER JOIN gplantel.plantel gp ON gp.id = ie.plantel_id 
                                      WHERE gp.director_actual_id IS NOT NULL AND ie.plantel_id = p.id ) > 0
                                       )
                                      THEN 1 
                                      ELSE 0 
                                      END
                              ) AS matriculo

                             , (SELECT COUNT(gp.id) FROM gplantel.plantel gp
                                      $sub_where_TOTAL gp.director_actual_id IS NOT NULL AND NOT EXISTS (SELECT * FROM
                                      matricula.inscripcion_estudiante ie WHERE ie.plantel_id = gp.id) ) AS noMatriculo

                                

                        FROM public.region r
                        
                        INNER JOIN public.estado e
                            ON r.id = e.region_id
                            
                        LEFT JOIN gplantel.plantel p
                            ON e.id = p.estado_id
                            
                        LEFT JOIN seguridad.usergroups_user u ON
                        u.id=p.director_actual_id
                     
                        LEFT JOIN public.municipio m
                            ON p.municipio_id = m.id
                    WHERE
                        $where

                    ORDER BY
                        $orderBy"

            ;

//            echo "<pre><code>$sql</code></pre>";
//            die();
            $connection = Yii::app()->db;
            $command = $connection->createCommand($sql);
            $resultado = $command->queryAll();
        }

        return $resultado;
    }

    public function reporteGrafico($estadoId = null) {
        $resultado = array();
        $camposSeleccionados = "e.region_id AS id, r.nombre AS region, e.id AS estado_id, e.nombre, 'AAA'||e.nombre AS titulo  ";
        $camposAgrupados = "e.region_id, r.nombre, e.id, e.nombre, titulo  ";
        $where = 'e.id != 45';
        $orderBy = 'titulo ASC, e.nombre ASC';

        if (!is_null($estadoId) && is_numeric($estadoId)) {
            $where .= " AND e.id = $estadoId ";
        }

        $sql = "SELECT  $camposSeleccionados
                        , SUM(CASE WHEN (p.id IS NOT NULL) THEN 1 ELSE 0 END) AS planteles
                        , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NOT NULL) THEN 1 ELSE 0 END) AS con_director
                        , SUM(CASE WHEN (p.id IS NOT NULL AND p.director_actual_id IS NULL) THEN 1 ELSE 0 END) AS sin_director
                FROM public.region r
                    INNER JOIN public.estado e
                        ON r.id = e.region_id
                    LEFT JOIN gplantel.plantel p
                        ON e.id = p.estado_id
                WHERE
                    $where
                GROUP BY
                    $camposAgrupados
                ORDER BY
                    $orderBy";

        //ld($sql);

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $resultado = $command->queryAll();

        return $resultado;
    }
    
    
    
       public function reporteDetalladoDirectores($column, $level, $dependency, $fecha = null, $orderBy = null) {

        $columnFilter = $this->getColumnFilter($column, $fecha);
        $drillDownFilter = $this->getGeoDrillDownFilter($level, $dependency);

        $where = $drillDownFilter . ' AND ' . $columnFilter.' ';

        if (is_null($orderBy)) {
            $orderBy = 'p.cod_plantel,'
                     . 'p.nombre, '
                     . 'es.nombre, '
                     . 'mc.nombre, '
                     . 'pq.nombre, '
                     . 'fundacion';
        }

        $sql = "SELECT DISTINCT
                    p.cod_plantel,
                    p.cod_estadistico,
                    dn.nombre AS denominacion,
                    p.nombre,
                    ze.nombre AS zona_educativa,
                    td.nombre AS tipo_dependencia,
                    ep.nombre AS estatus,
                    p.annio_fundado AS fundacion,
                    es.nombre AS estado,
                    mc.nombre AS municipio,
                    pq.nombre AS parroquia,
                    p.direccion,
                    p.correo,
                    p.telefono_fijo,
                    p.telefono_otro,
                    zu.nombre AS zona_ubicacion,
                    cp.nombre AS clase_plantel,
                    c.nombre AS categoria,
                    ce.nombre AS condicion_estudio,
                    g.nombre AS tipo_matricula,
                    t.nombre AS turno,
                    m.nombre AS modalidad,
                    u.cedula AS dir_cedula,
                    u.nombre AS dir_nombre,
                    u.apellido AS dir_apellido,
                    u.username AS dir_usuario,
                    u.telefono AS dir_telefono,
                    u.telefono_celular AS dir_celular,
                    u.email AS dir_email,
                    u.twitter AS dir_twitter
                FROM
                    gplantel.plantel p
                    LEFT JOIN gplantel.zona_educativa ze ON p.zona_educativa_id = ze.id
                    LEFT JOIN public.estado es ON p.estado_id = es.id
                    LEFT JOIN public.municipio mc ON p.municipio_id = mc.id
                    LEFT JOIN public.parroquia pq ON p.parroquia_id = pq.id
                    LEFT JOIN gplantel.modalidad m ON p.modalidad_id = m.id
                    LEFT JOIN gplantel.tipo_dependencia td ON p.tipo_dependencia_id = td.id
                    LEFT JOIN gplantel.estatus_plantel ep ON p.estatus_plantel_id = ep.id
                    LEFT JOIN gplantel.clase_plantel cp ON p.clase_plantel_id = cp.id
                    LEFT JOIN gplantel.categoria c ON p.categoria_id = c.id
                    LEFT JOIN gplantel.condicion_estudio ce ON p.condicion_estudio_id = ce.id
                    LEFT JOIN gplantel.genero g ON p.genero_id = g.id
                    LEFT JOIN gplantel.turno t ON p.turno_id = t.id
                    LEFT JOIN seguridad.usergroups_user u ON p.director_actual_id = u.id
                    LEFT JOIN gplantel.zona_ubicacion zu ON p.zona_ubicacion_id = zu.id
                    LEFT JOIN gplantel.autoridad_plantel ap ON p.id=ap.plantel_id
                    LEFT JOIN gplantel.denominacion dn ON p.denominacion_id = dn.id
                WHERE
                    $where
                ORDER BY
                    $orderBy";

        $connection = Yii::app()->db;
        $command = $connection->createCommand($sql);
        $resultado = $command->queryAll();

        return $resultado;
    }

    private function getColumnFilter($column, $fecha = null) {

        switch ($column) {
            case 'planteles':
                $where = "p.id IS NOT NULL";
                break;
            case 'con_director':
                $where = "p.id IS NOT NULL AND p.director_actual_id IS NOT NULL";
                break;
            case 'accedieron':
                $where = "p.id IS NOT NULL AND u.last_login IS NOT NULL";
                break;
            case 'noaccedieron':
                $where = "p.id IS NOT NULL AND p.director_actual_id IS NOT NULL AND u.last_login IS NULL";
                break;
            case 'matriculo':
                $where = "p.id IS NOT NULL AND p.director_actual_id IS NOT NULL AND ( SELECT COUNT(DISTINCT ie.id) FROM matricula.inscripcion_estudiante ie WHERE ie.plantel_id = p.id ) > 0";
                break;
            case 'nomatriculo':
                $where = "p.id IS NOT NULL  AND p.director_actual_id IS NOT NULL AND ( SELECT COUNT(DISTINCT ie.id) FROM matricula.inscripcion_estudiante ie WHERE ie.plantel_id = p.id ) = 0";
                break;
            default:
                $where = "p.id IS NULL";
                break;
        }

        return $where;
    }

    private function getGeoDrillDownFilter($level, $dependency) {
        switch ($level) {
            case 'region':
                if ((int) $dependency !== 0)
                    $where = 'es.region_id = ' . $dependency . ' AND es.id != 45'; //No incluye Dependencias Federales (45)
                else
                    $where = 'es.id != 45'; //No incluye Dependencias Federales (45)
                break;
            case 'estado':
                $where = 'es.id = ' . $dependency . ' AND es.id != 45'; //No incluye Dependencias Federales (45)
                break;
            case 'municipio':
                $where = 'mc.id = ' . $dependency . ' AND es.id != 45'; //No incluye Dependencias Federales (45)
                break;
            default:
                $where = '1 = 1';
                break;
        }

        return $where;
    }

}
