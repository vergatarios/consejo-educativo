
<div class="breadcrumb row row-fluid" style="margin-left: 2px;">
    <li>
        <?php if($nivel=='region'): ?><a>Región</a><?php else: ?><a onclick="reporteEstadisticoDirectores('region', 'x');">Región</a><?php endif; ?>
    </li>
    <?php if(!is_null($region)): ?>
    <li>
       <a title="Estados de la Región <?php echo ucwords(strtolower($region->nombre)); ?>" onclick="reporteEstadisticoDirectores('estado', '<?php echo ucwords(strtolower($region->id)); ?>');">
           <?php echo ucwords(strtolower($region->nombre)); ?>
       </a>
    </li>
    <?php endif; ?>
    <?php if(!is_null($estado)): ?>
    <li>
        <a title="Municipios del Estado <?php echo ucwords(strtolower($estado->nombre)); ?>" onclick="reporteEstadisticoDirectores('estado', '<?php echo ucwords(strtolower($estado->id)); ?>');">
            <?php echo ucwords(strtolower($estado->nombre)); ?>
        </a>
    </li>
    <?php endif; ?>
</div>

<div class="space-6"></div>

<table class="report table table-striped table-bordered table-hover">
    
    <thead>
        <tr>
            <th nowrap rowspan="2" class="center" style="width: 15%;">
                <?php echo $titulo; ?>
            </th>
            
            <th title="Planteles Con Director Registrados" rowspan="2" class="center">
               Planteles Con Director Registrados
            </th>
            
            <th title="Planteles con Directores que Ingresaron al Sistema" rowspan="2" class="center">
               Planteles con Directores que Ingresaron al Sistema
            </th>
            
            <th title="Planteles con Directores sin Ingresar al Sistema" rowspan="2" class="center">
               Planteles con Directores sin Ingresar al Sistema
            </th>
            
            <th title="Planteles que han inscrito o matriculado estudiantes" rowspan="2" class="center">
               Planteles con Directores Donde se ha Matriculado
            </th>
            <th title="Planteles sin Matricular Estudiantes" rowspan="2" class="center">
               Planteles con Director sin Matricular
            </th>
            <th title="Porcentaje" rowspan="2" class="center">
                %
            </th>
    </thead>
    
    <tbody>
        <?php if(empty($dataReport)): ?>
        <tr>
            <td colspan="11">
                <div class="alertDialogBox" style="margin-top: 10px;">
                    <p>
                        No se han encontrado Registros.
                    </p>
                </div>
            </td>
        </tr>
        <?php else: ?>
        
        <?php foreach ($dataReport as $data): ?>
        
        <?php 
            
            $avance = 0;
            if((int)$data['planteles']>0):
                $avance = ($data['con_director']*100)/$data['planteles'];
            endif;
            
            $avanceDirectores = 0;
            if((int)$data['planteles']>0):
                $avanceDirectores = ($data['matriculo']*100)/$data['con_director'];
            endif;
            
            $dependencyId = 0;
            
            if(strtolower($data['nombre'])=='total'){
                
                if($nivel=='region'){
                    $dependencyId = 0;
                }
                elseif($nivel=='estado'){
                    $dependencyId = $data['region_id'];
                }
                elseif($nivel=='municipio'){
                    $dependencyId = $data['estado_id'];
                }
    
            }else{
                $dependencyId = $data['id'];
            }
        ?>
        
        <tr>
            <td class="center">
                <?php if($nivel=='region' && strtolower($data['nombre'])!='total'): ?>
                    <a onclick="reporteEstadisticoDirectores(<?php echo "'$siguienteNivel'"; ?>, <?php echo $data['id']; ?>);"><?php echo ucwords(strtolower($data['nombre'])); ?></a>
                <?php elseif($nivel=='estado' && strtolower($data['nombre'])!='total'): ?>
                    <a onclick="reporteEstadisticoDirectores(<?php echo "'$siguienteNivel'"; ?>, <?php echo $data['id']; ?>);"><?php echo ucwords(strtolower($data['nombre'])); ?></a>
                <?php elseif(strtolower($data['nombre'])=='total'): ?>
                    <a><b><?php echo ucwords(Utiles::strtolower_utf8($data['nombre'])); ?></b></a>
                <?php else: ?>
                    <span><?php echo ucwords(Utiles::strtolower_utf8($data['nombre'])); ?></span>
                <?php endif; ?>
            </td>
<!--            <td class="center">
                <a href="/control/directoresReporte/reporteDetalladoDirectores/col/planteles/lev/<?php echo (strtolower($data['nombre'])!='total')?$nivel:$anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                    <?php echo $data['planteles']; ?>
                </a>
            </td>-->
            <td class="center">
                <a href="/control/directoresReporte/reporteDetalladoDirectores/col/con_director/lev/<?php echo (strtolower($data['nombre'])!='total')?$nivel:$anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                    <?php echo $data['con_director']; ?>
                </a>
            </td>
         
            <td class="center">
                <a href="/control/directoresReporte/reporteDetalladoDirectores/col/accedieron/lev/<?php echo (strtolower($data['nombre'])!='total')?$nivel:$anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                    <?php echo $data['accedieron']; ?>
                </a>
            </td>
            
            <td class="center">
                <a href="/control/directoresReporte/reporteDetalladoDirectores/col/noaccedieron/lev/<?php echo (strtolower($data['nombre'])!='total')?$nivel:$anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                    <?php echo $data['noaccedieron'];?>
                </a>
            </td>
           
            <td class="center">
                <a href="/control/directoresReporte/reporteDetalladoDirectores/col/matriculo/lev/<?php echo (strtolower($data['nombre'])!='total')?$nivel:$anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                    <?php echo $data['matriculo']; ?>
                </a>
            </td>
           <td class="center">
                <a href="/control/directoresReporte/reporteDetalladoDirectores/col/nomatriculo/lev/<?php echo (strtolower($data['nombre'])!='total')?$nivel:$anteriorNivel; ?>/dep/<?php echo $dependencyId; ?>">
                    <?php echo $data['nomatriculo']; ?>
                </a>
            </td>
             <td class="center">
                <a><?php echo number_format($avanceDirectores,2,',','.'); ?></a>
            </td>
            <?php if ($nivel == 'estado' && strtolower($data['nombre']) != 'total' && Yii::app()->user->pbac('admin')): ?>
<!--            <td class="center">
                <div class="action-buttons">
                    <a title="Datos de Contacto" data-id="<?php echo $data['id']; ?>" class="fa icon-user contact-data"></a>&nbsp;&nbsp;
                    <a title="Registrar Observación" data-id="<?php echo $data['id']; ?>" data-estado="<?php echo $data['nombre']; ?>" class="fa fa-pencil green observ-data"></a>&nbsp;&nbsp;
                    <a title="Ver Registro de Control" data-id="<?php echo $data['id']; ?>" data-estado="<?php echo $data['nombre']; ?>" class="fa fa-file-text-o red rep-control"></a>
                </div>
            </td>-->
            <?php endif; ?>
        </tr>

        <?php endforeach; ?>
        
        <?php endif; ?>
    </tbody>
    
</table>
<span class="small">Reporte: <?php echo date("d-m-Y H:i:s"); 
$periodoActualArray = PeriodoEscolar::model()->getPeriodoActivo();
$periodoActual=$periodoActualArray['periodo'];
echo ' Perido Escolar '.$periodoActual;
?> </span>
<script type="text/javascript">
    $(document).ready(function(){
        
        $('.contact-data').unbind('click');
        $('.contact-data').on('click',
                function(e) {
                    e.preventDefault();
                    var estado_id = $(this).attr('data-id');
                    verDatosContacto('zona_educativa', estado_id);
                }
        );

        $('.observ-data').unbind('click');
        $('.observ-data').on('click',
                function(e) {
                    e.preventDefault();
                    var estado_id = $(this).attr('data-id');
                    var estado = $(this).attr('data-estado');
                    dialogObservacion(estado_id, estado);
                }
        );

        $('.rep-control').unbind('click');
        $('.rep-control').on('click',
                function(e) {
                    e.preventDefault();
                    var estado_id = $(this).attr('data-id');
                    var estado = $(this).attr('data-estado');
                    dialogRegistroControl('zona_educativa', estado_id, estado);
                }
        );
        
    });
</script>