<div class="breadcrumb row row-fluid" style="margin-left: 2px;">

     
    <li>
        <!--<a title="Municipios del Estado <?php //echo ucwords(strtolower()); ?>" onclick="reporteEstadistico('municipio', '<?php //echo ucwords(strtolower($estado->id)); ?>');">
   
          
        </a>     !-->
    </li>
    
</div>

                
 
        <?php foreach ($dataReport as $data): 
            
     //var_dump($dataReport);
  //echo $data['estado'];
            ?>
      
        <?php 
            
            $avance = 0;
            if((int)$data['total']>0):
            $avance = ($data['con_director_total']*100)/$data['total'];
            //echo $avance;
            endif;
            
            $dependencyId = 0;
            ?>
        <?php endforeach; ?>

       <table class="report table table-striped table-bordered table-hover">
    
    <thead>
        <tr>
            <th nowrap rowspan="2" class="center">
                <?php echo $titulo; ?>
            </th>
            <!--<th title="Total de Planteles" rowspan="2" class="center">
                Total
            </th>-->
            <th title="Planteles con Director Registrado" rowspan="2" class="center">
                Total de Planteles con Director
            </th>
           <!-- <th title="Porcentaje de Avance" rowspan="2" class="center">
                %
            </th>-->
            <th colspan="2" class="center" title="Directores de Planteles de Dependencia Nacional, Estadal o Municipal">
                Planteles Oficiales
            </th>
            <th colspan="2" class="center" title="Directores de Planteles Privados">
                Planteles Privados
            </th>
            <th colspan="2" class="center" title="Directores de Planteles Subvencionados">
                Otros Planteles
            </th>
            <?php if($nivel=='estado'): ?>
       
            <?php endif; ?>
        </tr>
        <tr>
            <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Sin Director Registrado">
                Sin Director
            </th>
            <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Con Director Registrado">
               Con Director
            </th>
            <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Sin Director Registrado">
                Sin Director
            </th>
            <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Con Director Registrado">
               Con Director
            </th>
            <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Sin Director Registrado">
                Sin Director
            </th>
            <th class="center" title="Planteles de Dependencia Nacional, Estadal o Municipal Con Director Registrado">
               Con Director
            </th>
           
        </tr>
   
    </thead>
    
    <tbody>
        <?php if(empty($dataReport)): ?>
        <tr>
            <td colspan="11">
                <div class="alertDialogBox" style="margin-top: 10px;">
                    <p>
                        No se han encontrado Registros.
                    </p>
                </div>
            </td>
        </tr>
        <?php else: ?>
        
        <?php foreach ($dataReport as $data): ?>
        
        <?php 
            
            $avance = 0;
            if((int)$data['total']>0):
                $avance = ($data['con_director_total']*100)/$data['total'];
            endif;
            
            $dependencyId = 0;
            
         //   if(strtolower($data['nombre'])=='total'){
                
              
               if($nivel=='estado'){
                    $dependencyId = $data['estado'];
                }
//                elseif($nivel=='municipio'){
//                    $dependencyId = $data['municipio'];
//                }
    
//            }else{
//                $dependencyId = $data['id'];
//            }
        ?>
        
        <tr>
      
<!--            <td class="center">-->
<!--                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/planteles/lev/--><?php //echo (strtolower($data['nombre'])!='total')?$nivel:$anteriorNivel; ?><!--/dep/--><?php //echo $dependencyId; ?><!--">-->
<!--                    --><?php //echo $data['planteles']; ?>
<!--                </a>-->
<!--            </td>-->
            <td class="center">
                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/planteles/lev/<?php echo (strtolower($data['estado']))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data['estado_id']; ?>">
                    <?php echo $data['estado']; ?>
                </a>
            </td>
             <td class="center">
                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/con_director/lev/<?php echo (strtolower($data['estado']))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data['estado_id']; ?>">
                    <?php echo $data['total']; ?>
                </a>
            </td>
                 <td class="center">
                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/publ_sin_director/lev/<?php echo (strtolower($data['estado']))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data['estado_id']; ?>">
                    <?php echo $data['sin_director_oficial']; ?>
                </a>
            </td>
                 <td class="center">
                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/publ_con_director/lev/<?php echo (strtolower($data['estado']))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data['estado_id']; ?>">
                    <?php echo $data['con_director_oficial']; ?>
                </a>
            </td>
                 <td class="center">
                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/priv_sin_director/lev/<?php echo (strtolower($data['estado']))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data['estado_id']; ?>">
                    <?php echo $data['sin_director_privado']; ?>
                </a>
            </td>
                 <td class="center">
                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/priv_con_director/lev/<?php echo (strtolower($data['estado']))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data['estado_id']; ?>">
                    <?php echo $data['con_director_privado']; ?>
                </a>
            </td>
                    <td class="center">
                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/otros_sin_director/lev/<?php echo (strtolower($data['estado']))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data['estado_id']; ?>">
                    <?php echo $data['sin_director_otros']; ?>
                </a>
            </td>
                 <td class="center">
                <a href="/control/autoridadesPlantel/reporteDetalladoDirectores/col/otros_con_director/lev/<?php echo (strtolower($data['estado']))?$nivel:$anteriorNivel; ?>/dep/<?php echo $data['estado_id']; ?>">
                    <?php echo $data['con_director_otros']; ?>
                </a>
            </td>
                   </tr>
                   

        <?php endforeach; ?>
        
        <?php endif; ?>
    </tbody>
    
</table>
<span class="small">Reporte: <?php echo date("d-m-Y"); ?></span> 
        
       