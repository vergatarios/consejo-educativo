
<div id="indexConsultarTitulo">
    <?php
    /* @var $this AtencionSolicitudController */

    $this->breadcrumbs = array(
       // 'Asignación de Seriales' => array("../../titulo/atencionSolicitud"),
    'Consultar Titulo'
);
    ?>

    <div class = "widget-box">

        <div class = "widget-header">
            <h5>Consultar Titulo</h5>

            <div class = "widget-toolbar">
                <a href = "#" data-action = "collapse">
                    <i class = "icon-chevron-down"></i>
                </a>
            </div>

        </div>

        <div class = "widget-body">
            <div style = "display: block;" class = "widget-body-inner">
                <div class = "widget-main">
                    <div id="errorConsultaTitulo" class="hide errorDialogBox" ><p></p> </div>
<!--                    <div id="busqueda" class="hide alertDialogBox" ><p></p> </div>
                    <div id="campos_vacios" class="hide alertDialogBox" ><p></p> </div>-->
                    <div id="informacion">
                        <div class="infoDialogBox">
                            <p>
                                Por favor ingrese la cédula de identidad y el tipo de documento de identidad del estudiante para su busqueda.
                            </p>
                        </div>
                        <div id="alertaConsultarTitulo"></div>
                    </div>

                    <div class="row row-fluid" style="padding-bottom: 20px; padding-top: 20px">

                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'busqueda_form',
                                // 'action' => 'atencionSolicitud/mostrarConsultaPlantel'
                        ));
                        ?>
                        <div id="1eraFila" class="col-md-12">

                            <div class="col-md-4" >
                                <?php echo CHtml::label('Tipo de Documento de Identidad <span class="required">*</span>', '', array("class" => "col-md-12", 'id' => 'tipo_nacionalidad')); ?><span ></span>
                                <?php echo CHTML::dropDownList('tipoNacionalidad', 'tipoBusqueda', array('V' => 'Venezolana', 'E' => 'Extrajera', 'P' => 'Pasaporte'), array('empty' => '-SELECCIONE-', 'class' => 'span-7', 'id' => 'tipoNacionalidad')); ?>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('Estudiante Egresado <span class="required">*</span>', '', array("class" => "col-md-12", 'id' => 'Estuadiante_Egresado')); ?><span ></span>
                                <div id="campoBusqueda" >
                                    <?php echo CHtml::textField('campoBusqueda', '', array('class' => 'span-7', 'maxlength' => 10, 'style' => 'width: 90%', 'id' => 'EstuadianteEgresado')); ?>
                                </div>
                            </div>

                            <div class="col-md-4" >
                                <?php echo CHtml::label('&nbsp;&nbsp;', '', array("class" => "col-md-12")); ?>
  <!--  <a id="btnConsultar" href="<?php //echo Yii::app()->createUrl("titulo/atencionSolicitud/mostrarConsultaPlantel/tipoBusqueda");                                                                                  ?>"class="btn btn-primary btn-sm">
        <i class="icon-arrow-left"></i>
                                    Consultar
                                </a> -->
                                <button  id = "btnConsultar"  class="btn btn-primary btn-sm" type="submit" data-last="Finish">
                                    <i class="fa fa-search icon-on-right"></i>
                                    Consultar
                                </button>
                            </div>

                        </div>

                        <div class = "col-md-12"><div class = "space-6"></div></div>
                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
</div>

<div id="ResultEgresados"  class="hide">

</div>
<hr>
<div class="col-md-6" style="padding-top: 1%">
    <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("../.."); ?>"class="btn btn-danger">
        <i class="icon-arrow-left"></i>
        Volver
    </a>
</div>

<script>
    $(document).ready(function() {


        $("#EstuadianteEgresado").bind('keyup blur', function () {
  keyAlphaNum(this, true, true);
  });


        $("#btnConsultar").unbind('click');
        $("#btnConsultar").click(function(e) {
            e.preventDefault();
            var tipo_busqueda;
            var EstuadianteEgresado;
            var style = 'alerta';
            var msgtitulo = "<p>Estimado usuario, por for se requiere que ingrese ambos campos para poder realizar esta acción .</p>";
            tipoNacionalidad = $("#tipoNacionalidad").val();
            EstuadianteEgresado = $("#EstuadianteEgresado").val();

            if (tipoNacionalidad != '' && EstuadianteEgresado != '') {

                var conEfecto = true;
                var showHTML = true;
                var method = 'POST';
                var divResult = 'result-solicitud';
                var urlDir = '/titulo/ConsultarTitulo/busquedaEstudianteEgresado/';
                var datos;
                datos = {
                    tipoNacionalidad: tipoNacionalidad,
                    EstuadianteEgresado: EstuadianteEgresado
                };
                Loading.show();
                $.ajax({
                    url: urlDir,
                    data: datos,
                    dataType: 'html',
                    type: method,
                    success: function(resp, resp2, resp3) {


                        try {
                            var json = jQuery.parseJSON(resp3.responseText);
   
                             if (json.statusCode == 'alert') {
//                                $("#errorConsultaTitulo").addClass('hide');
//                                $("#errorConsultaTitulo").addClass('errorDialogBox');
//                                $("#errorConsultaTitulo p").html('');
//                                $("#ResultEgresados").html('');
//                                $("#ResultEgresados").addClass('hide');
//                                $("#errorConsultaTitulo").removeClass('hide');
//                                $("#errorConsultaTitulo p").html(json.mensaje);                               
//                            $("html, body").animate({scrollTop: 0}, "fast");
                                $("#alertaConsultarTitulo").html('');
                                $("#ResultEgresados").html('');
                                $("#ResultEgresados").addClass('hide');
                                $("#errorConsultaTitulo").removeClass('hide');
                                $("#errorConsultaTitulo p").html(json.mensaje);

//                            $("html, body").animate({scrollTop: 0}, "fast");

Loading.hide();

                            }

                        } catch (e) {
//                            $("#errorConsultaTitulo p").html('');
//                            $("#errorConsultaTitulo").addClass('hide');
//                            $("#errorConsultaTitulo p").html('');
//                            $("#indexConsultarTitulo").html('');
//                            $("#ResultEgresados").removeClass('hide');
//                            $("#ResultEgresados").html(resp);



                            $("#errorConsultaTitulo").addClass('hide');
                            $("#errorConsultaTitulo p").html('');
                            $("#indexConsultarTitulo").html('');
                            $("#ResultEgresados").removeClass('hide');
                            $("#ResultEgresados").html(resp);
Loading.hide();
                        }


                    }

                });
            } else {
//                $("#errorConsultaTitulo").addClass('hide');
//                $("#errorConsultaTitulo p").html('');
//                $("#errorConsultaTitulo").removeClass('hide');
//                $("#errorConsultaTitulo").removeClass('errorDialogBox');
                $("#errorConsultaTitulo").addClass('hide');
                displayDialogBox('alertaConsultarTitulo', style, msgtitulo);
                $("#errorConsultaTitulo").addClass('hide');
                
            }
        });
    });

</script>

