<div id="index">
    <?php
    $this->breadcrumbs = array(
        'Asignación de Seriales por Estados',
    );
    ?>

    <div id="error" class="hide errorDialogBox" ><p></p> </div>
    <div id="exitoso" class="hide successDialogBox" >
        <p></p>

        <div class="col-md-6" style="padding-top: 4%">
            <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("/titulo/asignacionSerialesEstados"); ?>"class="btn btn-danger">
                <i class="icon-arrow-left"></i>
                Volver
            </a>
        </div>
    </div>

    <div id="asignacion">
        <div class = "widget-box">

            <div class = "widget-header">
                <h5>Asignación de Seriales por Estados</h5>

                <div class = "widget-toolbar">
                    <a href = "#" data-action = "collapse">
                        <i class = "icon-chevron-down"></i>
                    </a>
                </div>

            </div>

            <div class = "widget-body">
                <div style = "display: block;" class = "widget-body-inner">
                    <div class = "widget-main">
                        <div id="respuestaBuscar" class="hide errorDialogBox" ><p></p> </div>
                        <div id="busqueda" class="hide alertDialogBox" ><p></p> </div>
                        <div id="campos_vacios" class="hide alertDialogBox" ><p></p> </div>
                        <div id="informacion">
                            <div class="infoDialogBox">
                                <p>
                                    Por favor seleccione el estado para realizar la asignación de seriales por estados.
                                </p>
                            </div>
                            <?php
                            if (($verificarImpresionReporte == 2 && $resultadoPlanteles == array() && $mostrar_boton == false) || ($verificarImpresionReporte == 4 && $resultadoPlanteles == array() && $mostrar_boton == true) || ($verificarImpresionReporte == 2 && $resultadoPlanteles == array() && $mostrar_boton == true)) {
                                ?>
                                <div class="alertDialogBox">
                                    <p>
                                        Por favor ingrese en otro momento la asignación de seriales en el estado <?php echo $nombreEstado; ?> aun no ha culminado, cuando se realice la asignación se le notificará con un correo.
                                    </p>
                                </div>
                            <?php } ?>

                            <?php if ($verificarImpresionReportePDF == 2) { ?>
                                <div class="alertDialogBox">
                                    <p>
                                        Por favor ingrese en otro momento para poder descargar el reporte en pdf correspondiente al estado <?php echo $nombreEstado; ?>, cuando se culmine el proceso se le notificará con un correo.
                                    </p>
                                </div>
                            <?php } ?>
                        </div>

                        <div class="row row-fluid" style="padding-bottom: 20px; padding-top: 20px">


                            <div id="1eraFila" class="col-md-12">

                                <div class="col-md-4" >
                                    <?php
                                    echo CHtml::label('', 'estado', array("class" => "col-md-12"));
                                    ?>
                                    <?php
                                    echo CHTML::dropDownList('id', 'nombre', $listaEstados, array('empty' => '- SELECCIONE UN ESTADO -', 'class' => 'span-7',
                                        'onChange' => 'mostrarPlanteles()'));
                                    ?>
                                </div>

                                <div class = "col-md-12"><div class = "space-6"></div></div>
                                <div class = "col-md-12"><div class = "space-6"></div></div>

                                <div class="col-md-12">
                                    <?php
                                    if (isset($resultadoPlanteles) && $resultadoPlanteles !== array()) {
                                        $this->widget(
                                                'zii.widgets.grid.CGridView', array(
                                            'id' => 'planteles_por_estados',
                                            'itemsCssClass' => 'table table-striped table-bordered table-hover',
                                            'dataProvider' => $resultadoPlanteles,
                                            'summaryText' => false,
                                            'columns' => array(
                                                array(
                                                    'name' => 'nombre',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Nombre del Plantel</b></center>'
                                                ),
                                                array(
                                                    'name' => 'cod_estadistico',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Código Estadístico</b></center>'
                                                ),
                                                array(
                                                    'name' => 'cod_plantel',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Código del Plantel</b></center>'
                                                ),
                                                array(
                                                    'name' => 'cant_estudiantes',
                                                    'type' => 'raw',
                                                    'header' => '<center><b>Cantidad Estudiantes en el Plantel</b></center>'
                                                ),
                                            ),
                                            'pager' => array(
                                                'header' => '',
                                                'htmlOptions' => array('class' => 'pagination'),
                                                'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                                                'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                                                'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                                                'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                                            ),
                                                )
                                        );
                                    }
                                    ?>
                                    <?php
                                    if ($poseeEstudiantes == true) {
                                        if ($nombreEstado != false) {
                                            ?>
                                            <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                                <thead>

                                                    <tr>
                                                        <th>
                                                            <b></b>
                                                        </th>
                                                    </tr>

                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <div class="alertDialogBox">
                                                                <p>
                                                                    Este estado "<?php echo $nombreEstado; ?>" no ha solicitado título, para asignar seriales deben solicitar título primero indicando los estudiantes a graduar.
                                                                </p>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tbody>

                                            </table>

                                            <?php
                                        }
                                    }
                                    ?>
                                </div>

                                <?php if ($sumaEstudiantes != null && $sumaTotal != null) { ?>
                                    <div class = "col-md-12"><div class = "space-6"></div></div>

                                    <div class="col-md-12">

                                        <table style="width:650%;" class="table table-striped table-bordered table-hover" class="align-center">
                                            <thead>

                                                <tr>
                                                    <th>
                                                        <b>
                                                            <center>Cantidad de asignación de seriales en el estado "<?php echo $nombreEstado; ?>"</center>
                                                        </b>
                                                    </th>
                                                    <th>
                                                        <b>
                                                            <center>Cantidad de seriales adicionales asignados al estado "<?php echo $nombreEstado; ?>"</center>
                                                        </b>
                                                    </th>
                                                    <th>
                                                        <b>
                                                            <center>Cantidad de seriales asignados</center>
                                                        </b>
                                                    </th>
                                                </tr>

                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td>
                                            <center><b><?php echo $sumaEstudiantes; ?></b></center>
                                            </td>
                                            <td>
                                            <center><b><?php echo $suma_restantes; ?></b></center>
                                            </td>
                                            <td>
                                            <center><b><?php echo $sumaTotal; ?></b></center>
                                            </td>
                                            </tr>
                                            </tbody>

                                        </table>

                                    </div>
                                <?php } ?>

                                <?php
                                if (($verificarImpresionReporte == false && $mostrar_boton == true && $resultadoPlanteles != array()) || ($verificarImpresionReporte == true && $mostrar_boton == true && $resultadoPlanteles != array())) {
                                    ?>
                                    <div class="row">
                                        <div class="pull-right" style="padding-left: 20px; padding: 20px">
                                            <a  id = "btnAsignarSeriales"  class="btn btn-primary btn-sm" type="submit" data-last="Finish">
                                                <i class="fa fa-save icon-on-right"></i>
                                                Asignar Seriales
                                            </a>
                                        </div>
                                        <?php
                                    }
                                    if ($verificarImpresionReporte == 4 && $mostrar_boton == false && $resultadoPlanteles == array()) {
                                        ?>
                                        <div class = "pull-left" style ="padding-left: 20px; padding: 20px">
                                            <a  href="<?php echo Yii::app()->createUrl("/titulo/asignacionSerialesEstados/reporteCSV/estado/" . base64_encode($estado_id)); ?>"  id="reporteAsignacionSerialesPorEstado"   class = "btn btn-primary btn-next btn-sm ">
                                                Imprimir Seriales en CSV
                                                <i class=" fa-file-text-o icon-on-right"></i>
                                            </a>
                                        </div>
                                        <?php if ($existePdf == false && $verificarImpresionReportePDF == 0) { ?>
                                            <div class = "pull-left" style ="padding-left: 20px; padding: 20px">
                                                <a  id="btnGenerarPdf"  class = "btn btn-success btn-next btn-sm"  type="submit" data-last="Finish">
                                                    Generar Seriales en PDF
                                                    <i class="fa fa-file-pdf-o icon-on-right"></i>
                                                </a>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <?php
                                        if ($existePdf == true && $verificarImpresionReportePDF == 4) {
                                            ?>
                                            <div class = "pull-left" style ="padding-left: 20px; padding: 20px">
                                                <a  id="btnObtenerPdf"  href="<?php echo Yii::app()->createUrl("/titulo/asignacionSerialesEstados/obtenerPdf/estado/" . base64_encode($estado_id)); ?>"  class = "btn btn-primary btn-next btn-sm ">
                                                    Imprimir Seriales en PDF
                                                    <i class="fa fa-file-pdf-o icon-on-right"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>

                            <div class = "col-md-12"><div class = "space-6"></div></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6" style="padding-top: 1%">
                <a id="btnRegresar" href="<?php echo Yii::app()->createUrl("../../"); ?>"class="btn btn-danger">
                    <i class="icon-arrow-left"></i>
                    Volver
                </a>
            </div>
        </div>
    </div>
    <div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>
</div>


<script>
    $(document).ready(function() {
        $("html, body").animate({scrollTop: 0}, "fast");
    });

    function mostrarPlanteles() {

        var id = $("#id").val();
        var esNumero = true;
        var data = {
            estado: $("#id").val()
        }
        if (isNaN(id)) {
            esNumero = false;
        }

        if ($("#id").val() != '' && esNumero == true) {
            $.ajax({
                url: "/titulo/asignacionSerialesEstados/mostrarPlanteles",
                data: data,
                dataType: 'html',
                type: 'post', success: function(resp) {

                    $("html, body").animate({scrollTop: 0}, "fast");
                    $("#index").html(resp);
                    Loading.hide();
                }
            });

        }
    }




    $("#btnAsignarSeriales").unbind('click');
    $("#btnAsignarSeriales").click(function() {

<?php if ($estado_id != null) { ?>
            var estado_id = <?php echo $estado_id; ?>;
<?php } ?>
<?php if ($sumaEstudiantes != null) { ?>
            var cantidad_estudiantes = <?php echo $sumaEstudiantes; ?>;
<?php } ?>
        Loading.show();
        $.ajax({
            url: "/titulo/asignacionSerialesEstados/guardarAsignacionSerialesPorEstados",
            data: {estado_id: estado_id,
                cantidad_estudiantes: cantidad_estudiantes},
            dataType: 'html',
            type: 'post',
            success: function(resp, resp2, resp3) {

                try {

                    var json = jQuery.parseJSON(resp3.responseText);
                    if (json.statusCode === "success") {


                        $("#error p").html('');
                        $("#error").addClass('hide');
                        $("#exitoso").removeClass('hide');
                        $("#exitoso p").html(json.mensaje);
                        $("#asignacion").addClass('hide');
                        $("html, body").animate({scrollTop: 0}, "fast");

                    } else if (json.statusCode == "error") {

                        $("#exitoso p").html('');
                        $("#exitoso").addClass('hide');
                        $("#error").removeClass('hide');
                        $("#error p").html(json.mensaje);
                        $("html, body").animate({scrollTop: 0}, "fast");

                    }
                    Loading.hide();
                } catch (e) {

                    $("html, body").animate({scrollTop: 0}, "fast");
                    Loading.hide();
                }
                Loading.hide();
            }
        });

    });




    $("#btnGenerarPdf").unbind('click');
    $("#btnGenerarPdf").click(function() {

<?php if ($estado_id != null) { ?>
            var estado_id = <?php echo $estado_id; ?>;
<?php } ?>

        Loading.show();
        $.ajax({
            url: "/titulo/asignacionSerialesEstados/generarReportePorEstado",
            data: {estado_id: estado_id},
            dataType: 'html',
            type: 'post',
            success: function(resp, resp2, resp3) {

                try {

                    var json = jQuery.parseJSON(resp3.responseText);
                    if (json.statusCode === "success") {


                        $("#error p").html('');
                        $("#error").addClass('hide');
                        $("#exitoso").removeClass('hide');
                        $("#exitoso p").html(json.mensaje);
                        $("#asignacion").addClass('hide');
                        $("html, body").animate({scrollTop: 0}, "fast");

                    } else if (json.statusCode == "error") {

                        $("#exitoso p").html('');
                        $("#exitoso").addClass('hide');
                        $("#error").removeClass('hide');
                        $("#error p").html(json.mensaje);
                        $("html, body").animate({scrollTop: 0}, "fast");

                    }
                    Loading.hide();
                } catch (e) {

                    $("html, body").animate({scrollTop: 0}, "fast");
                    Loading.hide();
                }
                Loading.hide();
            }
        });

    });

</script>


