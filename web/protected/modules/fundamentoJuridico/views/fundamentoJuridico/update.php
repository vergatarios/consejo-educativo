<?php
/* @var $this FundamentoJuridicoController */
/* @var $model FundamentoJuridico */

$this->breadcrumbs=array(
	'Fundamento Jurídicos' => array('index'),
    $model->id=>'Modificar',
);
/*
$this->menu=array(
	array('label'=>'List FundamentoJuridico', 'url'=>array('index')),
	array('label'=>'Create FundamentoJuridico', 'url'=>array('create')),
	array('label'=>'View FundamentoJuridico', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage FundamentoJuridico', 'url'=>array('admin')),
);
*/
?>


<?php 
//$this->renderPartial('_archivo', array('model'=>$model,'key'=>$key,'msj'=>$msj,'subtitulo'=>"Nuevo Fundamento Juridico")); 

$this->renderPartial('_form', array('model' => $model, 'modelArchivo' => $modelArchivo, 'key' => $key, 'msj' => $msj, 'llave' => $llave, 'subtitulo' => 'Modificar ' . $model->nombre));
?>