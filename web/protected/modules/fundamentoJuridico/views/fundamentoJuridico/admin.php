<?php
/* @var $this FundamentoJuridicoController */
/* @var $model FundamentoJuridico */
/*
  $this->breadcrumbs=array(
  'Fundamento Juridicos'=>array('index'),
  'Manage',
  );

  $this->menu=array(
  array('label'=>'List FundamentoJuridico', 'url'=>array('index')),
  array('label'=>'Create FundamentoJuridico', 'url'=>array('create')),
  );
 */
Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#fundamento-juridico-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
echo CHtml::scriptFile('/public/js/modules/fundamentoJuridico/fundamentoJuridico.js');
?>

<div class="widget-box">
    <div class="widget-header">
        <h4>Fundamentos Jurídicos</h4>

        <div class="widget-toolbar">
            <a href="#" data-action="collapse">
                <i class="icon-chevron-up"></i>
            </a>
        </div>

    </div>

    <div class="widget-body">
        <div style="display:block;" class="widget-body-inner">
            <div class="widget-main">
                <div class="row col-sm-8" id="resultadoOperacion"></div>
                <div class="row space-6"></div>
                <div>
                    <?php
                    if (Yii::app()->user->pbac('fundamentoJuridico.fundamentoJuridico.admin')) { ?>
                        <div class="pull-right" style="padding-left:10px;">
                            <a  type="submit" href="/fundamentoJuridico/fundamentoJuridico/create" data-last="Finish" class="btn btn-success btn-next btn-sm">
                                <i class="fa fa-plus icon-on-right"></i>
                                Registrar nuevo Fundamento jurídico
                            </a>
                        </div>
                    <?php
                    }
                    ?>
                    <div class="row space-20"></div>
                </div>
                <?php
                $this->widget('zii.widgets.grid.CGridView', array(
                    'itemsCssClass' => 'table table-striped table-bordered table-hover',
                    'id' => 'fundamento-juridico-grid',
                    'dataProvider' => $model->search(),
                    'filter' => $model,
                    'summaryText' => false,
                    'afterAjaxUpdate' => 'function(){$("#date-picker").datepicker();
        var fecha = new Date();
var anoActual = fecha.getFullYear(); 
$.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: "dd-mm-yy",
        "showOn":"focus",
        "showOtherMonths": false,
        "selectOtherMonths": true,
        "changeMonth": true,
        "changeYear": true,
        "yearRange": "1920:"+anoActual,
        minDate: new Date(1800, 1, 1),
        maxDate: "today"
    });}',
                    'pager' => array(
                        'header' => '',
                        'htmlOptions' => array('class' => 'pagination'),
                        'firstPageLabel' => '<span title="Primera página">&#9668;&#9668;</span>',
                        'prevPageLabel' => '<span title="Página Anterior">&#9668;</span>',
                        'nextPageLabel' => '<span title="Página Siguiente">&#9658;</span>',
                        'lastPageLabel' => '<span title="Última página">&#9658;&#9658;</span>',
                    ),
                    'columns' => array(
                        array(
                            'header' => '<center>Nombre del Fundamento Jurídico</center>',
                            'name' => 'nombre',
                        ),
                        array(
                            'header' => '<center>Fecha de emisión</center>',
                            'name' => 'fecha_emision',
                            'value' => array($this, 'fechaEmision'),
                            'filter' => CHtml::activeTextField($model, 'fecha_emision', array('id' => "date-picker",
                                'placeHolder' => 'dd-mm-aaaa',
                                'readonly' => 'readonly'
                            )),
                        ),
                        array(
                            'header' => '<center>Tipo</center>',
                            'name' => 'tipo_fundamento_id',
                            'value' => '$data->tipoFundamento->nombre',
                            'filter' => CHtml::listData(
                                TipoFundamento::model()->findAll(
                                    array(
                                        'order' => 'id ASC'
                                    )
                                ), 'id', 'nombre'
                            ),
                        ),
                        array(
                            'header' => '<center>Estatus</center>',
                            'name' => 'estatus',
                            'value' => array($this, 'estatus'),
                            'filter' => array('A' => 'Activo', 'E' => 'Inactivo'),
                        ),
                        array(
                            'type' => 'raw',
                            'header' => '<center>Acciones</center>',
                            'value' => array($this, 'columnaAcciones'),
                        ),
                    ),
                ));
                ?>
            </div>
        </div>
    </div>
</div>

<div><?php $this->widget('ext.loading.LoadingWidget'); ?></div>


<div id="dialogPantalla" class="hide"></div>

