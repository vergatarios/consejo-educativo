<?php
/* @var $this TituloDigitalController */
/* @var $data TituloDigital */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo_verificacion')); ?>:</b>
	<?php echo CHtml::encode($data->codigo_verificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('titulo_id')); ?>:</b>
	<?php echo CHtml::encode($data->titulo_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origen_estudiante')); ?>:</b>
	<?php echo CHtml::encode($data->origen_estudiante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_estudiante')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_estudiante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_estudiante')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_estudiante); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo_titulo')); ?>:</b>
	<?php echo CHtml::encode($data->codigo_titulo); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('zona_educativa')); ?>:</b>
	<?php echo CHtml::encode($data->zona_educativa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plantel')); ?>:</b>
	<?php echo CHtml::encode($data->plantel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('codigo_plantel')); ?>:</b>
	<?php echo CHtml::encode($data->codigo_plantel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nivel')); ?>:</b>
	<?php echo CHtml::encode($data->nivel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mencion')); ?>:</b>
	<?php echo CHtml::encode($data->mencion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nacido_en')); ?>:</b>
	<?php echo CHtml::encode($data->nacido_en); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_emision')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_emision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lugar_expedicion')); ?>:</b>
	<?php echo CHtml::encode($data->lugar_expedicion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_expedicion')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_expedicion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('anio_egreso')); ?>:</b>
	<?php echo CHtml::encode($data->anio_egreso); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origen_director_zona_educativa')); ?>:</b>
	<?php echo CHtml::encode($data->origen_director_zona_educativa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_director_zona_educativa')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_director_zona_educativa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_director_zona_educativa')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_director_zona_educativa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firma_digital_director_zona_educativa')); ?>:</b>
	<?php echo CHtml::encode($data->firma_digital_director_zona_educativa); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origen_director_plantel')); ?>:</b>
	<?php echo CHtml::encode($data->origen_director_plantel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_director_plantel')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_director_plantel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_director_plantel')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_director_plantel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firma_digital_director_plantel')); ?>:</b>
	<?php echo CHtml::encode($data->firma_digital_director_plantel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origen_coordinador_drcee')); ?>:</b>
	<?php echo CHtml::encode($data->origen_coordinador_drcee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_coordinador_drcee')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_coordinador_drcee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_coordinador_drcee')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_coordinador_drcee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firma_digital_coordinador_drcee')); ?>:</b>
	<?php echo CHtml::encode($data->firma_digital_coordinador_drcee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origen_funcionario_designado')); ?>:</b>
	<?php echo CHtml::encode($data->origen_funcionario_designado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cedula_funcionario_designado')); ?>:</b>
	<?php echo CHtml::encode($data->cedula_funcionario_designado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nombre_funcionario_designado')); ?>:</b>
	<?php echo CHtml::encode($data->nombre_funcionario_designado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firma_digital_funcionario_designado')); ?>:</b>
	<?php echo CHtml::encode($data->firma_digital_funcionario_designado); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('datos_verificacion')); ?>:</b>
	<?php echo CHtml::encode($data->datos_verificacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('observacion')); ?>:</b>
	<?php echo CHtml::encode($data->observacion); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estudiante_id')); ?>:</b>
	<?php echo CHtml::encode($data->estudiante_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_ini_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_ini_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_ini')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_ini); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('usuario_act_id')); ?>:</b>
	<?php echo CHtml::encode($data->usuario_act_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_act')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_act); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fecha_elim')); ?>:</b>
	<?php echo CHtml::encode($data->fecha_elim); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('estatus')); ?>:</b>
	<?php echo CHtml::encode($data->estatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('periodo_escolar_id')); ?>:</b>
	<?php echo CHtml::encode($data->periodo_escolar_id); ?>
	<br />

	*/ ?>

</div>