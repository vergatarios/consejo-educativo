<?php
/* @var $this ConstanciaController */
/* @var $model Constancia */

$this->breadcrumbs=array(
	'Constancias'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Constancia', 'url'=>array('index')),
	array('label'=>'Create Constancia', 'url'=>array('create')),
	array('label'=>'Update Constancia', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Constancia', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Constancia', 'url'=>array('admin')),
);
?>

<h1>View Constancia #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'codigo_qr',
		'url',
		'inscripcion_estudiante_id',
		'usuario_ini_id',
		'usuario_act_id',
		'fecha_ini',
		'fecha_act',
		'fecha_elim',
		'estatus',
		'tipo_constancia_id',
		'nombre_archivo',
	),
)); ?>
