<?php

class ConstanciaController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout = '//layouts/column2';

    /**
     * @return array action filters
     */
    public function filters() {
        return array(
            'userGroupsAccessControl', // perform access control for CRUD operations
            'postOnly + delete', // we only allow deletion via POST request
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    static $_permissionControl = array(
        'read' => 'Consulta de Constancias',
        'write' => 'Gestion de Constancias',
        'label' => 'Módulo de Constancias'
    );

    /**
     * @Return array filtros de acción
     */

    /**
     * Especifica las reglas de control de acceso.
     * Este método es utilizado por el filtro 'AccessControl'.
     * Reglas de control de acceso a una matriz @ return
     */
    public function accessRules() {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('index', 'view'),
                'pbac' => array('read', 'write'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions' => array('create', 'update', 'activar', 'delete', 'buscar', 'prueba'),
                'pbac' => array('write'),
            ),
            array('deny', // deny all users
                'users' => array('*'),
            ),
        );
    }
    public function actionBuscar() {
        Yii::import('ext.qrcode.QRCode');
        $cedula=$_POST['cedula'];
        $nacionalidad=$_POST['nacionalidad'];
        if(is_numeric($_POST['cedula']) and (isset($cedula))){
        $error='';
        $resultado=Constancia::model()->buscarEstudiante($cedula,$nacionalidad);
        if($resultado['cedula_identidad']==$cedula){
        $id_estudiante=$resultado['estudiante_id'];
        $fecha_nacimiento=$resultado['fecha_nacimiento'];
        $id=$id_estudiante;
        $id_codi=  base64_encode($id);
        $constancia=new Constancia();
        $fecha=explode('-', $resultado['fecha_nacimiento']);
        $codigo_qr=$cedula.$fecha[0];
        $url="gescolar.dev/constancia/constancia/accion/id/".$id_codi;
        $row = array(
                'codigo_qr' => $codigo_qr,
                'url' => $url,
                'inscripcion_estudiante_id' => $resultado['id_inscripcion'],
            );
        //var_dump("codigo qr".$codigo_qr."<br>".'url'.$url."<br>".'inscripcion_estudiante_id'."<br>".$id_estudiante); die();
        $constancia=Constancia::model()->insertarConstancia($row);
        $code = new QRCode($url);
        $code->create(Yii::app()->basePath.'/qr/'.$cedula.'.png');
        $command = 'chmod 777 -R . '.Yii::app()->basePath . '/qr/'.$cedula.'.png';
        exec($command);
        $mPDF = Yii::app()->ePdf->mpdf('', 'A4', 0, '', 15, 15, 16, 16, 9, 9, 'M');
        $mPDF->WriteHTML($this->renderPartial('_pdfHeader', array(), true));
        $mPDF->WriteHTML($this->renderPartial('view_pdf_estudiante', array('cedula' => $cedula,'url'=>$url,'nacionalidad'=>$nacionalidad,'fecha_nacimiento'=>$fecha_nacimiento), true, false));
        $mPDF->Output('Pdf' . '.pdf', EYiiPdf::OUTPUT_TO_DOWNLOAD);
        }else{
            $model = new Constancia('search');
            $error='error';
         $this->render('admin',array('error'=>$error,'cedula'=>$cedula,'model'=>$model));
         //$this->render("//msgBox", array('class' => 'errorDialogBox', 'message' => "Esa cedula: $cedula no se encuentra registrada"));
        
        }
        //}
        }
    }
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id) {
        $this->render('view', array(
            'model' => $this->loadModel($id),
        ));
    }

    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate() {
        $model = new Constancia;

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Constancia'])) {
            $model->attributes = $_POST['Constancia'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('create', array(
            'model' => $model,
        ));
    }

    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id) {
        $model = $this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if (isset($_POST['Constancia'])) {
            $model->attributes = $_POST['Constancia'];
            if ($model->save())
                $this->redirect(array('view', 'id' => $model->id));
        }

        $this->render('update', array(
            'model' => $model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id) {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if (!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex() {
        $error='';
        $groupId = Yii::app()->user->group;
        $model = new Constancia('search');
        if (isset($_GET['Constancia']))
            $model->attributes = $_GET['Constancia'];
        $usuarioId = Yii::app()->user->id;
        $dataProvider = new CActiveDataProvider('Constancia');
        $this->render('admin', array(
            'model' => $model,
            'groupId' => $groupId,
            'usuarioId' => $usuarioId,
            'dataProvider' => $dataProvider,
            'error'=>$error,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin() {
        $model = new Constancia('search');
        $model->unsetAttributes();  // clear any default values
        if (isset($_GET['Constancia']))
            $model->attributes = $_GET['Constancia'];

        $this->render('admin', array(
            'model' => $model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return Constancia the loaded model
     * @throws CHttpException
     */
    public function loadModel($id) {
        $model = Constancia::model()->findByPk($id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param Constancia $model the model to be validated
     */
    protected function performAjaxValidation($model) {
        if (isset($_POST['ajax']) && $_POST['ajax'] === 'constancia-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

    public function actionPrueba() {
        Yii::import('ext.qrcode.QRCode');
        $code = new QRCode("data to encode");
        $code->create(Yii::app()->basePath.'/qr/file.png');
    }

    public function columnaAcciones($datas) {
        $id = $datas["id"];
        $id2 = $datas["estatus"];
        $id = base64_encode($id);
        if ($id2 == "A") {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "title" => "Buscar Esta Constancia", "onClick" => "VentanaDialog('$id','/catalogo/mencion/view','Mención','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-pencil green", "title" => "Modificar Constancia", "onClick" => "VentanaDialog('$id','/catalogo/mencion/update','Modificar Mención','update')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa icon-trash red", "title" => "Eliminar Constancia", "onClick" => "VentanaDialog('$id','/catalogo/mencion/borrar','Eliminar Mención','borrar')")) . '&nbsp;&nbsp;';
        } else {
            $columna = CHtml::link("", "#", array("class" => "fa fa-search", "title" => "Buscar Mencion", "onClick" => "VentanaDialog('$id','/catalogo/mencion/view','Mención','view')")) . '&nbsp;&nbsp;';
            $columna .= CHtml::link("", "#", array("class" => "fa fa-check", "title" => "Activar Mencion", "onClick" => "VentanaDialog('$id','/catalogo/mencion/activar','activar Mención','activar')")) . '&nbsp;&nbsp;';
        }
        return $columna;
    }

}
