$(document).ready(function(){
    // Filters
    $("#Aplicacion_nombre").on('keyup blur', function(){
        keyText(this, true);
    });
    
    $("#Aplicacion_cedula_contacto").on('keyup blur', function(){
        keyNum(this, false, false);
    });
    
    $("#Aplicacion_nombre_apellido_contacto").on('keyup blur', function(){
        keyText(this, true);
    });
    
    $("#Aplicacion_datos_consultados").on('keyup blur', function(){
        keyText(this, true);
    });
    
    $("#Aplicacion_email_contacto").on('keyup blur', function(){
        keyEmail(this, false);
    });
    
    // Clear Fields
    
    $("#Aplicacion_nombre").on('blur', function(){
        clearField(this);
    });
    $("#Aplicacion_cedula_contacto").on('blur', function(){
        clearField(this);
    });
    
    $("#Aplicacion_nombre_apellido_contacto").on('blur', function(){
        clearField(this);
    });
    
    $("#Aplicacion_datos_consultados").on('blur', function(){
        clearField(this);
    });
    
    $("#Aplicacion_email_contacto").on('blur', function(){
        clearField(this);
    });
    
    // Masks

    $.mask.definitions['L'] = '[1-2]';
    $.mask.definitions['X'] = '[2|4|6]';
    
    $('#Aplicacion_telefono_fijo_contacto').mask('(0299)999-9999');
    $('#Aplicacion_telefono_celular_contacto').mask('(04LX)999-9999');      
});
    
    
