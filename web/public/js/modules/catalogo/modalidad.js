function VentanaDialog(id,direccion,title,accion,datos) {
//poner primera en minuscula
   /* direccion = d;
    title = 'Consulta de plantel';

*/
accion=accion;
    Loading.show();
    var data =
            { 
                id: id,
                datos:datos
               
               


            };

            if(accion=="create" || accion=="update"){    
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result,action)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '1100px',
                dragable: false,
                resizable: false,


                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
                title_html: true,
   
                        buttons: [
                        {   
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class": "btn btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },
                        {   
                            html: "<i class='icon-save bigger-110'></i>&nbsp; Guardar",
                            "class": "btn btn-primary",
                            /*executeAjax('_formEndogeno', '../../agregarProyecto', data, false, true, 'post', '');*/

                            click: function() {

                               
                                var divResult = "dialogPantalla";
                                var urlDir = "/catalogo/modalidad/"+accion+"/"+id;
                                var datos = $("#modalidad-form").serialize();
                                var conEfecto = true;
                                var showHTML = true;
                                var method = "POST";
                                var callback = function(){
                                    $('#modalidad-grid').yiiGridView('update', {
                                        data: $(this).serialize()
                                    });
                                };

                                $("html, body").animate({ scrollTop: 0 }, "fast");
                                if(datos){
                                executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
    
                                }
                                else{
                                $(this).dialog("close");    
                                }

                                
                               // $(this).dialog("close");




                            }
                        },


                    ],


                        

                      
                   
                        });



                    
                         

                $("#dialogPantalla").html(result);
            }
        });
        Loading.hide();
    }  

    else if(accion=="view"){
        

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result,action)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '700px',
                dragable: false,
                resizable: false,


                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
                title_html: true,
   
                        buttons: [
                        {   
                            html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                            "class": "btn btn-danger",
                            click: function() {
                                $(this).dialog("close");
                            }
                        },


                    ],


                        

                      
                   
                        });



                    
                         

                $("#dialogPantalla").html(result);
            }
        });
        Loading.hide();


    }

    else if(accion=="borrar"){

         $("#dialogPantalla").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Desea Inhabilitar esta Modalidad? </p></div>');

        var dialog = $("#dialogPantalla").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,


            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + "</h4></div>",
            title_html: true,

                    buttons: [
        {
            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
            "class": "btn btn-xs",
            click: function() {
                $(this).dialog("close");
            }
        },
        {
            html: "<i class='icon-trash bigger-110'></i>&nbsp; inhabilitar Modalidad",
            "class": "btn btn-danger btn-xs",
            click: function() {
                            var divResult = "resultadoOperacion";
                            var urlDir = "/catalogo/modalidad/"+accion+"/";
                            var datos = {id:id, accion:accion};
                            var conEfecto = true;
                            var showHTML = true;
                            var method = "POST";
                            var callback = function(){
                                $('#modalidad-grid').yiiGridView('update', {
                                    data: $(this).serialize()
                                });
                            };

                            $("html, body").animate({ scrollTop: 0 }, "fast");
                            if(datos){
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                            }
                        }
                    }
                ],


                        

                      
                   
                        });


        Loading.hide();


    }
    
    
    else if(accion=="activar"){

         $("#dialogPantalla").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Desea habilitar esta Modalidad? </p></div>');

        var dialog = $("#dialogPantalla").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,


            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + "</h4></div>",
            title_html: true,

                    buttons: [
        {
            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
            "class": "btn btn-xs",
            click: function() {
                $(this).dialog("close");
            }
        },
        {
            html: "<i class='icon-check bigger-110'></i>&nbsp; Reactivar",
            "class": "btn btn-success btn-xs",
            click: function() {
                            var divResult = "resultadoOperacion";
                            var urlDir = "/catalogo/modalidad/"+accion+"/";
                            var datos = {id:id, accion:accion};
                            var conEfecto = true;
                            var showHTML = true;
                            var method = "POST";
                            var callback = function(){
                                $('#modalidad-grid').yiiGridView('update', {
                                    data: $(this).serialize()
                                });
                            };

                            $("html, body").animate({ scrollTop: 0 }, "fast");
                            if(datos){
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                            }
                        }
                    }
                ],


                        

                      
                   
                        });


        Loading.hide();


    }
    
     else if(accion=="borrarNivel"){
         
         data={ id:id[id] };
         
      

         $("#dialogPantalla").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Esta seguro que desea eliminar este nivel? </p></div>');

        var dialog = $("#dialogPantalla").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,


            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> " + title + "</h4></div>",
            title_html: true,

                    buttons: [
        {
            html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
            "class": "btn btn-xs",
            click: function() {
                $(this).dialog("close");
            }
        },
        {
            html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
           "class": "btn btn-danger btn-xs",
            click: function() {
                            var divResult = "resultadoOperacion";
                            var urlDir = "/catalogo/modalidad/quitarNivel/";
                            var datos = data;
                            var conEfecto = true;
                            var showHTML = true;
                            var method = "POST";
                            var callback = function(){
                                $('#modalidad-grid').yiiGridView('update', {
                                    data: $(this).serialize()
                                });
                            };

                            $("html, body").animate({ scrollTop: 0 }, "fast");
                            if(datos){
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                            }
                        }
                    }
                ],


                        

                      
                   
                        });


        Loading.hide();


    }

}


function cambiarEstatus(){

   var id = $("id").val();
    var data =
            {
                id: id
            
            };
    var dialog = $("#dialogPantalla").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Modalidad</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar Modalidad",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('_borrar', '/catalogo/modalidad/borrar', data, false, true, 'post', '');
                   // $(this).dialog("close");
                }
            }
        ]
    });


}