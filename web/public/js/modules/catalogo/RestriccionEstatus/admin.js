function VentanaDialog(id,direccion,title,accion,datos) 
{

   
              accion = accion;
              Loading.show();
              var data =
            {
                id: id,
                datos: datos
            };

     

  
    if(accion=="borrar")
    {

        // $("#dialogPantalla").html('<div class="alert alert-warning"> ¿Esta seguro que desea inactivar este registro?</div>');

         var dialog = $("#dialogInhabilitar").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,
            title: "<div class='widget-header'><h4 class='smaller'><i class='icon-warning-sign red'></i>"  + title + "</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Inhabilitar Restricción del EStatus",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacion";
                        var urlDir = "/catalogo/restriccionEstatus/eliminacion?id=" + id;
                        var datos = {id: id, accion: accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function() {
                            $('#restriccion-estatus-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };

                        $("html, body").animate({scrollTop: 0}, "fast");
                        if (datos) {
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    }
                }

            ],
        });
        Loading.hide();
    }
    if(accion=="activar")
    {

         //$("#dialogPantallaActivar").html('<div class="alertDialogBox"><p class="bolder center grey"> ¿Esta seguro que desea activar este registro? </p></div>');

        var dialog = $("#dialogActivar").removeClass('hide').dialog({
            modal: true,
            width: '450px',
            dragable: false,
            resizable: false,
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> " + title + "</h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                    "class": "btn btn-xs",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i>&nbsp; Activar Restricción Estatus",
                    "class": "btn btn-danger btn-xs",
                    click: function() {
                        var divResult = "resultadoOperacion";
                        var urlDir = "/catalogo/restriccionEstatus/activar?id=" + id;
                        var datos = {id: id, accion: accion};
                        var conEfecto = true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function() {
                            $('#restriccion-estatus-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };

                        $("html, body").animate({scrollTop: 0}, "fast");
                        if (datos) {
                            executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                            $(this).dialog("close");
                        }
                    }
                }
            ],
        });
        Loading.hide();
    }

} // fin de la funcion para activacion i desactivacion de registros en la interfaz admin perteneciente a personal-plantel


