$(document).ready(function() {

    $('#EspecificacionEstatus_fecha_ini').datepicker();
    $.datepicker.setDefaults($.datepicker.regional = {
        dateFormat: 'dd-mm-yy',
        showOn: 'focus',
        showOtherMonths: false,
        selectOtherMonths: true,
        changeMonth: true,
        changeYear: true,
        minDate: new Date(1800, 1, 1),
        maxDate: 'today'
    });
    
    $('#EspecificacionEstatus_nombre').on('keyup blur', function() {
        keyText(this, true);
    });

    $('#EspecificacionEstatus_nombre').on('blur', function() {
        clearField(this);
    });
    
    $('#EspecificacionEstatus_fecha_ini').on('dblclick', function(){
        $(this).val('');
        $('#especificacion-estatus-grid').yiiGridView('update', {
            data: $(this).serialize()
        });
    });
    
});

