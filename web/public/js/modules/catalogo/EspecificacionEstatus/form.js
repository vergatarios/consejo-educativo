
$(document).ready(function(){
    $('#EspecificacionEstatus_nombre').unbind('keyup');
    $('#EspecificacionEstatus_nombre').bind('keyup', function() {
        keyAlphaNum(this,true,true);
        makeUpper(this);
    });
    $('#EspecificacionEstatus_nombre').unbind('blur');
    $('#EspecificacionEstatus_nombre').bind('blur', function() {
        clearField(this);
    });
});

