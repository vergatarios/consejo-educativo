    
//----------------------validacion de campos basicos


   $(document).ready(function() {
       
         $('#DependenciaNominal_nombre').bind('keyup blur', function() {
         keyAlphaNum(this, true, true);
         makeUpper(this);
         });
     
         $('#dependencia-form').on('submit', function(evt) {
            evt.preventDefault();
            crearDependencia();
        });

        $('#nombre_dependencia').bind('keyup blur', function() {
            keyTextDash(this, true);
            //makeUpper(this);
        });

    });

    
    
    
    
//-------------------------------------------------->DEPLEGAR FORMULARIO DE DETALLES 

function consultarDependencia(id) {


    direccion = 'consultarDependencia';

    title = 'Detalles de Dependencia';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '1100px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs ",
                        click: function() {
                            $(this).dialog("close");
                            refrescarGrid();
                        }
                    }
                ]
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

//-------------------------------------------->DESPLEGAR FORMULARIO UPDATE

function modificarDependencia(id) {
    
    

    direccion = 'modificarDependencia';

    title = 'Modificar Dependencia';
    Loading.show();
    var data = {id: id};
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                       modal: true,
                width: '800px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                
             
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                            refrescarGrid();
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            procesarCambio();
                            refrescarGrid();
                        }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}


//--------------------------------------------->desplegar pop-up para registrar

function registrarDependencia() {


    direccion = 'create';

    title = 'Crear Nueva Dependencia';
    Loading.show();
    
   
    
    $.ajax({
        url: direccion,
        
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                
                modal: true,
                width: '800px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                            refrescarGrid();
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            crearDependencia();
                            refrescarGrid();
                        }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}



//-------------------------------------------------------->REGISTRAR 
function crearDependencia() 
{
    

   

    direccion = 'crear';

    var nombre = $('#nombre_dependencia').val();
    var data = {nombre: nombre};
    
    executeAjax('error', direccion, data, false, true, 'POST', refrescarGrid);
       document.getElementById("dependencia-nominal-form").reset();
 
}

//----------------------------------------------->>MODIFICAR NOMBRE DE DEPENDENCIA sin serialize
function procesarCambio()
{

    direccion = 'procesarCambio';

    var id = $('#id').val();
    var nombre = $('#nombre_dependencia').val();
    
    var data = { Dependencia: {id: id, nombre: nombre} };
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);

}

//----------------------------------------------->>MODIFICAR NOMBRE DE DEPENDENCIA con serialize
function procesarCambioDirecto()
{


    direccion = 'procesarCambio';

    
  
    $.ajax({
        url: direccion,
        data: $("#dependencia-form").serialize(),
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            
            refrescarGrid();
            $('#dialogPantalla').dialog('close');
          
            alert('Su modificacion fue procesada');
            
        }
    });

}

//______________________________________________________________________________
//--------------------. DESACTIVAR REGISTRO .-----------------------------------


function borrar(id) {
    

    direccion = 'eliminar';

    var dialog = $("#dialogEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Dependencia Nominal</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar Dependencia Nominal",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogEliminar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxReg").show("slow");
                    $("#dialogBoxHab").hide("slow");
                }
            }
        ]
    });
}
//..............................................................................

//.-----------------------------.REACTIVAR.-------------------------------------

function reactivar(id) {
    

    direccion = 'reactivar';

    var dialog = $("#dialogReactivar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Reactivar Dependencia Nominal</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                    
                }
            },
            {
                html: "<i class='icon-ok bigger-110'></i>&nbsp; Reactivar",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogReactivar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
               refrescarGrid();
                    $("#dialogBoxReg").hide("slow");
                    $("#dialogBoxHab").show("slow");
                }
            }
        ]
    });
}

//------------------------------------------------------------------------------
function borrar33(id) 
{
    
    direccion = 'eliminar';
    
    var data = {id: id};
  //  alert(id);
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'GET', refrescarGrid);
}
function refrescarGrid(){
    
    $('#dependencia-grid').yiiGridView('update', {
    data: $(this).serialize()
});
    
}
function cerrar_dialogo()


{

    Loading.show();

//               $("#dialogPantalla").dialog("close");
    window.location.reload();
    document.getElementById("guardo").style.display = "block";


    Loading.hide();


}



