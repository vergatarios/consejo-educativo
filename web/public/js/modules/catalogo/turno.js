//------------------>Validaciones de los campos

$(document).ready(function() {
       
         $('#Turno_nombre').bind('keyup blur', function() {
         keyTextDash(this, true,true);
         makeUpper(this);
         });
     
         $('#turno-form').on('submit', function(evt) {
            evt.preventDefault();
            crearTurno();
        });

        $('#nombre_turno').bind('keyup blur', function() {
            keyTextDash(this, true, true);
            makeUpper(this);
        });

        $('#nombre_turno').bind('blur', function() {
          clearField(this);
      });
    });

//----------------------------------------------------------------------------->



//------------------->DESPLEGAR DETALLES DE REGISTROS INACTIVOS>>-----


function consultarTurnoInactivo(id) {

    direccion = 'consultarTurnoInactivo';
    title = 'Detalles de Turno Inactivo';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '1100px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-power-off red'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs ",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

//---------->DEPLEGAR FORMULARIO DE DETALLES PARA REGISTROS ACTIVOS

function consultarTurno(id) {

    direccion = 'consultarTurno';
    title = 'Detalles de Turno';

    Loading.show();
    var data =
            {
                id: id
            };
    //alert (id);

    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            var dialog = $("#dialogPantalla").removeClass('hide').dialog({
                modal: true,
                width: '1100px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs ",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ]
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}

//-------------------------------------------->DESPLEGAR FORMULARIO UPDATE

function modificarTurno(id) {
    
    
    direccion = 'modificarTurno';
    title = 'Modificar Turno';
    Loading.show();
    var data = {id: id};
    $.ajax({
        url: direccion,
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                       modal: true,
                width: '800px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-search'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                
             
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            procesarCambio();
                        }

                    }

                ],
                close: function() {
                    $("#dialogPantalla").html("");
                }
            });
            $("#dialogPantalla").html(result);
        }
    });
    Loading.hide();
}


//--------------------------------------------->desplegar pop-up para registrar

function registrarTurno() {
    
    direccion = 'create';
    title = 'Crear Nueva Turno';
    Loading.show();
    
     //var data = {id: id};
    
    $.ajax({
        url: direccion,
         //data: data,
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            $("#dialogPantalla").removeClass('hide').dialog({
                
                modal: true,
                width: '800px',
                dragable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-pencil'></i> " + title + "</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            refrescarGrid();
                            $(this).dialog("close");
                        }

                    },
                    {
                        html: "<i class='icon-save info bigger-110'></i>&nbsp; Guardar",
                        "class": "btn btn-primary btn-xs",
                          click: function() {

                            crearTurno();

                            
                           // $("#nombre_credencial").val("");
                            }

                    }

                ],
               });
            $("#dialogPantalla").html(result);
               
        }
    });
    Loading.hide();
}



//-------------------------------------------------------->REGISTRAR 
function crearTurno() 
{
    

    direccion = 'crear';

    var nombre = $('#nombre_turno').val();
    
    var data = {nombre: nombre};
    //alert(data);
    //executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
    executeAjax('error', direccion, data, true, true, 'POST', refrescarGrid);
    document.getElementById("turno-form").reset();
     
}


//----------------------------------------------->>MODIFICAR NOMBRE  sin serialize--EN USO
function procesarCambio()
{
    direccion = 'procesarCambio';
    var id = $('#id').val();
    var nombre = $('#nombre_turno').val();
    
    var data = { Turno: {id: id, nombre: nombre} };
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'POST', refrescarGrid);

}

//----------------------------------------------->>MODIFICAR NOMBRE con serialize
function procesarCambioDirecto()
{
    direccion = 'procesarCambio';
    
  
    $.ajax({
        url: direccion,
        data: $("#turno-form").serialize(),
        dataType: 'html',
        type: 'POST',
        success: function(result)
        {
            
            refrescarGrid();
            $('#dialogPantalla').dialog('close');
          
            alert('Su modificacion fue procesada');
            
        }
    });

}


//* ------------------------------------Desactivar registro .-------------------

function borrar(id) {
    
    direccion = 'eliminar';
    var dialog = $("#dialogEliminar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Turno</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                    
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogEliminar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxReg").show("slow");
                    $("#dialogBoxHab").hide("slow");

                }
            }
        ]
    });
}

//------------------------------------------------------------------------------
//---------------------. REACTIVACION .-----------------------------------------

function reactivar(id) {
    

    direccion = 'reactivar';

    var dialog = $("#dialogReactivar").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        dragable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Reactivar Turno</h4></div>",
        title_html: true,
        //html: 'hola....',
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    refrescarGrid();
                    $(this).dialog("close");
                    
                }
            },
            {
                html: "<i class='icon-ok bigger-110'></i>&nbsp; Reactivar",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id,
                                           };
                    // executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback)
                    executeAjax('dialogReactivar', direccion, data, false, true, 'GET', refrescarGrid);
                    $(this).dialog("close");
                    refrescarGrid();
                    $("#dialogBoxReg").hide("slow");
                    $("#dialogBoxHab").show("slow");
                }
            }
        ]
    });
}

//------------------------------------------------------------------------------
function borrar33(id) 
{

    direccion = 'eliminar';

    
    var data = {id: id};
  //  alert(id);
    
    executeAjax('dialogPantalla', direccion, data, false, true, 'GET', refrescarGrid);
}
function refrescarGrid(){
    
    $('#turno-grid').yiiGridView('update', {
    data: $(this).serialize()
});
    
}
function cerrar_dialogo()


{

    Loading.show();

//               $("#dialogPantalla").dialog("close");
    window.location.reload();
    document.getElementById("guardo").style.display = "block";


    Loading.hide();


}

