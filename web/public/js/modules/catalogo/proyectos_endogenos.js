function refrescarGrid() {
    $('#proyectos-endogenos-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}
function cambiarEstatus(id, descripction, accion) {

    var accionDes = new String();
    var boton = new String();
    var botonClass = new String();

    $('#confirm-description').html(descripction);

    if (accion === 'A') {
        accionDes = 'Activar';
        boton = "<i class='icon-ok bigger-110'></i>&nbsp; Activar Proyecto Endogeno";
        botonClass = 'btn btn-primary btn-xs';
    } else {
        accionDes = 'Inactivar';
        boton = "<i class='icon-trash bigger-110'></i>&nbsp; Inactivar Proyecto Endogeno";
        botonClass = 'btn btn-danger btn-xs';
    }

    $(".confirm-action").html(accionDes);

    $("#confirm-status").removeClass('hide').dialog({
        width: 800,
        resizable: false,
        draggable: false,
        modal: true,
        position: ['center', 50],
        title: "<div class='widget-header'><h5 class='smaller'><i class='icon-warning-sign red'></i> Cambio de Estatus del Proyecto Endogeno</h5></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: boton,
                "class": botonClass,
                click: function() {

                    var divResult = "div-result-message";
                    var urlDir = "/catalogo/proyectosEndogenos/cambiarEstatus/id/" + id;
                    var datos = "accion=" + accion;
                    var conEfecto = true;
                    var showHTML = true;
                    var method = "POST";
                    var callback = function() {
                        refrescarGrid();
                    };

                    $("html, body").animate({scrollTop: 0}, "fast");

                    executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);

                    $(this).dialog("close");

                }
            }

        ]
    });

}


$(document).ready(function() {
    $('.look-data').unbind('click');
    $('.look-data').on('click',
            function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                verProyectoEndogeno(id);
            }
    );

    $('.edit-data').unbind('click');
    $('.edit-data').on('click',
            function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                editarProyectoEndogeno(id);
            }
    );

    $('.change-status').unbind('click');
    $('.change-status').on('click',
            function(e) {
                e.preventDefault();
                var id = $(this).attr('data-id');
                var description = $(this).attr('data-description');
                var accion = $(this).attr('data-action');
                cambiarEstatus(id, description, accion);
            }
    );

    $('#nombre').bind('keyup blur', function() {
        keyText(this, false);
        makeUpper(this, true);
        clearField(this);
    });
    $('#ProyectosEndogenos_nombre').bind('keyup blur', function() {
        keyText(this, false);
        makeUpper(this, true);
        clearField(this);
    });

});