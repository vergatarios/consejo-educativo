function dialog_success(mensaje) {
    $("#dialog_success p").html(mensaje);
    var dialog_success = $("#dialog_success").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-check'></i> Actualización Exitosa </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    dialog_success.dialog("close");
                    window.location.reload();

                }
            }
        ]

    });
}
function dialogo_error(mensaje,title) {
    displayDialogBox('dialog_success','info',mensaje);
    //$("#dialog_error p").html(mensaje);
    var dialog = $("#dialog_success").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> "+title+" </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}
function dialogo(mensaje,title,divResult,icon) {
    displayDialogBox(divResult,'info',mensaje);
    var dialog = $("#"+divResult).removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-"+icon+"'></i> "+title+" </h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cerrar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            }
        ]
    });
}
function cambiarEstatusProcesoMatriculacion(id, accion) {
    var accionDes = new String();
    var boton = new String();
    var botonClass = new String();

    $("#resultadoElim").addClass('hide');
    $("#resultadoElim").html('');
    if (accion == 'A') {
        accionDes = 'Cerrar';
        boton = "<i class='icon-ok bigger-110'></i>&nbsp; Cerrar Proceso de Matriculación";
        botonClass = 'btn btn-primary btn-xs';
    } else {
        accionDes = 'Aperturar';
        boton = "<i class='icon-trash bigger-110'></i>&nbsp; Aperturar Proceso de Matriculación";
        botonClass = 'btn btn-danger btn-xs';
    }
    $(".confirm-action").html(accionDes);

    $("#confirm-status").removeClass('hide').dialog({
        width: 800,
        resizable: false,
        draggable: false,
        modal: true,
        position: ['center', 50],
        title: "<div class='widget-header'><h4 class='smaller'><i class='icon-warning-sign red'></i> Cambio de Estatus del Proceso de Matriculación</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: boton,
                "class": botonClass,
                click: function() {

                    var divResult = "div-result-message";
                    var urlDir = "/planteles/seccionPlantel/cambiarEstatusProcesoMatriculacion/";
                    var datos = {accion: accion, plantel_id: base64_encode(id)};
                    var conEfecto = true;
                    var showHTML = true;
                    var method = "POST";
                    var callback = function() {
                        //refrescarGrid();
                    };



                    $.ajax({
                        type: method,
                        url: urlDir,
                        dataType: "html",
                        data: datos,
                        beforeSend: function() {
                            if (conEfecto) {
                                var url_image_load = "<div class='center'><img title='Su transacci&oacute;n est&aacute; en proceso' src='/public/images/ajax-loader-red.gif'></div>";
                                displayHtmlInDivId(divResult, url_image_load);
                            }
                        },
                        success: function(datahtml, resp2, resp3) {

                            try {
                                var json = jQuery.parseJSON(resp3.responseText);
                                if (json.statusCode == 'success') {
                                    $("#confirm-status").dialog('close');
                                    dialog_success(json.mensaje);
                                }
                            } catch (e) {
                                $("html, body").animate({scrollTop: 0}, "fast");
                                if (showHTML) {
                                    $("#resultadoElim").removeClass('hide');
                                    displayHtmlInDivId('resultadoElim', datahtml, conEfecto);
                                }
                            }

                        },
                        statusCode: {
                            404: function() {
                                displayDialogBox(divResult, "error", "404: No se ha encontrado el recurso solicitado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            },
                            400: function() {
                                displayDialogBox(divResult, "error", "400: Error en la petici&oacute;n, comuniquese con el Administrador del Sistema para correcci&oacute;n de este posible error.");
                            },
                            401: function() {
                                displayDialogBox(divResult, "error", "401: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            403: function() {
                                displayDialogBox(divResult, "error", "403: Usted no est&aacute; autorizado para efectuar esta acci&oacute;n.");
                            },
                            500: function() {
                                if (typeof callback == "function")
                                    callback.call();
                                displayDialogBox(divResult, "error", "500: Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            },
                            503: function() {
                                displayDialogBox(divResult, "error", "503: El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            },
                            999: function(resp) {
                                displayDialogBox(divResult, "error", resp.status + ': ' + resp.responseText);
                            }
                        },
                        error: function(xhr, ajaxOptions, thrownError) {
                            //alert(thrownError);
                            if (xhr.status == '401') {
                                document.location.href = "http://" + document.domain + "/";
                            } else if (xhr.status == '400') {
                                displayDialogBox(divResult, "error", "Recurso no encontrado. Recargue la P&aacute;gina e intentelo de nuevo.");
                            } else if (xhr.status == '500') {
                                displayDialogBox(divResult, "error", "Se ha producido un error en el sistema, Comuniquese con el Administrador del Sistema para correcci&oacute;n del m&iacute;smo.");
                            } else if (xhr.status == '503') {
                                displayDialogBox(divResult, "error", "El servidor web se encuentra fuera de servicio. Comuniquese con el Administrador del Sistema para correcci&oacute;n del error.");
                            }
                            else if (xhr.status == '999') {
                                displayDialogBox('resultadoElim', "error", xhr.status + ': ' + xhr.responseText);
                            }
                        }
                    });

                    $(this).dialog("close");

                }
            }

        ]
    });

}

$('#capacidadSeccion').bind('keyup blur', function() {
    if ($("#capacidadSeccion").val() > 100)
    {
        $("#capacidadSeccion").val('100');
    }
    keyNum(this, false);// true acepta la ñ y para que sea español
});

function mostrarPlan() {

    Loading.show();

    $("#seccion_error p").html('');
    $("#seccion_error").hide();
    var mensaje = 'Por favor seleccione otro nivel que contenga un plan asociado';

    var data = {
        plantel_id: $("#plantel_id").val(),
        nivel_id: $("#nivelId").val()
    }

    $.ajax({
        url: "../../mostrarPlan",
        data: data,
        dataType: 'html',
        type: 'get',
        success: function(resp) {
            if (resp == 'false') {

                $("#planId").html('');
                var plan = document.getElementById("planId");
                var option = document.createElement("option");
                option.value = "";
                option.text = "-SELECCIONE-";
                plan.appendChild(option); // y aqui lo añadiste

                document.getElementById("seccion_error").style.display = "block";
                $("#seccion_error p").html(mensaje);

            } else {
                $("#planId").html(resp);
            }
            Loading.hide();
        }
    });

}


function mostrarGrado() {

    Loading.show();

    $("#seccion_error p").html('');
    $("#seccion_error").hide();
    var mensaje = 'Por favor seleccione otro nivel que contenga un plan y un grado asociado';

    var data = {
        plan_id: $("#planId").val(),
        nivel_id: $("#nivelId").val()
    }

    $.ajax({
        url: "../../mostrarGrado",
        data: data,
        dataType: 'html',
        type: 'get',
        success: function(resp) {
            //  alert(resp);

            if (resp == 'false') {

                $("#gradoId").html('');
                var grado = document.getElementById("gradoId");
                var option = document.createElement("option");
                option.value = "";
                option.text = "-SELECCIONE-";
                grado.appendChild(option); // y aqui lo añadiste

                document.getElementById("seccion_error").style.display = "block";
                $("#seccion_error p").html(mensaje);

            } else {
                $("#gradoId").html(resp);
            }
            Loading.hide();
        }
    });
}


function agregarSeccion() {

    Loading.show();
    var data = {
        plantel_id: $("#plantel_id").val()
    }

    $.ajax({
        url: "../../mostrarRegistrarSeccion",
        data: data,
        dataType: 'html',
        type: 'post',
        success: function(resp) {
            //  alert(resp);
            //  $("#dialog_registrarSeccion span").html($("#SeccionPlantel_seccion_id option:selected").text());
            $("#dialog_registrarSeccion").html(resp);
            var dialogRegistrar = $("#dialog_registrarSeccion").removeClass('hide').dialog({
                modal: true,
                width: '1000px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Asignar Nueva Sección</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                        "class": "btn btn-primary btn-xs",
                        click: function() {
                            var urlDir = '/planteles/seccionPlantel/guardarSeccion/';
                            var datos = '';
                            var divResult = '';
                            var loadingEfect=false;
                            var showResult=false;
                            var method='GET';
                            var responseFormat='html';
                            var beforeSendCallback=function(){};
                            var successCallback;
                            var errorCallback=function(){};
                            var data = {
                                //  seccion:$("#seccion-plantel-form").serialize(),
                                seccion_id: $("#seccionId").val(),
                                grado_id: $("#gradoId").val(),
                                capacidad: $("#capacidadAlumnos").val(),
                                turno_id: $("#turnoId").val(),
                                plantel_id: $("#plantel_id").val(),
                                nivel_id: $("#nivelId").val(),
                                plan_id: $("#planId").val()
                            }
                            Loading.show();
                            datos=$('#asignatura-docente-form').serialize()+'&seccion_id='+base64_encode($("#seccionId").val())+'&grado_id='+base64_encode($("#gradoId").val())+'&capacidad='+base64_encode($("#capacidadAlumnos").val())+'&turno_id='+base64_encode($("#turnoId").val())+'&plantel_id='+base64_encode($("#plantel_id").val())+'&nivel_id='+base64_encode($("#nivelId").val())+'&plan_id='+base64_encode($("#planId").val());
                            successCallback=function(response,statusCode,dom){
                                try{
                                    var responseJson = jQuery.parseJSON(dom.responseText);
                                    if(responseJson.statusCode =='SUCCESS'){
                                        dialogRegistrar.dialog('close');
                                        refrescarGrid();
                                        dialogo_error(responseJson.mensaje,responseJson.title);
                                    }
                                    if(responseJson.statusCode =='ERROR'){
                                        document.getElementById("resultadoRegistrar").style.display = "none";
                                        document.getElementById("resultadoSeccionRegistrar").style.display = "block";
                                        $("#resultadoSeccionRegistrar").html('');
                                        displayDialogBox('resultadoSeccionRegistrar','error',responseJson.mensaje);
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    }
                                }
                                catch(e){
                                    document.getElementById("resultadoRegistrar").style.display = "none";
                                    document.getElementById("resultadoSeccionRegistrar").style.display = "block";
                                    $("#resultadoSeccionRegistrar").html('');
                                    displayHtmlInDivId('resultadoSeccionRegistrar',response,true);
                                    $("html, body").animate({scrollTop: 0}, "fast");
                                }
                            };
                            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSendCallback, successCallback, errorCallback);

                            Loading.hide();
                            /*$.ajax({
                             url: "../../guardarSeccion",
                             data: data,
                             dataType: 'html',
                             type: 'post',
                             success: function(resp) {

                             if (isNaN(resp)) { // si la respuesta son caracteres muestra el error de ingreso
                             document.getElementById("resultadoRegistrar").style.display = "none";
                             document.getElementById("resultadoSeccionRegistrar").style.display = "block";
                             $("#resultadoSeccionRegistrar").html(resp);
                             //document.getElementById("resultadoSeccion").style.display = "none";
                             $("html, body").animate({scrollTop: 0}, "fast");
                             } else { //muestra mensaje que guardo
                             // alert('guardo');
                             $("#seccionId").val('');
                             $("#gradoId").val('');
                             $("#capacidadAlumnos").val('');
                             $("#turnoId").val('');
                             $("#nivelId").val('');
                             $("#planId").val('');
                             refrescarGrid();
                             document.getElementById("guardoRegistro").style.display = "block";
                             dialogRegistrar.dialog('close');
                             $("html, body").animate({scrollTop: 0}, "fast");
                             }
                             Loading.hide();
                             }
                             })*/

                        }
                    }
                ]
            });

            Loading.hide();
        }
    })
}


$("#resultadoSeccionRegistrar").click(function() {
    document.getElementById("resultadoSeccionRegistrar").style.display = "none";
    document.getElementById("resultadoRegistrar").style.display = "block";
});

$("#guardoRegistro").click(function() {
    //document.getElementById("resultadoSeccionRegistrar").style.display = "none";
    document.getElementById("guardoRegistro").style.display = "none";
    document.getElementById("resultadoElim").style.display = "none";
    //document.getElementById("resultadoRegistrar").style.display = "block";
});

$("#resultadoElim").click(function() {
    document.getElementById("resultadoElim").style.display = "none";
});


function mostrarSeccion(id) {

    Loading.show();

    var data = {
        id: id,
        plantel_id: $("#plantel_id").val()
    }
    //alert(id);
    $.ajax({
        url: "../../mostrarSeccion",
        data: data,
        dataType: 'html',
        type: 'get',
        success: function(resp) {
            //  alert(resp);
            //  $("#dialog_registrarSeccion span").html($("#SeccionPlantel_seccion_id option:selected").text());
            var dialogRegistrar = $("#dialog_registrarSeccion").removeClass('hide').dialog({
                modal: true,
                width: '850px',
                draggable: false,
                resizable: false,
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Asignar Nueva Sección</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            $(this).dialog("close");
                        }
                    },
                    {
                        html: "Guardar &nbsp; <i class='icon-save icon-on-right bigger-110'></i>",
                        "class": "btn btn-primary btn-xs",
                        click: function() {


                            var data = {
                                id: id,
                                seccion_id: $("#seccionId").val(),
                                grado_id: $("#gradoId").val(),
                                capacidad: $("#capacidadAlumnos").val(),
                                turno_id: $("#turnoId").val(),
                                nivel_id: $("#nivelId").val(),
                                plan_id: $("#planId").val(),
                                plantel_id: $("#plantel_id").val()
                            }

                            Loading.show();

                            $.ajax({
                                url: "../../modificarSeccion?id=" + id,
                                data: data,
                                dataType: 'html',
                                type: 'post',
                                success: function(resp) {

                                    if (isNaN(resp)) { // si la respuesta son caracteres muestra el error de ingreso
                                        document.getElementById("resultadoRegistrar").style.display = "none";
                                        document.getElementById("resultadoSeccionRegistrar").style.display = "block";
                                        $("#resultadoSeccionRegistrar").html(resp);
                                        //document.getElementById("resultadoSeccion").style.display = "none";
                                        $("html, body").animate({scrollTop: 0}, "fast");
                                    } else { //muestra mensaje que guardo
                                        // alert('guardo');
                                        $("#seccionId").val('');
                                        $("#gradoId").val('');
                                        $("#capacidadAlumnos").val('');
                                        $("#turnoId").val('');
                                        $("#nivelId").val('');
                                        $("#planId").val('');
                                        refrescarGrid();
                                        document.getElementById("guardoRegistro").style.display = "block";
                                        dialogRegistrar.dialog('close');
                                        $("html, body").animate({scrollTop: 0}, "fast");

                                    }
                                    Loading.hide();

                                }
                            })
                            //  $(this).dialog("close");

                        }
                    }
                ],
            });
            $("#dialog_registrarSeccion").html(resp);
            Loading.hide();
        }
    })
    //  $("#dialog_registrarSeccion").show();
}


function confirmarEliminacion(id) {


    var dialogElim = $("#dialog_eliminacion").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Eliminar Sección</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='icon-trash bigger-110'></i>&nbsp; Eliminar Sección",
                "class": "btn btn-danger btn-xs",
                click: function() {
                    var data = {
                        id: id
                    }
                    Loading.show();
                    $.ajax({
                        url: "../../eliminarSeccion?id=" + id,
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp) {
                            // alert(resp);
                            refrescarGrid();
                            dialogElim.dialog('close');
                            $('#resultadoElim').html(resp);
                            document.getElementById("resultadoElim").style.display = "block";
                            $("html, body").animate({scrollTop: 0}, "fast");
                            Loading.hide();
                        }
                    })
                }
            }
        ]
    });

    $("#dialog_registrarSeccion").show();
}


function activarSeccion(id) {
    var dialogAct = $("#dialog_activacion").removeClass('hide').dialog({
        modal: true,
        width: '450px',
        draggable: false,
        resizable: false,
        title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-exclamation-triangle'></i> Activar Sección</h4></div>",
        title_html: true,
        buttons: [
            {
                html: "<i class='icon-remove bigger-110'></i>&nbsp; Cancelar",
                "class": "btn btn-xs",
                click: function() {
                    $(this).dialog("close");
                }
            },
            {
                html: "<i class='fa icon-ok bigger-110'></i>&nbsp; Activar Sección",
                "class": "btn btn-success btn-xs",
                click: function() {
                    var data = {
                        id: id
                    }
                    Loading.show();
                    $.ajax({
                        url: "../../activarSeccion?id=" + id,
                        data: data,
                        dataType: 'html',
                        type: 'post',
                        success: function(resp) {
                            // alert(resp);
                            refrescarGrid();
                            dialogAct.dialog('close');
                            $('#resultadoElim').html(resp);
                            document.getElementById("resultadoElim").style.display = "block";
                            $("html, body").animate({scrollTop: 0}, "fast");
                            Loading.hide();
                        }
                    })
                }
            }
        ]
    });
    $("#dialog_registrarSeccion").show();
}



function vizualizar(id) {
    var data =
    {
        id: id
    };
    Loading.show();
    $.ajax({
        url: "../../vizualizar",
        data: data,
        dataType: 'html',
        type: 'GET',
        success: function(resp)
        {
            var dialog_vizualizarI = $("#dialog_vizualizar").removeClass('hide').dialog({
                modal: true,
                width: '800px',
                draggable: false,
                resizable: false,
                //position: 'top',
                title: "<div class='widget-header widget-header-small'><h4 class='smaller'>Detalles de Sección</h4></div>",
                title_html: true,
                buttons: [
                    {
                        html: "<i class='icon-arrow-left bigger-110'></i>&nbsp; Volver",
                        "class": "btn btn-danger btn-xs",
                        click: function() {
                            dialog_vizualizarI.dialog("close");
                        }

                    }

                ]
            });
            $("#dialog_vizualizar").html(resp);
            Loading.hide();
        }
    });
}

function refrescarGrid() {

    $('#seccion-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
}

$(document).ready(function() {

    $('#fileupload').fileupload({
        url: '/planteles/seccionPlantel/cargarFormulario/',
        acceptFileTypes: /(\.|\/)(xls|xlsx|ods)$/i,
        maxFileSize: 50000000, // 50MB
        singleFileUploads: true,
        autoUpload: true,
        process: [
            {
                action: 'load',
                fileTypes: /(\.|\/)(xls?x|ods)$/i,
                maxFileSize: 50000000 // 50MB
            },
            {
                action: 'resize',
                maxWidth: 1440,
                maxHeight: 900
            },
            {
                action: 'save'
            }
        ],
        error: function(jqXHR, textStatus, errorThrown) {
            console.log(jqXHR);
            console.log(textStatus);
            console.log(errorThrown);
            alert("Se ha producido un error en la carga del archivo.");
        }

    });

    $('#fileupload').bind('fileuploaddone', function(e, data) {
        var archivos = data.jqXHR.responseJSON.files;

        $("#notificacionArchivo").html("¡Archivo cargado con exito!");

        $.each(archivos, function(index, file) {

            var archivo = file.name;

            var divResult = "dialog_vizualizar";
            var urlDir = "/planteles/seccionPlantel/validarFormularioInscripcion/";
            var datos = {
                archivo: archivo,
            };
            var loadingEfect = false;
            var showResult = false;
            var method = "POST";
            var responseFormat = "json";
            var beforeSend = null;
            var successCallback = null;

            successCallback=function(response){
                if(response.statusCode =='SUCCESS'){
                    dialogo(response.mensaje,'Registro Exitoso',divResult,'check')
                }
                if(response.statusCode =='ERROR'){
                    dialogo(response.mensaje,'Notificación de Error',divResult,'exclamation-triangle')
                }
                if(response.statusCode =='INFO'){
                    dialogo(response.mensaje,'Notificación de Alerta',divResult,'exclamation-triangle')
                }
            };
            executeFormatedAjax(divResult, urlDir, datos, loadingEfect, showResult, method, responseFormat, beforeSend, successCallback);
        });
    });
    $('.change-status').unbind('click');
    $('.change-status').on('click',
        function(e) {
            e.preventDefault();
            var id = $("#plantel_id").val();
            var accion = $(this).attr('data-action');
            cambiarEstatusProcesoMatriculacion(id, accion);
        }
    );
});