function VentanaDialog(id, direccion, title, accion,datos) {
    accion = accion;
    //Loading.show();
    var data =
    {
        id: id,
        datos: datos
    };
    if (accion === "liberarDocente") {
        displayHtmlInDivId("#mensaje-confirm", '<p>¿Estimado Usuario, está seguro que desea <strong>Liberar</strong> la <strong>Asignatura</strong> <strong>"'+title+'"</strong>?</p>',true);
        var dialog = $("#dialogPantalla").removeClass('hide').dialog({
            modal: false,
            width: '450px',
            resizable: false,
            draggable: false,
            position: ['top', 100],
            title: "<div class='widget-header widget-header-small'><h4 class='smaller'><i class='fa fa-home'></i> Docente </h4></div>",
            title_html: true,
            buttons: [
                {
                    html: "<i class='icon-arrow-left bigger-110'></i> Volver",
                    "class": "btn btn-xs btn-orange",
                    click: function() {
                        $(this).dialog("close");
                    }
                },
                {
                    html: "<i class='icon-trash bigger-110'></i> Desvincular este Docente",
                    "class": "btn btn-danger btn-xs",
                    id: 'btnEliminarDocente',
                    click: function() {
                        var divResult = "#resultadoOperacion";
                        var urlDir = "/planteles/docente/liberarDocente/id/"+id;
                        var datos = {id: id};
                        var conEfecto=true;
                        var showHTML = true;
                        var method = "POST";
                        var callback = function() {
                            $('#docente-grid').yiiGridView('update', {
                                data: $(this).serialize()
                            });
                        };
                        executeAjax(divResult, urlDir, datos, conEfecto, showHTML, method, callback);
                        $(this).dialog("close");
                    }
                }
            ],
        });


    }



}